class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0:
            return 0
        elif n == 1:
            return 1
        elif n == 2:
            return 2
        else:
            r = [0 for _ in xrange(n)]
            r[0], r[1] = 1, 2
            for i in xrange(2, n):
                r[i] = r[i-1] + r[i-2]
            return r[-1]
