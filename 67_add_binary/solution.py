class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        La, Lb = len(a), len(b)
        pa, pb = La-1, Lb-1
        r = []
        c = 0
        while pa >= 0 and pb >= 0:
            da, db = int(a[pa]), int(b[pb])
            if da+db+c == 3:
                dr, c = 1, 1
            elif da+db+c == 2:
                dr, c = 0, 1
            elif da+db+c == 1:
                dr, c = 1, 0
            elif da+db+c == 0:
                dr, c = 0, 0
            else:
                print "Error! got result on digit {0}: {1}", pa, dr
                return ""
            print pa, dr, c
            r.insert(0, str(dr))
            pa -= 1
            pb -= 1
            
        if pa >= 0:
            while pa >= 0:
                da = int(a[pa])
                if da + c == 2:
                    dr, c = 0, 1
                elif da + c == 1:
                    dr, c = 1, 0
                elif da + c == 0:
                    dr, c = 0, 0
                else:
                    print "Error! got result on digit {0}: {1}", pa, dr
                    return ""
                r.insert(0, str(dr))
                pa -= 1
        elif pb >= 0:
            while pb >= 0:
                db = int(b[pb])
                if db + c == 2:
                    dr, c = 0, 1
                elif db + c == 1:
                    dr, c = 1, 0
                elif db + c == 0:
                    dr, c = 0, 0
                else:
                    print "Error! got result on digit {0}: {1}", pb, dr
                    return ""
                r.insert(0, str(dr))
                pb -= 1
        if c != 0:
            r.insert(0, str(c))
        return "".join(r)

if __name__ == "__main__":
    Sab = Solution().addBinary
    print Sab("11", "1")
