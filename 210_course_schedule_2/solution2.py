class Solution(object):
    def findOrder(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: List[int]
        """
        indegrees = [set() for v in xrange(numCourses)]
        
        for edge in prerequisites:
            indegrees[edge[0]].add(edge[1])
        
        Q = [i for i, v in enumerate(indegrees) if len(v) == 0]
        
        visited = set(Q)
        results = []
        while len(Q) > 0:
            currv = Q.pop(0)
            results.append(currv)
            for i in xrange(numCourses):
                v = indegrees[i]
                if currv in v:
                    v.discard(currv)
                if len(v) == 0 and i not in visited:
                    Q.append(i)
                    visited.add(i)
        if len(results) < numCourses:
            return []
        return results
