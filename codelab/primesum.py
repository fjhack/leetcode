class Solution:
    # @param A : integer
    # @return a list of integers
    def primesum0(self, A):
        primes = range(2, A+1)
        plen, lastplen = len(primes), len(primes)+1
        headpos = 0
        head = primes[headpos]
        c = head
        while plen != lastplen:
            head = primes[headpos]
            c = head

            while c < A:
                c += head
                
                if c in primes:
                    i = primes.index(c)
                    del primes[i]
            # print head, primes
            lastplen = plen
            plen = len(primes)
            print plen
            headpos += 1
        # print primes
        for p in primes:
            # print p, A-p
            if A-p in primes:
                return [p, A-p]
    
    def primesum(self, A):
        primes = {n: True for n in xrange(2, A+1)}
        # print primes
        for n, isprime in primes.iteritems():
            if isprime:
                for n2 in xrange(n*n, A+1, n):
                    primes[n2] = False

        for p in primes:
            if primes[p] and primes[A-p]:
                return [p, A-p]


if __name__ == "__main__":
    S = Solution()

    R = S.primesum(20)
    print R

    # R = S.primesum(114)
    # print R

    # R = S.primesum(8192)
    # print R

    # R = S.primesum(120)
    # print R

    R = S.primesum(16777214)
    print R
