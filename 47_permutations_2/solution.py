import unittest

class Solution(object):
    def permuter(self, results, nums, trail):
        if len(trail) == len(nums):
            results.append([nums[p] for p in trail])
            return
        else:
            for i in xrange(len(nums)):
                if i in trail:
                    continue
                elif i > 0 and nums[i] == nums[i-1] and i-1 not in trail:
                    continue
                else:
                    # print len(trail)*" ", trail
                    # print len(trail)*" ", i, nums[i], i-1, nums[i-1]
                    self.permuter(results, nums, trail + [i])

    def permute(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        results = []
        nums2 = sorted(nums)
        print nums2
        self.permuter(results, nums2, [])
        return results
    

if __name__ == "__main__":
    Sp = Solution().permute

    A = [1,1,2]
    print Sp(A)
