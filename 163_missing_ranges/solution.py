import unittest

class Solution(object):

    def findMissingRanges(self, nums, lower, upper):
        """
        :type nums: List[int]
        :type lower: int
        :type upper: int
        :rtype: List[str]
        """

        def makegap(start, end):
            if start+2 == end:
                return str(start+1)
            else:
                return "{0}->{1}".format(start+1, end-1)

        L = len(nums)
        results = []
        left = lower-1

        for i in xrange(L+1):
            right = 0
            if i == L:
                right = upper+1
            else:
                right = nums[i]

            if left+2 <= right:
                results.append(makegap(left,right))
            left = right

        return results

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_p_0(self):
        print
        print "positive test 0:"
        A = [0,1,3,50,75]
        R = self.S.findMissingRanges(A, 0, 99)
        print R

    def test_p_1(self):
        print
        print "positive test 1:"
        A = [0,1,3,50,75,99]
        R = self.S.findMissingRanges(A, 0, 99)
        print R

    def test_p_2(self):
        print
        print "positive test 2:"
        A = [3,50,75]
        R = self.S.findMissingRanges(A, 0, 99)
        print R

    def test_p_3(self):
        print
        print "positive test 0:"
        A = [0,1,3,50,75]
        R = self.S.findMissingRanges(A, 0, 64)
        print R

if __name__ == "__main__":
    unittest.main()
