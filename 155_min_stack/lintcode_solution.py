class MinStack(object):

    def __init__(self):
        # do some intialize if necessary
        self._stk = []
        self._mstk = []

    def push(self, number):
        # write yout code here
        topmin = sys.maxint
        if len(self._mstk) > 0:
            topmin = self._mstk[-1]
        self._stk.append(number)
        if number <= topmin:
            self._mstk.append(number)

    def pop(self):
        # pop and return the top item in stack
        if len(self._stk) == 0:
            return None
        else:
            topmin = sys.maxint
            if len(self._mstk) > 0:
                topmin = self._mstk[-1]
            stk_top = self._stk.pop()
            if stk_top == topmin:
                self._mstk.pop()
            return stk_top

    def min(self):
        # return the minimum number in stack
        if len(self._mstk) == 0:
            return None
        else:
            return self._mstk[-1]
