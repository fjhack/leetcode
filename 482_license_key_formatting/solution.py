import unittest

class Solution(object):
    def licenseKeyFormatting(self, S, K):
        """
        :type S: str
        :type K: int
        :rtype: str
        """
        alnums = S.split('-')
        alnum_len = [len(s) for s in alnums]
        alnum_str = ''.join(alnums)
        total_len = sum(alnum_len)
        key_lens = []
        if (total_len) % K != 0:
            key_lens.append(total_len % K)
        for i in xrange(total_len/K):
            key_lens.append(K)
        results = []
        start_pos = 0
        for keylen in key_lens:
            results.append(alnum_str[start_pos:start_pos+keylen].upper())
            start_pos = start_pos + keylen
        return "-".join(results)



class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()


    # def test_N(self):
    #     print "Test :"
    #     S = ""
    #     K = 0
    #     R = self.S.licenseKeyFormatting(S, K)
    #     print R


    def test_1(self):
        print "Test 1:"
        S = "2-4A0r7-4k"
        K = 4
        R = self.S.licenseKeyFormatting(S, K)
        print R

    def test_2(self):
        print "Test 2:"
        S = "2-4A0r7-4k"
        K = 3
        R = self.S.licenseKeyFormatting(S, K)
        print R

    def test_3(self):
        print "Test 3:"
        S = "r"
        K = 1
        R = self.S.licenseKeyFormatting(S, K)
        print R

    def test_4(self):
        print "Test 4:"
        S = "aaaa"
        K = 2
        R = self.S.licenseKeyFormatting(S, K)
        print R
        

if __name__ == "__main__":
    unittest.main()
