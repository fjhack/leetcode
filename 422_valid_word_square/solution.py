import unittest

class Solution(object):
    def validWordSquare(self, words):
        """
        :type words: List[str]
        :rtype: bool
        """
        rows = len(words)
        if rows == 0:
            return False
        cols = len(words[0])
        if rows != cols:
            return False

        i = 0
        while i < cols:
            w1 = words[i]
            try:
                w2 = "".join([words[j][i] for j in xrange(len(w1))])
            except IndexError:
                return False
            
            if w1 != w2:
                return False
            i += 1
        return True

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        words = ["abcd","bnrt","crm","dt"]
        R = self.S.validWordSquare(words)
        print R

if __name__ == "__main__":
    unittest.main()
