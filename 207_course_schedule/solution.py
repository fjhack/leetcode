import unittest

class SolutionDFS(object):
    def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        courses = {c: [] for c in xrange(numCourses)}
        indegree = {c: 0 for c in xrange(numCourses)}
        for p in prerequisites:
            courses[p[0]].append(p[1])
            indegree[p[1]] += 1

        starters = {c: ind for c, ind in indegree.items() if ind == 0}

        print courses
        print indegree
        print starters

        if len(starters) == 0:
            return False

        visited = {c: False for c in xrange(numCourses)}

        def dfs(path):
            curr = path[-1]
            nexts = courses[curr]
            if len(nexts) == 0:
                return True
            result = True
            for n in nexts:
                if n in path:
                    return False
                else:
                    result = result and dfs(path+[n])
            return result

        all_results = True
        for s in courses.keys():
            all_results = all_results and dfs([s])

        return all_results

class SolutionBFS(object):
    def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        courses = {c: [] for c in xrange(numCourses)}
        indegree = {c: 0 for c in xrange(numCourses)}
        for p in prerequisites:
            courses[p[0]].append(p[1])
            indegree[p[1]] += 1

        starters = [c for c, ind in indegree.items() if ind == 0]

        # print courses
        # print indegree
        # print starters

        if len(starters) == 0:
            return False

        visited = {c: False for c in xrange(numCourses)}

        while len(starters) > 0:
            head = starters.pop(0)
            visited[head] = True
            nexts = courses[head]
            for n in nexts:
                if visited[n]:
                    return False
                else:
                    indegree[n] -= 1
                    if indegree[n] == 0:
                        starters.append(n)
            # print visited
            # print indegree

        for n, ind in indegree.items():
            if ind > 0:
                return False
        return True

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = SolutionBFS()

    def test_0_1(self):
        print
        numC, G = 2, [[1,0]]
        result = self.S.canFinish(numC, G)
        print result
        print

    def test_0_2(self):
        print
        numC, G = 2, [[1,0], [0,1]]
        result = self.S.canFinish(numC, G)
        print result
        print

    def test_1_1(self):
        print
        numC, G = 6, [[1,0], [1,2], [2,3], [0,3], [3,5], [4,5]]
        result = self.S.canFinish(numC, G)
        print result
        print

    def test_1_2(self):
        print
        numC, G = 8, [[1,0],[2,6],[1,7],[5,1],[6,4],[7,0],[0,5]]
        result = self.S.canFinish(numC, G)
        print result
        print

if __name__ == "__main__":
    unittest.main()
