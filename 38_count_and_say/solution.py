class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        start, i = "1", 1
        while i < n:
            start = self.say(start)
            i += 1
        return start
    
    def say(self, n):
        s = str(n)
        L = len(s)
        res = ''
        last_num = s[0]
        count = 1
        for i in xrange(1, L):
            if s[i] == last_num:
                count += 1
            else:
                res += "{0}{1}".format(count, last_num)
                last_num = s[i]
                count = 1
        res += "{0}{1}".format(count, last_num)
        return res
