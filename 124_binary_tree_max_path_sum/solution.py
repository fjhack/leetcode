# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

import sys

class Solution(object):
    def dfs(self, node):
        if node is None:
            return -sys.maxint, 0
        else:
            left = self.dfs(node.left)
            right = self.dfs(node.right)
            crossmax = max(left[0], right[0], left[1]+right[1]+node.val)
            singlemax = max(left[1]+node.val, right[1]+node.val, 0)
            return crossmax, singlemax
            
    def maxPathSum(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        allmax, m = self.dfs(root)
        return allmax
