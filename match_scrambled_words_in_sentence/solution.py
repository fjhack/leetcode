import unittest

class Matcher:
    def __init__(self, sdict={}):
        self.sdict = sdict
        self.suffix = dict()
        self._icounter = 0
        self.results = []

    def reset_counter(self):
        self._icounter = 0

    def get_counter(self):
        return self._icounter

    def match(self, word, sentence="", memoization=True):
        if word == '':
            return sentence.lstrip()
        elif word in self.suffix:
            return ""
        else:
            wordlen = len(word)
            any_match = False

            # Minor modification - trying to match from longest possible
            for i in xrange(wordlen, 0, -1):
                prefix, rest = word[:i], word[i:]
                sprefix = ''.join(sorted(prefix))

                if sprefix in self.sdict:
                    self._icounter += 1
                    cmatch = self.match(rest, sentence + " " + self.sdict[sprefix], memoization)
                    any_match = len(cmatch) > 0
                if any_match:
                    return cmatch.lstrip()
                else:
                    if memoization:
                        self.suffix[word] = False

            return ""

class testMatcher(unittest.TestCase):
    def setUp(self):
        pass

    def test_0(self):
        print "\nTest 0: exact match"
        word = "hetodgsictue"
        sdict = {"eht": "the", "dgo": "dog", "is": "is", "cetu": "cute"}
        print "  word={0},\n  dict={1}".format(word, sdict)
        matcher = Matcher(sdict)
        result = matcher.match(word)
        print "  result={0}\n  recursive_calls={1}".format(result, matcher.get_counter())

    def test_1(self):
        print "\nTest 1: Same input as 0, with more prefix"
        word = "hetodgsictue"
        sdict = {"eht": "the", "eh": "he", "do": "do", "dgo": "dog", "i": "i", "is": "is", "cetu": "cute"}
        print "  word={0},\n  dict={1}".format(word, sdict)
        matcher = Matcher(sdict)
        result = matcher.match(word)
        print "  result={0}\n  recursive_calls={1}".format(result, matcher.get_counter())

    def test_2(self):
        print "\nTest 2: common and overlapped prefix in dict, without memoization"
        word = "aaaaaaaaaaaaaaaaaaaaaaaab"
        sdict = {"a": "a", "aa": "aa"}
        print "  word={0},\n  dict={1}".format(word, sdict)
        matcher = Matcher(sdict)
        result = matcher.match(word, memoization=False)
        print "  result={0}\n  recursive_calls={1}".format(result, matcher.get_counter())

    def test_2m(self):
        print "\nTest 2: common and overlapped prefix in dict, memoization on suffix"
        word = "aaaaaaaaaaaaaaaaaaaaaaaab"
        sdict = {"a": "a", "aa": "aa"}
        print "  word={0},\n  dict={1}".format(word, sdict)
        matcher = Matcher(sdict)
        result = matcher.match(word)
        print "  result={0}\n  recursive_calls={1}".format(result, matcher.get_counter())

if __name__ == "__main__":
    unittest.main()
