import random

class RandomizedSet(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.v2p = dict()
        self.vals = []
        self.vlen = 0

    def insert(self, val):
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        :type val: int
        :rtype: bool
        """
        if val not in self.v2p:
            self.vals.append(val)
            self.vlen += 1
            self.v2p[val] = self.vlen - 1
            return True
        else:
            return False
        

    def remove(self, val):
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        :type val: int
        :rtype: bool
        """
        if val in self.v2p:
            idx, lastval = self.v2p[val], self.vals[-1]
            self.vals[idx], self.v2p[lastval] = lastval, idx
            self.vals.pop()
            self.vlen -= 1
            self.v2p.pop(val, 0)
            return True
        else:
            return False

    def getRandom(self):
        """
        Get a random element from the set.
        :rtype: int
        """
        return self.vals[random.randint(0, self.vlen-1)]


# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()
