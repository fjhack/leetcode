# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def levelOrderBottom(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        Q, results = [], []
        if root is not None:
            Q.append(root)
        while len(Q) > 0:
            curr_len = len(Q)
            curr_lvl = []
            for i in xrange(curr_len):
                _p = Q.pop(0)
                if _p.left is not None:
                    Q.append(_p.left)
                if _p.right is not None:
                    Q.append(_p.right)
                curr_lvl.append(_p.val)
            results.insert(0, curr_lvl)
        return results
