class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        d = {}
        for i, s in enumerate(strs):
            ss = ''.join(sorted(s))
            if ss in d:
                d[ss].append(i)
            else:
                d[ss] = [i]
        results = []
        for s, ps in d.iteritems():
            results.append([strs[_i] for _i in ps])
        return results
    
