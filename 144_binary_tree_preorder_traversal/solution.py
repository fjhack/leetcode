# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        stk, result = [], []
        if root is not None:
            stk.append(root)
        
        while len(stk) > 0:
            _curr = stk.pop()
            result.append(_curr.val)
            if _curr.right is not None:
                stk.append(_curr.right)
            if _curr.left is not None:
                stk.append(_curr.left)
        
        return result
