class Solution(object):
    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        L = len(nums)
        zeroidx, twoidx = 0, L-1
        i = 0
        while i < L:
            print nums
            if nums[i] == 1:
                i += 1
                continue
            elif nums[i] == 0 and i > zeroidx:
                t = nums[zeroidx]
                nums[zeroidx] = nums[i]
                nums[i] = t
                zeroidx += 1
            elif nums[i] == 0:
                i += 1
                print i, "zero:", zeroidx
            elif nums[i] == 2 and i < twoidx:
                t = nums[twoidx]
                nums[twoidx] = nums[i]
                nums[i] = t
                twoidx -= 1
                print i, "two:", twoidx
            elif nums[i] == 2:
                i += 1
        return

if __name__ == "__main__":
    SsC = Solution().sortColors
    A = [1,2,0,1,0,0,1,1,2,0,1]
    SsC(A)
    print A
    
