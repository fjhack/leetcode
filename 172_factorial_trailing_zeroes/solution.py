class Solution(object):
    def trailingZeroes(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 5:
            return 1
        elif n < 5:
            return 0
        else:
            return n/5 + self.trailingZeroes(n/5)

if __name__ == "__main__":
    f = 1
    for i in xrange(1, 32):
        f *= i
        print i, f
