import unittest

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Codec0:

    def serialize(self, root):
        """Encodes a tree to a single string.
        
        :type root: TreeNode
        :rtype: str
        """
        queue, result = [], []
        all_none = root is None
        next_all_none = False
        pop_count, last_pop_count = 0, 0
        queue.append(root)
        while not next_all_none:
            next_all_none = True
            
            pop_count = len(queue)
            last_pop_count = pop_count
            while pop_count > 0:
                _node = queue.pop(0)
                if _node is None:
                    queue.append(None)
                    queue.append(None)
                    result.append('None')
                else:
                    queue.append(_node.left)
                    queue.append(_node.right)
                    if _node.left is not None or _node.right is not None:
                        next_all_none = False
                    result.append(str(_node.val))
                pop_count -= 1
        # result = result[0:len(result)-last_pop_count]
        return ','.join(result)

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        # dlist = [int(d) if d != 'None' and len(d) > 0 else None for d in data.split(',')]
        nodelist = [TreeNode(int(d)) if d != 'None' and len(d) > 0 else None for d in data.split(',')]
        dlen = len(nodelist)
        numleaves = (dlen+1) / 2
        # nodelist = [TreeNode(d) if d is not None else None for d in dlist]
        for i in xrange(dlen-numleaves):
            if nodelist[i] is not None:
                if i*2+1 < dlen:
                    nodelist[i].left = nodelist[i*2+1]
                if i*2+2 < dlen:
                    nodelist[i].right = nodelist[i*2+2]
        return nodelist[0]

class Codec1:
    def serialize(self, root):
        """Encodes a tree to a single string.
        
        :type root: TreeNode
        :rtype: str
        """
        q, result = [], []
        q.append(root)
        while len(q):
            x = q.pop(0)
            if x == None:
                result.append("None")
            else:
                result.append(str(x.val))
                q.append(x.left)
                q.append(x.right)
        return ",".join(result)

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        data, q, idx = data.split(","), [], 0
        if data[0] == "None":
            return None
        root = TreeNode(data[0])
        q.append(root)
        while len(q):
            x = q.pop(0)
            x.left = TreeNode(data[idx+1]) if data[idx+1] != "None" else None
            x.right = TreeNode(data[idx+2]) if data[idx+2] != "None" else None
            if x.left:
                q.append(x.left)
            if x.right:
                q.append(x.right)
            idx += 2
        return root


# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))

class testBTCoded(unittest.TestCase):
    def setUp(self):
        self.btc = Codec0()

    def test_1(self):
        root = TreeNode(1)
        root.left = TreeNode(2)
        root.right = TreeNode(3)
        root.right.left = TreeNode(4)
        root.right.right = TreeNode(5)
        root.right.left.left = TreeNode(6)
        result = self.btc.serialize(root)
        print result

        dresult = self.btc.deserialize(result)
        print dresult.val
        print dresult.left.val
        print dresult.right.val
        print dresult.right.left.val
        print dresult.right.right.val
        print dresult.right.left.left.val

    def test_2(self):
        root = TreeNode(-1)
        root.left = TreeNode(2)
        root.right = TreeNode(3)
        result = self.btc.serialize(root)
        print result

        dresult = self.btc.deserialize(result)
        print dresult.val
        print dresult.left.val
        print dresult.right.val

    def test_3(self):
        inputbuf = None
        with open('bigtest.txt', 'r') as testfile:
            inputbuf = testfile.readline()
        dresult = self.btc.deserialize(inputbuf)
        sresult = self.btc.serialize(dresult)
        print len(sresult), len(inputbuf)
        
if __name__ == "__main__":
    unittest.main()
