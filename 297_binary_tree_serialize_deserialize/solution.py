import math
import unittest

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


class Solution:

    '''
    @param root: An object of TreeNode, denote the root of the binary tree.
    This method will be invoked first, you should design your own algorithm 
    to serialize a binary tree which denote by a root node to a string which
    can be easily deserialized by your own "deserialize" method later.
    '''

    def serialize(self, root):
        # write your code here
        bfsq, result_list = [], []
        lvl_count = 0
        if root is None:
            return ",".join(result_list)
        bfsq.append(root)
        all_none = False
        while not all_none:
            all_none = True
            lvl_length = 2 ** lvl_count
            for i in xrange(lvl_length):
                n = bfsq.pop(0)
                if n is None:
                    result_list.append('#')
                    bfsq.append(None)
                    bfsq.append(None)
                else:
                    all_none = False
                    result_list.append(str(n.val))
                    if n.left is None:
                        bfsq.append(None)
                    else:
                        bfsq.append(n.left)
                    
                    if n.right is None:
                        bfsq.append(None)
                    else:
                        bfsq.append(n.right)
            lvl_count += 1
        
        print result_list
        print lvl_count, 2**(lvl_count-1)
        result_list = result_list[0:( len(result_list) - 2**(lvl_count-1) )]
        print result_list
        return ",".join(result_list)
    
    
    '''
    @param data: A string serialized by your serialize method.
    This method will be invoked second, the argument data is what exactly
    you serialized at method "serialize", that means the data is not given by
    system, it's given by your own serialize method. So the format of data is
    designed by yourself, and deserialize it here as you serialize it in 
    "serialize" method.
    '''
    def deserialize(self, data):
        # write your code here
        nodelist = data.split(',')
        L = len(nodelist)
        if L == 0:
            return None
        levels = math.log(L+1, 2)
        nodes = [TreeNode(int(v)) if v != '#' else None for v in nodelist]
        for i in xrange(L):
            idx = i+1
            if (idx*2-1) < L:
                nodes[i].left = nodes[idx*2-1]
            if (idx*2) < L:
                nodes[i].right = nodes[idx*2]

        return nodes[0]


def printBinTree(root):
    """
    :type root: TreeNode
    :rtype: List[List[int]]
    """
    Q, results = [], []
    if root is not None:
        Q.append(root)
    while len(Q) > 0:
        curr_len = len(Q)
        curr_lvl = []
        for i in xrange(curr_len):
            _p = Q.pop(0)
            if _p.left is not None:
                Q.append(_p.left)
            if _p.right is not None:
                Q.append(_p.right)
            curr_lvl.append(_p.val)
        print curr_lvl


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.des = self.S.deserialize
        self.ser = self.S.serialize

    def test_1(self):
        root = TreeNode(1)
        root.left = TreeNode(2)
        root.right = TreeNode(3)
        root.right.left = TreeNode(4)
        root.right.right = TreeNode(5)
        r = self.ser(root)
        print r
        des_result = self.des(r)
        print des_result.val
        print des_result.left.val
        print des_result.right.val
        print des_result.right.left.val
        print des_result.right.right.val

    def test_2(self):
        sed = "989,982,#,972,#,947,#,920,#,903,#,894,#,881,#,866,#,864,#,842,#,841,#,796,#,726,#,647,#,613,719,593,#,#,#,590,#,558,#,554,#,538,#,512,#,504,#,468,505,467,#,#,#,456,#,413,#,331,#,330,407,320,#,#,#,312,#,306,#,301,#,274,#,251,#,235,#,231,#,222,#,181,#,93,#,83,#,73,#,64,#,62,#,60,#,28,#,21,#,20,#,-32,#,-52,#,-70,#,-87,#,-98,#,-102,#,-115,#,-116,#,-139,#,-183,#,-224,#,-241,#,-263,#,-284,#,-294,#,-296,#,-320,#,-330,#,-392,#,-398,#,-407,#,-431,#,-445,#,-460,#,-463,#,-492,#,-507,#,-518,#,-539,#,-552,#,-558,#,-559,#,-587,#,-673,#,-736,#,-757,#,-766,#,-767,#,-823,#,-830,#,-867,#,-875,#,-891,#,-905,#,-910,#,-924,#,-960,#,-985"
        r = self.des(sed)
        printBinTree(r)

if __name__ == "__main__":
    unittest.main()

