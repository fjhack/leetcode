# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def mergeTwoLists(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        p1, p2 = l1, l2
        head = None
        if p1 is None:
            return l2
        elif p2 is None:
            return l1
        else:
            if p1.val < p2.val:
                head = ListNode(p1.val)
                p1 = p1.next
            else:
                head = ListNode(p2.val)
                p2 = p2.next
        presult = head
        while p1 is not None and p2 is not None:
            if p1.val < p2.val:
                _new = ListNode(p1.val)
                p1 = p1.next
            else:
                _new = ListNode(p2.val)
                p2 = p2.next
            presult.next = _new
            presult = presult.next
        if p1 is None and p2 is None:
            return head
        elif p1 is None:
            premains = p2
        else:
            premains = p1
        while premains is not None:
            _new = ListNode(premains.val)
            presult.next = _new
            presult = presult.next
            premains = premains.next
        return head
