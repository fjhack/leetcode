class Solution(object):
    def __init__(self):
        self.grid = None
        # 0=blue(SW) 1=white(NE)
        self.direction = 0

    def buildGrid(self, m, n):
        self.grid = [[0 for _ in xrange(n)] for _ in xrange(m)]
        self.grid[0][0] = 1

    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        self.buildGrid(m, n)
        if n == 1 and m == 1:
            i, j = 0, 0
        elif n == 1:
            i, j = 1, 0
        else:
            i, j = 0, 1

        while (i<m and j<n):
            # print i, j
            if i == 0 and j > 0:
                self.grid[i][j] = self.grid[i][j-1]
            elif i > 0 and j == 0:
                self.grid[i][j] = self.grid[i-1][j]
            elif i > 0 and j > 0:
                self.grid[i][j] = self.grid[i-1][j] + self.grid[i][j-1]

            if self.direction == 0:
                if (i < m-1 and j > 0):
                    i += 1
                    j -= 1
                elif (i < m-1 and j == 0):
                    i += 1
                    self.direction = 1
                elif (i == m-1 and j >= 0):
                    j += 1
                    self.direction = 1
            elif self.direction == 1:
                if (i > 0 and j < n-1):
                    i -= 1
                    j += 1
                elif (i == 0 and j < n-1):
                    j += 1
                    self.direction = 0
                elif (i >= 0 and j == n-1):
                    i += 1
                    self.direction = 0
        return self.grid[m-1][n-1]

if __name__ == "__main__":
    S = Solution()

    # 28
    print S.uniquePaths(3, 7)

    # 28
    print S.uniquePaths(7, 3)

    # 70
    print S.uniquePaths(5, 5)

    # 2
    print S.uniquePaths(2, 2)

    # 1
    print S.uniquePaths(2, 1)
    print S.grid

    # 1
    print S.uniquePaths(1, 2)
