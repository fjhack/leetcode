# There is a building of n floors. If an egg drops from the k th floor or above, it will break. If it's dropped from any floor below, it will not break.
# You're given two eggs, Find k while minimize the number of drops for the worst case. Return the number of drops in the worst case.
# For n = 10, a naive way to find k is drop egg from 1st floor, 2nd floor ... kth floor. But in this worst case (k = 10), you have to drop 10 times.
# Notice that you have two eggs, so you can drop at 4th, 7th & 9th floor, in the worst case (for example, k = 9) you have to drop 4 times.
# Example:
# Given n = 10, return 4.
# Given n = 100, return 14.

class Solution:
    # @param {int} n an integer
    # @return {int} an integer
    def dropEggs0(self, n):
        # Write your code here
        n2 = 0
        i = 1
        while True:
            n2 += i
            print "\t", n2, i
            if n2 >= n:
                return i
            else:
                i += 1

    def dropEggs(self, n):
        # Write your code here
        import math
        x = int(math.sqrt(n * 2))
        print "\t", x
        while x * (x + 1) / 2 < n:
            print "\t", x
            x += 1
        return x
    
if __name__ == "__main__":
    sde = Solution().dropEggs
    r = sde(10)
    print r
    print

    r = sde(100)
    print r
    print

    r = sde(62)
    print r
    print

