import sys
import unittest

class Solution:
    # @param pages: a list of integers
    # @param k: an integer
    # @return: an integer
    def copyBooks(self, pages, k):
        return self.copyBooksBS(pages, k)

    def copyBooksBS(self, pages, k):
        def countCopiers(pages, limit):
            L = len(pages)
            if L == 0:
                return 0
            print "countCopiers: limit={0}".format(limit)
            
            num_copiers = 1
            s = pages[0]
            for i in xrange(1, L):
                if s+pages[i] > limit:
                    num_copiers += 1
                    s = 0
                s += pages[i]
                print "  {0} copiers={1}, current_sum={2}".format(i, num_copiers, s)
            return num_copiers

        L = len(pages)
        if L == 0:
            return 0
        
        total = 0
        maxc = pages[0]
        for i in xrange(L):
            total += pages[i]
            if maxc < pages[i]:
                maxc = pages[i]

        start, end = maxc, total

        while start+1 < end:
            mid = start + (end-start)/2
            print "start={0}, end={1}, mid={2}".format(start, end, mid)
            cc = countCopiers(pages, mid)
            print "count_copiers={0}".format(cc)
            if cc > k:
                start = mid
            else:
                end = mid

        print "start={0}, end={1}".format(start, end)
        if countCopiers(pages, start) <= k:
            return start
        else:
            return end

    def copyBooksDP(self, pages, k):
        # Dynamic programming solution
        # Time complexity: O(n*n*k), will timeout for large dataset
        L = len(pages)
        if k > L:
            k = L

        prefixsum = [0]
        for i in xrange(L):
            prefixsum.append(prefixsum[-1] + pages[i])
        
        print len(prefixsum), prefixsum

        costrange = [[] for _ in xrange(L+1)]
        costrange[0] = []
        costrange[1] = prefixsum
        for i in xrange(2, L+1):
            _cr = [0 for _ in xrange(L+1)]
            for j in xrange(i, L+1):
                _cr[j] = prefixsum[j] - prefixsum[i-1]
            costrange[i] = _cr

        print len(costrange)
        for cr in costrange:
            print cr
        print

        DP = [[0 for _ in xrange(k+1)] for _ in xrange(L+1)]
        
        for i in xrange(1, L+1):
            DP[i][1] = prefixsum[i]
            
        for j in xrange(2, k+1):
            for i in xrange(j, L+1):
                minv = sys.maxint
                for sep in xrange(j-1, i):
                    cmax = max(DP[sep][j-1], costrange[sep+1][i])
                    if cmax < minv:
                        minv = cmax
                DP[i][j] = minv

        print len(DP)
        for d in DP:
            print d
        print

        
        return DP[L][k]

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        pages = [3,2,4]
        k = 2
        r = self.S.copyBooksBS(pages, k)
        print r
        print

    def test_1(self):
        pages = [3,5,4,1,6,2]
        k = 4
        r = self.S.copyBooksBS(pages, k)
        print r
        print

if __name__ == "__main__":
    unittest.main()
