"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""

import sys

class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {TreeNode} the root of the maximum average of subtree
    def __init__(self):
        self.maxavg = float(-sys.maxint)
        self.maxavgnode = None
    
    def postorder(self, node):
        if node is None:
            return 0, 0
        else:
            csum, ccount = 0, 0
            lr = self.postorder(node.left)
            csum, ccount = csum+lr[0], ccount+lr[1]
            rr = self.postorder(node.right)
            csum, ccount = csum+rr[0], ccount+rr[1]
            csum += node.val
            ccount += 1
            cavg = float(csum) / float(ccount)
            if cavg > self.maxavg:
                self.maxavg = cavg
                self.maxavgnode = node
            return csum, ccount
    
    def findSubtree2(self, root):
        # Write your code here
        self.maxavg = float(-sys.maxint)
        self.maxavgnode = None
        self.postorder(root)
        return self.maxavgnode
