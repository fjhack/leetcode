"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""

import sys

class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {TreeNode} the root of the minimum subtree

    def __init__(self):
        self.minsum = sys.maxint
        self.minsumnode = None

    def postorder(self, node):
        if node is None:
            return 0
        else:
            csum = 0
            csum += self.postorder(node.left)
            csum += self.postorder(node.right)
            csum += node.val
            if csum < self.minsum:
                self.minsum = csum
                self.minsumnode = node
            return csum
            
    def findSubtree(self, root):
        # Write your code here
        self.minsum = sys.maxint
        self.minsumnode = None
        self.postorder(root)
        return self.minsumnode
