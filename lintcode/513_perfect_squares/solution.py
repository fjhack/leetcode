import sys

class Solution:
    # @param {int} n a positive integer
    # @return {int} an integer
    def numSquares0(self, n):
        # Write your code here
        dp = [sys.maxint for _ in xrange(n+1)]
        i = 0
        while i*i <= n:
            dp[i*i] = 1
            i += 1
            
        if dp[n] == 1:
            return dp[n]
            
        i = 1
        while i <= n:
            if dp[i] == 0:
                i += 1
                continue
            j = 1
            while i+j*j <= n:
                dp[i+j*j] = min(dp[i]+1, dp[i+j*j])
                j += 1
            i += 1

        return dp[n]
    
    def numSquares(self, n):
        dp = [sys.maxint for _ in xrange(n+1)]
        i = 1
        while i*i <= n:
            k = i*i
            j = k
            while j <= n:
                dp[j] = max(dp[j], dp[j-k]+1)
                j += 1
            i += 1
        return dp[n]
    
if __name__ == "__main__":
    S = Solution()
    print S.numSquares(100000)
