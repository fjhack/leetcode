

class Solution:
    # @param {int[]} nums a mountain sequence which increase firstly and then decrease
    # @return {int} then mountain top
    def mountainSequence(self, nums):
        # Write your code here
        L = len(nums)
        if L == 0:
            return None
        if L == 1:
            return nums[0]
        left, right = 0, L-1
        while left + 1 < right:
            mid = left + (right - left) / 2
            if mid == 0:
                if nums[mid] > nums[mid+1]:
                    return nums[mid]
                else:
                    left = mid
            elif mid == L-1:
                if nums[mid] > nums[mid-1]:
                    return nums[mid]
                else:
                    right = mid
            elif nums[mid] > nums[mid-1] and nums[mid] > nums[mid+1]:
                return nums[mid]
            elif nums[mid] > nums[mid-1] and nums[mid] < nums[mid+1]:
                left = mid
            elif nums[mid] < nums[mid-1] and nums[mid] > nums[mid+1]:
                right = mid
        if left == 0 and nums[left] > nums[left+1]:
            return nums[left]
        elif nums[left] > nums[left-1] and nums[left] > nums[left+1]:
            return nums[left]
        if right == L-1 and nums[right] > nums[right-1]:
            return nums[right]
        elif nums[right] > nums[right-1] and nums[right] > nums[right+1]:
            return nums[right]

if __name__ == "__main__":
    sms = Solution().mountainSequence

    A = [1,2,4,8,6,3]
    print sms(A)

    A = [10, 9, 8, 7]
    print sms(A)

    A = [1,2,4,8,6,5,3]
    print sms(A)

    A = [1,8,6,5,4,3,2]
    print sms(A)

    A = [10, 8, 7, 5, 4]
    print sms(A)

    A = [2,5,7,11]
    print sms(A)
    
    A = [2,3,5,7,11]
    print sms(A)
