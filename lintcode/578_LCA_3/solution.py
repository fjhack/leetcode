"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""
import copy

class Solution:
    """
    @param {TreeNode} root The root of the binary tree.
    @param {TreeNode} A and {TreeNode} B two nodes
    @return Return the LCA of the two nodes.
    """ 
    def lowestCommonAncestor3(self, root, A, B):
        # write your code here
        ra, rb, lca = self.contains(root, A, B)
        if ra and rb:
            return lca
        else:
            return None
    
    def contains(self, root, A, B):
        # 3 return values:
        # 1: A matched or not; 2: B matched or not; 3: C matched or not
        if root is None:
            return False, False, None
        leftA, leftB, leftN = self.contains(root.left, A, B)
        rightA, rightB, rightN = self.contains(root.right, A, B)
        
        _a = leftA or rightA or root is A
        _b = leftB or rightB or root is B
        
        if root is A or root is B:
            return _a, _b, root
        elif leftN is not None and rightN is not None:
            return _a, _b, root
        elif leftN is not None:
            return _a, _b, leftN
        elif rightN is not None:
            return _a, _b, rightN
        else:
            return _a, _b, None
