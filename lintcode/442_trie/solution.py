"""
Your Trie object will be instantiated and called as such:
trie = Trie()
trie.insert("lintcode")
trie.search("lint") will return false
trie.startsWith("lint") will return true
"""

class TrieNode:
  def __init__(self):
    # Initialize your data structure here.
    self.D = dict()
    self.term = False

class Trie:
  def __init__(self):
    self.root = TrieNode()

  # @param {string} word
  # @return {void}
  # Inserts a word into the trie.
  def insert(self, word):
    currnode = self.root
    L = len(word)
    for i in xrange(L):
        if word[i] not in currnode.D:
            currnode.D[word[i]] = TrieNode()
            currnode = currnode.D[word[i]]
        else:
            currnode = currnode.D[word[i]]
        if i == L-1:
            currnode.term = True
    
  # @param {string} word
  # @return {boolean}
  # Returns if the word is in the trie.
  def search(self, word):
    currnode = self.root
    L = len(word)
    for i in xrange(L):
        if word[i] not in currnode.D:
            return False
        currnode = currnode.D[word[i]]
    return currnode.term
    
  # @param {string} prefix
  # @return {boolean}
  # Returns if there is any word in the trie
  # that starts with the given prefix.
  def startsWith(self, prefix):
    currnode = self.root
    L = len(prefix)
    for i in xrange(L):
        if prefix[i] not in currnode.D:
            return False
        currnode = currnode.D[prefix[i]]
    return True
