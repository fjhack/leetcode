class Solution:
    # @param {int} k an integer
    # @param {int[]} nums an integer array
    # return {int} kth smallest element
    def kthSmallest(self, k, nums):
        # Write your code here
        L = len(nums)
        if k > L:
            return None
        elif L == 0:
            return None
        elif L == 1:
            return nums[0]
        else:
            pos = -1
            target = k
            A1, A2 = [], []
            currA = nums
            while True:
                A1, A2 = [], []
                pivot = len(currA) - 1
                # print currA, "target position:", target, "pivot:", currA[pivot]
                for i in xrange(len(currA)-1):
                    if currA[i] < currA[pivot]:
                        A1.append(currA[i])
                    elif currA[i] > currA[pivot]:
                        A2.append(currA[i])
                pos = len(A1) + 1
                # print " ", A1, A2, "Pivot {0} is {1} smallest in array".format(currA[pivot], pos)
                if pos > target:
                    currA = A1
                elif pos < target:
                    currA = A2
                    target = target - pos
                else:
                    return currA[pivot]
        
import random

if __name__ == "__main__":
    S = Solution()
    kthsmallest = S.kthSmallest

    A = [3,4,1,2,5]
    r = kthsmallest(3, A)
    print r

    def generate_array():
        L = random.randint(2, 16)
        p = random.randint(0, L)
        A = list(set([random.randint(1, 200) for _ in xrange(L)]))
        return A


    for i in xrange(100):
        print
        print "Iteration", i
        A = generate_array()
        As = sorted(A)
        rpos = random.randint(0,len(A)-1)
        r = kthsmallest(rpos+1, A)
        print A, rpos
        print r, r == As[rpos]
        if r != As[rpos]:
            print "Error!!!"
            break
        
