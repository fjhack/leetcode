"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""
class Solution:
    """
    @param root, the root of binary tree.
    @return true if it is a mirror of itself, or false.
    """
    def isLeaf(self, n):
        return n.left is None and n.right is None
    
    def isRevert(self, a, b):
        if a is None and b is None:
            return True
        elif a is None or b is None:
            return False
        else:
            if a.val == b.val:
                return self.isRevert(a.left, b.right) and self.isRevert(a.right, b.left)
            else:
                return False
    
    def isSymmetric(self, root):
        # Write your code here
        if root is None or self.isLeaf(root):
            return True
        else:
            return self.isRevert(root.left, root.right)
