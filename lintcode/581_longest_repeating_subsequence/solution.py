class Solution:
    # @param {string} str a string
    # @return {int} the length of the longest repeating subsequence
    def longestRepeatingSubsequence(self, s):
        # Write your code here
        L = len(s)
        F = [[0 for _ in xrange(L+1)] for _ in xrange(L+1)]
        for i in xrange(1, L+1):
            for j in xrange(1, L+1):
                if s[i-1] == s[j-1] and (i != j):
                    F[i][j] = F[i-1][j-1] + 1
                else:
                    F[i][j] = max(F[i-1][j], F[i][j-1])

        return F[L][L]

if __name__ == "__main__":
    lrs = Solution().longestRepeatingSubsequence

    r = lrs("abab")
    print r

    r = lrs("abba")
    print r
    
