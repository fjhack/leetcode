# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {int} the length of the longest consecutive sequence path
    def __init__(self):
        self.maxlen = 0
    
    def dfs(self, node, clen, lastval):
        if node is None:
            if clen > self.maxlen:
                self.maxlen = clen
            return
        if node.val == lastval + 1:
            self.dfs(node.left, clen+1, node.val)
            self.dfs(node.right, clen+1, node.val)
        else:
            if clen > self.maxlen:
                self.maxlen = clen
            self.dfs(node.left, 1, node.val)
            self.dfs(node.right, 1, node.val)
            
        
    def longestConsecutive(self, root):
        # Write your code here
        self.maxlen = 0
        self.dfs(root, 1, root.val)
        return self.maxlen
