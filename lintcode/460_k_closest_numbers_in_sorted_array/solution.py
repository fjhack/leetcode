# Given a target number, a non-negative integer k and an integer array A sorted in ascending order, find the k closest numbers to target in A, sorted in ascending order by the difference between the number and target. Otherwise, sorted in ascending order by number if the difference is same.

# Example
# Given A = [1, 2, 3], target = 2 and k = 3, return [2, 1, 3].
# Given A = [1, 4, 6, 8], target = 3 and k = 3, return [4, 1, 6].


class Solution:
    # @param {int[]} A an integer array
    # @param {int} target an integer
    # @param {int} k a non-negative integer
    # @return {int[]} an integer array

    def closestNumber(self, A, target):
        # Write your code here
        L = len(A)
        if L == 0:
            return -1
        left, right = 0, L-1
        while left+1 < right:
            mid = left + (right - left) / 2
            if A[mid] == target:
                return mid
            elif A[mid] < target:
                left = mid
            else:
                right = mid
        
        if abs(target - A[left]) < abs(target - A[right]):
            return left
        else:
            return right
            
    def kClosestNumbers(self, A, target, k):
        # Write your code here
        L = len(A)
        if L == 0:
            return []
            
        closestIdx = self.closestNumber(A, target)
        if closestIdx > 0:
            left, right = closestIdx-1, closestIdx
        else:
            left, right = closestIdx, closestIdx+1
        
        results = []
        for i in xrange(k):
            if left < 0:
                results.append(A[right])
                right += 1
            elif right == len(A):
                results.append(A[left])
                left -= 1
            else:
                if target - A[left] <= A[right] - target: 
                    results.append(A[left])
                    left -= 1
                else:
                    results.append(A[right])
                    right += 1
                    
        # while len(results) < k:
        #     ldiff, rdiff = abs(A[left] - target), abs(A[right] - target)
        #     print left, right, A
        #     if ldiff < rdiff:
        #         results.append(A[left])
        #         print "Appending:", A[left], "at", left
        #         if left > 0:
        #             left -= 1
        #         elif right < L-1:
        #             right += 1
        #         else:
        #             print "Reached end:", left, right
        #             #return results
        #     elif ldiff > rdiff:
        #         results.append(A[right])
        #         print "Appending:", A[right], "at", right
        #         if right < L-1:
        #             right += 1
        #         elif left > 0:
        #             left -= 1
        #         else:
        #             print "Reached end:", left, right
        #             #return results
        #     else:
        #         if A[left] < A[right]:
        #             results.append(A[left])
        #             print "Appending:", A[left], "at", left
        #             if left > 0:
        #                 left -= 1
        #             elif right < L-1:
        #                 right += 1
        #             else:
        #                 print "Reached end:", left, right
        #                 #return results
        #         else:
        #             results.append(A[right])
        #             print "Appending:", A[right], "at", right
        #             if right < L-1:
        #                 right += 1
        #             elif left > 0:
        #                 left -= 1
        #             else:
        #                 print "Reached end:", left, right
        #                 #return results
                    
        
        return results

if __name__ == "__main__":
    skcn = Solution().kClosestNumbers

    A = [1,2,3]
    target, k = 2, 3
    r = skcn(A, target, k)
    print r
    print
    
    A = [1,4,6,8]
    target, k = 3, 3
    r = skcn(A, target, k)
    print r
    print
        
