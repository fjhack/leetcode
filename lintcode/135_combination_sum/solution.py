class Solution:
    # @param candidates, a list of integers
    # @param target, integer
    # @return a list of lists of integers
    def __init__(self):
        self.results = []
        self.C = None
        self.T = 0
        self.L = 0
        
    def rec(self, prefix, pos, rest):
        if rest == 0:
            self.results.append(prefix)
            return
        print prefix, pos, rest

        for i in xrange(pos, self.L):
            if rest < self.C[i]:
                return
            self.rec(prefix + [self.C[i]], i, rest - self.C[i])
    
    def combinationSum(self, candidates, target):
        # write your code here
        self.results = []
        # QUESTION: Why change candidate array to an unique array can get rid of duplicated results?
        self.C = sorted(candidates)
        self.T = target
        self.L = len(candidates)
        
        self.rec([], 0, target)
        return self.results

if __name__ == "__main__":
    S = Solution()
    r = S.combinationSum([8,7,4,3], 11)
    print r

    print

    r = S.combinationSum([2,2,3], 7)
    print r
