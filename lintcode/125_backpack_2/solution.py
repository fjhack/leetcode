class Solution:
    # @param m: An integer m denotes the size of a backpack
    # @param A & V: Given n items with size A[i] and value V[i]
    # @return: The maximum value
    def backPackII(self, m, A, V):
        # write your code here
        L = len(A)
        DP1 = [0 for _ in xrange(m+1)]
        DP2 = [0 for _ in xrange(m+1)]
        for i in xrange(1, L+1):
            for j in xrange(1, m+1):
                if j-A[i-1] < 0:
                    DP2[j] = DP1[j]
                else:
                    DP2[j] = max(DP1[j], DP1[j-A[i-1]]+V[i-1])
            for j in xrange(m+1):
                DP1[j] = DP2[j]
        return DP2[m]
