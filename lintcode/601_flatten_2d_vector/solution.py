class Vector2D(object):

    # @param vec2d {List[List[int]]}
    def __init__(self, vec2d):
        # Initialize your data structure here
        self.v2 = vec2d
        self.pos1 = 0
        self.pos2 = 0
        self.L1 = len(vec2d)
        self.L2 = 0
    

    # @return {int} a next element
    def next(self):
        # Write your code here
        r = self.v2[self.pos1][self.pos2]
        if self.pos2 < len(self.v2[self.pos1]) - 1:
            self.pos2 += 1
            return r
        else:
            self.pos1 += 1
            self.pos2 = 0
            return r
        

    # @return {boolean} true if it has next element
    # or false
    def hasNext(self):
        # Write your code here
        if self.pos1 == self.L1:
            return False
        elif len(self.v2[self.pos1]) == 0:
            while self.pos1 < self.L1 and len(self.v2[self.pos1]) == 0:
                self.pos1 += 1
            if self.pos1 == self.L1:
                return False
            else:
                return True
        else:
            return True
        

# Your Vector2D object will be instantiated and called as such:
# i, v = Vector2D(vec2d), []
# while i.hasNext(): v.append(i.next())
