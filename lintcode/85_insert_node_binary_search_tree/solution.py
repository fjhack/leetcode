"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of the binary search tree.
    @param node: insert this node into the binary search tree.
    @return: The root of the new binary search tree.
    """
    def insertNode(self, root, node):
        # write your code here
        curr = root
        if root is None:
            root = node
        while curr is not None:
            if node.val > curr.val:
                if curr.right is None:
                    curr.right = node
                    break
                else:
                    curr = curr.right
            elif node.val < curr.val:
                if curr.left is None:
                    curr.left = node
                    break
                else:
                    curr = curr.left
        return root
