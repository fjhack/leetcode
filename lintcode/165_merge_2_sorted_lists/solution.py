"""
Definition of ListNode
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param two ListNodes
    @return a ListNode
    """
    def mergeTwoLists(self, l1, l2):
        # write your code here
        n1, n2 = l1, l2
        
        head = ListNode(-1)
        currnode = head
        
        while n1 is not None and n2 is not None:
            if n1.val < n2.val:
                currnode.next = n1
                currnode = currnode.next
                n1 = n1.next
            else:
                currnode.next = n2
                currnode = currnode.next
                n2 = n2.next
                
        while n1 is not None:
            currnode.next = n1
            currnode = currnode.next
            n1 = n1.next
        
        while n2 is not None:
            currnode.next = n2
            currnode = currnode.next
            n2 = n2.next
            
        return head.next
