class Solution:
    # @param s, a string
    # @return a list of lists of string
    def __init__(self):
        self.results = None
        self.S = None
        self.L = 0
    
    def ispalindrome(self, s):
        L = len(s)
        for i in xrange(L/2):
            if s[i] != s[L-1-i]:
                return False
        return True
    
    def rec(self, prefix, pos):
        if pos == self.L:
            self.results.append(prefix)
            return
        for i in xrange(pos, self.L):
            ss = self.S[pos:i+1]
            if self.ispalindrome(ss):
                self.rec(prefix + [ss], i+1)    
    
    def partition(self, s):
        # write your code here
        self.results = []
        self.S = s
        self.L = len(s)
        
        self.rec([], 0)
        return self.results
