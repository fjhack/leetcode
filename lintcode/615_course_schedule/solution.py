import unittest

class Solution:
    # @param {int} numCourses a total of n courses
    # @param {int[][]} prerequisites a list of prerequisite pairs
    # @return {boolean} true if can finish all courses or false
    def canFinish(self, numCourses, prerequisites):
        # Write your code here
        no_prereqs = []
        
        # prerequisites.sort()

        preq = dict()
        indegree = {course: 0 for course in xrange(numCourses)}

        _indget = indegree.get
        
        print prerequisites
        startc = 0
        for c in prerequisites:
            indegree[c[0]] = indegree[c[0]] + 1

        no_prereqs = [c for c in indegree.keys() if indegree[c] == 0]
        # print no_prereqs
        reached = set(no_prereqs)
        print indegree
        print reached
        
        bfsq = [p for p in prerequisites if p[1] in reached]
        print bfsq
        while len(bfsq) > 0:
            curr = bfsq.pop(0)
            print curr
            if indegree[curr[0]] > 0:
                indegree[curr[0]] = indegree[curr[0]] - 1
            if indegree[curr[0]] == 0:
                reached.add(curr[0])
                nexthead = [p for p in prerequisites if p[1] == curr[0]]
                bfsq += nexthead
        
        print reached
        if len(reached) < numCourses:
            return False
        else:
            return True

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
    
    def test_e_0(self):
        print
        nc = 5
        pr = [[1,0], [3,1], [3,2], [4,2], [4,3]]
        r = self.S.canFinish(nc, pr)
        print r

    def test_e_1(self):
        print
        nc = 10
        pr = [[5,8],[3,5],[1,9],[4,5],[0,2],[1,9],[7,8],[4,9]]
        r = self.S.canFinish(nc, pr)
        print r

if __name__ == "__main__":
    unittest.main()

