import sys

class Solution:
    # @param {int} m the number of eggs
    # @param {int} n the umber of floors
    # @return {int} the number of drops in the worst case
    def dropEggs2(self, m, n):
        # Write your code here
        DP = [[0 for i in xrange(n+1)] for _ in xrange(m+1)]
        
        for e in xrange(1,m+1):
            DP[e][0] = 0
            DP[e][1] = 1
            
        for l in xrange(1,n+1):
            DP[1][l] = l
            
        for e in xrange(2, m+1):
            for l in xrange(2, n+1):
                DP[e][l] = sys.maxint
                for k in xrange(1, l+1):
                    DP[e][l] = min(DP[e][l], 1+max(DP[e-1][k-1], DP[e][l-k]))
        
        return DP[m][n]
