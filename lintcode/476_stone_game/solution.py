import sys

class Solution:
    # @param {int[]} A an integer array
    # @return {int} an integer
    def stoneGame(self, A):
        # Write your code here
        L = len(A)
        prefixsum = [[0 for _ in xrange(L)] for _ in xrange(L)]
        for i in xrange(L):
            prefixsum[i][i] = A[i]
            for j in xrange(i+1, L):
                prefixsum[i][j] = prefixsum[i][j-1] + A[j]
        
        print "Prefixsum:"
        for r in prefixsum:
            print r

        DP = [[0 for _ in xrange(L)] for _ in xrange(L)]
        for i in xrange(L-1):
            DP[i][i+1] = A[i] + A[i+1]
        
        print "DP:"
        for r in DP:
            print r

        i, j = 0, 2
        while True:
            k = j-i
            p = i
            DP[i][j] = sys.maxint
            
            while p < j:
                s1b, s1e = i, p
                s2b, s2e = p+1, j
                DP[i][j] = min(DP[i][j], DP[s1b][s1e] + prefixsum[s1b][s1e] + DP[s2b][s2e] + prefixsum[s2b][s2e])
                p += 1
            
            if i == 0 and j == L-1:
                break
            elif j == L-1:
                i, j = 0, k+1
            else:
                i, j = i+1, j+1
                
        print "DP:"
        for r in DP:
            print r

        return DP[0][L-1]

if __name__ == "__main__":
    S = Solution()
    A1 = [4,1,1,4]
    print S.stoneGame(A1)

    print
    A2 = [4,4,5,9]
    print S.stoneGame(A2)

    print
    A3 = [1,1,1,1]
    print S.stoneGame(A3)
