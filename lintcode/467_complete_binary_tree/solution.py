"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""
class Solution:
    """
    @param root, the root of binary tree.
    @return true if it is a complete binary tree, or false.
    """
    def isComplete(self, root):
        # Write your code here
        def level2Count(l):
            return 2**l
        
        bfsq = []
        lvl, lvlcount = 0, 0
        lastlvl = False
        if root is None:
            return True
        bfsq.append(root)
        while len(bfsq) > 0:
            lvlcount = 0
            clvl = []
            lrange = level2Count(lvl)
            for i in xrange(lrange):
                n = bfsq.pop(0)
                clvl.append(n)
                if n is not None:
                    bfsq.append(n.left)
                    bfsq.append(n.right)
                    lvlcount += 1
            
            if lvlcount < lrange:
                foundNone = False
                for i in xrange(lrange):
                    if not foundNone:
                        if clvl[i] is None:
                            foundNone = True
                        else:
                            continue
                    else:
                        if clvl[i] is not None:
                            return False
                        else:
                            continue
                for i in xrange(len(bfsq)):
                    if bfsq[i] is not None:
                        return False
                return True
            else:
                lvl += 1
                continue
        # This is actually not necessary
        return True
