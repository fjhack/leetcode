class Solution:
    # @param {int[][]} grid  a 2D integer grid
    # @return {int} an intege
    def __init__(self):
        self.G = None
        self.rows = 0
        self.cols = 0
        self.zcount = 0
        self.total = 0
        
    def turnz(self, i, j):
        def _valid(x, y):
            return (x >= 0 and x < self.rows) and (y >= 0 and y < self.cols) and (self.G[x][y] == 0)
        
        DV = [(-1,0), (0,1), (1,0), (0,-1)]
        if self.G[i][j] == 1:
            nextz = [(i+d[0], j+d[1]) for d in DV if _valid(i+d[0], j+d[1])]
            return nextz
        else:
            return []
    
    # Only looking for zombie cells, tracking zombie cells using a queue
    def zombie(self, grid):
        self.G = grid
        self.rows = len(grid)
        if self.rows == 0:
            return 0
        self.cols = len(grid[0])
        self.zcount = 0
        self.total = self.cols * self.rows
        self.wallcount = 0
        iters = 0
        self.zqueue = []

        for i in xrange(self.rows):
            for j in xrange(self.cols):
                if self.G[i][j] == 1:
                    self.zcount += 1
                    self.zqueue.append((i, j))
                elif self.G[i][j] == 2:
                    self.wallcount += 1
        while len(self.zqueue) > 0:
            # self.zcount = 0
            qlen = len(self.zqueue)
            print "Before {0}: ".format(iters), self.zqueue
            for i in xrange(qlen):
                zcell = self.zqueue.pop(0)
                zx, zy = zcell[0], zcell[1]
                # self.zcount += 1
                nz = self.turnz(zx, zy)
                for c in nz:
                    if self.G[c[0]][c[1]] == 0:
                        self.G[c[0]][c[1]] = 1
                        self.zcount += 1
                        self.zqueue.append((c[0], c[1]))
            if len(self.zqueue) > 0:
                iters += 1
            print "After {0}: ".format(iters), self.zqueue
            print "  ", self.zcount

        if self.zcount + self.wallcount == self.total:
            return iters
        else:
            return -1

    # Brute force O(n^2) + BFS solution        
    def zombie0(self, grid):
        # Write your code here
        self.G = grid
        self.rows = len(grid)
        if self.rows == 0:
            return 0
        self.cols = len(grid[0])
        self.zcount = 0
        self.total = self.cols * self.rows
        iters = 0
        while self.zcount < self.total:
            iters += 1
            turned = []
            self.zcount = 0
            for i in xrange(self.rows):
                for j in xrange(self.cols):
                    if self.G[i][j] == 1:
                        self.zcount += 1
                        nz = self.turnz(i, j)
                        print "  ", nz
                        for c in nz:
                            if self.G[c[0]][c[1]] == 0:
                                self.G[c[0]][c[1]] = -1
                                turned.append((c[0], c[1]))
                                self.zcount += 1
                    if self.G[i][j] == 2:
                        self.zcount += 1
            for z in turned:
                self.G[z[0]][z[1]] = 1
            if len(turned) == 0:
                break
            print iters, turned
            print self.zcount
        if self.zcount < self.total:
            return -1
        else:
            return iters

if __name__ == "__main__":
    S = Solution()
    A = [[0,0,0],[0,0,0],[0,0,1]]
    r = S.zombie(A)
    print r
