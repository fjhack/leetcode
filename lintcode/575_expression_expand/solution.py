import unittest

class Solution:
    # @param {string} s  an expression includes numbers, letters and brackets
    # @return {string} a string
    def __init__(self):
        self.pos = 0

    def expand(self, s):
        expstr, left = [], 0
        if s[self.pos] == '[':
            left += 1
            self.pos += 1
        else:
            # Error
            return "", -1

        while left > 0:
            if s[self.pos].isalpha():
                expstr.append(s[self.pos])
                self.pos += 1
                continue
            elif s[self.pos] == ']':
                left -= 1
                self.pos += 1
                continue
            elif s[self.pos].isdigit():
                _times = ""
                while s[self.pos].isdigit():
                    _times += s[self.pos]
                    self.pos += 1
                _times = int(_times)
                expanded, endpos = self.expand(s)
                expstr.append(expanded*_times)
                self.pos = endpos
                continue
        # print "\t", expstr
        return ("".join(expstr), self.pos)

    def expressionExpand(self, s):
        # Write your code here
        L = len(s)
        self.pos = 0
        r = []
        while self.pos < L:
            if s[self.pos].isalpha():
                r.append(s[self.pos])
                self.pos += 1
                continue
            elif s[self.pos].isdigit():
                _times = ""
                while s[self.pos].isdigit():
                    _times += s[self.pos]
                    self.pos += 1
                _times = int(_times)
                # print "\t", _times, self.pos, s[self.pos]
                expanded, endpos = self.expand(s)
                # print "\t", expanded
                r.append(expanded*_times)
                self.pos = endpos
                continue
        return "".join(r)


class testSolution(unittest.TestCase):
    def setUp(self):
        self.ee = Solution().expressionExpand

    def test_1_0(self):
        e = "abc3[a]"
        r = self.ee(e)
        print r

    def test_1_1(self):
        e = "3[abc]"
        r = self.ee(e)
        print r

    def test_1_2(self):
        e = "4[ac]dy"
        r = self.ee(e)
        print r

    def test_2_0(self):
        e = "3[2[ad]3[pf]]xyz"
        g = "adadpfpfpfadadpfpfpfadadpfpfpfxyz"
        r = self.ee(e)
        print r, r == g


if __name__ == "__main__":
    unittest.main()
