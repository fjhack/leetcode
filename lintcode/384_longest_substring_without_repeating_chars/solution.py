class Solution:
    # @param s: a string
    # @return: an integer
    def lengthOfLongestSubstring(self, s):
        # write your code here
        
        L = len(s)
        if L == 0:
            return 0
        elif L == 1:
            return 1

        start, end = 0, 1
        seen = set()
        maxlen = 1
        seen.add(s[start])
        while end < L:
            if s[end] not in seen:
                seen.add(s[end])
                maxlen = max(maxlen, end-start+1)
                end += 1
            else:
                maxlen = max(maxlen, end-start)
                while s[end] in seen:
                    seen.discard(s[start])
                    start += 1
        return maxlen

if __name__ == "__main__":
    S = Solution()

    print
    s = "abcba"
    R = S.lengthOfLongestSubstring(s)
    print R

                    
