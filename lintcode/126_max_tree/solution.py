"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    # @param A: Given an integer array with no duplicates.
    # @return: The root of max tree.
    def maxTree(self, A):
        # write your code here
        stack = []
        for elem in A:
            curr_node = TreeNode(elem)
            while len(stack) > 0 and elem > stack[-1].val:
                curr_node.left = stack.pop()
            if len(stack) > 0:
                stack[-1].right = curr_node
            stack.append(curr_node)
        return stack[0]
