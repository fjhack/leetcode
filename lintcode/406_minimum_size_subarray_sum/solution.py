import sys

class Solution:
     # @param nums: a list of integers
     # @param s: an integer
     # @return: an integer representing the minimum size of subarray
    def minimumSize(self, nums, s):
        # write your code here
        L = len(nums)
        if L == 0:
            return -1

        start, end = 0, 0
        currsum = nums[0]
        if currsum >= s:
            return 1

        min_len = sys.maxint
        while end < L:
            if currsum < s:
                if end < L-1:
                    end += 1
                    currsum += nums[end]
                else:
                    break
            else:
                diff = currsum - s
                while currsum >= s:
                    min_len = min(min_len, end-start+1)
                    currsum -= nums[start]
                    diff = currsum - s
                    start += 1

            # print nums[start:end+1]
        if min_len == sys.maxint:
            return -1
        return min_len

if __name__ == "__main__":
    S = Solution()

    print
    A = [2,3,1,2,4,3]
    s = 7
    R = S.minimumSize(A, s)
    print R
