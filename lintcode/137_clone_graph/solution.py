
# Definition for a undirected graph node
# class UndirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []

# BFS Solution
class Solution:
    # @param node, a undirected graph node
    # @return a undirected graph node
    def __init__(self):
        self.lbl2node = {}
        
    def cloneGraph(self, node):
        # write your code here
        self.lbl2node = {}
        
        if node is None:
            return None
        
        bfsq = [node]
        nodes = [node]
        while len(bfsq) > 0:
            # con: current old node
            con = bfsq.pop(0)
            if con.label not in self.lbl2node:
                self.lbl2node[con.label] = UndirectedGraphNode(con.label)
                
            for conb in con.neighbors:
                if conb.label not in self.lbl2node:
                    self.lbl2node[conb.label] = UndirectedGraphNode(conb.label)
                    bfsq.append(conb)
                    nodes.append(conb)


        for nn in nodes:
            for nb in nn.neighbors:
                self.lbl2node[nn.label].neighbors.append(self.lbl2node[nb.label])

        return self.lbl2node[node.label]


# Recursive DFS
class SolutionDFS:
    def __init__(self):
        self.dict = {}
        
    def cloneGraph(self, node):
        if node == None:
            return None
        if node.label in self.dict:
            return self.dict[node.label]
        root = UndirectedGraphNode(node.label)
        self.dict[node.label] = root
        for item in node.neighbors:
            root.neighbors.append(self.cloneGraph(item))
        return root
