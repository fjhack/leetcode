class Solution:
    # @param board, a list of lists of 1 length string
    # @param words: A list of string
    # @return: A list of string
    
    def __init__(self):
        self.visited = None
        self.rows, self.cols = 0, 0
        self._board = None
        
    def neighbors(self, rpos, cpos):
        directions = [(0,1), (1,0), (0,-1), (-1,0)]
        return [(rpos+d[0], cpos+d[1]) for d in directions if (rpos+d[0] >=0 and rpos+d[0] < self.rows)
                and (cpos+d[1] >= 0 and cpos+d[1] < self.cols)
                and not self.visited[rpos+d[0]][cpos+d[1]]]
        
    def search(self, word, rpos, cpos):
        if len(word) == 0:
            return True
        else:
            anymatch = False
            self.visited[rpos][cpos] = True
            neighbors = self.neighbors(rpos, cpos)
            for n in neighbors:
                if self._board[n[0]][n[1]] == word[0]:
                    anymatch = anymatch or self.search(word[1:], n[0], n[1])
            self.visited[rpos][cpos] = False
            return anymatch
    
    def exist(self, word):
        # write your code here
        anymatch = False
        for i in xrange(self.rows):
            for j in xrange(self.cols):
                if self._board[i][j] == word[0]:
                    anymatch = anymatch or self.search(word[1:], i, j)
        return anymatch
        
    def wordSearchII(self, board, words):
        # write your code here
        self.rows = len(board)
        if self.rows == 0:
            return "" in words
        self.cols = len(board[0])
        if self.cols == 0:
            return "" in words
        self.visited = [[False for _ in xrange(self.cols)] for _ in xrange(self.rows)]
        self._board = board

        return [w for w in words if self.exist(w)]
