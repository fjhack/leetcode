# Definition for a Directed graph node
# class DirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []

class Solution:
    """
    @param graph: A list of Directed graph node
    @return: A list of graph nodes in topological order.
    """
    def topSort(self, graph):
        # write your code here
        
        results = []
        indegree = {}
        _indget = indegree.get
        for node in graph:
            for neighbor in node.neighbors:
                indegree[neighbor] = _indget(neighbor, 0) + 1
        
        bfsq = []
        for node in graph:
            if node not in indegree:
                bfsq.append(node)
                results.append(node)

        while len(bfsq) > 0:
            curr = bfsq.pop(0)
            for neighbor in curr.neighbors:
                indegree[neighbor] = indegree[neighbor] - 1
                if indegree[neighbor] == 0:
                    results.append(neighbor)
                    bfsq.append(neighbor)
        
        return results
                    
