"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of the binary search tree.
    @param k1 and k2: range k1 to k2.
    @return: Return all keys that k1<=key<=k2 in ascending order.
    """
    
    def searchRange(self, root, k1, k2):
        # write your code here
        results = []
        stk = []
        findk1, findk2 = False, False
        top = root
        while top is not None or len(stk) > 0:
            while top is not None:
                stk.append(top)
                top = top.left
            top = stk.pop()
            if top.val >= k1:
                findk1 = True
            if top.val > k2:
                findk2 = True
            if findk1 and not findk2:
                results.append(top.val)
            top = top.right
        return results
