class Solution:
    # @param s : A string
    # @return : An integer
    def lengthOfLongestSubstringKDistinct(self, s, k):
        # write your code here
        L = len(s)
        if L == 0 or L == 1:
            return L
        if k == 0:
            return 0

        uniqchars = set()
        start, end = 0, 0
        uniqchars.add(s[start])
        maxlen = 1

        while end < L-1:
            end += 1
            
            if len(uniqchars) <= k:
                print "  Extending"
                end += 1
                if end == L:
                    break
                uniqchars.add(s[end])
                if len(uniqchars) <= k:
                    maxlen = max(maxlen, end-start+1)
                print "  extended right", s[end]

                
            else:
                print "  Shriking"
                while len(uniqchars) > k:
                    uniqchars.discard(s[start])
                    print "  ", s[start], start
                    start += 1
                maxlen = max(maxlen, end-start)
            print maxlen, len(uniqchars), uniqchars, end-start, s[start:end]
        return maxlen


if __name__ == "__main__":
    S = Solution()
    
    print
    R = S.lengthOfLongestSubstringKDistinct("eceba", 3)
    print R

    print
    R = S.lengthOfLongestSubstringKDistinct("eqgkcwGFvjjmxutystqdfhuMblWbylgjxsxgnoh", 16)
    print R
