class Solution:
    """
    @param s: A string 
    @param p: A string includes "?" and "*"
    @return: A boolean
    """
    def __init__(self):
        self._mc = None

    def isMatch(self, s, p):
        # write your code here

        # return self.recmatch0(s, p)
        Ls, Lp = len(s), len(p)
        self._mc = [[-1 for _ in xrange(Lp+1)] for _ in xrange(Ls+1)]
        return self.recmatch1(s, p)

    # Brute force recursive search
    def recmatch0(self, s, p):
        Ls, Lp = len(s), len(p)
        if Ls == 0:
            if Lp == 0:
                return True
            elif p[0] == '*':
                return self.recmatch0(s, p[1:])
            else:
                return False
        elif Lp == 0:
            return False
        
        if p[0] == s[0]:
            return self.recmatch0(s[1:], p[1:])
        elif p[0] == '?':
            return self.recmatch0(s[1:], p[1:])
        elif p[0] == '*':
            matched = False
            for i in xrange(Ls+1):
                if self.recmatch0(s[i:], p[1:]):
                    matched = True
                    break
            return matched
        else:
            return False

    # Search with memoization
    def recmatch1(self, s, p):
        Ls, Lp = len(s), len(p)
        if self._mc[Ls][Lp] != -1:
            return self._mc[Ls][Lp] == 1

        if Ls == 0:
            if Lp == 0:
                self._mc[Ls][Lp] = 1
                return True
            elif p[0] == '*':
                self._mc[Ls][Lp] = self.recmatch1(s, p[1:])
                return self._mc[Ls][Lp]
            else:
                self._mc[Ls][Lp] = 0
                return False
        elif Lp == 0:
            self._mc[Ls][Lp] = 0
            return False
        
        if p[0] == s[0]:
            self._mc[Ls][Lp] = self.recmatch1(s[1:], p[1:])
            return self._mc[Ls][Lp]
        elif p[0] == '?':
            self._mc[Ls][Lp] = self.recmatch1(s[1:], p[1:])
            return self._mc[Ls][Lp]
        elif p[0] == '*':
            matched = False
            for i in xrange(Ls+1):
                if self.recmatch1(s[i:], p[1:]):
                    matched = True
                    break
            self._mc[Ls][Lp] = matched
            return matched
        else:
            self._mc[Ls][Lp] = 0
            return False
