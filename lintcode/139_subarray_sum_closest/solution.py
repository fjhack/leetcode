class Solution:
    """
    @param nums: A list of integers
    @return: A list of integers includes the index of the first number 
             and the index of the last number
    """
    def subarraySumClosest(self, nums):
        # write your code here
        L = len(nums)
        pfsums = [0]
        _s = 0
        for i in xrange(L):
            _s += nums[i]
            pfsums.append(_s)
        
        pfsum_pos = sorted([(pfsums[i], i) for i in xrange(L+1)])
        mindiff = sys.maxint
        minpair = None
        
        for i in xrange(L):
            cdiff = pfsum_pos[i+1][0] - pfsum_pos[i][0]
            if abs(cdiff) < mindiff:
                mindiff = abs(cdiff)
                minpair = sorted([pfsum_pos[i][1], pfsum_pos[i+1][1]])
                minpair = [minpair[0], minpair[1]-1]
        return minpair
