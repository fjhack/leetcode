import unittest

class Solution:
    # @param nums {int[]} an array of integer
    # @param target {int} an integer
    # @return {int} an integer
    def twoSum6(self, nums, target):
        # Write your code here
        L = len(nums)
        if L == 0:
            return 0
        nums = sorted(nums)
        lpos, rpos = 0, L-1
        result = 0
        while lpos < rpos:
            csum = nums[lpos] + nums[rpos]
            old_left = nums[lpos]
            old_right = nums[rpos]
            if csum < target:
                while nums[lpos] == old_left and lpos < rpos:
                    lpos += 1
            elif csum > target:
                while nums[rpos] == old_right and rpos > lpos:
                    rpos -= 1
            else:
                result += 1
                while nums[lpos] == old_left and lpos < rpos:
                    lpos += 1
                while nums[rpos] == old_right and rpos > lpos:
                    rpos -= 1
        return result

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        nums = [1,1,2,45,46,46]
        target = 47
        print "Test 0:"
        R = self.S.twoSum6(nums, target)
        print R

if __name__ == "__main__":
    unittest.main()
