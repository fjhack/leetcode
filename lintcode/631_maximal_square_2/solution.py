import unittest

class Solution:
    def __init__(self):
        self.DP = None
        # number of zeroes, horizontal and vertical
        self.zH, self.zV = None, None
        self.rows, self.cols = 0, 0
    
    #param {int[][]} matrix a matrix of 0 and 1
    #return {int} an integer
    def maxSquare2(self, matrix):
        # write your code here
        self.rows = len(matrix)
        if self.rows == 0:
            return 0
        self.cols = len(matrix[0])
        if self.cols == 0:
            return 0

        self.DP = [[0 for _ in xrange(self.cols)] for _ in xrange(self.rows)]
        maxsize = 0
        for i in xrange(self.rows):
            if matrix[i][0] == 1:
                self.DP[i][0] = 1
                maxsize = max(maxsize, self.DP[i][0])
        for i in xrange(self.cols):
            if matrix[0][i] == 1:
                self.DP[0][i] = 1
                maxsize = max(maxsize, self.DP[0][i])
                
        self.zH = [[0 for _ in xrange(self.cols)] for _ in xrange(self.rows)]
        self.zV = [[0 for _ in xrange(self.cols)] for _ in xrange(self.rows)]
        
        for i in xrange(self.rows):
            zcount = 0
            if matrix[i][0] == 0:
                zcount = 1
            self.zH[i][0] = zcount
            for j in xrange(1, self.cols):
                if matrix[i][j] == 1:
                    zcount = 0
                else:
                    zcount += 1
                self.zH[i][j] = zcount
                
        for j in xrange(self.cols):
            zcount = 0
            if matrix[0][j] == 0:
                zcount += 1
            self.zV[0][j] = zcount
            for i in xrange(1, self.rows):
                if matrix[i][j] == 1:
                    zcount = 0
                else:
                    zcount += 1
                self.zV[i][j] = zcount
        print "\nInitial state:"

        for _d in self.DP:
            print _d
        print "zH:"
        for _zh in self.zH:
            print _zh
        print "zV"
        for _zv in self.zV:
            print _zv

        for i in xrange(1, self.rows):
            for j in xrange(1, self.cols):
                if matrix[i][j] == 1:
                    # if self.DP[i-1][j-1] == min(self.zH[i][j], self.zV[i][j]):
                    #     self.DP[i][j] = self.DP[i-1][j-1] + 1
                    # elif self.DP[i-1][j-1] > min(self.zH[i][j], self.zV[i][j]):
                    #     self.DP[i][j] = min(self.zH[i][j], self.zV[i][j])
                    # else:
                    #     self.DP[i][j] = 1
                    
                    self.DP[i][j] = min(self.DP[i-1][j-1], min(self.zH[i][j-1], self.zV[i-1][j])) + 1
                    maxsize = max(maxsize, self.DP[i][j])

        print "\nFinal state:"

        for _d in self.DP:
            print _d
        print "zH:"
        for _zh in self.zH:
            print _zh
        print "zV"
        for _zv in self.zV:
            print _zv
        print maxsize*maxsize

        return maxsize*maxsize



class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        print "\nTest 0:"
        M = [[1,0,0,1],
             [0,1,0,0],
             [1,0,1,0],
             [1,0,0,1]]
        self.S.maxSquare2(M)

    def test_1(self):
        print "\nTest 1:"
        M = [[0,1,0,0],
             [0,0,1,0],
             [1,0,0,1],
             [0,0,1,0]]
        self.S.maxSquare2(M)

    def test_2(self):
        print "\nTest 2:"
        M = [[1,0,1,0,0],
             [1,0,0,1,0],
             [1,1,0,0,1],
             [1,0,0,1,0]]
        self.S.maxSquare2(M)

    def test_3(self):
        print "\nTest 3:"
        M = [[1],
             [1],
             [1],
             [1]]
        self.S.maxSquare2(M)

    def test_4(self):
        print "\nTest 4:"
        M = [[1,1,1,1,1,0,1,1,0],
             [0,0,1,1,1,0,1,1,0],
             [0,0,0,1,0,0,0,1,1],
             [0,1,1,1,1,0,0,0,1],
             [1,0,1,1,0,0,1,1,0],
             [1,0,0,0,1,1,0,1,0]]
        self.S.maxSquare2(M)
        
if __name__ == "__main__":
    unittest.main()
