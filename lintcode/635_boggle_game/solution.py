import unittest

class Trie(object):
    def __init__(self):
        self._d = dict()
        self.terminate = False
        self._result = []

    def insert(self, word):
        if len(word) == 0:
            self.terminate = True
            return
        else:
            if word[0] not in self._d:
                self._d[word[0]] = Trie()
            self._d[word[0]].insert(word[1:])
            return

    def search(self, word):
        if len(word) == 0:
            return self.terminate
        else:
            if word[0] in self._d:
                return self._d[word[0]].search(word[1:])
            else:
                return False

    def allStartsWith(self, prefix):
        if len(prefix) == 0:
            results = set()
            Q = [(self, '')]
            while len(Q) > 0:
                curr = Q.pop(0)
                # print curr[0], curr[0].terminate, curr[1]
                if curr[0].terminate:
                    results.add(curr[1])
                if len(curr[0]._d) == 0:
                    results.add(curr[1])
                for k, v in curr[0]._d.iteritems():
                    Q.append((v, curr[1]+k))
            return results
        else:
            if prefix[0] in self._d:
                results = self._d[prefix[0]].allStartsWith(prefix[1:])
                return set([prefix[0]+result for result in results])
            else:
                return set()

    def startsWith(self, prefix):
        if len(prefix) == 0:
            return True
        else:
            if prefix[0] in self._d:
                return self._d[prefix[0]].startsWith(prefix[1:])
            else:
                return False

directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]

class Solution:
    # @param {char[][]} board a list of lists of char
    # @param {str[]} words a list of string
    # @return {int} an integer
    def __init__(self):
        self.rows, self.cols = 0, 0
        self.maxlen = 0
        self.maxresult = []
        self.matched_cache = dict()

        self.board = None
        self.GV = None

    # Trie version
    # def dfs(self, rwords, result, wordtrie):
    def dfs(self, rwords, rlen, wordtrie):
        _dprefix = "  " * rlen
        for r in xrange(self.rows):
            for c in xrange(self.cols):
                if self.GV[r][c]:
                    continue
                else:
                    # Current cell starts with correct letter

                    cc = self.board[r][c]
                    Q = [[(r, c)]]
                    widx = 0

                    # This can use trie
                    if cc not in self.matched_cache:
                        self.matched_cache[cc] = wordtrie.allStartsWith(cc)
                    matched = self.matched_cache[cc]
                    # print cc, matched

                    V2 = set()

                    nextwords = []
                    while len(Q) > 0:
                        # print _dprefix, Q
                        curr = Q.pop(0)
                        curr_r, curr_c = curr[-1]
                        curr_w = ''.join([self.board[c[0]][c[1]] for c in curr])

                        V2.add((curr_r, curr_c))

                        if curr_w in matched:
                            # Found
                            # print _dprefix, "Found matched word in board:", curr, [board[c[0]][c[1]] for c in curr]
                            # print _dprefix, "Current result:", result + [matched_word]
                            # print _dprefix, "Continue finding next word (DFS)..."

                            if rlen + 1 > self.maxlen:
                                self.maxlen = rlen + 1
                                # self.maxresult = result + [curr_w]

                            nextwords.append(curr)

                        nextcells = [(curr_r+d[0], curr_c+d[1]) for d in directions if curr_r+d[0] >= 0 and curr_r+d[0] < self.rows and
                                     curr_c+d[1] >= 0 and curr_c+d[1] < self.cols and
                                     not self.GV[curr_r+d[0]][curr_c+d[1]] and
                                     (curr_r+d[0], curr_c+d[1]) not in V2]
                        # print nextcells
                        for nc in nextcells:
                            if self.GV[nc[0]][nc[1]] or nc in V2:
                                continue
                            # print " ", nc, board[nc[0]][nc[1]]
                            for matched_word in matched:
                                # print matched_word
                                if len(curr) < len(matched_word) and self.board[nc[0]][nc[1]] == matched_word[len(curr)]:
                                    Q.append(curr + [nc])

                    for curr in nextwords:
                        curr_w = ''.join([self.board[c[0]][c[1]] for c in curr])
                        for cell in curr:
                            self.GV[cell[0]][cell[1]] = True

                        self.dfs(rwords, rlen+1, wordtrie)

                        # change self.GV back to normal and change removed back
                        for cell in curr:
                            self.GV[cell[0]][cell[1]] = False


    def boggleGame(self, board, words):
        # Write your code here
        self.rows, self.cols = len(board), len(board[0])
        self.maxlen = 0
        self.maxresult = []
        self.matched_cache = dict()

        self.board = board
        wordset = set(words)
        wordtrie = Trie()
        for w in words:
            wordtrie.insert(w)

        self.GV = [[False for _ in xrange(len(board[0]))] for _ in xrange(len(board))]

        self.dfs(wordset, 0, wordtrie)
        return self.maxlen

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    # def test_0(self):
    #     board = [['a', 'b', 'c'],
    #              ['d', 'e', 'f'],
    #              ['g', 'h', 'i']]
    #     words = ["abc", "cfi", "beh", "defi", "gh"]
    #     result = self.S.boggleGame(board, words)
    #     print result

    # def test_1(self):
    #     board = ["abcdefg",
    #              "huyuyww",
    #              "ghihjui",
    #              "wuiiuww"]
    #     words = ["efg","defi","gh","iuw","ww","iw","ghih","dasf","aaa"]
    #     result = self.S.boggleGame(board, words)
    #     print result

    # def test_2s(self):
    #     board = ["aaa","aaa","aaa"]
    #     words = ["aa","a"]
    #     result = self.S.boggleGame(board, words)
    #     print result

    def test_2(self):
        board = ["aaaa","aaaa","aaaa"]
        words = ["aaa","aa","a","aaaa","aaaaa","aaaaaa"]
        result = self.S.boggleGame(board, words)
        print result

if __name__ == "__main__":
    unittest.main()
