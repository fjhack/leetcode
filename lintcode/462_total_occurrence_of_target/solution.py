class Solution:
    # @param {int[]} A an integer array sorted in ascending order
    # @param {int} target an integer
    # @return {int} an integer
    def totalOccurrence(self, A, target):
        # Write your code here
        L = len(A)
        if L == 0:
            return 0
        left, right = 0, L-1
        mid = left + (right - left) / 2
        while left + 1 < right:
            mid = left + (right - left) / 2
            #print mid
            if A[mid] == target:
                break
            elif A[mid] < target:
                left = mid
            elif A[mid] > target:
                right = mid
        #print mid, A[mid]
        
        if A[right] == target:
            mid = right
        elif A[mid] != target:
            return 0
        left, right = mid, mid
        #print left, right
        while (A[left] == target) or (A[right] == target):
            if left >= 0 and A[left] == target:
                left -= 1
            if right < L and A[right] == target:
                right += 1
            if left < 0 and right >= L:
                break
            
        return right - left - 1

# Based on find range
class Solution2:
    # @param {int[]} A an integer array sorted in ascending order
    # @param {int} target an integer
    # @return {int} an integer
    def totalOccurrence(self, A, target):
        # Write your code here
        def findRHead(nums, target):
            L = len(nums)
            head, tail = 0, L-1
            while head <= tail:
                mid = head + (tail-head)/2
                if nums[mid] < target:
                    head = mid + 1
                elif nums[mid] > target:
                    tail = mid - 1
                else:
                    tail = mid - 1
            if head >= 0 and head < L and nums[head] == target:
                return head
            else:
                return -1
                
        def findRTail(nums, target):
            L = len(nums)
            head, tail = 0, L-1
            while head <= tail:
                mid = head + (tail-head)/2
                if nums[mid] < target:
                    head = mid + 1
                elif nums[mid] > target:
                    tail = mid - 1
                else:
                    head = mid + 1
            if tail >= 0 and tail < L and nums[tail] == target:
                return tail
            else:
                return -1
                
        tail = findRTail(A, target)
        head = findRHead(A, target)
        if tail == -1 or head == -1:
            return 0
        else:
            return tail - head + 1
        
if __name__ == "__main__":
    sto = Solution().totalOccurrence
    A = [1,3,3,4,5]
    r = sto(A, 3)
    print r
    print
    A = [1,2,3,4,5]
    r = sto(A, 6)
    print r
    print
    A = [1,2,5,6,7]
    r = sto(A, 4)
    print r

    print
    A = [1,1,1,1,1,1,1,1,1,1,1]
    r = sto(A, 1)
    print r
