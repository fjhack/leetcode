"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    # @param {TreeNode} root the root of the binary tree
    # @return {List[str]} all root-to-leaf paths
    def __init__(self):
        self.paths = []
    
    def dfs(self, node, cpath):
        if node is None:
            return
        elif node.left is None and node.right is None:
            newpath = cpath + [str(node.val)]
            self.paths.append(newpath)
        else:
            newpath = cpath + [str(node.val)]
            self.dfs(node.left, newpath)
            self.dfs(node.right, newpath)
            
    def path2str(self):
        return ["->".join(p) for p in self.paths]
        
    def binaryTreePaths(self, root):
        # Write your code here
        self.paths = []
        self.dfs(root, [])
        return self.path2str()
