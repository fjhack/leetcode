import sys
import unittest

class Solution:
    # @param A: An integer array.
    # @param target: An integer.
    def __init__(self):
        self._c0 = None

    # Solution 0: Brute force backtracking solution
    def MinAdjustmentCost(self, A, target):
        # write your code here
        L = len(A)
        if L == 0:
            return 0
        self._c0 = [[sys.maxint for _ in xrange(100)] for _ in xrange(L)]
        return self.rec1(A, target, 0)

    def rec0(self, A, target, cidx):
        L = len(A)
        if cidx >= L:
            return 0

        curr_mindiff = sys.maxint
        orig = A[cidx]
        for i in xrange(1,101):
            if cidx > 0 and abs(A[cidx-1] - i) > target:
                continue
            
            cdiff = abs(i - A[cidx])
            A[cidx] = i
            next_mindiff = self.rec0(A, target, cidx+1)
            A[cidx] = orig
            curr_mindiff = min(curr_mindiff, cdiff + next_mindiff)
        
        return curr_mindiff

    def rec1(self, A, target, cidx):
        L = len(A)
        if cidx >= L:
            return 0

        curr_mindiff = sys.maxint
        orig = A[cidx]
        for i in xrange(1,101):
            if cidx > 0 and abs(A[cidx-1] - i) > target:
                continue

            if self._c0[cidx][i-1] <> sys.maxint:
                cdiff = self._c0[cidx][i-1]
                curr_mindiff = min(curr_mindiff, cdiff)
                continue


            cdiff = abs(i - A[cidx])
            A[cidx] = i
            next_mindiff = self.rec1(A, target, cidx+1)
            curr_mindiff = min(curr_mindiff, cdiff + next_mindiff)
            A[cidx] = orig
            self._c0[cidx][i-1] = cdiff + next_mindiff

        # print "  "*cidx, curr_mindiff, A[min(cidx-1,0):]
        return curr_mindiff

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_b0(self):
        A = [1,4,2,3]
        print "\nTest b0:", A
        r = self.S.MinAdjustmentCost(A, 1)
        print r

    [12,3,7,4,5,13,2,8,4,7,6,5,7], 2
    def test_b1(self):
        A = [12,3,7,4,5,13,2,8,4,7,6,5,7]
        print "\nTest 2", A
        r = self.S.MinAdjustmentCost(A, 2)
        print r

    

if __name__ == "__main__":
    unittest.main()
