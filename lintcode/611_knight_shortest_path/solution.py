import unittest

# Definition for a point.
class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b

class Solution:
    # @param {boolean[][]} grid a chessboard included 0 (False) and 1 (True)
    # @param {Point} source a point
    # @param {Point} destination a point
    # @return {int} the shortest path 
    def __init__(self):
        self.G = None
        self.rows = 0
        self.cols = 0
        self.visited = None
    
    def nextJump(self, x, y):
        jump = [(2, 1), (1, 2), (-1, 2), (-2, 1), (-2, -1), (-1, -2), (1, -2), (2, -1)]
        def _valid(nx, ny):
            return (nx >= 0 and nx < self.rows) and (ny >= 0 and ny < self.cols) and (self.G[nx][ny] == 0) and (not self.visited[nx][ny])
        return [(x+d[0], y+d[1]) for d in jump if _valid(x+d[0], y+d[1])]
    
    def shortestPath(self, grid, source, destination):
        # Write your code here
        self.G = grid
        self.rows = len(self.G)
        if self.rows == 0:
            return -1
        self.cols = len(self.G[0])
        self.visited = [[False for j in xrange(self.cols)] for i in xrange(self.rows)]
        self.visited[source.x][source.y] = True
        
        bfsq = [(source.x, source.y)]
        climit = 1
        hops = 1
        while len(bfsq) > 0:
            newlimit = 0
            for i in xrange(climit):
                currcell = bfsq.pop(0)
                nextcells = self.nextJump(currcell[0], currcell[1])
                print nextcells
                for nc in nextcells:
                    if nc[0] == destination.x and nc[1] == destination.y:
                        return hops
                    else:
                        bfsq.append((nc[0], nc[1]))
                        self.visited[nc[0]][nc[1]] = True
                        newlimit += 1
            climit = newlimit
            hops += 1
        return -1

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_e_0(self):
        grid = [[0,0,0],[0,0,0],[0,0,0]]
        S = Point(2,0)
        D = Point(0,2)
        R = self.S.shortestPath(grid, S, D)
        print R

if __name__ == "__main__":
    unittest.main()
