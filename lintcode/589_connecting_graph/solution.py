class ConnectingGraph:

    # @param {int} n
    def __init__(self, n):
        # initialize your data structure here.
        self._parents = [v for v in xrange(n+1)]

    def find(self, node):
        if self._parents[node] == node:
            return self._parents[node]
        self._parents[node] = self.find(self._parents[node])
        return self._parents[node]

    # @param {int} a, b
    # return nothing
    def connect(self, a, b):
        # Write your code here
        parent_a, parent_b = self.find(a), self.find(b)
        self._parents[parent_b] = parent_a

    # @param {int} a, b
    # return {boolean} true if they are connected or false
    def query(self, a, b):
        # Write your code here
        parent_a, parent_b = self.find(a), self.find(b)
        return parent_a == parent_b
