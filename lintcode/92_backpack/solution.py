class Solution:
    # @param m: An integer m denotes the size of a backpack
    # @param A: Given n items with size A[i]
    # @return: The maximum size

    # Raw method, O(mn) space and time complexity
    def backPack0(self, m, A):
        # write your code here
        L = len(A)
        # DP = [[0 for _ in xrange(m+1)] for _ in xrange(L+1)]
        DP1 = [0 for _ in xrange(m+1)]
        DP2 = [0 for _ in xrange(m+1)]
        for i in xrange(1, L+1):
            for j in xrange(1, m+1):
                if j-A[i-1] < 0:
                    # DP[i][j] = DP[i-1][j]
                    DP2[j] = DP1[j]
                else:
                    # DP[i][j] = max(DP[i-1][j], DP[i-1][j-A[i-1]]+A[i-1])
                    DP2[j] = max(DP1[j], DP1[j-A[i-1]]+A[i-1])
            for j in xrange(m+1):
                DP1[j] = DP2[j]
        return DP2[m]

    # Occasionally cannot accept
    def backPack1(self, m, A):
        # write your code here
        L = len(A)
        DP2 = [0 for _ in xrange(m+1)]
        for cv in A:
            for j in xrange(m, -1, -1):
                r = j - cv
                if r >= 0:
                    DP2[j] = max(DP2[j], DP2[r]+cv)
        return DP2[m]

    # Optimized inner loop lower bound, always pass
    def backPack(self, m, A):
        # write your code here
        L = len(A)
        DP2 = [0 for _ in xrange(m+1)]
        for i in xrange(1, L+1):
            cv = A[i-1]
            bound = max(m-sum(A[i-1:L]), cv)
            for j in range(m, bound-1, -1):
                r = j - cv
                if r >= 0:
                    DP2[j] = max(DP2[j], DP2[r]+cv)
        return DP2[m]
    
    # Always accept, from solution
    def backPack2(self, m, A):
        # write your code here
        n = len(A)
        dp = [0 for x in range(m+1)]
        dp[0] = 1
        ans = 0
        for item in A:
            for i in range(m,-1,-1):
                if i-item >=0 and dp[i-item] > 0:
                    ans = max(ans,i)
                    dp[i] = 1
        return ans    

if __name__ == "__main__":
    S = Solution()
    A = [3,4,8,5]
    r = S.backPack(10, A)
    print r
