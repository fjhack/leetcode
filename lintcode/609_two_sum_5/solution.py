import unittest

class Solution:
    # @param nums {int[]} an array of integer
    # @param target {int} an integer
    # @return {int} an integer
    def twoSum5(self, nums, target):
        # Write your code here
        L = len(nums)
        if L == 0:
            return 0
        nums = sorted(nums)
        lpos, rpos = 0, L-1
        result = 0
        while lpos < rpos:
            lv, rv = nums[lpos], nums[rpos]
            csum = lv + rv
            if csum > target:
                rpos -= 1
            else:
                lpos1 = lpos
                # while lpos1 < rpos and nums[lpos1] + rv <= target:
                #     result += 1
                #     lpos1 += 1
                # rpos -= 1
                result += (rpos - lpos)
                lpos += 1
        return result


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        nums = [2,7,11,15]
        target = 24
        print "Test 0:"
        R = self.S.twoSum5(nums, target)
        print R

    def test_1(self):
        nums = [1,0,-1]
        target = 1
        print "Test 1:"
        R = self.S.twoSum5(nums, target)
        print R

if __name__ == "__main__":
    unittest.main()
