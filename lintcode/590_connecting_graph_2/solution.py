class ConnectingGraph2:

    # @param {int} n
    def __init__(self, n):
        # initialize your data structure here.
        self._parents = [v for v in xrange(n+1)]
        self._comps = [1 for v in xrange(n+1)]

    def find(self, node):
        if self._parents[node] == node:
            return self._parents[node]
        self._parents[node] = self.find(self._parents[node])
        self._comps[node] = self.comps[self._parents[node]]
        return self._parents[node]

    # @param {int} a, b
    # return nothing
    def connect(self, a, b):
        # Write your code here
        parent_a, parent_b = self.find(a), self.find(b)
        self._parents[parent_b] = parent_a
        if parent_a != parent_b:
            self._comps[parent_a] += self._comps[parent_b]
            self._comps[parent_b] = self._comps[parent_a]

    # @param {int} a
    # return {int}  the number of nodes connected component
    # which include a node.
    def query(self, a):
        # Write your code here
        parent_a = self.find(a)
        return self._comps[parent_a]
