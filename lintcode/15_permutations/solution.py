class Solution:
    """
    @param nums: A list of Integers.
    @return: A list of permutations.
    """
    def __init__(self):
        self.results = []
        self.nums = None
        self.L = 0
        
    def rec(self, prefix):
        if len(prefix) == self.L:
            self.results.append(prefix)
            return
        for n in self.nums:
            if n not in prefix:
                np = prefix + [n]
                self.rec(np)
                
    def permute(self, nums):
        # write your code here
        self.results = []
        self.nums = nums
        self.L = len(nums)
        
        self.rec([])
        return self.results

if __name__ == "__main__":
    S = Solution()
    r = S.permute([1])
    print r
