import sys
import unittest

class Solution:
    # @param {int[]} nums an array with positive and negative numbers
    # @param {int} k an integer
    # @return {double} the maximum average

    # Brute force solution
    def maxAverageBF(self, nums, k):
        # Write your code here
        L = len(nums)
        if L == 0 or k > L:
            return 0.0
        
        prefix_sums = [0 for _ in xrange(L+1)]

        csum, cavg = 0, 0.0
        for i in xrange(1, L+1):
            csum += nums[i-1]
            prefix_sums[i] = csum

        print nums
        print prefix_sums
        
        start = k
        maxavg, minavg = -sys.float_info.max, sys.float_info.max
        
        max_sum = -sys.maxint
        max_avg = -sys.float_info.max
        for i in xrange(start, L+1):
            print i
            lb, hb = 0, i-k+1
            print lb, hb
            for j in xrange(lb, hb):
                print nums[lb:hb]
                crsum = prefix_sums[i] - prefix_sums[j]
                cravg = float(crsum) / float(i-j)
                if cravg > max_avg:
                    max_avg = cravg
        
        return max_avg

    # Binary search solution
    def maxAverageBS(self, nums, k):
        l, r = float(sys.maxint), float(-sys.maxint)
        L = len(nums)
        for n in nums:
            if n < l:
                l = float(n)
            if n > r:
                r = float(n)

        csum = [0.0 for _ in xrange(L+1)]

        for i in xrange(1, L+1):
            csum[i] = float(csum[i-1] + nums[i-1])
            
        while r-l > 0.000001:
            mid = float(l+r) / 2.0
            min_pre = 0.0
            # Reached min allowed subarray length
            reached = False
            print "l={0}\tr={1}\tmid={2}\tmin_pre={3}".format(l, r, mid, min_pre)

            for i in xrange(1, L+1):
                print "\ti={0}:".format(i)
                csum[i] = float(csum[i-1] + nums[i-1] - mid)
                print "\t  csum[i]={0}, min_pre={1}".format(csum[i], min_pre)
                if i >= k and csum[i] - min_pre >= 0:
                    reached = True
                    break
                if i >= k:
                    min_pre = min(min_pre, csum[i-k+1])
                    print "\t  updating min_pre={0}".format(min_pre)

            if reached:
                l = mid
                print " ", "Reached"
            else:
                r = mid
            print
        return l


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [1, 12, -5, -6, 50, 3]
        k = 3
        r = self.S.maxAverageBS(A, k)
        print r

if __name__ == "__main__":
    unittest.main()

