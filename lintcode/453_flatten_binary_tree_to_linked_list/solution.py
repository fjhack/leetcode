
from treeserde import TreeSerDe

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

class Solution:
    # @param root: a TreeNode, the root of the binary tree
    # @return: nothing
    def flatten(self, root):
        # write your code here
        if root == None:
            return
        self.flatten(root.left)
        self.flatten(root.right)
        print "Current:", root.val
        p = root
        if p.left == None:
            return
        p = p.left
        while p.right:
            p = p.right
        print "  p:", p.val
        print "  root:", root.val
        print "  root->right:\t", root.right.val
        print "  root->left:\t", root.left.val
        p.right = root.right
        root.right = root.left
        root.left = None

if __name__ == "__main__":

    S = Solution()
    serde = TreeSerDe()
    
    R = TreeNode(1)
    R.left = TreeNode(2)
    R.left.left = TreeNode(3)
    R.left.right = TreeNode(4)
    R.right = TreeNode(5)
    R.right.right = TreeNode(6)
    print serde.serialize(R)
    S.flatten(R)
    print serde.serialize(R)

