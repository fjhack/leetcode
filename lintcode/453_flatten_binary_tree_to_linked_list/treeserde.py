import unittest

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


class TreeSerDe:
    def serialize(self, root):
        if root is None:
            return "{}"

        queue = [root]
        index = 0
        while index < len(queue):
            if queue[index] is not None:
                queue.append(queue[index].left)
                queue.append(queue[index].right)
            index += 1

        while queue[-1] is None:
            queue.pop()

        return '{%s}' % ','.join([str(node.val) if node is not None else '#'
                                  for node in queue])


    def deserialize(self, data):
        data = data.strip('\n')

        if data == '{}':
            return None

        vals = data[1:-1].split(',')
            
        root = TreeNode(int(vals[0]))
        queue = [root]
        isLeftChild = True
        index = 0

        for val in vals[1:]:
            if val is not '#':
                node = TreeNode(int(val))
                if isLeftChild:
                    queue[index].left = node
                else:
                    queue[index].right = node
                queue.append(node)

            if not isLeftChild:
                index += 1
            isLeftChild = not isLeftChild

        return root

    def printBinTree(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        Q, results = [], []
        if root is not None:
            Q.append(root)
        while len(Q) > 0:
            curr_len = len(Q)
            curr_lvl = []
            for i in xrange(curr_len):
                _p = Q.pop(0)
                if _p.left is not None:
                    Q.append(_p.left)
                if _p.right is not None:
                    Q.append(_p.right)
                curr_lvl.append(_p.val)
            print curr_lvl


class testSerDe(unittest.TestCase):
    def setUp(self):
        self.S = TreeSerDe()
        self.des = self.S.deserialize
        self.ser = self.S.serialize

    def test_1(self):
        root = TreeNode(1)
        root.left = TreeNode(2)
        root.right = TreeNode(3)
        root.right.left = TreeNode(4)
        root.right.right = TreeNode(5)
        r = self.ser(root)
        print r
        des_result = self.des(r)
        print des_result.val
        print des_result.left.val
        print des_result.right.val
        print des_result.right.left.val
        print des_result.right.right.val

    def test_2(self):
        sed = "989,982,#,972,#,947,#,920,#,903,#,894,#,881,#,866,#,864,#,842,#,841,#,796,#,726,#,647,#,613,719,593,#,#,#,590,#,558,#,554,#,538,#,512,#,504,#,468,505,467,#,#,#,456,#,413,#,331,#,330,407,320,#,#,#,312,#,306,#,301,#,274,#,251,#,235,#,231,#,222,#,181,#,93,#,83,#,73,#,64,#,62,#,60,#,28,#,21,#,20,#,-32,#,-52,#,-70,#,-87,#,-98,#,-102,#,-115,#,-116,#,-139,#,-183,#,-224,#,-241,#,-263,#,-284,#,-294,#,-296,#,-320,#,-330,#,-392,#,-398,#,-407,#,-431,#,-445,#,-460,#,-463,#,-492,#,-507,#,-518,#,-539,#,-552,#,-558,#,-559,#,-587,#,-673,#,-736,#,-757,#,-766,#,-767,#,-823,#,-830,#,-867,#,-875,#,-891,#,-905,#,-910,#,-924,#,-960,#,-985"
        r = self.des(sed)
        self.S.printBinTree(r)

if __name__ == "__main__":
    unittest.main()


