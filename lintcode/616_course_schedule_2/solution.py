import unittest

class Solution:
    # @param {int} numCourses a total of n courses
    # @param {int[][]} prerequisites a list of prerequisite pairs
    # @return {int[]} the course order
    def findOrder(self, numCourses, prerequisites):
        # Write your code here
        inedge = [[] for _ in xrange(numCourses)]
        for preq in prerequisites:
            inedge[preq[0]].append(preq[1])

        startq = [i for i, e in enumerate(inedge) if len(e) == 0]
        results = []

        while len(startq) > 0:
            curr = startq.pop(0)
            results.append(curr)
            cnexts = [p for p in prerequisites if p[1] == curr]
            for _next in cnexts:
                inedge[_next[0]].remove(_next[1])
                if len(inedge[_next[0]]) == 0:
                    startq.append(_next[0])
                    
        if len(results) < numCourses:
            return []
        return results

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        print
        prereqs = [[1,0],[2,0],[3,1],[3,2]]
        r = self.S.findOrder(4, prereqs)
        print r

if __name__ == "__main__":
    unittest.main()
