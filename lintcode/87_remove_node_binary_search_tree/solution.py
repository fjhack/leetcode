import unittest

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


class SerDe:
    def serialize(self, root):
        if root is None:
            return "{}"

        queue = [root]
        index = 0
        while index < len(queue):
            if queue[index] is not None:
                queue.append(queue[index].left)
                queue.append(queue[index].right)
            index += 1

        while queue[-1] is None:
            queue.pop()

        return '{%s}' % ','.join([str(node.val) if node is not None else '#'
                                  for node in queue])

    def deserialize(self, data):
        data = data.strip('\n')

        if data == '{}':
            return None

        vals = data[1:-1].split(',')
            
        root = TreeNode(int(vals[0]))
        queue = [root]
        isLeftChild = True
        index = 0

        for val in vals[1:]:
            if val is not '#':
                node = TreeNode(int(val))
                if isLeftChild:
                    queue[index].left = node
                else:
                    queue[index].right = node
                queue.append(node)

            if not isLeftChild:
                index += 1
            isLeftChild = not isLeftChild

        return root

    
class Solution:
    """
    @param root: The root of the binary search tree.
    @param value: Remove the node with given value.
    @return: The root of the binary search tree after removal.
    """
    
    def isLeaf(self, node):
        return node.left is None and node.right is None
    
    def maxSubtree(self, sroot, prev=None, depth=0):
        if self.isLeaf(sroot):
            return sroot, prev, depth
        elif sroot.right is None:
            return sroot, prev, depth
        else:
            return self.maxSubtree(sroot.right, sroot, depth+1)
    
    def minSubtree(self, sroot, prev=None, depth=0):
        if self.isLeaf(sroot):
            return sroot, prev, depth
        elif sroot.left is None:
            return sroot, prev, depth
        else:
            return self.minSubtree(sroot.left, sroot, depth+1)
    
    def findLastDir(self, prev, curr):
        if prev is None:
            return -1
        if prev.left == curr:
            return 0
        elif prev.right == curr:
            return 1
        else:
            return -1
    
    def removeNode(self, root, value):
        # write your code here
        curr, prev = root, TreeNode(-1)
        prev.right = curr
        # find the node first
        if root is None:
            return root
        
        while curr.val != value and curr is not None:            
            if value > curr.val:
                prev = curr
                curr = curr.right
            elif value < curr.val:
                prev = curr
                curr = curr.left
        
        lastdir = self.findLastDir(prev, curr)
        if lastdir == -1:
            # root is the node to be deleted and only node
            return None
            
        if self.isLeaf(curr):
            if lastdir == 0:
                prev.left = None
            elif lastdir == 1:
                prev.right = None
        elif curr.left is None:
            if lastdir == 0:
                prev.left = curr.right
            elif lastdir == 1:
                prev.right = curr.right
        elif curr.right is None:
            if lastdir == 0:
                prev.left = curr.left
            elif lastdir == 1:
                prev.right = curr.left
        else:
            maxNS, maxprev, maxD = self.maxSubtree(curr.left)
            minNS, minprev, minD = self.minSubtree(curr.right)
            if minD < maxD:
                # left subtree is deeper - we sift it up by replace max of left to current
                orig_right = curr.right
                if lastdir == 0:
                    prev.left = maxNS
                    if maxprev is not None:
                        _maxlastdir = self.findLastDir(maxprev, maxNS)
                        if _maxlastdir == 0:
                            maxprev.left = None
                        elif _maxlastdir == 1:
                            maxprev.right = None
                    prev.left.right = orig_right
                elif lastdir == 1:
                    prev.right = maxNS
                    if maxprev is not None:
                        _maxlastdir = self.findLastDir(maxprev, maxNS)
                        if _maxlastdir == 0:
                            maxprev.left = None
                        elif _maxlastdir == 1:
                            maxprev.right = None
                    prev.right.right = orig_right
            else:
                orig_left = curr.left
                orig_right = curr.right
                print curr.left.val, curr.right.val
                print minNS.left, minNS.right
                if lastdir == 0:
                    prev.left = minNS
                    if minprev is not None:
                        _minlastdir = self.findLastDir(minprev, minNS)
                        if _minlastdir == 0:
                            minprev.left = None
                        elif _minlastdir == 1:
                            minprev.right = None
                    prev.left.left = orig_left
                elif lastdir == 1:
                    print prev.val, curr.val, minNS.val, minprev.val, minD
                    prev.right = minNS
                    if minprev is not None:
                        _minlastdir = self.findLastDir(minprev, minNS)
                        if _minlastdir == 0:
                            minprev.left = None
                        elif _minlastdir == 1:
                            minprev.right = None
                    prev.right.left = orig_left
                    prev.right.right = orig_right
        return prev.right

def printBinTree(root):
    """
    :type root: TreeNode
    :rtype: List[List[int]]
    """
    Q, results = [], []
    if root is not None:
        Q.append(root)
    while len(Q) > 0:
        curr_len = len(Q)
        curr_lvl = []
        for i in xrange(curr_len):
            _p = Q.pop(0)
            if _p.left is not None:
                Q.append(_p.left)
            if _p.right is not None:
                Q.append(_p.right)
            curr_lvl.append(_p.val)
        print curr_lvl

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.SerDe = SerDe()
        self.ser = self.SerDe.serialize
        self.des = self.SerDe.deserialize

    def test_t_e1(self):
        R = TreeNode(20)
        R.left = TreeNode(1)
        R.right = TreeNode(40)
        R.right.left = TreeNode(35)
        res = self.S.removeNode(R, 20)
        print res, res.val, res.left.val, res.right

    def test_t_e2(self):
        R = TreeNode(1)
        R.right = TreeNode(2)
        R.right.right = TreeNode(3)
        R.right.right.right = TreeNode(4)
        R.right.right.right.right = TreeNode(5)
        res = self.S.removeNode(R, 3)
        print res, res.val, res.right
        print R.val, R.right.val, R.right.right.val, R.right.right.right.val

    def test_t_e2(self):
        sed = "989,982,#,972,#,947,#,920,#,903,#,894,#,881,#,866,#,864,#,842,#,841,#,796,#,726,#,647,#,613,719,593,#,#,#,590,#,558,#,554,#,538,#,512,#,504,#,468,505,467,#,#,#,456,#,413,#,331,#,330,407,320,#,#,#,312,#,306,#,301,#,274,#,251,#,235,#,231,#,222,#,181,#,93,#,83,#,73,#,64,#,62,#,60,#,28,#,21,#,20,#,-32,#,-52,#,-70,#,-87,#,-98,#,-102,#,-115,#,-116,#,-139,#,-183,#,-224,#,-241,#,-263,#,-284,#,-294,#,-296,#,-320,#,-330,#,-392,#,-398,#,-407,#,-431,#,-445,#,-460,#,-463,#,-492,#,-507,#,-518,#,-539,#,-552,#,-558,#,-559,#,-587,#,-673,#,-736,#,-757,#,-766,#,-767,#,-823,#,-830,#,-867,#,-875,#,-891,#,-905,#,-910,#,-924,#,-960,#,-985"
        R = self.des(sed)
        res = self.S.removeNode(R, -313)
        


if __name__ == "__main__":
    unittest.main()
