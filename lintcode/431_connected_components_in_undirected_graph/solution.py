import unittest

# Definition for a undirected graph node
class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []

class Solution:
    # @param {UndirectedGraphNode[]} nodes a array of undirected graph node
    # @return {int[][]} a connected set of a undirected graph
    def connectedSet(self, nodes):
        # Write your code here
            
        parents = {node.label:node.label for node in nodes}

        def find(label):
            if parents[label] == label:
                return label
            else:
                parents[label] = find(parents[label])
                return parents[label]

        def union(label1, label2):
            parent1, parent2 = find(label1), find(label2)
            if parent1 != parent2:
                parents[label2] = parent1
                for pk in parents.keys():
                    if parents[pk] == parent2:
                        parents[pk] = parent1

        for node in nodes:
            for neighbor in node.neighbors:
                union(node.label, neighbor.label)

        components = dict()

        for node in nodes:
            node_parent = find(node.label)
            if node_parent not in components:
                components[node_parent] = [node.label]
            else:
                components[node_parent].append(node.label)

        return [components[parent] for parent in components.keys()]
