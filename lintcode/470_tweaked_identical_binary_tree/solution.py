"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""
class Solution:
    """
    @param a, b, the root of binary trees.
    @return true if they are tweaked identical, or false.
    """
    def isTweakedIdentical(self, a, b):
        # Write your code here
        def isLeaf(n):
            return n.left is None and n.right is None

        if a is None and b is None:
            return True
        elif a is None or b is None:
            return False
        elif isLeaf(a) and isLeaf(b):
            return a.val == b.val
        else:
            if a.val == b.val:
                return (self.isTweakedIdentical(a.left, b.right) and self.isTweakedIdentical(a.right, b.left)) or (self.isTweakedIdentical(a.left, b.left) and self.isTweakedIdentical(a.right, b.right))
            else:
                return False
