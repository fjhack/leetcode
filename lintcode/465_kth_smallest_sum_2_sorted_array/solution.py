import heapq

# Find the kth smallest numbers in an unsorted integer array.
# Example
# Given [3, 4, 1, 2, 5], k = 3, the 3rd smallest numbers are [1, 2, 3].

class Solution:
    # @param {int[]} A an integer arrays sorted in ascending order
    # @param {int[]} B an integer arrays sorted in ascending order
    # @param {int} k an integer
    # @return {int} an integer
    def kthSmallestSum(self, A, B, k):
        # Write your code here
        if len(A) == 0 or len(B) == 0:
            return 0
        LA, LB = len(A), len(B)
        
        heap = []
        for i in xrange(min(k, LB)):
            heap.append((A[0] + B[i], 0, i))
        heapq.heapify(heap)
        
        while k > 1:
            min_element = heapq.heappop(heap)
            x, y = min_element[1], min_element[2]
            if x + 1 < LA:
                heapq.heappush(heap, (A[x + 1] + B[y], x + 1, y))
            k -= 1

        return heapq.heappop(heap)[0]

if __name__ == "__main__":
    kss = Solution().kthSmallestSum

    A = [1,7,11]
    B = [2,4,6]

    print kss(A, B, 3)
    print kss(A, B, 4)
    print kss(A, B, 8)
