class Solution:
    # @param nums {int[]} a list of integers
    # @param k {int} size of window
    # @return {int[]} the sum of element inside the window at each moving
    def winSum(self, nums, k):
        # Write your code here
        L = len(nums)
        if k == 0:
            return []
        if k > L:
            return []
        tsum = sum(nums[:k])
        results = [tsum]
        # return [sum(nums[i:i+k]) for i in xrange(len(nums)-k+1)]
        for i in xrange(1, L-k+1):
            tsum = tsum - nums[i-1] + nums[i-1+k]
            results.append(tsum)
        
        return results
