"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    # @param {ListNode} node a list node in the list
    # @param {int} x an integer
    # @return {ListNode} the inserted new list node
    def insert(self, node, x):
        # Write your code here
        newnode = ListNode(x)
        newnode.next = newnode
        if node is None:
            return newnode
            
        ptr1, ptr2 = node, node.next
        if ptr1 == ptr2:
            ptr1.next = newnode
            newnode.next = ptr1
            return newnode
        else:
            while True:
                if ptr1.val <= x and x < ptr2.val:
                    ptr1.next = newnode
                    newnode.next = ptr2
                    break
                elif ptr2.val == x:
                    newnode.next = ptr2.next
                    ptr2.next = newnode
                    break
                elif ptr1.val > ptr2.val and (x > ptr1.val or x < ptr2.val):
                    ptr1.next = newnode
                    newnode.next = ptr2
                    break
                elif ptr2 == node:
                    newnode.next = ptr2.next
                    ptr2.next = newnode
                    break
                else:
                    ptr2 = ptr2.next
                    ptr1 = ptr1.next
                    continue
            return newnode
