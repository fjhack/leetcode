class ConnectingGraph3:

    # @param {int} n
    def __init__(self, n):
        # initialize your data structure here.
        self._parents = [v for v in xrange(n+1)]
        self._comps = n

    def find(self, node):
        if self._parents[node] == node:
            return self._parents[node]
        self._parents[node] = self.find(self._parents[node])
        return self._parents[node]

    # @param {int} a, b
    # return nothing
    def connect(self, a, b):
        # Write your code here
        parent_a, parent_b = self.find(a), self.find(b)
        self._parents[parent_b] = parent_a
        if parent_a != parent_b:
            self._comps -= 1

    # @param {int} a
    # return {int} the number of connected component
    # in the graph
    def query(self):
        # Write your code here
        return self._comps
