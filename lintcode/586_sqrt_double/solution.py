# Implement double sqrt(double x) and x >= 0.
# Compute and return the square root of x.

class Solution:
    # @param {double} x a double
    # @return {double} the square root of x
    def sqrt(self, x):
        x = float(x)
        # Write your code here
        thres = 0.000000000001
        r = x / 2.0
        while (abs(r*r - x) > thres):
            r = (r + x/r) / 2
        return r

if __name__ == "__main__":
    S = Solution()
    print S.sqrt(2.0)
    print S.sqrt(3.0)
    print S.sqrt(4.0)
    print S.sqrt(5.0)
    print S.sqrt(64.0)
    print S.sqrt(0.25)
    print S.sqrt(0.000000012)

