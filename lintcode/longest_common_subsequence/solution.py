class Solution:
    """
    @param A, B: Two strings.
    @return: The length of longest common subsequence of A and B.
    """
    def longestCommonSubsequence(self, A, B):
        # write your code here
        LA, LB = len(A), len(B)
        F = [[0 for _ in xrange(LB+1)] for _ in xrange(LA+1)]
        for i in xrange(1, LA+1):
            for j in xrange(1, LB+1):
                if A[i-1] == B[j-1]:
                    F[i][j] = max(F[i-1][j], F[i][j-1], F[i-1][j-1]+1)
                else:
                    F[i][j] = max(F[i-1][j], F[i][j-1])
        return F[LA][LB]
