import unittest

# Solution 1: BFS

class Solution:
    # @param {int} n an integer
    # @param {int[][]} edges a list of undirected edges
    # @return {boolean} true if it's a valid tree, or false
    def validTree(self, n, edges):
        # Write your code here
        if len(edges) < n-1:
            return False

        e2 = sorted([sorted(e) for e in edges], key=lambda x:x[0])
        print edges
        print e2
        
        edgelist = [set() for _ in xrange(n)]
        for e in e2:
            edgelist[e[0]].add(e[1])
            edgelist[e[1]].add(e[0])

        startq = [0]
        visited = set()
        
        print edgelist
        
        while len(startq) > 0:
            currl = len(startq)
            for i in xrange(currl):
                _curr = startq.pop(0)
                print _curr, visited
                if _curr in visited:
                    continue
                else:
                    visited.add(_curr)
                startq.extend(edgelist[_curr])
            # print

        if len(visited) == n:
            return True
        else:
            return False

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0_pos(self):
        print "Test 0 - positive"
        edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
        r = self.S.validTree(5, edges)
        print r

    def test_0_neg(self):
        print "Test 0 - negative"
        edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]
        r = self.S.validTree(5, edges)
        print r

    def test_1_neg(self):
        print "Test 1 - negative"
        edges = [[0,1],[5,6],[6,7],[9,0],[3,7],[4,8],[1,8],[5,2],[5,3]]
        r = self.S.validTree(10, edges)
        print r

    def test_big_1(self):
        print "Test big dataset 1 - positive"
        
if __name__ == "__main__":
    unittest.main()
