class Solution:
    # @param values: a list of integers
    # @return: a boolean which equals to True if the first player will win
    def firstWillWin(self, values):
        # write your code here
        L = len(values)
        S = sum(values)
        DP = [0 for _ in xrange(L+1)]
        if L == 0:
            return True
        if L == 1 or L == 2:
            return True
        if L == 2:
            return (values[-1]+values[-2])*2 > S

        DP[0] = 0
        DP[1] = values[-1]
        DP[2] = values[-1] + values[-2]
        DP[3] = values[-2] + values[-3]
        
        for i in xrange(4, L+1):
            DP[i] = max(min(DP[i-3], DP[i-4])+values[-i]+values[-i+1], min(DP[i-2], DP[i-3])+values[-i])

        return DP[-1]*2 > S
