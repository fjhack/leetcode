# Definition for a undirected graph node
# class UndirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []

class Solution:
    # @param {UndirectedGraphNode[]} graph a list of undirected graph node
    # @param {dict} values a dict, <UndirectedGraphNode, (int)value>
    # @param {UndirectedGraphNode} node an Undirected graph node
    # @param {int} target an integer
    # @return {UndirectedGraphNode} a node
    def searchNode(self, graph, values, node, target):
        # Write your code here
        visited = set()
        if node is None:
            return None
        bfsq = [node]
        limit = 1
        visited.add(node)
        
        while len(bfsq) > 0:
            newlimit = 0
            for i in xrange(limit):
                curr = bfsq.pop(0)
                visited.add(curr)
                if values[curr] == target:
                    return curr
                new_neighbors = [n for n in curr.neighbors if n not in visited]
                newlimit += len(new_neighbors)

                bfsq = bfsq + new_neighbors 
                for n in new_neighbors:
                    visited.add(n)
                    
            limit = newlimit
        return None

