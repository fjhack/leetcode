import heapq
import unittest

class Solution(object):
    def trapRainWater(self, heightMap):
        """
        :type heightMap: List[List[int]]
        :rtype: int
        """
        rows = len(heightMap)
        if rows == 0:
            return 0
        cols = len(heightMap[0])
        if cols == 0:
            return 0
            
        visited = [[False for _ in xrange(cols)] for _ in xrange(rows)]
        border = []
        
        for c in xrange(cols):
            heapq.heappush(border, (heightMap[0][c], (0, c)))
            heapq.heappush(border, (heightMap[rows-1][c], (rows-1, c)))
            visited[0][c], visited[rows-1][c] = True, True
        
        for r in xrange(1, rows-1):
            heapq.heappush(border, (heightMap[r][0], (r, 0)))
            heapq.heappush(border, (heightMap[r][cols-1], (r, cols-1)))
            visited[r][0], visited[r][cols-1] = True, True
        
        neighbors = [[-1,0], [0,1], [1,0], [0,-1]]
        water_amount = 0
        while len(border) > 0:
            cell = heapq.heappop(border)
            cmin = cell[0]
            crow, ccol = cell[1][0], cell[1][1]
            
            for direction in neighbors:
                nextr, nextc = crow + direction[0], ccol + direction[1]
                if nextr >= 0 and nextr < rows and nextc >= 0 and nextc < cols and not visited[nextr][nextc]:
                    visited[nextr][nextc] = True
                    heapq.heappush(border, (max(cmin, heightMap[nextr][nextc]), (nextr, nextc)))

                    if cmin - heightMap[nextr][nextc] > 0:
                        water_amount += (cmin - heightMap[nextr][nextc])


            print "Visited:"
            for vr in visited:
                print vr
            print "Water amount:", water_amount
            
        return water_amount
                        
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_e_0(self):
        print
        heightMap = [[1,4,3,1,3,2],[3,2,1,3,2,4],[2,3,3,2,3,1]]
        r = self.S.trapRainWater(heightMap)
        print "Result:", r

    def test_e_1(self):
        print
        heightMap = [[12,13,1,12],[13,4,13,12],[13,8,10,12],[12,13,12,12],[13,13,13,13]]
        r = self.S.trapRainWater(heightMap)
        print "Result:", r
        
if __name__ == "__main__":
    unittest.main()
    
