
import sys

class Solution(object):
    
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        L = len(s)

        # Build an list represent number of characters' occurrence in t
        charmap = {chr(c): 0 for c in xrange(128)}
        for c in t:
            charmap[c] += 1
        
        left, right = 0, 0
        # counter: number of total occurrence of character in t
        head, counter = 0, len(t)
        min_len = sys.maxint

        while right < L:
            if charmap[s[right]] > 0: # number of occurrence of char at position (right) in current window is still smaller than its total occurrence in t
                counter -= 1 # since we already included this char in the window, decrease the counter
                
            charmap[s[right]] -= 1 # decrease the per-character counter, only will become negative if character right is NOT in t
            right += 1 # Keep increasing the window size by moving the right end

            while counter == 0: # all characters in t have been appeared in current window
                if right - left < min_len:
                    min_len = right - left
                    head = left

                # since we already covered all of t, it's time to move the left end of the window (trying to shrink size)
                if charmap[s[left]] == 0:
                    # Also, if the character at position (left) is in t, make sure we change the counter
                    counter += 1
                # since character at (left) will be moved out of window we also need to fix the per-character counter
                charmap[s[left]] += 1
                left += 1
        
        if min_len == sys.maxint:
            return ""
        else:
            return s[head:head+min_len]

if __name__ == "__main__":
    smw = Solution().minWindow
    s, t = "aaba", "ab"
    R = smw(s, t)
    print R
