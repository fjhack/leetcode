# """
# This is the interface that allows for creating nested lists.
# You should not implement it, or speculate about its implementation
# """
#class NestedInteger(object):
#    def __init__(self, value=None):
#        """
#        If value is not specified, initializes an empty list.
#        Otherwise initializes a single integer equal to value.
#        """
#
#    def isInteger(self):
#        """
#        @return True if this NestedInteger holds a single integer, rather than a nested list.
#        :rtype bool
#        """
#
#    def add(self, elem):
#        """
#        Set this NestedInteger to hold a nested list and adds a nested integer elem to it.
#        :rtype void
#        """
#
#    def setInteger(self, value):
#        """
#        Set this NestedInteger to hold a single integer equal to value.
#        :rtype void
#        """
#
#    def getInteger(self):
#        """
#        @return the single integer that this NestedInteger holds, if it holds a single integer
#        Return None if this NestedInteger holds a nested list
#        :rtype int
#        """
#
#    def getList(self):
#        """
#        @return the nested list that this NestedInteger holds, if it holds a nested list
#        Return None if this NestedInteger holds a single integer
#        :rtype List[NestedInteger]
#        """

class Solution(object):
    def deserialize(self, s):
        """
        :type s: str
        :rtype: NestedInteger
        """
        L = len(s)

        root = NestedInteger()
        list_stk = []

        i = 0
        cnode, intnode = None, None
        buf = ""
        while i < L:
            if s[i].isdigit():
                intnode = NestedInteger()
                cnode = intnode
                while i < L and s[i].isdigit():
                    buf += s[i]
                    i += 1
                intnode.setInteger(int(buf))
                if len(list_stk) > 0:
                    list_stk[-1].add(intnode)
                # i += 1
            elif s[i] == '-':
                buf = '-'
                i += 1
            elif s[i] == ',':
                buf = ''
                i += 1
            elif s[i] == '[':
                listnode = NestedInteger()
                cnode = listnode
                list_stk.append(listnode)
                i += 1
                buf = ""
            elif s[i] == ']':
                cnode = list_stk.pop()
                if len(list_stk) > 0:
                    list_stk[-1].add(cnode)
                i += 1
            print i, buf
            # print i, s[i], list_stk
        # print list_stk
        if len(list_stk) > 0:
            return list_stk[0]
        return cnode
