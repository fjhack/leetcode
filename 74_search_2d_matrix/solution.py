class Solution(object):
    def searchRow(self, matrix, target, rowid):
        head, tail = 0, len(matrix[rowid])-1
        while head+1 < tail:
            mid = head+(tail-head)/2
            if target == matrix[rowid][mid]:
                return True
            elif target < matrix[rowid][mid]:
                tail = mid
            else:
                head = mid
        if matrix[rowid][head] == target or matrix[rowid][tail] == target:
            return True
        else:
            return False
            
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        
        rows = len(matrix)
        if rows == 0:
            return False
        cols = len(matrix[0])
        if cols == 0:
            return False
        start_row, end_row = 0, rows-1
        while start_row+1 < end_row:
            mid_row = start_row + (end_row - start_row) / 2
            if target > matrix[mid_row][0] and target < matrix[mid_row][cols-1]:
                return self.searchRow(matrix, target, mid_row)
            elif target == matrix[mid_row][0] or target == matrix[mid_row][cols-1]:
                return True
            elif target < matrix[mid_row][0]:
                end_row = mid_row
            elif target > matrix[mid_row][cols-1]:
                start_row = mid_row
        if target > matrix[start_row][0] and target < matrix[start_row][cols-1]:
            return self.searchRow(matrix, target, start_row)
        elif target == matrix[start_row][0] or target == matrix[start_row][cols-1]:
            return True
        elif target > matrix[end_row][0] and target < matrix[end_row][cols-1]:
            return self.searchRow(matrix, target, end_row)
        elif target == matrix[end_row][0] or target == matrix[end_row][cols-1]:
            return True
        else:
            return False
