import unittest

class Solution(object):
        
    def alienOrder(self, words):
        """
        :type words: List[str]
        :rtype: str
        """
        edges = []
        wordslen = [len(w) for w in words]
        maxlen = max(wordslen)
        indegrees = dict()
        if len(words) == 1:
            return words[0]
        for i in xrange(len(words)-1):
            word1, word2 = words[i], words[i+1]
            minlen = min(wordslen[i], wordslen[i+1])
            for pos in xrange(minlen):
                c1, c2 = word1[pos], word2[pos]
                if c1 != c2 and (c1, c2) not in edges:
                    edges.append((c1, c2))
                    break
                else:
                    indegrees[c1] = set()

            endpos = pos
            pos2 = min(endpos, minlen)
            while pos2 < wordslen[i]:
                if word1[pos2] not in indegrees:
                    indegrees[word1[pos2]] = set()
                pos2 += 1

            pos2 = min(endpos, minlen)

            while pos2 < wordslen[i+1]:
                if word2[pos2] not in indegrees:
                    indegrees[word2[pos2]] = set()
                pos2 += 1

        print "Edges:", edges

        for e in edges:
            if e[0] not in indegrees:
                indegrees[e[0]] = set()
            if e[1] not in indegrees:
                indegrees[e[1]] = set(e[0])
            else:
                indegrees[e[1]].add(e[0])
        print "All in-edges:", indegrees

        starters = [v for v, inv in indegrees.iteritems() if len(inv) == 0]
        print "Start nodes:", starters
        Q = starters
        results = ""
        while len(Q) > 0:
            currv = Q.pop(0)
            results += currv
            for v, inv in indegrees.iteritems():
                if currv in inv:
                    indegrees[v].discard(currv)
                if len(indegrees[v]) == 0 and v not in results and v not in Q:
                    Q.append(v)
                # print results, Q, indegrees

        # check for loop
        for v, ind in indegrees.iteritems():
            if len(ind) > 0:
                return ""
        print results
        return results
            


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        print "\nTest 0:"
        words = ["wrt", "wrf", "er", "ett", "rftt"]
        self.S.alienOrder(words)

    def test_1(self):
        print "\nTest 1:"
        words = ["z", "x"]
        self.S.alienOrder(words)

    def test_1b(self):
        print "\nTest 1b:"
        words = ["z", "z"]
        self.S.alienOrder(words)

    def test_1c(self):
        print "\nTest 1c:"
        words = ["zy", "zx"]
        self.S.alienOrder(words)

    def test_2(self):
        print "\nTest 2:"
        words = ["z", "x", "z"]
        self.S.alienOrder(words)

    def test_3(self):
        print "\nTest 3:"
        words = ["za", "zb", "ca", "cb"]
        self.S.alienOrder(words)

    def test_4(self):
        print "\nTest 4:"
        words = ["ab", "adc"]
        self.S.alienOrder(words)

    def test_5(self):
        print "\nTest 5:"
        words = ["vlxpwiqbsg", "cpwqwqcd"]
        self.S.alienOrder(words)

    def test_6(self):
        print "\nTest 6:"
        words = ["ri","xz","qxf","jhsguaw","dztqrbwbm","dhdqfb","jdv","fcgfsilnb","ooby"]
        self.S.alienOrder(words)

    def test_7(self):
        print "\nTest 7:"
        words = ["wnlb"]
        self.S.alienOrder(words)
    
if __name__ == "__main__":
    unittest.main()
