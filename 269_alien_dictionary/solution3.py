import unittest

class Solution(object):
    def alienOrder(self, words):
        """
        :type words: List[str]
        :rtype: str
        """
        edges = set()
        indegrees = dict()
        wordslen = [len(w) for w in words]
        maxwordlen = max(wordslen)

        for i in xrange(len(words)-1):
            w1, w2 = words[i], words[i+1]
            minlen = min(len(w1), len(w2))

            for pos in xrange(minlen):
                if w1[pos] != w2[pos] and (w1[pos], w2[pos]) not in edges:
                    edges.add((w1[pos], w2[pos]))
                    if w2[pos] not in indegrees:
                        indegrees[w2[pos]] = set()
                        indegrees[w2[pos]].add(w1[pos])
                    else:
                        indegrees[w2[pos]].add(w1[pos])
                    break
                else:
                    if w2[pos] not in indegrees:
                        indegrees[w2[pos]] = set()
                    if w1[pos] not in indegrees:
                        indegrees[w1[pos]] = set()
            p2 = min(pos, minlen)

            while p2 < len(w1):
                if w1[p2] not in indegrees:
                    indegrees[w1[p2]] = set()
                p2 += 1

            p2 = min(pos, minlen)
            while p2 < len(w2):
                if w2[p2] not in indegrees:
                    indegrees[w2[p2]] = set()
                p2 += 1
                
        #print edges
        #print indegrees
        starts = [v for v, inv in indegrees.iteritems() if len(inv) == 0]
        #print starts
        Q = starts
        results = ""
        while len(Q) > 0:
            curr_head = Q.pop(0)
            results += curr_head

            for v, inv in indegrees.iteritems():
                if curr_head in inv:
                    indegrees[v].discard(curr_head)
                if len(indegrees[v]) == 0 and v not in results and v not in Q:
                    Q.append(v)


        for v, inv in indegrees.iteritems():
            if len(inv) > 0:                
                return ""
        print results
        return results
        
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        print "\nTest 0:"
        words = ["wrt", "wrf", "er", "ett", "rftt"]
        self.S.alienOrder(words)

    def test_1(self):
        print "\nTest 1:"
        words = ["z", "x"]
        self.S.alienOrder(words)

    def test_1b(self):
        print "\nTest 1b:"
        words = ["z", "z"]
        self.S.alienOrder(words)

    def test_1c(self):
        print "\nTest 1c:"
        words = ["zy", "zx"]
        self.S.alienOrder(words)

    def test_2(self):
        print "\nTest 2:"
        words = ["z", "x", "z"]
        self.S.alienOrder(words)

    def test_3(self):
        print "\nTest 3:"
        words = ["za", "zb", "ca", "cb"]
        self.S.alienOrder(words)

    def test_4(self):
        print "\nTest 4:"
        words = ["ab", "adc"]
        self.S.alienOrder(words)

    def test_5(self):
        print "\nTest 5:"
        words = ["vlxpwiqbsg", "cpwqwqcd"]
        self.S.alienOrder(words)

    def test_6(self):
        print "\nTest 6:"
        words = ["ri","xz","qxf","jhsguaw","dztqrbwbm","dhdqfb","jdv","fcgfsilnb","ooby"]
        self.S.alienOrder(words)

    def test_7(self):
        print "\nTest 7:"
        words = ["wnlb"]
        self.S.alienOrder(words)
    
if __name__ == "__main__":
    unittest.main()
