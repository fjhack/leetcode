import unittest

class Solution(object):
    def alienOrder(self, words):
        """
        :type words: List[str]
        :rtype: str
        """
        L = len(words)
        char_orders = set()
        char_deps = {}
        indegree = {}
        p, wi = 0, 0
        anywords = True
        while anywords:
            anywords = False
            while wi < L-1:
                if p < len(words[wi]) and p < len(words[wi+1]):
                    anywords = True
                    # print (words[wi][p], words[wi+1][p])
                    if words[wi][p] != words[wi+1][p]:
                        char_orders.add((words[wi][p], words[wi+1][p]))

                        if words[wi][p] not in indegree:
                            indegree[words[wi][p]] = 0

                        if words[wi][p] not in char_deps:
                            char_deps[words[wi][p]] = [words[wi+1][p]]
                            if words[wi+1][p] not in indegree:
                                indegree[words[wi+1][p]] = 1
                            else:
                                indegree[words[wi+1][p]] += 1
                        else:
                            if words[wi+1][p] not in char_deps[words[wi][p]]:
                                char_deps[words[wi][p]].append(words[wi+1][p])
                            if words[wi+1][p] not in indegree:
                                indegree[words[wi+1][p]] = 1
                            elif words[wi+1][p] not in char_deps[words[wi][p]]:
                                indegree[words[wi+1][p]] += 1

                    wi += 1
                    continue
                else:
                    while wi < L-1 and not (p < len(words[wi]) and p < len(words[wi+1])):
                        wi += 1
            p += 1
            wi = 0
            # print p, anywords

        # print char_orders
        for cd in char_deps:
            print cd, char_deps[cd]
        print indegree

        starters = [c for c, ind in indegree.items() if ind == 0]
        print starters
        if len(starters) == 0:
            return ""

        visited = {c: False for c in indegree.keys()}
        results = []
        while len(starters) > 0:
            head = starters.pop(0)
            results.append(head)
            visited[head] = True
            nexts = []
            if head in char_deps:
                nexts = char_deps[head]
            print head, nexts
            for n in nexts:
                if visited[n]:
                    return False
                else:
                    indegree[n] -= 1
                    if indegree[n] <= 0:
                        starters.append(n)
            print starters
        print results
        for n, ind in indegree.items():
            if ind > 0:
                return ""

        return ''.join(results)

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0_1(self):
        print
        ad = ["wrt",  "wrf",  "er",  "ett",  "rftt"]
        result = self.S.alienOrder(ad)
        print result
        print

    def test_2(self):
        print
        ad = ["ri","xz","qxf","jhsguaw","dztqrbwbm","dhdqfb","jdv","fcgfsilnb","ooby"]
        result = self.S.alienOrder(ad)
        print result
        print
        

if __name__ == "__main__":
    unittest.main()
