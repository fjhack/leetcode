# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def hasPathSum(self, root, target):
        """
        :type root: TreeNode
        :type target: int
        :rtype: bool
        """
        def pathsum(node, s, t):
            if node is None:
                return s == t
            if node.left is None and node.right is None:
                if s+node.val == t:
                    return True
                else:
                    return False
            elif node.left is None and node.right is not None:
                return pathsum(node.right, s+node.val, t)
            elif node.left is not None and node.right is None:
                return pathsum(node.left, s+node.val, t)
            else:
                return pathsum(node.left, s+node.val, t) or pathsum(node.right, s+node.val, t)
        if root is None:
            return False
        return pathsum(root, 0, target)
