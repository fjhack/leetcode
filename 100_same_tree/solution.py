# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isSameTree(self, p, q):
        """
        :type p: TreeNode
        :type q: TreeNode
        :rtype: bool
        """
        def walktree(np, nq):
            if np is None and nq is not None:
                return False
            elif np is not None and nq is None:
                return False
            elif np is None and nq is None:
                return True
            else:
                if np.val == nq.val:
                    return walktree(np.left, nq.left) and walktree(np.right, nq.right)
                else:
                    return False
        return walktree(p, q)
        
