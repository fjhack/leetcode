class Solution(object):
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        L = len(nums)
        head, tail = 0, L-1
        while head+1 < tail:
            mid = head + (tail - head) / 2
            if nums[mid] == target:
                return mid
            elif nums[mid] < target:
                head = mid
            else:
                tail = mid
        if target <= nums[head]:
            return head
        elif target > nums[tail]:
            return tail+1
        else:
            return tail
