import subprocess

class Solution(object):
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """

        L = len(nums)
        k2 = k
        k = k % L
        if L <= 1:
            return
        left, right = 0, L-k-1
        t = 0
        while left < right:
            t = nums[left]
            nums[left] = nums[right]
            nums[right] = t
            left, right = left+1, right-1
        left, right = L-k, L-1
        while left < right:
            t = nums[left]
            nums[left] = nums[right]
            nums[right] = t
            left, right = left+1, right-1
        left, right = 0, L-1
        while left < right:
            t = nums[left]
            nums[left] = nums[right]
            nums[right] = t
            left, right = left+1, right-1

