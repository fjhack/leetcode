class Solution(object):
    def __init__(self):
        self._cache = dict()
        
    def add(self, num1, num2):
        L1, L2 = len(num1), len(num2)
        if L1 < L2:
            L1, L2 = L2, L1
            num1, num2 = num2, num1

        L, cL = max(L1, L2), min(L1, L2)
        res = ""
        carry = 0
        for i in xrange(cL):
            p1, p2 = L1-1-i, L2-1-i
            # print p1, p2
            # print num1[p1], num2[p2]
            newdigit = int(num1[p1]) + int(num2[p2]) + carry
            carry = newdigit / 10
            newdigit = newdigit % 10
            res = str(newdigit) + res

        for i in xrange(L-cL):
            p1 = L1-cL-i-1
            newdigit = int(num1[p1]) + carry
            carry = newdigit / 10
            newdigit = newdigit % 10
            res = str(newdigit) + res
        if carry != 0:
            res = '1' + res
        return res
    
    def multiply_digit(self, num1, digit):
        res = "0"
        if digit == '0' or num1 == '0':
            return '0'
        L = len(num1)
        if digit in self._cache:
            return self._cache[digit]
        
        for i in xrange(L):
            pos = L-1-i
            _prod = str(int(num1[pos]) * int(digit)) + '0'*i
            # print _prod
            res = self.add(res, _prod)
            
        self._cache[digit] = res
        return res
    
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        L1, L2 = len(num1), len(num2)
        if L1 > L2:
            L1, L2 = L2, L1
            num1, num2 = num2, num1

        _sum = ''
        for i in xrange(L2):
            didx = L2-i-1
            _prod = self.multiply_digit(num1, num2[didx])
            if _prod != '0':
                _prod = _prod + '0'*i
            _sum = self.add(_sum, _prod)

        return _sum


if __name__ == "__main__":
    S = Solution()
    a, b = "10325", "310"
    R = S.add(a, b)
    print R

    S = Solution()
    a, b = "20", "80"
    R = S.add(a, b)
    print R

    a, b = "10325", "4"
    R = S.multiply_digit(a, b)
    print R

    a, b = "10325", "310"
    R = S.multiply_digit(a, b)
    print R

    a, b = "581852037460725882246068583352420736139988952640866685633288423526139", "2723349969536684936041476639043426870967112972397011150925040382981287990380531232"
    R = S.multiply_digit(a, b)
    print R
    
