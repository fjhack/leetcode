class Solution(object):
    def __init__(self):
        # 0=blue(SW) 1=white(NE)
        self.direction = 0

    def uniquePathsWithObstacles(self, obstacleGrid):
        """
        :type obstacleGrid: List[List[int]]
        :rtype: int
        """
        m = len(obstacleGrid)
        n = len(obstacleGrid[0])
        for i in xrange(m):
            for j in xrange(n):
                if obstacleGrid[i][j] == 1:
                    obstacleGrid[i][j] = -1
                elif obstacleGrid[i][j] == 0:
                    continue
                else:
                    # Error!
                    return -1
        if obstacleGrid[0][0] == -1:
            # error!
            return -1
        elif obstacleGrid[m-1][n-1] == -1:
            return -1
        else:
            # self.buildGrid(m, n)
            obstacleGrid[0][0] = 1

        if n == 1 and m == 1:
            i, j = 0, 0
        elif n == 1:
            i, j = 1, 0
        else:
            i, j = 0, 1

        while (i<m and j<n):
            # print i, j
            if obstacleGrid[i][j] == -1:
                pass
            elif i == 0 and j > 0:
                if obstacleGrid[i][j-1] != -1:
                    obstacleGrid[i][j] = obstacleGrid[i][j-1]
                else:
                    obstacleGrid[i][j] = 0
            elif i > 0 and j == 0:
                if obstacleGrid[i-1][j] != -1:
                    obstacleGrid[i][j] = obstacleGrid[i-1][j]
                else:
                    obstacleGrid[i][j] = 0                
            elif i > 0 and j > 0:
                if (obstacleGrid[i-1][j] != -1) and (obstacleGrid[i][j-1] != -1):
                    obstacleGrid[i][j] = obstacleGrid[i-1][j] + obstacleGrid[i][j-1]
                elif (obstacleGrid[i-1][j] == -1) and (obstacleGrid[i][j-1] != -1):
                    obstacleGrid[i][j] = obstacleGrid[i][j-1]
                elif (obstacleGrid[i-1][j] != -1) and (obstacleGrid[i][j-1] == -1):
                    obstacleGrid[i][j] = obstacleGrid[i-1][j]
                else:
                    obstacleGrid[i][j] = 0

            if self.direction == 0:
                if (i < m-1 and j > 0):
                    i += 1
                    j -= 1
                elif (i < m-1 and j == 0):
                    i += 1
                    self.direction = 1
                elif (i == m-1 and j >= 0):
                    j += 1
                    self.direction = 1
            elif self.direction == 1:
                if (i > 0 and j < n-1):
                    i -= 1
                    j += 1
                elif (i == 0 and j < n-1):
                    j += 1
                    self.direction = 0
                elif (i >= 0 and j == n-1):
                    i += 1
                    self.direction = 0
        return obstacleGrid[m-1][n-1]



if __name__ == "__main__":
    S = Solution()
    sup = S.uniquePathsWithObstacles
    
    A = [[0, 0, 0],
         [0, 1, 0],
         [0, 0, 0]]
    print sup(A)

    A = [[0, 0, 0, 0, 0],
         [0, 1, 0, 1, 0],
         [0, 0, 0, 0, 0]]

    print sup(A)

    A = [[0, 0, 1, 1, 0],
         [0, 0, 0, 1, 0],
         [0, 0, 0, 0, 0]]

    print sup(A)
