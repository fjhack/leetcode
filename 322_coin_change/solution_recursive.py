"""
16, 5,2

16 0
11 (1) (11+5)
6  (2) (6+5+5)
1  0   

"""
import sys

class Solution(object):
    def __init__(self):
        self.coins = None
        self.changes = {}

    def pickCoin(self, amount, cidx, lvl):
        if (amount - self.coins[cidx]) < 0:
            return -1
        elif (amount - self.coins[cidx]) == 0:
            return 1
        else:
            min_coins = 65536
            min_idx = -1
            _coins = []
            # print " "*lvl, cidx
            for ci in xrange(cidx, len(self.coins)):
                _c = self.pickCoin(amount - self.coins[cidx], ci, lvl+1)
                # print " "*lvl, _c, amount, self.coins[ci]
                if _c == -1:
                    continue
                else:
                    _coins.append(_c)
                    if _c < min_coins:
                        min_coins = _c
                        min_idx = ci
            # print " "*lvl, _coins
            return min_coins+1

    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        self.changes = {}
        self.coins = sorted(coins, reverse=True)

        min_result = 65536
        for ci in xrange(0, len(self.coins)):
            _c = self.pickCoin(amount, ci, 0)
            if _c == -1:
                continue
            else:
                if _c < min_result:
                    min_result = _c
        return min_result

        # return self.pickCoin(amount, 0, 0)

if __name__ == "__main__":
    sys.setrecursionlimit(32768)
    S = Solution()

    # 5
    print S.coinChange([5,2], 16)

    # 3
    print S.coinChange([5,2,1], 11)

    # 5
    print S.coinChange([5,3], 17)

    # 1
    print S.coinChange([1,2], 1)

    # 2
    print S.coinChange([1,2], 3)

    # 20
    print S.coinChange([186,419,83,408], 6249)

    print S.coinChange([3,7,405,436], 8839)
