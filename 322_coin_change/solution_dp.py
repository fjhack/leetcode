
class Solution(object):
    def __init__(self):
        self.coins = None
        self.results = None

    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        import sys
        if amount == 0:
            return 0
        self.coins = sorted(coins)
        self.results = [-1 for _ in xrange(amount+1)]
        for x in coins:
            if x <= amount:
                self.results[x] = 1

        for i in xrange(amount+1):
            if self.results[i] == -1:
                min_result = sys.maxint
                for c in self.coins:
                    if i-c < 0:
                        continue
                    elif self.results[i-c] != -1:
                        if (self.results[i-c] + 1) < min_result:
                            min_result = self.results[i-c] + 1
                if min_result != sys.maxint:
                    self.results[i] = min_result

        return self.results[amount]

if __name__ == "__main__":
    S = Solution()

    # 1
    print S.coinChange([1,2], 1)

    # 5
    print S.coinChange([5,2], 16)

    # 3
    print S.coinChange([5,2,1], 11)

    # 5
    print S.coinChange([5,3], 17)

    # 2
    print S.coinChange([1,2], 3)

    # 20
    print S.coinChange([186,419,83,408], 6249)

    # 25
    print S.coinChange([3,7,405,436], 8839)

    # -1
    print S.coinChange([2147483647], 2)

    # 8
    print S.coinChange([439,317,487,136,51,296,309,192,342,270,60], 3186)

    print S.coinChange([1], 0)
