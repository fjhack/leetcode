import unittest

class Solution(object):
    def __init__(self):
        self.board = None
        self.words = None
        self.rows, self.cols = 0, 0
        
    def getNext(self, row, col, visited):
        directions = [(0,1), (1,0), (0,-1), (-1,0)]
        return [(row+d[0], col+d[1]) for d in directions if row+d[0] >= 0 and row+d[0] < self.rows and col+d[1] >= 0 and col+d[1] < self.cols and (row+d[0], col+d[1]) not in visited]
    
    def findWordB(self, row, col, word):
        # BFS
        cword, wpos = word, 0
        Q = [(row, col)]
        visited = set(Q)
        lvlsize, nextlvl = len(Q), 0
        print
        while len(Q) > 0:
            if wpos == len(word)-1:
                # Found
                return True

            for i in xrange(lvlsize):
                curr = Q.pop(0)
                print curr, self.board[curr[0]][curr[1]]
                nextcells = self.getNext(curr[0], curr[1], visited)
                for nc in nextcells:
                    if self.board[nc[0]][nc[1]] == word[wpos+1]:
                        Q.append(nc)
                        visited.add(nc)
                        nextlvl += 1
            lvlsize = nextlvl
            nextlvl = 0
            print visited
            print wpos, word[wpos]
            print Q
            wpos += 1
        return False

    def findWord(self, row, col, word, visited=set()):
        # DFS
        #print row, col, self.board[row][col], word, visited
        if len(word) == 1 and self.board[row][col] == word[0]:
            return True
        if self.board[row][col] != word[0]:
            return False
        else:
            nextcells = self.getNext(row, col, visited|set([(row, col)]))
            for nc in nextcells:
                if self.findWord(nc[0], nc[1], word[1:], visited|set([(row, col)])):
                    return True
            return False
        
    def findWords(self, board, words):
        """
        :type board: List[List[str]]
        :type words: List[str]
        :rtype: List[str]
        """
        self.board = board
        self.words = words
        self.rows = len(board)
        self.cols = len(board[0])

        # build the dict using first char
        firstc = dict()
        for w in words:
            if w[0] not in firstc:
                firstc[w[0]] = [w]
            else:
                firstc[w[0]].append(w)

        results = []
        for row in xrange(self.rows):
            for col in xrange(self.cols):
                if self.board[row][col] not in firstc:
                    continue
                else:
                    newfound = []
                    numwords = len(firstc[self.board[row][col]])
                    i = 0
                    while i < numwords:
                        cword = firstc[self.board[row][col]][i]
                        # print cword
                        if cword in results:
                            i += 1
                            continue
                        found = self.findWord(row, col, cword)
                        if found:
                            results.append(cword)
                            firstc[self.board[row][col]].pop(i)
                            numwords -= 1
                            # break
                        else:
                            i += 1
                    # print firstc
        return results


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        board = [
            ['o','a','a','n'],
            ['e','t','a','e'],
            ['i','h','k','r'],
            ['i','f','l','v']
        ]
        words = ["oath","pea","eat","rain"]
        R = self.S.findWords(board, words)
        print R
        R = self.S.findWord(0, 0, "oath")
        print R
        R = self.S.findWord(1, 3, "eat")
        print R
        R = self.S.findWord(2, 1, "hieo")
        print R

    def test_1(self):
        board = [
            ['a']
        ]
        words = ["a","a"]
        R = self.S.findWords(board, words)
        print R

    def test_2(self):
        board = ["ab","cd"]
        words = ["ab","cb","ad","bd","ac","ca","da","bc","db","adcb","dabc","abb","acb"]
        R = self.S.findWords(board, words)
        print R
        
    def test_3(self):
        board = ["ab","aa"]
        words = ["aba","baa","bab","aaab","aaa","aaaa","aaba"]
        R = self.S.findWords(board, words)
        print R
        R1 = self.S.findWord(0, 1, "bab")
        print R1
        R1 = self.S.findWord(1, 0, "aaba")
        print R1
        

if __name__ == "__main__":
    unittest.main()
    
