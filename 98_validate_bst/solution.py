
class Solution(object):
    def helper(self, root):
        if root is None:
            return True, sys.maxint, -sys.maxint
        if root.left is None and root.right is None:
            return True, root.val, root.val
        elif root.left is None:
            r, rmin, rmax = self.helper(root.right)
            return (root.val < rmin and r), min(root.val, rmin), max(root.val, rmax)
        elif root.right is None:
            r, rmin, rmax = self.helper(root.left)
            return (rmax < root.val and r), min(root.val, rmin), max(root.val, rmax)
        else:
            lr, lrmin, lrmax = self.helper(root.left)
            rr, rrmin, rrmax = self.helper(root.right)
            return (lrmax < root.val and root.val < rrmin and lr and rr), min(lrmin, rrmin, root.val), max(lrmax, rrmax, root.val)
    
    def isValidBST(self, root):
        r, rmin, rmax = self.helper(root)
        return r
