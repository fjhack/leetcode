# Below is the interface for Iterator, which is already defined for you.
#
# class Iterator(object):
#     def __init__(self, nums):
#         """
#         Initializes an iterator object to the beginning of a list.
#         :type nums: List[int]
#         """
#
#     def hasNext(self):
#         """
#         Returns true if the iteration has more elements.
#         :rtype: bool
#         """
#
#     def next(self):
#         """
#         Returns the next element in the iteration.
#         :rtype: int
#         """

class PeekingIterator(object):
    def __init__(self, iterator):
        """
        Initialize your data structure here.
        :type iterator: Iterator
        """
        self._iter = iterator
        self._halt = False
        self._halted = None
        

    def peek(self):
        """
        Returns the next element in the iteration without advancing the iterator.
        :rtype: int
        """
        if self._halt:
            return self._halted
        else:
            self._halt = True
            self._halted = self.iter.next()
            return self._halted

    def next(self):
        """
        :rtype: int
        """
        if self._halt:
            self._halt = False
            R = self._halted
            self._halted = None
            return R
        else:
            return self._iter.next()

    def hasNext(self):
        """
        :rtype: bool
        """
        if not self._halt:
            return self._iter.hasNext()
        else:
            if not self._iter.hasNext():
                return True
            return self._iter.hasNext()

# Your PeekingIterator object will be instantiated and called as such:
# iter = PeekingIterator(Iterator(nums))
# while iter.hasNext():
#     val = iter.peek()   # Get the next element but not advance the iterator.
#     iter.next()         # Should return the same value as [val].
