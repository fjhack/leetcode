import unittest

class Solution(object):
    def reverseWords(self, s):
        """
        :type s: a list of 1 length strings (List[str])
        :rtype: nothing
        """
        L = len(s)
        spaces = [i for i in xrange(L) if s[i] == ' ']
        if len(spaces) == 0:
            return
        spaces.append(L)
        startpos = 0
        for i in spaces:
            j = startpos
            k = i - 1
            while j < k:
                t = s[j]
                s[j] = s[k]
                s[k] = t
                j += 1
                k -= 1
            startpos = i+1
        j = 0
        k = L-1
        while j < k:
            t = s[j]
            s[j] = s[k]
            s[k] = t
            j += 1
            k -= 1
        

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        s1 = "the sky is blue"
        ls1 = [c for c in s1]
        print ls1
        self.S.reverseWords(ls1)

if __name__ == "__main__":
    unittest.main()
