import unittest

class Vector2D(object):
    def __init__(self, vec2d):
        """
        Initialize your data structure here.
        :type vec2d: List[List[int]]
        """
        self.vec2d = vec2d
        self.i, self.j = 0, 0
        
    def next(self):
        if not self.hasNext():
            return None

        while self.i < len(self.vec2d) and self.j == len(self.vec2d[self.i]):
            self.j = 0
            self.i += 1

        retval = self.vec2d[self.i][self.j]
        self.j += 1
        while self.i < len(self.vec2d) and self.j == len(self.vec2d[self.i]):
            self.j = 0
            self.i += 1
        return retval

    def hasNext(self):
        return self.i < (len(self.vec2d))
    
    def remove(self):
        del self.vec2d[self.i][self.j]
        while self.i < len(self.vec2d) and self.j == len(self.vec2d[self.i]):
            self.j = 0
            self.i += 1


class testSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_0(self):
        V = [[1,2,3,4],[5,6],[7,8,9]]
        S0 = Vector2D(V)
        print S0.next()
        print S0.next()
        print S0.remove()
        print S0.remove()
        print S0.next()

        print S0.next()
        print S0.next()
        print S0.next()
        print S0.next()
        print S0.next()
        
    def test_1(self):
        V = [[1,2,3], [], [4,5]]
        S0 = Vector2D(V)
        print S0.next()
        print S0.next()
        print S0.remove()
        print S0.remove()
        print S0.next()
        print S0.next()

    def test_2(self):
        V = [[1,2,3], [], [], [], [4,5]]
        S0 = Vector2D(V)
        print S0.next()
        print S0.next()
        print S0.remove()
        print S0.remove()
        print S0.next()
        print S0.next()

    def test_3(self):
        V = [[], [], [1,2,3], [], [4,5]]
        S0 = Vector2D(V)
        print S0.next()
        print S0.next()
        print S0.remove()
        print S0.remove()
        print S0.next()
        print S0.next()

if __name__ == "__main__":
    unittest.main()
    
