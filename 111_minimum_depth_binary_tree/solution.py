"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of binary tree.
    @return: An integer
    """ 
    def minDepth(self, root):
        # write your code here
        if root is None:
            return 0
        else:
            lmin, rmin = self.minDepth(root.left), self.minDepth(root.right)
            if lmin == 0 and rmin == 0:
                return 1
            elif lmin == 0 and rmin != 0:
                return rmin + 1
            elif lmin != 0 and rmin == 0:
                return lmin + 1
            else:
                return min(lmin, rmin) + 1
