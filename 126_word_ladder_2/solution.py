import unittest

class Solution(object):
    def findLadders0(self, beginWord, endWord, wordList):
        lbegin, lend = len(beginWord), len(endWord)
        if lbegin != lend:
            return 0
        words = set(wordList)
        words.add(beginWord)
        results = []
        if endWord not in wordList:
            return []
        step, clen = 1, 1
        nextlen = 0
        Q = [(beginWord, [beginWord])]
        found = False
        while len(Q) > 0 and not found:
            step += 1
            if step == 4:
                print Q
            for _ in xrange(clen):
                item = Q.pop(0)
                for pos in xrange(len(item[0])):
                    p1, p2 = item[0][:pos], item[0][pos+1:]
                    for c in "abcdefghijklmnopqrstuvwxyz":
                        newword = p1 + c + p2
                        if newword == item[0]:
                            continue
                        elif newword == endWord:
                            found = True
                            results.append(item[1] + [newword])
                        elif newword in words and newword not in item[1]:
                            Q.append((newword, item[1] + [newword]))
                            # words.remove(newword)
                            nextlen += 1
            clen = nextlen
            nextlen = 0
        return results
    
    def findLadders(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: List[List[str]]
        """
        lbegin, lend = len(beginWord), len(endWord)
        if lbegin != lend:
            return 0
        words = set(wordList)
        words_o = set(wordList)
        words.add(beginWord)
        words_o.add(beginWord)
        results = []
        if endWord not in wordList:
            return []
        step, clen = 1, 1
        nextlen = 0
        Q = [beginWord]
        Td = dict()
        found = False
        while len(Q) > 0 and not found:
            step += 1
            # print Q
            for _ in xrange(clen):
                item = Q.pop(0)
                for pos in xrange(len(item)):
                    p1, p2 = item[:pos], item[pos+1:]
                    for c in "abcdefghijklmnopqrstuvwxyz":
                        newword = p1 + c + p2
                        if newword == item:
                            continue
                        elif newword == endWord:
                            found = True
                            if newword in Td:
                                Td[newword].append(item)
                            else:
                                Td[newword] = [item]
                            
                        elif newword in words:
                            if newword in Td:
                                Td[newword].append(item)
                            else:
                                Td[newword] = [item]
                            words.discard(newword)
                            Q.append(newword)
                            nextlen += 1
                        elif newword in words_o:
                            if newword in Td:
                                Td[newword].append(item)
                            else:
                                Td[newword] = [item]
                            
            clen = nextlen
            nextlen = 0
        print Td, found
        if not found:
            return []
        R2 = []
        trail = [endWord]
        Q2 = [(endWord, (endWord,))]
        clen, nextlen = 1, 0
        step2 = 1
        reached = False
        lvlset = set()
        while len(Q2) > 0 and not reached:
            step2 += 1
            lvlset = set()
            for _ in xrange(clen):
                item = Q2.pop(0)
                for prev in Td[item[0]]:
                    if prev == beginWord:
                        reached = True
                        R2.append((prev,) + item[1])
                    # if prev not in lvlset:
                    #     lvlset.add(prev)
                    newitem = (prev, (prev,) + item[1])
                    Q2.append(newitem)
                    nextlen += 1
            clen = nextlen
            nextlen = 0
        return list(set(R2))


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        print "\nTest 0:"
        beginWord, endWord = "hit", "cog"
        wordList = ["hot","dot","dog","lot","log","cog"]
        R = self.S.findLadders(beginWord, endWord, wordList)
        R2 = self.S.findLadders0(beginWord, endWord, wordList)
        print "Result:\n", "\n".join([str(r) for r in R])
        print "Result (comparison):\n", "\n".join([str(r) for r in R2])

    def test_1(self):
        print "\nTest 1:"
        beginWord, endWord = "hot", "dog"
        wordList = ["hot", "dog"]
        R = self.S.findLadders(beginWord, endWord, wordList)
        R2 = self.S.findLadders0(beginWord, endWord, wordList)
        print "Result:\n", "\n".join([str(r) for r in R])
        print "Result (comparison):\n", "\n".join([str(r) for r in R2])

    def test_2(self):
        print "\nTest 2:"
        beginWord, endWord = "red", "tax"
        wordList = ["ted","tex","red","tax","tad","den","rex","pee"]
        R = self.S.findLadders(beginWord, endWord, wordList)
        R2 = self.S.findLadders0(beginWord, endWord, wordList)
        print "Result:\n", "\n".join([str(r) for r in R])
        print "Result (comparison):\n", "\n".join([str(r) for r in R2])
        
    
if __name__ == "__main__":
    unittest.main()


