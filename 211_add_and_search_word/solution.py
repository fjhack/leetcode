class WordDictionary(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.cmap = {}
        
    def addWord(self, word):
        """
        Adds a word into the data structure.
        :type word: str
        :rtype: void
        """
        if len(word) == 0:
            self.cmap['TERM'] = True
            return
        c1 = word[0]

        if c1 not in self.cmap:
            self.cmap[c1] = WordDictionary()
        self.cmap[c1].addWord(word[1:])

    def search(self, word):
        """
        Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter.
        :type word: str
        :rtype: bool
        """

        if len(word) == 0:
            return 'TERM' in self.cmap and self.cmap['TERM']
        c1 = word[0]
        if c1 == '.':
            matchany, matchterm = False, False
            for k in self.cmap:
                if k == 'TERM':
                    if len(word) == 1:
                        matchterm = True
                    continue
                if k not in self.cmap:
                    continue
                else:
                    matchany = True
                    res = self.cmap[k].search(word[1:])
                    if res:
                        return True
            if matchterm and matchany:
                return True
            return False

        if c1 not in self.cmap:
            return False
        else:
            return self.cmap[c1].search(word[1:])

        
    def __str__(self, depth=0):
        buf = ""
        for prefixkey in self.cmap:
            if prefixkey == 'TERM':
                buf += " "*depth + 'TERM:' + str(self.cmap['TERM'])
                continue
            if self.cmap[prefixkey] is None:
                continue
            else:
                # buf += "\n{0}{1}: {2}{3}\n".format(" "*depth, prefixkey, self.cmap.__repr__(), self.cmap[prefixkey].__str__(depth+1))
                buf += "\n{0}{1}: {2}\n".format(" "*depth, prefixkey, self.cmap[prefixkey].__str__(depth+1))
        return buf

if __name__ == "__main__":
    wd = WordDictionary()
    wd.addWord('ac')
    wd.addWord('bac')
    wd.addWord('ad')
    wd.addWord('adb')
    wd.addWord('d')
    wd.addWord('bdaa')

    print wd.cmap
    print wd

    print wd.search('ac')
    print wd.search('acd')
    print wd.search('b')
    print wd.search('d')
    print wd.search('.ac')
    print wd.search('ba.')
    print wd.search('ac.')
