import unittest

class Solution(object):
    def intersect(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        nums1 = sorted(nums1)
        nums2 = sorted(nums2)
        L1, L2 = len(nums1), len(nums2)
        p1, p2 = 0, 0
        results = []
        while p1 < L1 and p2 < L2:
            if nums1[p1] < nums2[p2]:
                p1 += 1
            elif nums1[p1] > nums2[p2]:
                p2 += 1
            else:
                results.append(nums1[p1])
                p1, p2 = p1+1, p2+1
        return results

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        n1, n2 = [1,2,2,1], [2,2]
        R = self.S.intersect(n1, n2)
        print R

if __name__ == "__main__":
    unittest.main()
