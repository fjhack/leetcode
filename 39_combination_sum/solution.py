import unittest

class Solution(object):
    def __init__(self):
        self.results = []
        
    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        self.results = []
        self.csi1(candidates, target, [])
        return self.results

    def csi1(self, candidates, target, rl):
        found = False
        for n in candidates:
            r = target - n
            # print n, r
            nc = [c for c in candidates if (c >= n and c <= r)]
            # print nc
            nt = r
            if r == 0:
                found = True
                self.results.append(rl+[n])
            else:
                self.csi1(nc, nt, rl + [n])

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        pass

    def test_a_1(self):
        A = [2,3,6,7]
        t = 7
        print self.S.combinationSum(A, t)

    def test_a_2(self):
        S = Solution()
        A = [2,3,6,7]
        t = 8
        print self.S.combinationSum(A, t)

    def test_a_3(self):
        S = Solution()
        A = [2,3,6,7]
        t = 9
        print self.S.combinationSum(A, t)


if __name__ == "__main__":
    unittest.main()
