class fileWordCount(object):
    def __init__(self):
        self._BUFSIZE = 1024
        pass

    def wordcount(self, inputfile):
        wcount = 0
        lastspace = True
        with open(inputfile, 'r') as infile:
            buf = infile.read(self._BUFSIZE)
            while len(buf) > 0:
                if lastspace:
                    wcount += len(buf.split())
                else:
                    wcount += len(buf.split()) - 1
                if buf[-1] == ' ' or buf[-1] == '\n':
                    lastspace = True
                else:
                    lastspace = False
                buf = infile.read(self._BUFSIZE)
        return wcount

if __name__ == "__main__":
    counter = fileWordCount()
    R = counter.wordcount('file1.txt')
    print R
