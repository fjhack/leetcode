import unittest

class Solution(object):
    def jump(self, board, pos):
        L = len(board)
        queue = [pos]
        while len(queue) > 0:
            currpos = queue.pop(0)
            if board[currpos] == 0:
                return True
            offsets = [board[currpos]*d for d in [-1, 1]]
            for o in offsets:
                if currpos+o >= 0 and currpos+o < L:
                    queue.append(currpos+o)
        return False

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        print
        B = [2, 1, 0, 3, 1, 1, 5]
        pos = 5
        R = self.S.jump(B, pos)
        print R
        print
        
if __name__ == "__main__":
    unittest.main()
