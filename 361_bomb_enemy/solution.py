import unittest

class Solution0(object):
    def maxKilledEnemies(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        
        rows = len(grid)
        if rows == 0:
            return 0
        cols = len(grid[0])

        DP1 = [[0 for _ in xrange(cols)] for _ in xrange(rows)]
        DP2 = [[0 for _ in xrange(cols)] for _ in xrange(rows)]
        DP3 = [[0 for _ in xrange(cols)] for _ in xrange(rows)]
        DP4 = [[0 for _ in xrange(cols)] for _ in xrange(rows)]

        for i in xrange(rows):
            ecount = 0
            for j in xrange(cols-1, -1, -1):
                if grid[i][j] == 'E':
                    ecount += 1
                elif grid[i][j] == 'W':
                    ecount = 0
                else:
                    DP1[i][j] = ecount

        for i in xrange(rows):
            ecount = 0
            for j in xrange(cols):
                if grid[i][j] == 'E':
                    ecount += 1
                elif grid[i][j] == 'W':
                    ecount = 0
                else:
                    DP2[i][j] = ecount

        for j in xrange(cols):
            ecount = 0
            for i in xrange(rows-1, -1, -1):
                if grid[i][j] == 'E':
                    ecount += 1
                elif grid[i][j] == 'W':
                    ecount = 0
                else:
                    DP3[i][j] = ecount

        for j in xrange(cols):
            ecount = 0
            for i in xrange(rows):
                if grid[i][j] == 'E':
                    ecount += 1
                elif grid[i][j] == 'W':
                    ecount = 0
                else:
                    DP4[i][j] = ecount

        # Debug
        print DP1
        print DP2
        print DP3
        print DP4

        maxhit = 0
        for i in xrange(rows):
            for j in xrange(cols):
                curr_hit = DP1[i][j] + DP2[i][j] + DP3[i][j] + DP4[i][j]
                if curr_hit > maxhit:
                    maxhit = curr_hit

        return maxhit

class Solution(object):
    def maxKilledEnemies(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        
        rows = len(grid)
        if rows == 0:
            return 0
        cols = len(grid[0])

        DP1 = [[0 for _ in xrange(cols)] for _ in xrange(rows)]
        DP2 = [[0 for _ in xrange(cols)] for _ in xrange(rows)]

        # left to right, bottom to top
        colhits1 = [0 for _ in xrange(cols)]
        for i in xrange(rows):
            ecount = 0
            for j in xrange(cols-1, -1, -1):
                if grid[i][j] == 'E':
                    ecount += 1
                    colhits1[j] += 1
                elif grid[i][j] == 'W':
                    ecount = 0
                    colhits1[j] = 0
                else:
                    DP1[i][j] = ecount + colhits1[j]

        # right to left, top to bottom
        colhits2 = [0 for _ in xrange(cols)]
        for i in xrange(rows-1, -1, -1):
            ecount = 0
            for j in xrange(cols):
                if grid[i][j] == 'E':
                    ecount += 1
                    colhits2[j] += 1
                elif grid[i][j] == 'W':
                    ecount = 0
                    colhits2[j] = 0
                else:
                    DP2[i][j] = ecount + colhits2[j]

        # Debug
        print DP1
        print DP2

        maxhit = 0
        for i in xrange(rows):
            for j in xrange(cols):
                curr_hit = DP1[i][j] + DP2[i][j]
                if curr_hit > maxhit:
                    maxhit = curr_hit

        return maxhit

class Solution2(object):
    def maxKilledEnemies(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        
        rows = len(grid)
        if rows == 0:
            return 0
        cols = len(grid[0])
        maxhit = 0
        rowhits, colhits = 0, [0 for _ in xrange(cols)]
        for i in xrange(rows):
            for j in xrange(cols):
                if j == 0 or grid[i][j-1] == 'W':
                    rowhits = 0
                    k = j
                    while k < cols and grid[i][k] != 'W':
                        if grid[i][k] == 'E':
                            rowhits += 1
                        k += 1
                if i == 0 or grid[i-1][j] == 'W':
                    colhits[j] = 0
                    k = i
                    while k < rows and grid[k][j] != 'W':
                        if grid[k][j] == 'E':
                            colhits[j] += 1
                        k += 1
                print rowhits, colhits
                if grid[i][j] == '0':
                    maxhit = max(maxhit, rowhits + colhits[j])
        return maxhit
    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S0 = Solution0()
        self.S = Solution()
        self.S2 = Solution2()
        
    def test_0(self):
        grid = [['0', 'E', '0', '0'],
                ['E', '0', 'W', 'E'],
                ['0', 'E', '0', '0']]
        R0 = self.S0.maxKilledEnemies(grid)
        R = self.S.maxKilledEnemies(grid)
        R2 = self.S2.maxKilledEnemies(grid)
        print R0
        print R
        print R2
        
if __name__ == "__main__":
    unittest.main()
