class Solution(object):
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        def generate(n, leftp, rightp, trail, results):
            if n == leftp or n == rightp:
                if leftp == rightp:
                    results.append(trail)
                    return
                elif n == leftp and n > rightp:
                    generate(n, leftp, rightp+1, trail + ')', results)
                else:
                    return
            else:
                if leftp == rightp:
                    # can only append left paren now
                    generate(n, leftp+1, rightp, trail + '(', results)
                else:
                    generate(n, leftp, rightp+1, trail + ')', results)
                    generate(n, leftp+1, rightp, trail + '(', results)

        results = []
        generate(n, 0, 0, "", results)
        return results

if __name__ == "__main__":
    S = Solution()
    SgP = S.generateParenthesis
    print SgP(3)
