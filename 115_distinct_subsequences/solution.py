class Solution(object):
    def numDistinct(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: int
        """
        Ls, Lt = len(s), len(t)
        if Lt == 0:
            return 1
        F = [[0 for _ in xrange(Lt+1)] for _ in xrange(Ls+1)]
        
        for i in xrange(Ls+1):
            F[i][0] = 1
        
        for i in xrange(1, Ls+1):
            for j in xrange(1, min(i+1, Lt+1)):
                if s[i-1] == t[j-1]:
                    F[i][j] = F[i-1][j] + F[i-1][j-1]
                else:
                    F[i][j] = F[i-1][j]
                    
        return F[Ls][Lt]
