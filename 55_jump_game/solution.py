class Solution(object):
    # DP Solution - will exceed time limit in Leetcode
    def canJump(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        L = len(nums)
        R = [False for _ in xrange(L)]
        R[0] = True
        for i in xrange(L):
            if not R[i]:
                continue
            for j in xrange(i+nums[i], i-1, -1):
                if j == L-1:
                    return True
                if j < L:
                    R[j] = True
        return False
