
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Solution(object):
    def symm(self, L, R):
        if L is None and R is not None:
            return False
        if L is not None and R is None:
            return False
        if L is None and R is None:
            return True
        if L.val != R.val:
            return False
        elif L.val == R.val:
            return self.symm(L.left, R.right) and self.symm(L.right, R.left)
            
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if root is None:
            return True
        else:
            return self.symm(root.left, root.right)
