import time

class Solution(object):
    def __init__(self):
        self._tr = []
        self._allresults = []
        
    def make_combinations_BFS(self, numbers, k):
        # print self._tr
        for _r in self._tr:
            if len(_r) == k:
                return
            elif len(_r) > k:
                return
            else:
                for n in numbers:
                    if n < _r[0]:
                        # print [n] + _r
                        if ([n] + _r) not in self._tr:
                            self._tr.append([n] + _r)
        self.make_combinations_BFS(numbers, k)
            
        
    def combineBFS(self, n, k):
        numbers = range(1, n+1)
        self._tr = [[n] for n in numbers]
        self.make_combinations_BFS(numbers, k)
        # print self._tr
        _r = [_r for _r in self._tr if len(_r) == k]
        # print _r
        return _r

    def make_combinations_DFS(self, n, k, trail):
        if len(trail) == k:
            self._allresults.append(trail)
            return
        else:
            if len(trail) == 0:
                last = 0
            else:
                last = trail[-1]
            # print len(trail)*' ', 'L45\t',last, trail
            for i in range(last+1, n+1):
                # print len(trail)*' ', 'L47\t', trail, trail + [i]
                self.make_combinations_DFS(n, k, trail + [i])
    
    def combineDFS(self, n, k):
        self._allresults = []
        self.make_combinations_DFS(n, k, [])
        return self._allresults

    def combine_old(self, n, k):
        return self.combineDFS(n, k)
        
    def combine(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: List[List[int]]
        """
        if k == 1:
            return [[i] for i in range(1, n+1)]
        if n == k:
            return [[i for i in range(1, n+1)]]
        return [i + [n] for i in self.combine(n-1,k-1)] + [i for i in self.combine(n-1, k)]
    
if __name__ == "__main__":
    S = Solution()
    Sc = S.combine
    ScD = S.combineDFS
    start_time = time.time()
    Sc(20,16)
    elapsed_lc = time.time() - start_time
    print "List comprehension time:", elapsed_lc
    
    start_time = time.time()
    ScD(20,16)
    elapsed_dfs = time.time() - start_time
    print "DFS time:", elapsed_dfs
    
