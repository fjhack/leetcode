class Solution(object):
    def __init__(self):
        self.M = None
        self.V = None
        self.numrows, self.numcols = 0, 0
        
    def search_from(self, row_idx, col_idx, prefix, targetword):
        """
        DFS on the grid.
        
        I prefer DFS here since the logic for checking visited cell is easier
        in DFS. For BFS we need to check the visited cell for each path.
        """
        if prefix == targetword:
            return True
        elif len(prefix) == len(targetword):
            return False
        else:
            directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]
            neighbors = [(row_idx+d[0], col_idx+d[1]) for d in directions
                         if ((row_idx+d[0] >= 0 and row_idx+d[0] < self.numrows)
                             and (col_idx+d[1] >= 0 and col_idx+d[1] < self.numcols)
                             and not self.V[row_idx+d[0]][col_idx+d[1]])]

            for n in neighbors:
                if targetword.startswith(prefix+self.M[n[0]][n[1]]):
                    self.V[n[0]][n[1]] = True
                    result = self.search_from(n[0], n[1], prefix+self.M[n[0]][n[1]], targetword)
                    if result:
                        return result
                    self.V[n[0]][n[1]] = False
            return False

    def exist(self, board, word):
        """
        Main method
        
        Iterating through each cell in grid, start DFS if the first character matches.
        """
        self.numrows, self.numcols = len(board), len(board[0])
        self.M = board
        self.V = [[False for _ in xrange(self.numcols)] for _ in xrange(self.numrows)]
        for row_idx in xrange(self.numrows):
            for col_idx in xrange(self.numcols):
                if self.M[row_idx][col_idx] == word[0]:
                    self.V[row_idx][col_idx] = True
                    found = self.search_from(row_idx, col_idx, word[0], word)
                    self.V[row_idx][col_idx] = False
                    if found:
                        return found
        
        return False

class SolutionBFS(object):
    """
    For this problem BFS won't be acceptable. When searching for strings which
    could have lots of common prefix its time complexity would be unacceptable.

    Check test case 3 below.
    """
    def path_to_word(self, path):
        return ''.join([self.M[c[0]][c[1]] for c in path])

    def get_neighbors(self, row_idx, col_idx):
        directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]
        neighbors = [(row_idx+d[0], col_idx+d[1]) for d in directions
                     if ((row_idx+d[0] >= 0 and row_idx+d[0] < self.numrows)
                         and (col_idx+d[1] >= 0 and col_idx+d[1] < self.numcols)
                         and not self.V[row_idx+d[0]][col_idx+d[1]])]
        return neighbors

    def search_bfs(self, row_idx, col_idx, targetword):
        queue = [[(row_idx, col_idx)]]

        while len(queue) > 0:
            # print queue
            _qhead = queue.pop(0)
            _prefix = self.path_to_word(_qhead)
            # print _prefix
            print len(_prefix)

            if _prefix == targetword:
                return True
            elif len(_prefix) == len(targetword):
                continue
            neighbors = self.get_neighbors(_qhead[-1][0], _qhead[-1][1])
            for n in neighbors:
                newpath = _qhead + [(n)]
                if n not in _qhead and targetword.startswith(self.path_to_word(newpath)):
                    queue.append(_qhead + [(n)])

        return False
    
    def exist(self, board, word):
        """
        Main method
        
        Iterating through each cell in grid, start BFS if the first character matches.
        """
        self.numrows, self.numcols = len(board), len(board[0])
        self.M = board
        self.V = [[False for _ in xrange(self.numcols)] for _ in xrange(self.numrows)]
        for row_idx in xrange(self.numrows):
            for col_idx in xrange(self.numcols):
                if self.M[row_idx][col_idx] == word[0]:
                    found = self.search_bfs(row_idx, col_idx, word)
                    if found:
                        return found
        
        return False

if __name__ == "__main__":
    S = SolutionBFS()
    board = ["ABCE","SFCS","ADEE"]
    word = "ABCCED"
    R = S.exist(board, word)
    print R

    board = ["baabab","abaaaa","abaaab","ababba","aabbab","aabbba","aabaab"]
    word = "aabbbbabbaababaaaabababbaaba"
    R = S.exist(board, word)
    print R

    board = ["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaab"]
    word = "baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    R = S.exist(board, word)
    print R
