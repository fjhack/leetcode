class Solution(object):
    def valid(self, v, nums, m):
        segs, csum = 1, 0
        for n in nums:
            csum += n
            if csum > v:
                csum = n
                segs += 1
                if segs > m:
                    return False
        return True
    
    def splitArray(self, nums, m):
        """
        :type nums: List[int]
        :type m: int
        :rtype: int
        """
        singlemax, maxsum = max(nums), sum(nums)
        if m == 1:
            return maxsum
        
        left, right = singlemax, maxsum
        
        while left <= right:
            mid = left + (right - left) / 2
            r = self.valid(mid, nums, m)
            if r:
                right = mid - 1
            else:
                left = mid + 1
        return left


if __name__ == "__main__":
    S = Solution()
    nums = [7,2,5,10,8]
    m = 2

    R = S.splitArray(nums, m)
    print R
