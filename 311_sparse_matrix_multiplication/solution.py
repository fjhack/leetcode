import unittest

class Solution(object):
    # Brute Force
    def multiply(self, A, B):
        """
        :type A: List[List[int]]
        :type B: List[List[int]]
        :rtype: List[List[int]]
        """
        numRowsA, numRowsB = len(A), len(B)
        numColsA, numColsB = len(A[0]), len(B[0])
        resultM = [[0 for _ in xrange(numColsB)] for _ in xrange(numRowsA)]

        for i in xrange(numRowsA):
            for j in xrange(numColsB):
                for k in xrange(numColsA):
                    if A[i][k] == 0 or B[k][j] == 0:
                        continue
                    resultM[i][j] += A[i][k] * B[k][j]

        return resultM


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.mmulti = self.S.multiply

    # def test_t(self):
    #     A = []
    #     B = []
    #     RM = self.mmulti(A, B)
    #     for R in RM:
    #         print R

    def test_basic_0(self):
        A = [[ 1, 0, 0],
            [-1, 0, 3]]
        B = [[ 7, 0, 0 ],
             [ 0, 0, 0 ],
             [ 0, 0, 1 ]]
        RM = self.mmulti(A, B)
        for R in RM:
            print R

if __name__ == "__main__":
    unittest.main()
