import unittest

class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        def generate(nums, curr_num, results):
            if curr_num == len(nums):
                return
            else:
                L = len(results)
                print results
                for i in xrange(L):
                    print results[i], nums[len(nums) - curr_num - 1]
                    results.append([nums[len(nums) - curr_num - 1]] + results[i])
                generate(nums, curr_num+1, results)
        results = [[]]
        generate(nums, 0, results)
        return results

class Solution2(object):
    """
    @param S: The set of numbers.
    @return: A list of lists. See example.
    """
    def subsets(self, S):
        # write your code here
        r = [[]]
        S = sorted(S)
        L = len(S)
        for i in S:
            r = [s + [i] for s in r] + r
        return r

if __name__ == "__main__":
    S = Solution2()
    Ss = S.subsets
    print Ss([1,2,3])
