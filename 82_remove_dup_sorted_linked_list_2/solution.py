# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        dhead = ListNode(-1)
        dhead.next = head
        curr, lookahd = head, head
        last = dhead
        while curr is not None:
            lookahd = lookahd.next
            if lookahd is not None and curr.val != lookahd.val:
                last = curr
                curr = curr.next
                continue
            elif lookahd is None:
                curr = curr.next
                continue
            else:
                while lookahd is not None and curr.val == lookahd.val:
                    lookahd = lookahd.next
                last.next = lookahd
                curr = lookahd
        return dhead.next
