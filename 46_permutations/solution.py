import unittest

class Solution(object):

    def permuter(self, results, nums, trail):
        if len(trail) == len(nums):
            results.append([nums[p] for p in trail])
            return
        else:
            for i in xrange(len(nums)):
                if i not in trail:
                    self.permuter(results, nums, trail + [i])

    def permute(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        results = []
        self.permuter(results, nums, [])
        return results
    

if __name__ == "__main__":
    Sp = Solution().permute
    A = [1,5,3]
    print Sp(A)

    A = [1,1,2]
    print Sp(A)
