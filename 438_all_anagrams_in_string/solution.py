class Solution(object):
    def findAnagrams(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: List[int]
        """
        def dictEqual(d1, d2):
            if len(d1) != len(d2):
                return False
            
            k1, k2 = ''.join(sorted(d1.keys())), ''.join(sorted(d2.keys()))
            print k1, k2
            if k1 != k2:
                return False
                
            for k1 in d1:
                if d2[k1] != d1[k1]:
                    return False
            return True
        
        dp, Lp = dict(), len(p)
        for c in p:
            if c in dp:
                dp[c] += 1
            else:
                dp[c] = 1
        
        dwindow = dict()
        results = []
        if len(s) < Lp:
            return results
        
        for i in xrange(Lp):
            if s[i] in dwindow:
                dwindow[s[i]] += 1
            else:
                dwindow[s[i]] = 1
            
        anagram = dictEqual(dp, dwindow)
        print anagram
        if anagram:
            results.append(0)
        
        for i in xrange(1, len(s)-Lp):
            # print s[i:i+Lp]
            if s[i-1] == s[i+Lp-1]:
                if dictEqual(dp, dwindow):
                    results.append(i)
            else:
                if s[i+Lp-1] in dwindow:
                    dwindow[s[i+Lp-1]] += 1
                else:
                    dwindow[s[i+Lp-1]] = 1
                if s[i-1] in dwindow:
                    dwindow[s[i-1]] -= 1
                    if dwindow[s[i-1]] == 0:
                        del dwindow[s[i-1]]
                if dictEqual(dp, dwindow):
                    results.append(i)

            # print dwindow
        return results

if __name__ == "__main__":
    S = Solution()
    s, p = "cbaebabacd", "abc"
    R = S.findAnagrams(s, p)
    print R

    s, p = "aa", "bb"
    R = S.findAnagrams(s, p)
    print R
