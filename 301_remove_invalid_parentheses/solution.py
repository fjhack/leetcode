import unittest

class Solution(object):

    def __init__(self):
        self.results = set()
        self.M = dict()
        
    def mismatch(self, s):
        a, b = 0, 0
        pvalue = {'(': 1, ')': -1}
        pvalue_get = pvalue.get
        for char in s:
            a += pvalue_get(char, 0)
            b += (a < 0)
            a = max(a, 0)
        return a+b

    def RIP(self, s):
        L = len(s)
        mismatches = self.mismatch(s)
        
        if s in self.M:
            return self.M[s]
        
        if mismatches == 0:
            self.results.add(s)
            return True

        anysuccess = False
        for i in xrange(L):
            if not (s[i] == '(' or s[i] == ')'):
                continue
            newstr = s[:i] + s[i+1:]
            newmismatches = self.mismatch(newstr)
            if newmismatches > mismatches:
                continue
            else:
                _success = self.RIP(newstr)
                self.M[newstr] = _success
                anysuccess = anysuccess or _success
        return anysuccess

    def removeInvalidParentheses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        self.results = set()
        self.M = dict()
        self.RIP(s)
        print self.M
        return list(self.results)

    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        stk = []
        L = len(s)
        for i in xrange(L):
            if s[i] == '(' or s[i] == '[' or s[i] == '{':
                stk.append(s[i])
            elif s[i] == ')':
                if len(stk) > 0 and stk[-1] == '(':
                    stk.pop()
                else:
                    return False
            elif s[i] == ']':
                if len(stk) > 0 and stk[-1] == '[':
                    stk.pop()
                else:
                    return False
            elif s[i] == '}':
                if len(stk) > 0 and stk[-1] == '{':
                    stk.pop()
                else:
                    return False
        if len(stk) == 0:
            return True
        else:
            return False

    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    # def test_t(self):
    #     ps = ''
    #     print "Test  :", ps
    #     R = self.S.removeInvalidParentheses(ps)
    #     print R

    def test_1(self):
        ps = '()())()'
        print "Test 1 :", ps
        R = self.S.removeInvalidParentheses(ps)
        print R

    def test_2(self):
        ps = '(a)())()'
        print "Test 2 :", ps
        R = self.S.removeInvalidParentheses(ps)
        print R

    def test_3(self):
        ps = ')('
        print "Test 3 :", ps
        R = self.S.removeInvalidParentheses(ps)
        print R

    def test_4(self):
        ps = ')(((((((('
        print "Test 4 :", ps
        R = self.S.removeInvalidParentheses(ps)
        print R

        
if __name__ == "__main__":
    unittest.main()

