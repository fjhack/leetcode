class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        pold, pnew = 0, 0
        L = len(nums)
        print nums
        for pold in xrange(L):
            if nums[pold] == val:
                continue
            nums[pnew] = nums[pold]
            print nums
            pnew += 1
        print
        return pnew

if __name__ == "__main__":
    sre = Solution().removeElement
    A = [3,3,2,1,3,4]
    print sre(A,3 )
