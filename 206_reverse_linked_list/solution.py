# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        lastptr = []
        if head is None:
            return head
            
        curr = head
        while curr is not None:
            lastptr.append(curr)
            curr = curr.next
        
        newhead = lastptr.pop()
        c2 = newhead
        while len(lastptr) > 0:
            c2.next = lastptr.pop()
            c2 = c2.next
        
        c2.next = None
        return newhead
