import unittest

def isPalindrome(self, s, i, j):
    while i < j:
        if s[i] != s[j]:
            return False
    return True

class Trie(object):
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self._topd = dict()

    def insert(self, word, value=None):
        """
        Inserts a word into the trie.
        :type word: str
        :rtype: void
        """
        L = len(word)
        curr_dict = self._topd
        for i in xrange(L):
            if word[i] in curr_dict:
                curr_dict = curr_dict[word[i]]
            else:
                curr_dict[word[i]] = dict()
                curr_dict = curr_dict[word[i]]
        curr_dict["TERM"] = value
        return 

    def search(self, word):
        """
        Returns if the word is in the trie.
        :type word: str
        :rtype: bool
        """
        L = len(word)
        curr_dict = self._topd
        for i in xrange(L):
            if word[i] in curr_dict:
                curr_dict = curr_dict[word[i]]
            else:
                return None
        if "TERM" in curr_dict:
            return curr_dict["TERM"]
        else:
            return None

    def startsWith(self, prefix):
        """
        Returns if there is any word in the trie that starts with the given prefix.
        :type prefix: str
        :rtype: bool
        """
        L = len(prefix)
        curr_dict = self._topd
        for i in xrange(L):
            if prefix[i] in curr_dict:
                curr_dict = curr_dict[prefix[i]]
            else:
                return False
        return True
        
    def allStartsWith(self, prefix):
        """
        Returns all words in the trie that starts with the given prefix.
        :type prefix: str
        :rtype: List[str]
        """
        L = len(prefix)
        curr_dict = self._topd
        for i in xrange(L):
            if prefix[i] in curr_dict:
                curr_dict = curr_dict[prefix[i]]
            else:
                return []
        results = []
        def dfs(cd, prefix):
            # print " "*len(prefix), cd
            if "TERM" in cd:
                results.append((prefix, cd["TERM"]))
            for k in cd:
                if k == "TERM":
                    continue
                dfs(cd[k], prefix+k)
            return
        dfs(curr_dict, prefix)
        return results

class Solution0(object):
    def palindromePairs(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """
        rev_words = Trie()
        fwd_words = Trie()
        for i, w in enumerate(words):
            rev_words.insert(w[::-1], i)
            fwd_words.insert(w, i)

        print "Trie built:"
        print rev_words._topd
        print fwd_words._topd
        results = []
        
        for i, w in enumerate(words):
            # Search in rev prefix first
            print "Searching word: {0} at {1}".format(w, i)
            _revprefix = rev_words.allStartsWith(w)
            print " Found in trie: ", _revprefix
            if len(_revprefix) > 0:
                for _rp in _revprefix:
                    if i == _rp[1]:
                        continue
                    _ns = w + _rp[0][::-1]
                    print "  Constructed palindrome candidate:", _ns
                    if self.isPalindrome(_ns):
                        results.append([i, _rp[1]])
                        if len(w) == 0:
                            if [_rp[1], i] not in results:
                                results.append([_rp[1], i])

            # search in forward prefix
            print "Searching word in forward: {0} at {1}".format(w, i)
            _fwdprefix = fwd_words.allStartsWith(w)
            print " Found in fwd trie: ", _fwdprefix
            if len(_fwdprefix) > 0:
                for _fp in _fwdprefix:
                    if i == _fp[1]:
                        continue
                    _ns = _fp[0] + w
                    print "  Constructed palindrome candidate (fwd):", _ns
                    if self.isPalindrome(_ns):
                        if [_fp[1], i] not in results:
                            results.append([_fp[1], i])
                        if len(w) == 0:
                            if [_fp[1], i] not in results:
                                results.append([_fp[1], i])
                        
        return results

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

        """
    def test_trie_1(self):
        T = Trie()
        T.insert("abort")
        T.insert("abdomen")
        T.insert("abomination")
        T.insert("apollo")
        T.insert("ab")
        R = T.allStartsWith("ab")
        print R
        print T
        """

    def test_1(self):
        words = ["abcd", "dcba", "lls", "s", "sssll"]
        R = self.S.palindromePairs(words)
        print R

    def test_1_2(self):
        words = ["bat", "tab", "cat"]
        R = self.S.palindromePairs(words)
        print R

    def test_2(self):
        print "\nTest 2: empty string"
        words = ["a",""]
        R = self.S.palindromePairs(words)
        print R

    def test_3(self):
        print "\nTest 3"
        words = ["a","b","c","ab","ac","aa"]
        R = self.S.palindromePairs(words)
        print R

    def test_4(self):
        print "\nTest 4: words are reversal of each"
        words = ["ab","ba","abc","cba"]
        R = self.S.palindromePairs(words)
        print R

if __name__ == "__main__":
    unittest.main()
