import unittest

def isPalindrome(s1):
    L = len(s1)
    for i in xrange(L/2):
        if s1[i] != s1[L-1-i]:
            return False
    return True


class Solution(object):
    def palindromePairsBF(self, words):
        """
        Brute force solution

        :type words: List[str]
        :rtype: List[List[int]]
        """
        rwords = [w[::-1] for w in words]
        L = len(words)
        results = []
        for i in xrange(L):
            # print words[i], "\t",
            for j in xrange(L):
                if (i != j) and (words[i].startswith(rwords[j]) or rwords[j].startswith(words[i])) and isPalindrome(words[i]+words[j]):
                    # print words[i], rwords[j], " ",i, j, words[i]+words[j]
                    results.append([i, j])
            # print
        return results

    def palindromePairs(self, words):
        word2pos = dict()
        for idx, word in enumerate(words):
            word2pos[word] = idx

        results = []
        rset = set()
        for idx, word in enumerate(words):
            L = len(word)
            for j in xrange(L+1):
                s1, s2 = word[:j], word[j:]
                if isPalindrome(s1):
                    if s2[::-1] in word2pos and word2pos[s2[::-1]] != idx:
                        # print "Palindrome: '{1}' Symmetric: '{0}' '{2}'".format(s2[::-1], s1, s2)
                        _p = (word2pos[s2[::-1]], idx)
                        if _p not in rset:
                            rset.add(_p)
                            results.append(_p)
                if isPalindrome(s2):
                    if s1[::-1] in word2pos and word2pos[s1[::-1]] != idx:
                        # print "Palindrome: '{1}' Symmetric: '{0}' '{2}'".format(s1[::-1], s2, s1)
                        _p = (idx, word2pos[s1[::-1]])
                        if _p not in rset:
                            rset.add(_p)
                            results.append(_p)
        return list(rset)
        

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        print
        words = ["bat", "tab", "cat"]
        print "Test 0:"
        print "Words:", words
        R = self.S.palindromePairs(words)
        print "Results:", R

    def test_1(self):
        print
        words = ["abcd", "dcba", "lls", "s", "sssll"]
        print "Test 1:"
        print "Words:", words
        R = self.S.palindromePairs(words)
        print "Results:", R

    def test_1a(self):
        print
        words = ["abcd", "dcba", "lls", "s", "sssll", "sssssll"]
        print "Test 1a:"
        print "Words:", words
        R = self.S.palindromePairs(words)
        print "Results:", R

    def test_2(self):
        print
        words = ["bb","bababab","baab","abaabaa","aaba","","bbaa","aba","baa","b"]
        print "Test 2:"
        print "Words:", words
        R = self.S.palindromePairs(words)
        print "Results:", R

if __name__ == "__main__":
    unittest.main()
