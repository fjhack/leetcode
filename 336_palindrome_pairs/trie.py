import unittest

class TrieNode(object):
    def __init__(self):
        self.d = dict()
        self.t = dict()
        self.results = []

    def addWord(self, word, payload=0):
        if len(word) == 0:
            self.t[''] = [-1]
            return
        c1 = word[0]
        if c1 not in self.d:
            self.d[c1] = TrieNode()
        if len(word) == 1:

            if c1 not in self.t:
                self.t[c1] = [payload]
            else:
                self.t[c1].append(payload)
            return
        self.d[c1].addWord(word[1:], payload)

    def startsWith(self, prefix):
        """
        Returns None if no match
        returns [-1] if there are words starts with given prefix, but no exact match
        returns [payload] if there is exact match
        """
        if len(prefix) > 0 and len(self.d) == 0 and len(self.t) == 0:
            return None
        elif len(prefix) == 1:
            if prefix[0] not in self.t:
                # print "\t", prefix, self.d, self.t
                return [-1]
            else:
                return self.t[prefix[0]]
        elif len(prefix) > 0:
            c1 = prefix[0]
            # print c1
            if c1 not in self.d:
                return None
            return self.d[c1].startsWith(prefix[1:])

class TrieSearcher(object):
    def __init__(self):
        self.results = []

    def dfs(self, node, prefix, path=""):
        if len(prefix) > 1:
            if prefix[0] in node.d:
                self.dfs(node.d[prefix[0]], prefix[1:], path+prefix[0])
        elif len(prefix) == 1:
            if prefix[0] in node.t:
                # print path+prefix[0], node.t[prefix[0]]
                self.results.append((path+prefix[0], node.t[prefix[0]]))
            if prefix[0] in node.d:
                self.dfs(node.d[prefix[0]], "", path+prefix[0])
        else:
            for kt in node.t:
                # print path+kt, node.t[kt]
                self.results.append((path+kt, node.t[kt]))
            for k in node.d:
                self.dfs(node.d[k], "", path+k)


    def allStartsWith(self, node, prefix):
        self.results = []
        self.dfs(node, prefix)
        return self.results


class testTrie(unittest.TestCase):
    def setUp(self):
        pass

    def test_1(self):
        Troot = TrieNode()
        s1 = "derp"
        Troot.addWord(s1)
        print Troot.d
        print Troot.d['d'].d
        print Troot.d['d'].d['e'].d
        print Troot.d['d'].d['e'].d['r'].d
        print Troot.d['d'].d['e'].d['r'].d['p'].d
        r = Troot.startsWith("de")
        print r

    def test_2(self):
        T = TrieNode()
        TS = TrieSearcher()
        print
        ss = ['a', 'boot', 'ass', 'asshole', 'bored', 'cat', 'category', 'cannon']
        for word in ss:
            T.addWord(word)
        print 'a', T.startsWith('a')
        print 'as', T.startsWith('as')
        print 'ass', T.startsWith('ass')
        print 'asshole', T.startsWith('asshole')
        print 'boa', T.startsWith('boa')
        print 'ac', T.startsWith('ac')
        print 'ca', T.startsWith('ca')
        print
        print TS.allStartsWith(T, 'a')

if __name__ == "__main__":
    unittest.main()
