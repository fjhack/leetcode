import unittest

def isPalindrome(s1):
    L = len(s1)
    for i in xrange(L/2):
        if s1[i] != s1[L-1-i]:
            return False
    return True

class TrieNode(object):
    def __init__(self):
        self.d = dict()
        self.t = dict()
        self.results = []

    def addWord(self, word, payload=0):
        if len(word) == 0:
            self.t[''] = [-1]
            return
        c1 = word[0]
        if c1 not in self.d:
            self.d[c1] = TrieNode()
        if len(word) == 1:
            
            if c1 not in self.t:
                self.t[c1] = [payload]
            else:
                self.t[c1].append(payload)
            return
        self.d[c1].addWord(word[1:], payload)

    def startsWith(self, prefix):
        """
        Returns None if no match
        returns [-1] if there are words starts with given prefix, but no exact match
        returns [payload] if there is exact match
        """
        if len(prefix) > 0 and len(self.d) == 0 and len(self.t) == 0:
            return None
        elif len(prefix) == 1:
            if prefix[0] not in self.t:
                # print "\t", prefix, self.d, self.t
                return [-1]
            else:
                return self.t[prefix[0]]
        elif len(prefix) > 0:
            c1 = prefix[0]
            # print c1
            if c1 not in self.d:
                return None
            return self.d[c1].startsWith(prefix[1:])

class TrieSearcher(object):
    def __init__(self):
        self.results = []

    def dfs(self, node, prefix, path=""):
        if len(prefix) > 1:
            if prefix[0] in node.d:
                self.dfs(node.d[prefix[0]], prefix[1:], path+prefix[0])
        elif len(prefix) == 1:
            if prefix[0] in node.t:
                # print path+prefix[0], node.t[prefix[0]]
                self.results.append((path+prefix[0], node.t[prefix[0]]))
            if prefix[0] in node.d:
                self.dfs(node.d[prefix[0]], "", path+prefix[0])
        else:
            for kt in node.t:
                # print path+kt, node.t[kt]
                self.results.append((path+kt, node.t[kt]))
            for k in node.d:
                self.dfs(node.d[k], "", path+k)

    
    def allStartsWith(self, node, prefix):
        self.results = []
        self.dfs(node, prefix)
        return self.results




class Solution(object):
    def palindromePairs0(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """
        wt = TrieNode()
        rwt = TrieNode()
        rwords = [w[::-1] for w in words]

        L = len(words)
        for i in xrange(L):
            wt.addWord(words[i], i)
            rwt.addWord(rwords[i], i)

        results = []
        for i in xrange(L):
            # print words[i], "\t",
            for j in xrange(L):
                if (i != j) and (wt.startsWith(rwords[j]) or rwt.startsWith(words[i])) and isPalindrome(words[i]+words[j]):
                    print words[i], rwords[j], " ",i, j, words[i]+words[j], words[i].startswith(rwords[j]), rwords[j].startswith(words[i]), wt.startsWith(rwords[j]), rwt.startsWith(words[i])
                    results.append([i, j])
            # print
        return results

    def palindromePairs(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """
        wt = TrieNode()
        rwt = TrieNode()
        rwords = [w[::-1] for w in words]

        L = len(words)
        for i in xrange(L):
            wt.addWord(words[i], i)
            rwt.addWord(rwords[i], i)

        TS = TrieSearcher()
        results = []
        r2 = []
        for i in xrange(L):
            word, rword = words[i], rwords[i]
            wt_r = TS.allStartsWith(wt, rword)
            rwt_w = TS.allStartsWith(rwt, word)
            print word, i, rwt_w
            print rword, i, wt_r
            for r in wt_r:
                # print "  ", rword[::-1]+r[0]
                if r[1][0] != i and isPalindrome(rword[::-1]+r[0]):
                    if [i, r[1][0]] not in r2:
                        r2.append([i, r[1][0]])
            for w in rwt_w:
                # print "  ", word+w[0][::-1]
                if w[1][0] != i and isPalindrome(word+w[0][::-1]):
                    if [i, w[1][0]] not in r2:
                        r2.append([i, w[1][0]])
        print r2

        for i in xrange(L):
            # print words[i], "\t",
            for j in xrange(L):
                if (i != j) and (wt.startsWith(rwords[j]) or rwt.startsWith(words[i])) and isPalindrome(words[i]+words[j]):
                    print i, words[i], j, rwords[j], " ",words[i]+words[j], "\n\twordstrie starts with {0}({1}): {2}".format(rwords[j], j, wt.startsWith(rwords[j])), ", rwordstrie starts with {0}({1}): {2}".format(words[i], i, rwt.startsWith(words[i]))
                    results.append([i, j])
            # print
        return results
        


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        print
        words = ["bat", "tab", "cat"]
        print "Test 0:"
        print "Words:", words
        R = self.S.palindromePairs(words)
        print "Results:", R

    def test_1(self):
        print
        words = ["abcd", "dcba", "lls", "s", "sssll"]
        print "Test 1:"
        print "Words:", words
        R = self.S.palindromePairs(words)
        print "Results:", R

    def test_1a(self):
        print
        words = ["abcd", "dcba", "lls", "s", "sssll", "sssssll"]
        print "Test 1a:"
        print "Words:", words
        R = self.S.palindromePairs(words)
        print "Results:", R

    def test_2(self):
        print
        words = ["bb","bababab","baab","abaabaa","aaba","","bbaa","aba","baa","b"]
        print "Test 2:"
        print "Words:", words
        R = self.S.palindromePairs(words)
        print "Results:", R

if __name__ == "__main__":
    unittest.main()
