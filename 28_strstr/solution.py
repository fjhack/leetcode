class Solution(object):

    # Brute force, first attempt
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        Lh, Ln = len(haystack), len(needle)
        if Lh == 0 and Ln == 0:
            return 0

        # WRONG:
        # (when haystack is not empty, needle is empty)
        # Empty string is considered occur everywhere
        # Fix this:
        elif Ln == 0 or Lh == 0:
            return -1

        # To:
        elif Ln == 0:
            return 0
        elif Lh == 0:
            return -1

        elif Lh < Ln:
            return -1
        
        for i in xrange(Lh - Ln + 1):
            j = 0
            if needle[j] != haystack[i]:
                continue
            else:
                i2 = i
                while i2 < Lh and j < Ln and needle[j] == haystack[i2]:
                    i2 += 1
                    j += 1
                if j == Ln:
                    return i
        return -1
        
