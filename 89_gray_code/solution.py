class Solution(object):
    def grayCode(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        if n == 0:
            return [0]
        final = '1'+'0'*(n-1)
        flipped_bits = [False for _ in xrange(n)]
        binstr = ['0' for _ in xrange(n)]
        def generate(n, bstr, flipped_bits, results):
            if ''.join(bstr) == final:
                return
            else:
                lowest_nonflipped = n-1
                for i in xrange(n-1, -1, -1):
                    if flipped_bits[i]:
                        flipped_bits[i] = False
                        continue
                    else:
                        lowest_nonflipped = i
                        flipped_bits[i] = True
                        break
                if bstr[lowest_nonflipped] == '0':
                    bstr[lowest_nonflipped] = '1'
                else:
                    bstr[lowest_nonflipped] = '0'
                results.append(int(''.join(bstr), 2))
                generate(n, bstr, flipped_bits, results)
                return

        results = [int(''.join(binstr), 2)]
        generate(n, binstr, flipped_bits, results)
        return results

if __name__ == "__main__":
    S = Solution()
    SgC = S.grayCode
    print SgC(2)
    print SgC(3)
    print SgC(1)
    print SgC(0)

