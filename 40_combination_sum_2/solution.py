import unittest

class Solution(object):
    def __init__(self):
        self.results = []
        
    def combinationSum2(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        c2 = sorted(candidates)
        self.results = []
        self.csi1(c2, target, [])
        return self.results

    def csi1(self, candidates, target, rl):
        found = False
        # print rl
        cu = list(set(candidates))
        for n in cu:
            r = target - n
            # print " ", n, r
            nc = [c for c in candidates if (c >= n and c <= r)]
            if len(nc) != 0:
                nc.pop(0)
            nt = r
            if r == 0:
                found = True
                self.results.append(rl+[n])
            else:
                self.csi1(nc, nt, rl + [n])

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        pass

    def test_a_1(self):
        A = [10,1,2,7,6,1,5]
        t = 7
        print self.S.combinationSum2(A, t)

    def test_a_2(self):
        A = [10,1,2,7,6,1,5]
        t = 8
        print self.S.combinationSum2(A, t)


if __name__ == "__main__":
    unittest.main()
