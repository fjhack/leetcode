class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        L = len(nums)
        if L < 2:
            return
        else:
            z = 0
            while z < L:
                while z < L and nums[z] != 0:
                    z += 1
                if z == L:
                    return
                nnz = z
                while nnz < L and nums[nnz] == 0:
                    nnz += 1
                if nnz == L:
                    return
                # swap
                t = nums[nnz]
                nums[nnz] = 0
                nums[z] = t
                z += 1
            
