import unittest

class Solution(object):
    def findMin(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        L = len(nums)
        head, tail = 0, L-1
        if nums[head] <= nums[tail]:
            return nums[0]
        while head+1 < tail:
            mid = head + (tail-head)/2
            if nums[mid-1] > nums[mid]:
                return mid
            if nums[head] > nums[mid]:
                tail = mid
            else:
                head = mid
        if nums[head] < nums[tail]:
            return head
        else:
            return tail

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def generate_array(self):
        import random
        L = random.randint(10, 16)
        p = random.randint(0, L)
        A = [random.randint(1, 200) for _ in xrange(L)]
        As = sorted(list(set(A)))
        Ar = As[p:] + As[:p]
        # print L, As
        # print As[:p], As[p:]
        # print Ar
        return Ar

    def test_1(self):
        A = [4,5,6,7,0,1,2]
        t = 6
        print
        print self.S.findMin(A)


    def test_1_1(self):
        A = [1]
        t = 0
        print
        print self.S.findMin(A)

    def test_1_2(self):
        A = [1]
        t = 2
        print
        print self.S.findMin(A)

    def test_1_3(self):
        A = [1,3]
        t = 2
        print
        print self.S.findMin(A)

    def test_1_4(self):
        A = [3,1]
        t = 1
        print
        print self.S.findMin(A)

    def test_1_d1(self):
        A = [4,4,5,6,7,0,1,2]
        t = 6
        print
        print self.S.findMin(A)

    def test_1_d2(self):
        A = [4,5,6,0,0,1,2]
        t = 6
        print
        print self.S.findMin(A)

if __name__ == "__main__":
    unittest.main()
