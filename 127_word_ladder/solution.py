class Solution(object):
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """
        lbegin, lend = len(beginWord), len(endWord)
        if lbegin != lend:
            return 0
        words = set(wordList)
        words.add(beginWord)

        if endWord not in wordList:
            return 0
        step, clen = 1, 1
        nextlen = 0
        Q = [beginWord]
        while len(Q) > 0:
            step += 1
            for _ in xrange(clen):
                item = Q.pop(0)
                for pos in xrange(len(item)):
                    p1, p2 = item[:pos], item[pos+1:]
                    for c in "abcdefghijklmnopqrstuvwxyz":
                        newword = p1 + c + p2
                        if newword == item:
                            continue
                        elif newword == endWord:
                            return step
                        elif newword in words:
                            Q.append(newword)
                            words.remove(newword)
                            nextlen += 1
            clen = nextlen
            nextlen = 0
        return 0

    def ladderLength1(self, beginWord, endWord, wordList):
        """
        Also print out the transformation path

        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """
        lbegin, lend = len(beginWord), len(endWord)
        if lbegin != lend:
            return 0
        words = set(wordList)
        words.add(beginWord)

        if endWord not in wordList:
            return 0
        step, clen = 1, 1
        nextlen = 0
        Q = [(beginWord, [beginWord])]
        while len(Q) > 0:
            step += 1
            for _ in xrange(clen):
                item = Q.pop(0)
                for pos in xrange(len(item[0])):
                    p1, p2 = item[0][:pos], item[0][pos+1:]
                    for c in "abcdefghijklmnopqrstuvwxyz":
                        newword = p1 + c + p2
                        if newword == item[0]:
                            continue
                        elif newword == endWord:
                            return step
                        elif newword in words:
                            Q.append((newword, item[1] + [newword]))
                            words.remove(newword)
                            nextlen += 1
            clen = nextlen
            nextlen = 0
        return 0
