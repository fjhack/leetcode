class Solution(object):
    def countNumbersWithUniqueDigits(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n > 10:
            return 0
        elif n == 0:
            return 1
        elif n == 1:
            return 10
        elif n < 0:
            return 0
        else:
            p = 1
            for i in xrange(1, n+1):
                p = p * min((9-i+2), 9)
            return p + self.countNumbersWithUniqueDigits(n-1)
