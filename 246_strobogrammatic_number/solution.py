class Solution(object):
    def isStrobogrammatic(self, num):
        """
        :type num: str
        :rtype: bool
        """
        if '2' in num or '3' in num or '4' in num or '5' in num or '7' in num:
            return False

        def stbl(c1, c2):
            if (c1 == '6' and c2 == '9') or (c1 == '9' and c2 == '6'):
                return True
            elif c1 == c2 and (c1 == '0' or c1 == '1' or c1 == '8'):
                return True
            else:
                return False
        
        L = len(num)
        for i in xrange(L/2):
            print num[i], num[L-1-i]
            if not stbl(num[i], num[L-1-i]):
                return False
        
        if L % 2 != 0:
            if num[L/2] == '9' or num[L/2] == '6':
                return False
            else:
                return True
        return True

if __name__ == "__main__":
    S = Solution()
    print S.isStrobogrammatic("10")
