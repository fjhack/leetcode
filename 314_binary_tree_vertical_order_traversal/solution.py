# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

from collections import OrderedDict
import unittest

class Solution(object):
    def __init__(self):
        self._columnnodes = dict()

    def walk(self, node, column):
        if node is None:
            return
        else:
            if column in self._columnnodes:
                self._columnnodes[column].append(node.val)
            else:
                self._columnnodes[column] = [node.val]
            self.walk(node.left, column-1)
            self.walk(node.right, column+1)

    def walkBFS(self, root):
        queue = [(root, 0)]
        while len(queue) > 0:
            _curr = queue.pop(0)
            curr_node, curr_column = _curr[0], _curr[1]
            if curr_column in self._columnnodes:
                self._columnnodes[curr_column].append(curr_node.val)
            else:
                self._columnnodes[curr_column] = [curr_node.val]

            if curr_node.left is not None:
                queue.append((curr_node.left, curr_column-1))
            if curr_node.right is not None:
                queue.append((curr_node.right, curr_column+1))
            
    def verticalOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        self._columnnodes = OrderedDict()
        self.walkBFS(root)
        results = []
        for key in sorted(self._columnnodes):
            results.append(self._columnnodes[key])
        return results

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        root = TreeNode(3)
        root.left = TreeNode(9)
        root.right = TreeNode(20)
        root.right.left = TreeNode(15)
        root.right.right = TreeNode(7)
        R = self.S.verticalOrder(root)
        print R

if __name__ == "__main__":
    unittest.main()
