class Solution:
    # @param A, a list of integers
    # @return an integer
    def jump_0(self, A):
        # write your code here
        L = len(A)
        max_reaches = []
        min_steps = [0 for _ in xrange(L)]
        max_reaches.append((A[0], 1))
        
        for i in xrange(1, L):
            cmr = max_reaches[-1]
            if i <= cmr[0]:
                if i+A[i] > cmr[0]:
                    max_reaches.append((i+A[i], cmr[1]+1))
                LMR = len(max_reaches)
                for j in xrange(LMR-1, -1, -1):
                    if j >= 0 and i <= max_reaches[j][0]:
                        continue
                    else:
                        break
                min_steps[i] = j+1
            else:
                break
        return min_steps[-1]

    def jump(self, A):
        # write your code here
        L = len(A)
        last_reach = 0
        steps = 0
        curr_reach = 0
        
        for i in xrange(L):
            if i > last_reach:
                steps += 1
                last_reach = curr_reach
            curr_reach = max(curr_reach, A[i]+i)
        if curr_reach < A[-1]:
            return 0
        return steps
    
if __name__ == "__main__":
    S = Solution()
    A = [13,52,42,21,58]
    r = S.jump(A)
    print r
