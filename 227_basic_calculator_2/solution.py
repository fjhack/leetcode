import math

class Solution(object):
    def calculate(self, s):
        """
        :type s: str
        :rtype: int
        """

        s = s.replace(' ', '')
        L = len(s)
        if L == 0:
            return 0
            
        result = 0
        val, preval = 0, 0
        buf = ""
        i = 0
        sign = '+'
        while i < L:
            buf = ""
                
            while i < L and s[i].isdigit():
                buf += s[i]
                i += 1
            val = int(buf)
            print val, sign, preval
            if sign == '+':
                result += preval
                preval = int(buf)
            elif sign == '-':
                result += preval
                preval = -int(buf)
            elif sign == '*':
                preval = preval * int(buf)
            elif sign == '/':
                if (val<0) == (preval<0):
                    preval = preval / int(buf)
                else:
                    preval = int(math.ceil(float(preval)/float(buf)))
            if i < L and s[i] != ' ':
                sign = s[i]
                i += 1
        
        result += preval
        return result
