class Solution:
    # @param n, an integer
    # @return an integer
    def reverseBits(self, n):
        reverse = ''
        n2 = n
        while n2 > 1:
            r, n2 = n2 % 2, n2 / 2
            reverse = str(r) + reverse

        reverse = str(n2) + reverse
        while len(reverse) < 32:
            reverse = '0' + reverse
        return int(reverse[::-1], 2)

if __name__ == "__main__":
    S = Solution()
    r1 = S.reverseBits(16)
    print r1
    r2 = S.reverseBits(43261596)
    print r2
