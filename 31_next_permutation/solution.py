import sys
import unittest

class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        L = len(nums)
        for i in xrange(L-1,0,-1):
            if nums[i-1] < nums[i]:
                # Found the first pair of increasing adjancent numbers
                # ...and we want to swap between the smaller number (first in the pair) with
                # the next large number appears after it
                # 1. Find the next large number first
                min_diff, min_pos = sys.maxint, i-1
                for j in xrange(i, L):
                    _diff = nums[j] - nums[i-1]
                    if _diff > 0 and _diff < min_diff:
                        min_diff = _diff
                        min_pos = j
                # print i-1, min_pos
                # But now we cannot just swap, we need to make sure the sequence after
                # the switch point is also the lowest sequence
                t = nums[i-1]
                nums[i-1] = nums[min_pos]
                nums[min_pos] = t
                nums[i:L] = sorted(nums[i:L])

                return
        for i in xrange(L/2):
            t = nums[i]
            nums[i] = nums[L-i-1]
            nums[L-i-1] = t
        return

if __name__ == "__main__":
    np = Solution().nextPermutation
    A = [3,1,2]
    np(A)
    print A

    A = [3,1,5,4]
    np(A)
    print A

    A = [1,3,2]
    np(A)
    print A
