import math

class Solution(object):
    def countBits(self, num):
        """
        :type num: int
        :rtype: List[int]
        """
        if num == 0:
            return [0]
        elif num == 1:
            return [0, 1]
        else:
            R = [0, 1]
            current_step, j = 2, 0
            for i in xrange(2, num+1):
                if math.log(i, 2).is_integer():
                    current_step = i
                R.append(R[i-current_step]+1)
            return R

if __name__ == "__main__":
    S = Solution()

    A = 5
    print S.countBits(A)

    A = 11
    print S.countBits(A) 
