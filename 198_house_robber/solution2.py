class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        L = len(nums)
        if L == 0:
            return 0
        elif L == 1:
            return nums[0]
        elif L == 2:
            return max(nums[0], nums[1])
        
        DP = [0 for _ in xrange(L)]
        DP[0], DP[1] = nums[0], max(nums[0], nums[1])
        
        for i in xrange(2, L):
            DP[i] = max(nums[i]+DP[i-2], DP[i-1])
        
        return DP[-1]
