class Solution(object):
    def rob0(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        elif len(nums) == 1:
            return nums[0]
        elif len(nums) == 2:
            return max(nums[1], nums[0])

        omax = nums[0]
        emax = max(nums[0], nums[1])
        cmax = 0
        i = 2
        while (i < len(nums)):
            if i % 2 == 0:
                # odd
                cmax = max(omax + nums[i], emax)
                omax = max(cmax, omax)
            else:
                # even
                cmax = max(emax + nums[i], omax)
                emax = max(cmax, emax)
            i += 1
        return max(omax, emax)

    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        L = len(nums)
        if L < 3:
            if L == 1:
                return nums[0]
            elif L == 2:
                return max(nums[0], nums[1])
            else:
                return 0
        else:
            r = [0 for _ in xrange(L)]
            r[0], r[1] = nums[0], max(nums[0], nums[1])
            for i in xrange(2, L):
                r[i] = max(r[i-1], nums[i]+r[i-2])
            return r[-1]
if __name__ == "__main__":
    S = Solution()
    H0 = [1,4]
    print S.rob(H0)

    H1 = [3,2,1,5,6,4]
    #     3 3 4 8 10 12
    #         3 4 8  10
    #         4 8 10 12
    print S.rob(H1)

    H2 = [2,1,1,2]
    #     2 2 3 4 
    #         2 3
    #         3 4
    print S.rob(H2)
