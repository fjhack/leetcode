# The isBadVersion API is already defined for you.
# @param version, an integer
# @return a bool
# def isBadVersion(version):

class Solution(object):
    def firstBadVersion(self, n):
        """
        :type n: int
        :rtype: int
        """
        start, end = 1, n
        if n == 0:
            return n
        if n == 1:
            if isBadVersion(n):
                return n
            else:
                return 0
        while end - start > 1:
            mid = (start + end) / 2
            if isBadVersion(mid):
                start, end = start, mid
            else:
                start, end = mid, end
        if isBadVersion(start):
            return start
        else:
            return end
