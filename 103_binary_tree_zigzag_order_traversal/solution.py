# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def zigzagLevelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        output = []
        tq = []
        if root is None:
            return output
        direction = True # True - left to right, False - right to left
        tq.append(root)
        ccount = 1
        while len(tq) > 0:
            lvl_result = []
            ncount = 0
            for i in xrange(ccount):
                node = tq.pop(0)
                if direction:
                    lvl_result.append(node.val)
                else:
                    lvl_result.insert(0, node.val)
                    
                if node.left is not None:
                    tq.append(node.left)
                    ncount += 1
                if node.right is not None:
                    tq.append(node.right)
                    ncount += 1
            # print lvl_result
            output.append(lvl_result)
            direction = not direction
            ccount = ncount
        return output

