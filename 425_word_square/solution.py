import unittest

class Solution(object):
    def __init__(self):
        self._results = None
        self._prefixmap = None

    def validWordSquare(self, words, plen):
        """
        :type words: List[str]
        :rtype: bool
        """
        i = 0
        while i <= plen:
            w1 = words[i][:plen+1]
            try:
                w2 = "".join([words[j][i] for j in xrange(plen+1)])
            except IndexError:
                return False
            if w1 != w2:
                return False
            i += 1
        return True

    def dfs(self, words, cpos):
        valid = self.validWordSquare(words, cpos)
        # print words, cpos, valid
        if not valid:
            return
        if cpos == len(words[0])-1:
            self._results.append(words)
        else:
            next_prefix = ''.join([words[i][cpos+1] for i in xrange(len(words))])
            if next_prefix in self._prefixmap:
                for prefix in self._prefixmap[next_prefix]:
                    if len(prefix) <= len(words[cpos]):
                        self.dfs(words+[prefix], cpos+1)
    
    def wordSquares(self, words):
        """
        :type words: List[str]
        :rtype: List[List[str]]
        """
        self._results = []
        self._prefixmap = {}
        
        for word in words:
            for i in xrange(1, len(word)+1):
                _prefix = word[:i]
                if _prefix not in self._prefixmap:
                    self._prefixmap[_prefix] = [word]
                else:
                    self._prefixmap[_prefix].append(word)
        
        # for v in self._prefixmap:
        #     print v, self._prefixmap[v]
                    
        for word in words:
            self.dfs([word], 0)

        return self._results


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        words = ["area","lead","wall","lady","ball"]
        R = self.S.wordSquares(words)
        print R

if __name__ == "__main__":
    unittest.main()
