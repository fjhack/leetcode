import unittest

class Solution(object):
    def evalRPN(self, tokens):
        """
        :type tokens: List[str]
        :rtype: int
        """
        L = len(tokens)
        stk = []
        i = 0
        while i < L:
            if tokens[i] in ['+', '-', '*', '/']:
                op2 = stk.pop()
                op1 = stk.pop()
                # exp = op1 + tokens[i] + op2
                # r = eval(exp)

                if tokens[i] == '+':
                    r = op1 + op2
                elif tokens[i] == '-':
                    r = op1 - op2
                elif tokens[i] == '*':
                    r = op1 * op2
                elif tokens[i] == '/':
                    r = int(op1 / op2)

                # print "Eval:", op1, tokens[i], op2, " = ", r
                stk.append(r)
                # print "Stack:", stk
                i += 1
                continue
            else:
                try:
                    v = int(tokens[i])
                except ValueError:
                    # Error
                    return 0
                stk.append(float(tokens[i]))
                # print "Stack:", stk
                i += 1
                continue
        return int(stk[-1])

if __name__ == "__main__":
    evl = Solution().evalRPN
    E = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
    R = evl(E)
    print R
