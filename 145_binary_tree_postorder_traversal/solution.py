import unittest

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

class Solution:
    """
    @param root: The root of binary tree.
    @return: Postorder in ArrayList which contains node values.
    """
    def __init__(self):
        self._result = []
    
    def PoTR(self, root):
        if root is None:
            return
        else:
            self.PoTR(root.left)
            self.PoTR(root.right)
            self._result.append(root.val)

    def PoTRS(self, root, S=[]):
        if root is None:
            print " "*len(S), "EMPT", S
            return
        else:
            print " "*len(S), "PRET", S, root.val
            print " "*len(S), "PRET", root.left, root.right
            self.PoTRS(root.left, S+[root.val])
            self.PoTRS(root.right, S+[root.val])
            self._result.append(root.val)
            print " "*len(S), "POST", self._result

    def PoTI(self, root):
        def canVisit(node, prev):
            # Put current node into output stream only when:
            # 1. It is a leaf node, OR:
            # 2. It's not a leaf node and it's already been visited before
            return (node.left is None and node.right is None) or (prev is not None and (node.left == prev or node.right == prev))
        stk = []
        curr = root
        prev = None
        if root is None:
            return
        stk.append(curr)
        while len(stk) > 0:
            curr = stk[-1]
            if canVisit(curr, prev):
                print " "*len(stk), "LEAF", [n.val for n in stk]
                curr = stk.pop()
                self._result.append(curr.val)
                prev = curr
                print " "*len(stk), "LEAF", self._result
            else:
                print " "*len(stk), "PRET", [n.val for n in stk], curr.val
                print " "*len(stk), "PRET", curr.left, curr.right
                if curr.right is not None:
                    stk.append(curr.right)
                if curr.left is not None:
                    stk.append(curr.left)
                print " "*len(stk), "POST", self._result
    
    def postorderTraversal(self, root, R=False):
        # write your code here
        self._result = []
        if R:
            self.PoTR(root)
            return self._result
        else:
            self.PoTI(root)
            return self._result
    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        root = TreeNode(4)
        root.left = TreeNode(2)
        root.left.left = TreeNode(1)
        root.left.right = TreeNode(3)
        root.left.right.right = TreeNode(4)
        root.right = TreeNode(8)
        root.right.left = TreeNode(5)
        root.right.left.right = TreeNode(7)
        root.right.right = TreeNode(11)
        R = self.S.postorderTraversal(root)
        print self.S._result
        
if __name__ == "__main__":
    unittest.main()
