import unittest

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def __init__(self):
        self.checked = False
        self.found = False
        self.prev = None
        self.result = None
        self.p = None
        
    def check(self, node):
        if not self.checked and node.val == self.p.val:
            self.checked = True
            self.prev = node
            return False
        elif self.prev is not None and self.prev.val == self.p.val and not self.found:
            self.result = node
            self.found = True
            return True
        else:
            return False
            
    def inorder(self, root):
        if root is None:
            return
        r = self.inorder(root.left)
        if r:
            return r
        res = self.check(root)
        if res:
            return res
        self.inorder(root.right)
    
    def inorderSuccessor(self, root, p):
        """
        :type root: TreeNode
        :type p: TreeNode
        :rtype: TreeNode
        """
        self.checked = False
        self.found = False
        self.result = None
        self.prev = None
        self.p = p
        self.inorder(root)
        return self.result


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        root = TreeNode(5)
        root.left = TreeNode(3)
        root.right = TreeNode(6)
        root.left.left = TreeNode(2)
        root.left.right = TreeNode(4)
        root.left.left.left = TreeNode(1)
        R = self.S.inorderSuccessor(root, root.left.left.left)
        print R.val

if __name__ == "__main__":
    unittest.main()
    
