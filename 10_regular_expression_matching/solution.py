import unittest

class Solution(object):
    def isMatch0(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        """
        Note: This solution will fail on case 9
        """
        L, patternL = len(s), len(p)
        match = True
        si, pi = 0, 0
        while si < L and pi < patternL:
            print si, pi, s[si], p[pi]
            if pi < patternL-1:
                plh = p[pi+1]
            else:
                plh = None
            if plh == '*':
                if s[si] == p[pi] and p[pi] != '.':
                    while si < L and s[si] == p[pi]:
                        print " ", si, pi, s[si], p[pi]
                        si += 1
                    pi += 2
                    print " (e)", si, pi
                    continue
                elif s[si] != p[pi] and p[pi] != '.':
                    pi += 2
                    continue
                elif p[pi] == '.':
                    while si < L:
                        si += 1
                    pi += 2
                    continue
            elif s[si] == p[pi] and p[pi] != '.':
                si += 1
                pi += 1
            elif p[pi] == '.':
                si += 1
                pi += 1
            else:
                match = False
                return match
        print "END: ", si, pi
        if si == L and pi == patternL:
            return match
        else:
            return False

    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        L, patternL = len(s), len(p)
        if patternL == 0:
            return L == 0
        si, pi = 0, 0
        print "\n\t", s, si, p, pi 
        if pi+1 < patternL and p[pi+1] != '*':
            if p[pi] != '*':
                return (s[si] == p[pi] or (si < L and p[pi] == '.')) and self.isMatch(s[1:], p[1:])
            else:
                return False
        else:
            print "found star", si, s, pi, p
            while s[si] == p[pi] or (si < L and p[pi] == '.'):
                print "\t\t", si, s, pi, p
                if self.isMatch(s, p[2:]):
                    return True
                else:
                    si += 1
            return self.isMatch(s, p[2:])
        
        
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.isM = self.S.isMatch

    def test_1(self):
        s, p = "aa", "a"
        print "\n", s, p
        print self.isM(s, p)
        assert self.isM(s, p) == False

    def test_2(self):
        s, p = "aa", "aa"
        print "\n", s, p
        print self.isM(s, p)
        assert self.isM(s, p) == True

    def test_3(self):
        s, p = "aaa", "aa"
        print "\n", s, p
        print self.isM(s, p)
        assert self.isM(s, p) == False

    def test_4(self):
        s, p = "aa", "a*"
        print "\n", s, p
        print self.isM(s, p)
        assert self.isM(s, p) == True

    def test_5(self):
        s, p = "aa", ".*"
        print "\n", s, p
        print self.isM(s, p)
        assert self.isM(s, p) == True

    # def test_6(self):
    #     s, p = "ab", ".*"
    #     print "\n", s, p, self.isM(s, p)
    #     assert self.isM(s, p) == True

    # def test_7(self):
    #     s, p = "ab", "a*b"
    #     print "\n", s, p, self.isM(s, p)
    #     assert self.isM(s, p) == True

    # def test_8(self):
    #     s, p = "aab", "c*a*b"
    #     print "\n", s, p, self.isM(s, p)
    #     assert self.isM(s, p) == True

    # def test_9(self):
    #     s, p = "aaa", "a*a"
    #     print "\n", s, p, self.isM(s, p)
    #     assert self.isM(s, p) == True

if __name__ == "__main__":
    unittest.main()
