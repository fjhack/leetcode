class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        ls, lp = len(s), len(p)
        matches = [[False for _ in xrange(lp+1)] for _ in xrange(ls+1)]
        matches[0][0] = True
        
        for j in xrange(1, lp+1):
            matches[0][j] = j > 1 and p[j-1] == '*' and matches[0][j-2]
            
        for i in xrange(1, ls+1):
            for j in xrange(1, lp+1):
                if s[i-1] == p[j-1]:
                    matches[i][j] = matches[i-1][j-1]
                elif p[j-1] == '.':
                    matches[i][j] = matches[i-1][j-1]
                elif p[j-1] == '*':
                    if p[j-2] != s[i-1]:
                        matches[i][j] = matches[i][j-2]
                    elif p[j-2] == s[i-1] or p[j-2] == '.':
                        matches[i][j] = matches[i-1][j] or matches[i][j-1] or matches[i][j-2]

        print
        for m in matches:
            print m
        print
        
        return matches[ls][lp]

if __name__ == "__main__":
    S = Solution()
    s, p = "aa", ".*"
    R = S.isMatch(s, p)
    print R
