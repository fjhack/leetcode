import unittest

class Solution(object):
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        results = []
        def match(s, level, trail):
            if level == 4 and len(s) == 0:
                print " "*level, level, trail
                results.append(".".join(trail))
                return
            elif level == 4:
                return
            elif level < 4 and len(s) == 0:
                return
            else:
                print " "*level, level, trail
                # Need to use the min of:
                #   1. length of remainder string
                #   2. 4, which is max octet length + 1
                # Otherwise could ended up with duplicated results - since in level 4 we only check for empty
                for i in xrange(1,min(len(s)+1, 4)):
                    curr = s[0:i]
                    print " "*level, i, s, s[i:]
                    if i == 1 and curr == "0":
                        match(s[i:], level+1, trail + [curr])
                    elif curr[0] != '0' and int(curr) <= 255:
                        match(s[i:], level+1, trail + [curr])
        match(s, 0, [])
        return results

if __name__ == "__main__":
    Sr = Solution().restoreIpAddresses
    # 1. Test case for duplicated end string in level 4
    print Sr("25525511135")

    # 2. Test case for stripping leading zero(s)
    print Sr("010010")

    # 3. Test case for all-zero
    print Sr("0000")
