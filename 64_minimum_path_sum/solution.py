import unittest

class Solution(object):
    def __init__(self):
        # 0=blue(SW) 1=white(NE)
        self.direction = 0

    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m = len(grid)
        n = len(grid[0])
        sumgrid = [[0 for _ in xrange(n)] for _ in xrange(m)]

        sumgrid[0][0] = grid[0][0]

        if n == 1 and m == 1:
            i, j = 0, 0
        elif n == 1:
            i, j = 1, 0
        else:
            i, j = 0, 1

        while (i<m and j<n):
            if i == 0 and j > 0:
                sumgrid[i][j] = sumgrid[i][j-1] + grid[i][j]
            elif i > 0 and j == 0:
                sumgrid[i][j] = sumgrid[i-1][j] + grid[i][j]
            elif i > 0 and j > 0:
                sumgrid[i][j] = min((sumgrid[i-1][j] + grid[i][j]),
                                    (sumgrid[i][j-1] + grid[i][j]))
            if self.direction == 0:
                if (i < m-1 and j > 0):
                    i += 1
                    j -= 1
                elif (i < m-1 and j == 0):
                    i += 1
                    self.direction = 1
                elif (i == m-1 and j >= 0):
                    j += 1
                    self.direction = 1
            elif self.direction == 1:
                if (i > 0 and j < n-1):
                    i -= 1
                    j += 1
                elif (i == 0 and j < n-1):
                    j += 1
                    self.direction = 0
                elif (i >= 0 and j == n-1):
                    i += 1
                    self.direction = 0
        return sumgrid[m-1][n-1]

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [[4,5,1],
             [2,3,10],
             [7,6,8]]
        print self.S.minPathSum(A)

if __name__ == "__main__":
    unittest.main()

