import sys
import unittest

class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        L = len(prices)
        if L == 0:
            return 0
        min_sofar = prices[0]
        max_profit_fwd = [0]
        for i in xrange(1, L):
            max_profit_fwd.append(max(max_profit_fwd[i-1], prices[i]-min_sofar))
            if prices[i] < min_sofar:
                min_sofar = prices[i]
        max_profit_bkwd = [0]
        max_bkwd = prices[-1]
        for i in xrange(1, L):
            max_profit_bkwd.insert(0, max(max_profit_bkwd[0], max_bkwd-prices[L-1-i]))
            if prices[L-1-i] > max_bkwd:
                max_bkwd = prices[L-1-i]
        # print max_profit_fwd
        # print max_profit_bkwd
        max_profit = 0
        for i in xrange(L):
            if (max_profit_fwd[i] + max_profit_bkwd[i]) > max_profit:
                max_profit = (max_profit_fwd[i] + max_profit_bkwd[i])
        return max_profit

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [4,2,1,5,6,3,7,13,10,12]
        print self.S.maxProfit(A)

if __name__ == "__main__":
    unittest.main()
