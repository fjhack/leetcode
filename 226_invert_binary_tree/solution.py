class Solution(object):
    def isLeaf(self, x):
        return (x is None) or (x.left is None and x.right is None)
        
    def invertTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if self.isLeaf(root):
            return root
        else:
            oL, oR = root.left, root.right
            root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)
            return root
