# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

class Solution(object):
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """
        itvls = sorted(intervals, key=lambda x:x[0])
        L, merged = len(itvls), []
        i, merging = 0, False
        if L <= 1:
            return intervals

        curr = itvls[0]
        while i < L-1:
            print itvls[i], curr
            if not merging:
                curr = itvls[i]
            if curr[1] >= itvls[i+1][0]:
                curr[1] = max(curr[1], itvls[i+1][1])
                merging = True
            else:
                merged.append(curr)
                merging = False
            print merging
            i += 1
        # print merging
        if not merging:
            merged.append(itvls[i])
        else:
            merged.append(curr)
        # print merged
        return merged

# Leetcode submit version - using Interval class
class SolutionLC(object):
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """
        itvls = sorted(intervals, key=lambda x:x.start)
        L, merged = len(itvls), []
        i, merging = 0, False
        if L <= 1:
            return intervals
            
        curr = Interval(itvls[0].start, itvls[0].end)
        while i < L-1:
            if not merging:
                curr = itvls[i]
            if curr.end >= itvls[i+1].start:
                curr.end = max(curr.end, itvls[i+1].end)
                merging = True
            else:
                merged.append(curr)
                merging = False
            i += 1
        # print merging
        if not merging:
            merged.append(itvls[i])
        else:
            merged.append(curr)
        # print merged
        return merged

if __name__ == "__main__":
    Smg = Solution().merge
    itvls = [[1, 3], [8, 10], [2, 6], [15, 18]]
    print Smg(itvls)

    itvls = [[1, 3], [2, 6], [8, 10], [15, 18], [16, 21]]
    print Smg(itvls)

    print Smg([])

    print Smg([[2,3],[4,5],[6,7],[8,9],[1,10]])
