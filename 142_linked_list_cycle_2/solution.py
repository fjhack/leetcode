# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        dummy = ListNode(-1)
        dummy.next = head
        ptr1, ptr2 = dummy, dummy
        found_cycle = False
        while ptr1 is not None and ptr2 is not None:
            ptr1 = ptr1.next
            if ptr2.next is not None:
                ptr2 = ptr2.next.next
            else:
                return None
            if ptr1 == ptr2:
                found_cycle = True
                break
        if not found_cycle:
            return None
        else:
            ptr2 = dummy
            while ptr1 != ptr2:
                ptr1 = ptr1.next
                ptr2 = ptr2.next
            return ptr1
