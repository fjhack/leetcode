class Solution(object):
    # O(N**2)
    def minSubArrayLen0(self, s, nums):
        """
        :type s: int
        :type nums: List[int]
        :rtype: int
        """
        # subarray_sum[i:j] = prefix_sum[j+1] - prefix_sum[i+1]
        numslen = len(nums)
        prefix_sum = [0]
        psum = 0
        for i in xrange(numslen):
            psum += nums[i]
            prefix_sum.append(psum)
        #print prefix_sum
        min_range = sys.maxint
        for i in xrange(1, numslen+1):
            for j in xrange(i+1):
                if prefix_sum[i] - prefix_sum[j] >= s:
                    #print i, j
                    min_range = min(min_range, i-j)
        if min_range == sys.maxint:
            return 0
        return min_range

    # O(N)
    def minSubArrayLen(self, s, nums):
        left, right = 0, 1
        numslen = len(nums)
        prefix_sum = [0]
        psum = 0
        for i in xrange(numslen):
            psum += nums[i]
            prefix_sum.append(psum)
        csum, min_range = 0, sys.maxint
        while right < numslen+1:
            csum = prefix_sum[right] - prefix_sum[left]

            while left < right and csum >= s:
                min_range = min(min_range, right-left)
                left += 1
                csum = prefix_sum[right] - prefix_sum[left]
            if csum < s:
                right += 1                
        if min_range == sys.maxint:
            return 0
        return min_range
