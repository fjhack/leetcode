# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self._curr = None

    def getSize(self, head):
        sz = 0
        while head is not None:
            head = head.next
            sz += 1
        return sz

    def helper(self, head, size):
        if size == 0:
            return None
        else:
            root = TreeNode(0)
            root.left = self.helper(self._curr, size/2)
            root.val = self._curr.val
            self._curr = self._curr.next
            root.right = self.helper(self._curr, size - size/2 - 1)
            return root

    def sortedListToBST(self, head):
        """
        :type head: ListNode
        :rtype: TreeNode
        """
        L = self.getSize(head)
        self._curr = head
        res = self.helper(self._curr, L)
        return res
