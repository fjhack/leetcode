class Solution(object):
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        L = len(nums)
        if L == 0:
            return False
        D = {}
        for v in nums:
            if v not in D:
                D[v] = 1
            else:
                D[v] += 1
        for k, v in D.iteritems():
            if v > 1:
                return True
        return False

        
