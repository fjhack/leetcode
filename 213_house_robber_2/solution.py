class Solution(object):
    def rob_linear(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        elif len(nums) == 1:
            return nums[0]
        elif len(nums) == 2:
            return max(nums[1], nums[0])

        omax = nums[0]
        emax = max(nums[0], nums[1])
        cmax = 0
        i = 2
        while (i < len(nums)):
            if i % 2 == 0:
                # odd
                cmax = max(omax + nums[i], emax)
                omax = max(cmax, omax)
            else:
                # even
                cmax = max(emax + nums[i], omax)
                emax = max(cmax, emax)
            i += 1
        return max(omax, emax)

    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        elif len(nums) == 1:
            return nums[0]
        else:
            return max(self.rob_linear(nums[0:-1]), self.rob_linear(nums[1:]))

    def robshit(self, A):
        L = len(A)
        if L == 0:
            return 0
        if L == 1:
            return A[0]
        if L == 2:
            return max(A[0], A[1])
            
        maxsum = -1
        D = [0 for _ in xrange(L)]
        Dn1 = [0 for _ in xrange(L)]
        D[0], D[1] = A[0], A[1]
        Dn1[0], Dn1[1] = 0, A[1]
        for i in xrange(2, L):
            D[i] = max(D[i-1], D[i-2]+A[i])
            Dn1[i] = max(Dn1[i-1], Dn1[i-2]+A[i])
            
        print D
        print Dn1
        print
        return max(D[L-2], Dn1[L-1])


if __name__ == "__main__":
    import sys
    S = Solution()
    H4 = [4,1,2,7,5,3,1]
    print S.robshit(H4)
    sys.exit(0)

    H0 = [2]
    print S.rob(H0)

    H0 = [1,4]
    print S.rob(H0)

    H1 = [3,2,1,5,6,4]
    print S.rob(H1)

    H2 = [3,2,1,5,6,4,7]
    print S.rob(H2)

    H3 = [2,1,1,2]
    print S.rob(H3)
