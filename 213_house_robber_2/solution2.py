# [2,1,2,1,2]
#  2 2 4 4
#    1 2 2 4

class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        L = len(nums)
        if L == 0:
            return 0
        elif L == 1:
            return nums[0]
        elif L == 2:
            return max(nums[0], nums[1])
        
        DP1 = [0 for _ in xrange(L-1)]
        DP2 = [0 for _ in xrange(L-1)]
        DP1[0], DP1[1] = nums[0], max(nums[0], nums[1])
        DP2[0], DP2[1] = nums[1], max(nums[1], nums[2])
        
        for i in xrange(2, L-1):
            DP1[i] = max(DP1[i-1], DP1[i-2]+nums[i])
            DP2[i] = max(DP2[i-1], DP2[i-2]+nums[i+1])
        
        return max(DP1[-1], DP2[-1])
