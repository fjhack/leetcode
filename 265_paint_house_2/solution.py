import sys

class Solution(object):
    def minCostII(self, costs):
        """
        :type costs: List[List[int]]
        :rtype: int
        """
        L = len(costs)
        if L == 0:
            return 0
        elif L == 1:
            return min(costs[0])

        k = len(costs[0])
        DP = [[0 for _ in xrange(k)] for _ in xrange(L)]
        
        for i in xrange(k):
            DP[0][i] = costs[0][i]

        for i in xrange(1, L):
            for j in xrange(k):
                premin = sys.maxint
                for m in xrange(k):
                    if m == j:
                        continue
                    premin = min(premin, DP[i-1][m])
                DP[i][j] = costs[i][j] + premin

        return min(DP[-1])
