class Solution(object):
    def isSubsequence(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        Ls, Lt = len(s), len(t)
        if Ls == 0:
            return True
        if Lt == 0:
            return False
        dp = [-1 for _ in xrange(Lt)]
        ps, maxmatch = 0, -1
        for i in xrange(Lt):
            if t[i] == s[ps]:
                maxmatch = ps
                ps += 1
            dp[i] = maxmatch
            if ps == Ls:
                return True
        return dp[-1] == Ls-1
