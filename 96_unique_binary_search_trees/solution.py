import unittest

class Solution(object):
    def numTrees(self, n):
        """
        :type n: int
        :rtype: int

        f(1): 1
        f(2): 1+1 = 2
        f(3): f(2) + f(1) + f(2)
              2+1+2 = 5
        f(4): f(3) + f(2) + f(2) + f(3)
              5+(2)+(2)+5 = 14
        f(5): f(4) + f(3) + (f(2) + f(2)) + f(3) + f(4)
              
        """
        f = [0,1,2]
        if n < 3:
            return f[n]
        for v in xrange(3, n+1):
            s = 0
            for i in xrange(1, v+1):
                lc, rc = i-1, v-i
                print "{0}: f({1})={2}, f({3})={4}".format(v, lc, f[lc], rc, f[rc])
                if f[rc] == 0:
                    s += f[lc]
                elif f[lc] == 0:
                    s += f[rc]
                else:
                    s += (f[rc] * f[lc])
            f.append(s)
        return f[-1]

if __name__ == "__main__":
    S = Solution().numTrees

    print S(3)
    print S(6)
