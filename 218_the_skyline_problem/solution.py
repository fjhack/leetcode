import heapq
import unittest

from hashheap import HashHeap

# Using hash heap
class SolutionHH(object):
    def getSkyline(self, buildings):
        """
        :type buildings: List[List[int]]
        :rtype: List[List[int]]
        """
        L = len(buildings)
        if L == 0:
            return []
        startp = {b[0]:b[2] for b in buildings}
        endp = {b[1]:b[2] for b in buildings}
        points = sorted(list(set([b[0] for b in buildings] + [b[1] for b in buildings])))

        print points
        # print startp
        # print endp
        
        bheap = HashHeap()
        results = []
        lastb = max(buildings, key=lambda x:x[1])
        
        bheap.add(0, 0)

        for pos in points:
        # for pos in xrange(lastb[1]+1):
            if pos in startp:
                height = startp[pos]
                if height > bheap.max():
                    results.append([pos, height])
                bheap.add(height, height)
            if pos in endp:
                height = endp[pos]
                orig_max = bheap.max()
                print height, orig_max
                oldb = bheap.remove(height)
                if height == orig_max:
                    cmax = bheap.max()
                    results.append([pos, cmax])

        return results

class SolutionHQ(object):
    def getSkyline(self, buildings):
        """
        :type buildings: List[List[int]]
        :rtype: List[List[int]]
        """
        L = len(buildings)
        positions = sorted(list(set([b[0] for b in buildings] + [b[1] for b in buildings])))
        bheap = []
        results = [[-1, 0]]
        bidx = 0
        for pos in positions:
            while bidx < L and buildings[bidx][0] <= pos:
                heapq.heappush(bheap, (-buildings[bidx][2], buildings[bidx][1]))
                bidx += 1
            while len(bheap) > 0 and bheap[0][1] <= pos:
                heapq.heappop(bheap)
            
            cheight = 0
            if len(bheap) > 0:
                cheight = -bheap[0][0]
            if cheight != results[-1][1]:
                results.append([pos, cheight])

        return results[1:]

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = SolutionHQ()

    def test_e_0(self):
        print
        print "Test 0:"
        buildings = [ [2, 9, 10], [3, 7, 15], [5, 12, 12], [15, 20, 10], [19, 24, 8] ]
        R = self.S.getSkyline(buildings)
        print R

    def test_e_1_dup(self):
        print
        print "Test 1: Duplicate start-end"
        buildings = [ [2, 9, 10], [3, 7, 15], [5, 12, 12], [12,13,14], [15, 20, 10], [19, 24, 8] ]
        R = self.S.getSkyline(buildings)
        print R

    def test_e_2(self):
        print
        print "Test 1: Duplicate start-end"
        buildings = [[0,2,3],[2,5,3]]
        R = self.S.getSkyline(buildings)
        print R

if __name__ == "__main__":
    unittest.main()
