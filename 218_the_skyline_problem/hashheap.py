class HashHeap:
    
    def __init__(self):
        self.heap = [0]
        self.hash = {}
        
    def add(self, key, value):
        print "HashHeap DEBUG: adding", key, value
        print "HashHeap DEBUG: self.heap:", self.heap
        print "HashHeap DEBUG: self.hash:", self.hash

        self.heap.append((key, value))
        self.hash[key] = self.heap[0] + 1
        self.heap[0] += 1
        self._siftup(self.heap[0])
        
    def remove(self, key):
        print "HashHeap DEBUG: removing", key
        print "HashHeap DEBUG: self.heap:", self.heap
        print "HashHeap DEBUG: self.hash:", self.hash
        index = self.hash[key]
        self._swap(index, self.heap[0])
        del self.hash[self.heap[self.heap[0]][0]]
        self.heap.pop()
        self.heap[0] -= 1
        if index <= self.heap[0]:
            index = self._siftup(index)
            self._siftdown(index)
        
    def hasKey(self, key):
        return key in self.hash
        
    def max(self):
        return 0 if self.heap[0] == 0 else self.heap[1][1]
    
    def _swap(self, a, b):
        self.heap[a], self.heap[b] = self.heap[b], self.heap[a]
        self.hash[self.heap[a][0]] = a
        self.hash[self.heap[b][0]] = b
        
    def _siftup(self, index):
        while index != 1:
            if self.heap[index][1] <= self.heap[index / 2][1]:
                break
            self._swap(index, index / 2)
            index = index / 2
        return index
        
    def _siftdown(self, index):
        size = self.heap[0]
        while index < size:
            t = index
            if index * 2 <= size and self.heap[t][1] < self.heap[index * 2][1]:
                t = index * 2
            if index * 2 + 1 <= size and self.heap[t][1] < self.heap[index * 2 + 1][1]:
                t = index * 2 + 1
            if t == index:
                break
            self._swap(index, t)
            index = t
        return index
