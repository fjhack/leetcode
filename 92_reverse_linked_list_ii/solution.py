
import unittest

class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

def buildList(A):
    if len(A) == 0:
        return None
    else:
        curr = ListNode(A[0])
        curr.next = buildList(A[1:])
        return curr

def printList(L):
    if L.next == None:
        print L.val
        return
    else:
        print L.val, '->',
        printList(L.next)
        return

class Solution(object):
    def reverseBetween(self, head, m, n):
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        walk_ptr = head
        walk_cntr = 1
        S = []
        last_before = None
        new_head = head
    
        while walk_ptr is not None:
            if walk_cntr == (m-1):
                # reached last element before reverse zone
                last_before = walk_ptr
            elif walk_cntr >= m and walk_cntr < n:
                S.append(walk_ptr)
            elif walk_cntr == n:
                S.append(walk_ptr)
                first_after = walk_ptr.next
                while len(S) > 0:
                    curr_node = S.pop()
                    print curr_node.val
                    if (last_before is None):
                        new_head = curr_node
                    else:
                        last_before.next = curr_node
                    last_before = curr_node
                curr_node.next = first_after
            walk_cntr += 1
            walk_ptr = walk_ptr.next
        
        return new_head

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [i for i in xrange(1,6)]
        L = buildList(A)
        printList(L)
        newL = self.S.reverseBetween(L, 2, 4)
        printList(newL)

    def test_2(self):
        A = [i for i in xrange(1,8)]
        L = buildList(A)
        printList(L)
        newL = self.S.reverseBetween(L, 3, 6)
        printList(newL)

    def test_3(self):
        A = [i for i in xrange(1,8)]
        L = buildList(A)
        printList(L)
        newL = self.S.reverseBetween(L, 3, 7)
        printList(newL)

    def test_4(self):
        A = [i for i in xrange(1,8)]
        L = buildList(A)
        printList(L)
        newL = self.S.reverseBetween(L, 1, 4)
        printList(newL)

if __name__ == "__main__":
    unittest.main()
