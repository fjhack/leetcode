class Solution(object):
    def getPermutation(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: str
        """
        result = []
        if n < 1 or n > 9:
            return ''
        digits = [str(i) for i in xrange(1,n+1)]
        # p_nums: array for factorial values
        p_nums = [1]
        _p = 2
        for i in xrange(1, n+1):
            p_nums.append(p_nums[-1]*i)
            
        print digits
        print p_nums
        k2 = k
        for i in xrange(n):
            d = (k2-1) / p_nums[n-i-1]
            result.append(digits[d])
            digits.remove(digits[d])
            k2 = (k2-1) % p_nums[n-i-1] + 1
            
        return "".join(result)
    
if __name__ == "__main__":
    Sgp = Solution().getPermutation
    print Sgp(4,9)

