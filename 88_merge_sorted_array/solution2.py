class Solution(object):
    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: void Do not return anything, modify nums1 in-place instead.
        """
        p1, p2 = m-1, n-1
        v1, v2 = nums1[p1], nums2[p2]
        pt = m+n-1
        while p1 >= 0 and p2 >= 0:
            if nums1[p1] > nums2[p2]:
                nums1[pt] = nums1[p1]
                pt -= 1
                p1 -= 1
            else:
                nums1[pt] = nums2[p2]
                pt -= 1
                p2 -= 1

        print nums1, p1, p2
        
        while p1 >= 0:
            nums1[pt] = nums1[p1]
            pt -= 1
            p1 -= 1

        while p2 >= 0:
            nums1[pt] = nums2[p2]
            pt -= 1
            p2 -= 1

        
if __name__ == "__main__":
    S = Solution()
    
    n1 = [4,5,6,0,0,0]
    m = 3
    n2 = [1,2,3]
    n = 3

    S.merge(n1, m, n2, n)
    print n1
