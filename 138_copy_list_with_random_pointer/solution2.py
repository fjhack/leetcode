import unittest

# Definition for singly-linked list with a random pointer.
# class RandomListNode(object):
#     def __init__(self, x):
#         self.label = x
#         self.next = None
#         self.random = None

class Solution(object):
    def copyRandomList(self, head):
        """
        :type head: RandomListNode
        :rtype: RandomListNode
        """
        newdummy = RandomListNode(-1)
        oldptr, newptr = head, newdummy
        old2new = dict()
        
        while oldptr is not None:
            newptr.next = RandomListNode(oldptr.label)
            old2new[oldptr] = newptr.next
            if oldptr.random in old2new:
                newptr.next.random = old2new[oldptr.random]
            newptr = newptr.next
            oldptr = oldptr.next
            
        oldptr, newptr = head, newdummy.next
        
        while oldptr is not None and newptr is not None:
            if newptr.random is None and oldptr.random is not None:
                newptr.random = old2new[oldptr.random]
            oldptr, newptr = oldptr.next, newptr.next
        
        return newdummy.next
