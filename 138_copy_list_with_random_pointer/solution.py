# Definition for singly-linked list with a random pointer.
class RandomListNode(object):
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None

class Solution(object):
    def copyRandomList(self, head):
        """
        :type head: RandomListNode
        :rtype: RandomListNode
        """
        refmap = dict()
        dummy = RandomListNode(-1)
        cptr = dummy
        optr = head
        
        while optr is not None:
            _new = RandomListNode(optr.label)
            if optr.random in refmap:
                _new.random = refmap[optr.random]
            refmap[optr] = _new
            cptr.next = _new
            cptr = cptr.next
            optr = optr.next
            
        cptr = dummy.next
        optr = head
        while optr is not None:
            if cptr.random is None and optr.random is not None:
                cptr.random = refmap[optr.random]
            cptr = cptr.next
            optr = optr.next
        
        return dummy.next

