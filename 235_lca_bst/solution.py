        
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def LCA(self, root, p, q):
        if p.val < root.val and root.val < q.val:
            return root
        elif q.val < root.val:
            return self.LCA(root.left, p, q)
        elif q.val == root.val:
            return root
        elif root.val < p.val:
            return self.LCA(root.right, p, q)
        elif root.val == p.val:
            return root
            
    def lowestCommonAncestor(self, root, p, q):
        """
        :type root: TreeNode
        :type p: TreeNode
        :type q: TreeNode
        :rtype: TreeNode
        """
        if p.val > q.val:
            p, q = q, p
        return self.LCA(root, p, q)
