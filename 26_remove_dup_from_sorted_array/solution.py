class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        pold, pnew = 0, 0
        L = len(nums)
        if L == 0:
            return 0
        for pold in xrange(1,L):
            if nums[pnew] == nums[pold]:
                continue
            pnew += 1
            nums[pnew] = nums[pold]

        return pnew+1
