import unittest

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
        
class Solution:
    """
    @param root: The root of binary tree.
    @return: Inorder in ArrayList which contains node values.
    """
    def __init__(self):
        self._result = []
        
    def ITR(self, root):
        if root is None:
            return
        else:
            self.ITR(root.left)
            self._result.append(root.val)
            self.ITR(root.right)

    def ITRS(self, root, S=[]):
        if root is None:
            print " "*len(S), "E", S
            return
        else:
            print " "*len(S), "P", S, root.val
            self.ITR(root.left, S+[root.val])
            self._result.append(root.val)
            self.ITR(root.right, S+[root.val])
            print " "*len(S), "F", self._result

    def ITI(self, root):
        stk = []
        top = root
        while top is not None or len(stk) > 0:
            print [n.val for n in stk]
            while top is not None:
                stk.append(top)
                top = top.left
                print "\t", [n.val for n in stk]
            top = stk.pop()
            print "Adding", top.val, "to stack:", self._result
            self._result.append(top.val)
            top = top.right

    def inorderTraversal(self, root, R = True):
        # write your code here
        self._result = []
        if R:
            self.ITR(root)
            return self._result
        else:
            self.ITI(root)
            return self._result

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        root = TreeNode(4)
        root.left = TreeNode(2)
        root.left.left = TreeNode(1)
        root.left.right = TreeNode(3)
        root.right = TreeNode(8)
        root.right.left = TreeNode(5)
        root.right.left.right = TreeNode(7)
        root.right.right = TreeNode(11)
        R = self.S.inorderTraversal(root, False)
        print R
        
if __name__ == "__main__":
    unittest.main()
