class Solution(object):

    def inorderTraversalR(self, root):
        # write your code here
        # Recursive version
        result = []
        def walk(root, results):
            if root is not None and root.left is not None:
                walk(root.left, results)
            if root is not None:
                results.append(root.val)
            if root is not None and root.right is not None:
                walk(root.right, results)
                
        walk(root, result)
        return result
        
    def inorderTraversal(self, root):
        # write your code here
        stk, result = [], []
        curr = root
        
        while curr is not None or len(stk) != 0:
            while curr is not None:
                stk.append(curr)
                curr = curr.left
            curr = stk.pop()
            result.append(curr.val)
            curr = curr.right
            
        return result
