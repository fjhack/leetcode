def decodeStr(s):
    L = len(s)
    result = ""
    pos, in_compressed = 0, False
    decomp, repeat = "", 0
    stack = []
    c = 0
    while pos < L:
        c += 1
        if c > 50:
            break
        print s[pos], in_compressed, s[pos-1], decomp, stack
	if s[pos].isdigit() and not in_compressed:
	    buf = ""
	    while s[pos].isdigit():
		buf += s[pos]
		pos += 1
	    repeat = int(buf)
	    if s[pos] == '[':
		in_compressed = True
		pos += 1
		continue
	if not in_compressed and s[pos].isalpha():
	    result += s[pos]
	    pos += 1
	    continue
	if in_compressed and s[pos] == ']':
            if len(stack) == 0:
		in_compressed = False
		result += decomp * repeat
		decomp = ""
		pos += 1
	    else:
		# Restore progress from stack
		stkitem = stack.pop()
		last_decomp, last_repeat = stkitem[0], stkitem[1]
		last_decomp = last_decomp + (decomp * repeat)
		decomp, repeat = last_decomp, last_repeat
                pos += 1
	    continue
	if in_compressed:
            print "DERP", s[pos]
	    if s[pos].isalpha():
		decomp += s[pos]
		pos += 1
		continue
	    elif s[pos].isdigit():
		# Find nested compressed section
                print "DERP"
		buf = ""
		while s[pos].isdigit():
		    buf += s[pos]
		    pos += 1
		last_repeat = repeat
		repeat = int(buf)
                print "\t", repeat
		if s[pos] == '[':
		    stack.append((decomp, last_repeat))
		    decomp = ""
		    pos += 1
		    continue
    return result

if __name__ == "__main__":
    s = "3[abc]dd4[mn]"
    # print decodeStr(s)

    s = "3[ab5[ve]c]dd4[mn]"
    print decodeStr(s)

