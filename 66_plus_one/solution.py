class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        L = len(digits)
        c = 1
        for i in xrange(L-1, -1, -1):
            r = digits[i] + c
            if r >= 10:
                c = 1
                digits[i] = r - 10
            else:
                c = 0
                digits[i] = r
        if c > 0:
            digits.insert(0, c)
        return digits
