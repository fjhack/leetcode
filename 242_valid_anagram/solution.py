class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        ds, dt = {}, {}
        for c in s:
            if c in ds:
                ds[c] += 1
            else:
                ds[c] = 1
        for c in t:
            if c in dt:
                dt[c] += 1
            else:
                dt[c] = 1
        if len(ds) != len(dt):
            return False
        for c, o in ds.iteritems():
            if c not in dt:
                return False
            elif ds[c] != dt[c]:
                return False
        return True
