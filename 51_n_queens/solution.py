import unittest

class Solution(object):

    def validGrid(self, grid):
        g2 = [r for r in grid if r != -1]
        rows = len(g2)
        uniq_rows = list(set(g2))
        if len(uniq_rows) != rows:
            return False
        for i in xrange(rows-1):
            for j in xrange(i+1, rows):
                if abs(g2[j] - g2[i]) == abs(j-i):
                    return False
        return True

    def buildResult(self, grid):
        return [''.join(['Q' if c == r else '.' for c in xrange(len(grid))]) for r in grid]
        
    def solver(self, rownum, grid, results):
        validated = self.validGrid(grid)
        if (rownum == len(grid)) and (validated):
            results.append(self.buildResult([r for r in grid]))
            return
        elif validated and rownum < len(grid):
            for r in xrange(len(grid)):
                grid[rownum] = r
                self.solver(rownum + 1, grid, results)
                grid[rownum] = -1
        else:
            return
    
    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        
        # -1: not yet placed queen in current row
        # 0 to n: column of queen placed in current row
        grid = [-1 for _ in xrange(n)]
        results = []
        rn = 0
        self.solver(rn, grid, results)
        return results

    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.SvG = self.S.validGrid
        self.snq = self.S.solveNQueens

    def test_SvG_1(self):
        G1 = [1, 3, 0, 2]
        G2 = [2, 0, 3, 1]
        G3 = [2, 0]
        print self.SvG(G1)
        print self.SvG(G2)
        print self.SvG(G3)

    def test_SvG_2(self):
        Giv1 = [1, 3, 2, 0]
        print self.SvG(Giv1)

    def test_svnq_1(self):
        print self.snq(0)
        print self.snq(1)
        print self.snq(2)
        print self.snq(3)
        print self.snq(4)
        
if __name__ == "__main__":
    unittest.main()
    
