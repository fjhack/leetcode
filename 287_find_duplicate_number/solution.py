        
class Solution(object):
    def findDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        l, r = 1, max(nums)
        while l<r:
            m = l + (r-l) / 2
            c1 = 0
            for v in nums:
                if v <= m:
                    c1 += 1
            if c1 > m:
                r = m
            else:
                l = m+1
        `return r
