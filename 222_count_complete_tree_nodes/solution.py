# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def countNodes0(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if root is None:
            return 0
        elif root.left is None and root.right is None:
            return 1
        elif root.left is not None and root.right is None:
            return self.countNodes(root.left) + 1
        else:
            return self.countNodes(root.left) + self.countNodes(root.right) + 1
            
    def lheight(self, node):
        if node is None:
            return 0
        else:
            return self.lheight(node.left) + 1
    
    def countNodes(self, root):
        if root is None:
            return 0
        lh, rh = self.lheight(root.left), self.lheight(root.right)
        if lh == rh:
            return 2 ** rh + self.countNodes(root.right)
        else:
            return 2 ** rh + self.countNodes(root.left)
        
