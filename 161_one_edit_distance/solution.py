class Solution(object):
    def check_1more(self, s, t):
        ls, lt = len(s), len(t)
        if ls < lt:
            s1, s2 = s, t
            l1, l2 = ls, lt
        else:
            s1, s2 = t, s
            l1, l2 = lt, ls
        
        i1, i2 = 0, 0
        diffcount = 0
        while i1 < l1 and i2 < l2:
            if s1[i1] == s2[i2]:
                i1, i2 = i1+1, i2+1
            else:
                if diffcount == 0:
                    i2 += 1
                    diffcount += 1
                else:
                    return False
        return True
    
    def check_1diff(self, s, t):
        L, diffcount = len(s), 0
        for i in xrange(L):
            if s[i] != t[i]:
                diffcount += 1
            if diffcount > 1:
                return False
        if diffcount == 1:
            return True
        else:
            return False

    def isOneEditDistance(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        len_s, len_t = len(s), len(t)
        if abs(len_s - len_t) > 1:
            return False
        elif abs(len_s - len_t) == 1:
            return self.check_1more(s, t)
        elif len_s == len_t:
            return self.check_1diff(s, t)
        
