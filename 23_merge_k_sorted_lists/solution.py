import heapq
import unittest

# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

def list2LL(L):
    head = ListNode(L[0])
    ptr = head
    for i in xrange(1, len(L)):
        ptr.next = ListNode(L[i])
        ptr = ptr.next
    return head

def printLL(LL):
    head = LL
    while head is not None:
        print head.val, '->', 
        head = head.next
    print

class Solution(object):
    # Build a new list
    def mergeKListsNew(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        ps = [(ptr.val, ptr) for ptr in lists if ptr is not None]
        heapq.heapify(ps)

        head = ListNode(0)
        ptr = head

        while len(ps) > 0:
            _p  = heapq.heappop(ps)
            # print _p[0], _p[1]
            ptr.next = ListNode(_p[0])
            if _p[1].next is not None:
                heapq.heappush(ps, (_p[1].next.val, _p[1].next))
            # print ps
            # heapq.heapify(ps)
            ptr = ptr.next

        return head.next

    # Merge in-place using original list nodes
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        ptrs = [(ptr.val, ptr) for ptr in lists if ptr is not None]
        heapq.heapify(ptrs)
        
        dummy = ListNode(-1)
        bptr = dummy
        
        while len(ptrs) > 0:
            _p = heapq.heappop(ptrs)
            curr = _p[1]
            cnext = curr.next
            bptr.next = curr
            bptr = bptr.next
            if cnext is not None:
                heapq.heappush(ptrs, (cnext.val, cnext))
                
                
        return dummy.next
    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A1 = [ 2,13,5,8,4 ]
        A2 = [ 9,3,6,1,11 ]
        A3 = [ 4,10,7,12,2 ]
        Ls = [list2LL(sorted(A1)), list2LL(sorted(A2)), list2LL(sorted(A3))]
        RLL = self.S.mergeKLists(Ls)
        printLL(RLL)

if __name__ == "__main__":
    unittest.main()
