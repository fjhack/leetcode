class ZigzagIterator(object):

    def __init__(self, v1, v2):
        """
        Initialize your data structure here.
        :type v1: List[int]
        :type v2: List[int]
        """
        self._v1, self._v2 = v1, v2
        self._l1, self._l2 = len(v1), len(v2)
        self._currv = 1
        self._v1p, self._v2p = 0, 0

    def next(self):
        """
        :rtype: int
        """
        if not self.hasNext():
            return
        
        if self._currv == 1:
            if self._v1p < self._l1:
                retval = self._v1[self._v1p]
            else:
                self._currv = 2
                return self.next()
            if self._v2p < self._l2:
                self._currv = 2
            self._v1p += 1
        else:
            if self._v2p < self._l2:
                retval = self._v2[self._v2p]
            else:
                self._currv = 1
                return self.next()
            if self._v1p < self._l1:
                self._currv = 1
            self._v2p += 1
        return retval

    def hasNext(self):
        """
        :rtype: bool
        """
        return self._v1p < self._l1 or self._v2p < self._l2


# Your ZigzagIterator object will be instantiated and called as such:
# i, v = ZigzagIterator(v1, v2), []
# while i.hasNext(): v.append(i.next())
