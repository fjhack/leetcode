import unittest

class Solution(object):
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        L = len(matrix)
        r0, c0 = 0, 0
        r1, c1 = L-1, L-1
        while r1 > r0 and c1 > c0:
            rows, cols = [r0, r0, r1, r1], [c0, c1, c1, c0]
            count = 0
            while count < r1-r0:
                t = matrix[rows[3]][cols[3]]
                matrix[rows[3]][cols[3]] = matrix[rows[2]][cols[2]]
                matrix[rows[2]][cols[2]] = matrix[rows[1]][cols[1]]
                matrix[rows[1]][cols[1]] = matrix[rows[0]][cols[0]]
                matrix[rows[0]][cols[0]] = t
                
                cols[0] += 1
                rows[1] += 1
                cols[2] -= 1
                rows[3] -= 1
                count += 1

            r0, c0 = r0+1, c0+1
            r1, c1 = r1-1, c1-1

        
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        M = [[v for v in xrange(rowstart, rowstart+5)] for rowstart in xrange(1,22,5)]
        print "Before rotate:"
        for row in M:
            print row
        print
        self.S.rotate(M)
        print "After rotate:"
        for row in M:
            print row

if __name__ == "__main__":
    unittest.main()
