import unittest

class Solution(object):
    def maxNumber(self, nums1, nums2, k1):
        def prep(nums, k):
            drop = len(nums) - k
            out = []
            for n in nums:
                while drop and out and out[-1] < n:
                    out.pop()
                    drop -= 1
                out.append(n)
            return out[:k]

        def merge(a, b):
            return [max(a, b).pop(0) for _ in a+b]

        return max(merge(prep(nums1, i), prep(nums2, k1-i)) for i in xrange(k1+1) if i <= len(nums1) and k1-i <= len(nums2))

class TestSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        nums1 = [3, 4, 6, 5]
        nums2 = [9, 1, 2, 5, 8, 3]
        k = 5
        R = self.S.maxNumber(nums1, nums2, k)
        print R

    def test_2(self):
        nums1 = [6, 7]
        nums2 = [6, 0, 4]
        k = 5
        R = self.S.maxNumber(nums1, nums2, k)
        print R

    def test_3(self):
        nums1 = [3, 9]
        nums2 = [8, 9]
        k = 3
        R = self.S.maxNumber(nums1, nums2, k)
        print R

if __name__ == "__main__":
    unittest.main()
