class Solution(object):
    def maxNumber(self, nums1, nums2, k1):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :type k: int
        :rtype: List[int]
        """
        L1, L2 = len(nums1), len(nums2)
        DP = [['' for _ in xrange(L2+1)] for _ in xrange(L1+1)]

        for i in xrange(1, L1+1):
            for j in xrange(i):
                DP[i][0] = max(DP[i][0], DP[j][0]+str(nums1[i-1]))

        for i in xrange(1, L2+1):
            for j in xrange(i):
                DP[0][i] = max(DP[0][i], DP[0][j]+str(nums2[i-1]))

        # for r in DP:
        #     print r

        for i in xrange(1, L1+1):
            for j in xrange(1, L2+1):
                # if i == L1 and j == L2:
                #     print DP[i-1][j], DP[i][j-1]

                for k in xrange(i):
                    # if i == L1 and j == L2:
                    #     print DP[k][j] # +str(nums1[i-1])
                    DP[i][j] = max(DP[i][j], DP[k][j]+str(nums1[i-1]))

                for k in xrange(j):
                    # if i == L2 and j == L2:
                    #     print DP[i][k] # +str(nums2[j-1])
                    DP[i][j] = max(DP[i][j], DP[i][k]+str(nums2[j-1]))

        # for r in DP:
        #     print r

        return int(DP[L1][L2][:k1])


import unittest

class TestSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        nums1 = [3, 4, 6, 5]
        nums2 = [9, 1, 2, 5, 8, 3]
        k = 5
        R = self.S.maxNumber(nums1, nums2, k)
        print R

    def test_2(self):
        nums1 = [6, 7]
        nums2 = [6, 0, 4]
        k = 5
        R = self.S.maxNumber(nums1, nums2, k)
        print R

    def test_3(self):
        nums1 = [3, 9]
        nums2 = [8, 9]
        k = 3
        R = self.S.maxNumber(nums1, nums2, k)
        print R

if __name__ == "__main__":
    unittest.main()
