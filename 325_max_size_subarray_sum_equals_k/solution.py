import unittest

class Solution(object):
    def maxSubArrayLen0(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        pfsum_pos = {0: [0]}
        pfsums = [0]
        pfsum = 0
        L = len(nums)
        if L == 0:
            return 0
        for i in xrange(L):
            pfsum += nums[i]
            if pfsum in pfsum_pos:
                pfsum_pos[pfsum].append(i)
            else:
                pfsum_pos[pfsum] = [i]
            pfsums.append(pfsum)

        # print pfsums
        # print pfsum_pos
        # print
        
        max_len = 0
        for i in xrange(L+1):
            # print "i={0}, pfsums[i]={1}".format(i, pfsums[i])
            diff_sum = pfsums[i] + k
            if diff_sum in pfsum_pos:
                print "prefix sum at position {0}: {1}, prefix sum positions equals {2}".format(i, pfsums[i], pfsum_pos[diff_sum])
                for p in pfsum_pos[diff_sum]:
                    curr_len = p - i + 1
                    max_len = max(max_len, curr_len)
                    print nums[i:p+1]
        return max_len
                    
    def maxSubArrayLen(self, nums, k):
        prefix_sum = 0
        prefix_sums = {0: -1}
        L = len(nums)
        result = 0
        for i in xrange(L):
            prefix_sum += nums[i]
            if prefix_sum not in prefix_sums:
                prefix_sums[prefix_sum] = i
            if prefix_sum-k in prefix_sums:
                result = max(result, i-prefix_sums[prefix_sum-k])
        return result
                
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        print "Test 1"
        nums = [1,-1,5,-2,3]
        k = 3
        R = self.S.maxSubArrayLen(nums, k)
        print R

    def test_2(self):
        print "Test 2"
        nums = [-2,-1,2,1]
        k = 1
        R = self.S.maxSubArrayLen(nums, k)
        print R

    def test_n_2(self):
        print "Test n2"
        nums = [-1]
        k = -1
        R = self.S.maxSubArrayLen(nums, k)
        print R

    def test_n_1(self):
        print "Test n1"
        nums = [1]
        k = 0
        R = self.S.maxSubArrayLen(nums, k)
        print R

if __name__ == "__main__":
    unittest.main()
