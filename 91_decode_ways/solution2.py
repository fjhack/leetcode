import unittest

class Solution(object):
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        L = len(s)
        if L == 0:
            return 0
        elif s[0] == '0':
            return 0
        elif L == 1:
            return 1

        n = [0 for _ in xrange(L+1)]
        n[0] = 1
        n[1] = 1

        for i in xrange(1, L):
            if s[i] == '0':
                if s[i-1] == '1' or s[i-1] == '2':
                    n[i+1] = n[i-1]
                else:
                    return 0
            elif int(s[i]) >= 1 and int(s[i]) <= 9:
                l, l2 = int(s[i]), int(s[i-1:i+1])
                if l2 > 10 and l2 <= 26:
                    n[i+1] = n[i] + n[i-1]
                else:
                    n[i+1] = n[i]
        return n[-1]

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def runtest(self, s, n):
        print "\nTest {0}:".format(n)
        print s
        R = self.S.numDecodings(s)
        print R

    def test_a(self):
        self.runtest('123', 1)
        self.runtest('204', 2)
        self.runtest('120', 3)
        self.runtest('1320', 4)
        self.runtest('2190', 5)
        self.runtest('1210', 6)
        self.runtest('1342', 7)
        self.runtest('13205', 8)


if __name__ == "__main__":
    unittest.main()
