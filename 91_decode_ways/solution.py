import unittest


class Solution(object):
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        L = len(s)
        if L == 0:
            return 0
        elif s[0] == '0':
            return 0
        elif L == 1:
            return 1
        else:
            r = [0 for _ in xrange(L)]
            r[0] = 1
            if int(s[0:2]) == 10 or int(s[0:2]) == 20:
                r[1] = 1
            elif int(s[0:2]) <= 26 and int(s[0:2]) > 10:
                r[1] = 2
            elif int(s[1]) != 0:
                r[1] = 1
            else:
                print "derp"
                return 0

            for i in xrange(2, L):
                if int(s[i-1:i+1]) == 10 or int(s[i-1:i+1]) == 20:
                    r[i] = r[i-2]
                elif int(s[i-1:i+1]) <= 26 and int(s[i-1:i+1]) > 10:
                    r[i] = r[i-1] + r[i-2]
                elif int(s[i]) != 0:
                    r[i] = r[i-1]
                else:
                    return 0
            print s, r
            print
            return r[-1]

    def numDecodings2(self, s):
        # Write your code here
        if not s.isdigit():
            return 0
        L = len(s)
        if L == 0:
            return 0
        elif s[0] == '0':
            return 0
        elif L == 1:
            return 1
        elif '0' in s:
            return 0
        
        r = [1 for _ in xrange(L)]
        for i in xrange(1, L):
            prev2 = int(s[i-1:i+1])
            if prev2 == 10 or prev2 == 20:
                r[i] = r[i-1]
            elif prev2 > 10 and prev2 <= 26:
                r[i] = r[i-1] + 1
            else:
                r[i] = r[i-1]
        return r[-1]

if __name__ == "__main__":
    SnD = Solution().numDecodings2
    print SnD("10")
    
    print SnD("110")
    print SnD("301")
        
    print SnD("12120")
