import unittest
import math

class Solution(object):
    def __init__(self):
        self.Q = None

    def search(self, A, B, currQ, trail):
        if A not in self.Q:
            return None

        nextV = self.Q[A]
        anymatch = False
        for nv in nextV:
            if nv == B and nextV[nv] is not None:
                return currQ * nextV[nv]
            elif nv not in trail and nv != A and nextV[nv] is not None:
                R = self.search(nv, B, currQ*nextV[nv], trail + [nv])
                if R is not None:
                    return R
        return None

    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """
        L = len(equations)
        self.Q = dict()
        V = set([v for E in equations for v in E])

        for var in V:
            self.Q[var] = dict()
            for v2 in V:
                if v2 == var:
                    self.Q[var][v2] = 1.0
                else:
                    self.Q[var][v2] = None

        for i in xrange(L):
            E = equations[i]
            val = values[i]
            self.Q[E[0]][E[1]] = val
            self.Q[E[1]][E[0]] = 1.0 / val

        results = [self.search(qry[0], qry[1], 1.0, []) for qry in queries]
        for i in xrange(len(results)):
            if results[i] is None:
                results[i] = -1.0
        return results            
        

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        equations = [ ["a", "b"], ["b", "c"], ["x", "b"] ]
        values = [2.0, 3.0, 4.0]
        queries = [ ["a", "c"], ["x", "c"], ["x", "a"], ["a", "x"], ["a", "e"] ]
        R = self.S.calcEquation(equations, values, queries)
        print R

    def test_1(self):
        equations = [["x1","x2"],["x2","x3"],["x1","x4"],["x2","x5"]]
        values = [3.0,0.5,3.4,5.6]
        queries = [["x2","x4"],["x1","x5"],["x1","x3"],["x5","x5"],["x5","x1"],["x3","x4"],["x4","x3"],["x6","x6"],["x0","x0"]]
        R = self.S.calcEquation(equations, values, queries)
        print R
        
if __name__ == "__main__":
    unittest.main()
    
