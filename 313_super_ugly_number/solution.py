class Solution(object):
    def nthSuperUglyNumber(self, n, primes):
        """
        :type n: int
        :type primes: List[int]
        :rtype: int
        """
        numprimes = len(primes)
        ugly_nums = [1]
        ugly_pos = [0 for v in primes]

        def _inc(_idx):
            if ugly_nums[ugly_pos[_idx]]*primes[_idx] == min_ugly:
                return ugly_pos[_idx] + 1
            else:
                return ugly_pos[_idx]
            
        while len(ugly_nums) < n:
            _curr_ugly = [ugly_nums[ugly_pos[i]]*primes[i] for i in xrange(numprimes)]
            min_ugly = min(_curr_ugly)
            ugly_nums.append(min_ugly)
            # ugly_pos = map(_inc, range(numprimes))
            
            for i in xrange(numprimes):
                if ugly_nums[ugly_pos[i]]*primes[i] == min_ugly:
                    ugly_pos[i] += 1
                
        return ugly_nums[-1]
        

