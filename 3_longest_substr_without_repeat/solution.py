class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        start, L = 0, len(s)
        fin = start+1
        longest = ""
        while fin < L:
            _s = s[start:fin]
            _first_idx = start
            if len(_s) == 1:
                fin += 1
                if len(_s) > len(longest):
                    longest = _s
                continue
            elif _s[-1] in _s[:-1]:
                _first_idx += _s[:-1].find(_s[-1])
                start = _first_idx + 1
                fin = start + 1
            else:
                if len(_s) > len(longest):
                    longest = _s
                fin += 1
        return longest

    def f(self, s):
        ans = 0
        left = 0
        last = {}
        for i in xrange(len(s)):
            if s[i] in last and last[s[i]] >= left:
                left = last[s[i]] + 1
            last[s[i]] = i
            ans = max(ans, i - left + 1)
        return ans
    
if __name__ == "__main__":
    S = Solution()
    print S.lengthOfLongestSubstring("pwwkew")
    print S.lengthOfLongestSubstring("pwewkaw")
