class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        L = len(nums)
        nums = sorted(nums)
        return nums[L/2]
