# This won't pass large string - O(n^3) time complexity since need duplicated computation for checking palindrome for every substring
class Solution0(object):
    def isPalin(self, s):
        L = len(s)
        for i in xrange(L/2):
            if s[i] != s[L-i-1]:
                return False
        return True

    def minCut(self, s):
        """
        :type s: str
        :rtype: int
        """
        L = len(s)
        F = [i for i in xrange(L+1)]
        for i in xrange(1, L+1):
            for j in xrange(i):
                if self.isPalin(s[j:i]):
                    F[i] = min(F[i], F[j]+1)
        return F[-1]-1
    

class Solution1(object):
    def isPalinMatrix(self, s):
        L = len(s)
        M = [[False for _ in xrange(L)] for _ in xrange(L)]
        for i in xrange(L):
            M[i][i] = True
            
        for i in xrange(L-1):
            M[i][i+1] = (s[i] == s[i+1])

        for length in xrange(2, L):
            for start in xrange(0, L-length):
                M[start][start+length] = (M[start+1][start+length-1] and s[start] == s[start+length])
        
        return M

    def minCut(self, s):
        """
        :type s: str
        :rtype: int
        """
        L = len(s)
        F = [i for i in xrange(L+1)]
        M = self.isPalinMatrix(s)
        
        for i in xrange(1, L+1):
            for j in xrange(i):
                if M[j][i]:
                    F[i] = min(F[i], F[j]+1)
        return F[-1]-1
