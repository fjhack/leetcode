class Solution:
    # @param s: A string s
    # @param dict: A dictionary of words dict
    def wordBreak(self, s, wordDict):
        return self.helper(s, wordDict, {})

    def helper(self, s, wordDict, h):
        if s in h:
            return h[s]
        h[s] = []
        for i in range(len(s)):
            if s[:i+1] in wordDict:
                if i+1 == len(s):
                    h[s].append(s[:i+1])
                else:
                    for rest in self.helper(s[i+1:], wordDict, h):
                        h[s].append(s[:i+1] + ' ' + rest)
        return h[s]
    
    def wordBreak_fullDP(self, s, wordDict):
        # write your code here
        L = len(s)
        F = [False for _ in xrange(L+1)]
        F[0] = True
        if len(wordDict) == 0:
            return len(s) == 0
        
        maxWordLen = max([len(w) for w in wordDict])
        wordCombs = [[] for _ in xrange(L+1)]
        
        for i in xrange(1, L+1):
            minStart = max(0, i-maxWordLen)
            for j in xrange(minStart, i):
                s2 = s[j:i]
                if F[j] and s2 in wordDict:
                    F[i] = True
                    if len(wordCombs[j]) == 0:
                        wordCombs[i].append(s2)
                    else:
                        # print "\t wordCombs At {0}:".format(j), wordCombs[j]
                        # print "\t", [" ".join(wc.split() + [s2]) for wc in wordCombs[j]]
                        # wordCombs[i].extend([" ".join(wc.split() + [s2]) for wc in wordCombs[j]])
                        for wc in wordCombs[j]:
                            wordCombs[i].append(wc + " " + s2)
                    # print i, j, s[0:j], s2
                    # print wordCombs
                    # break
        # print wordCombs
        return wordCombs[-1]

if __name__ == "__main__":
    swb = Solution().wordBreak
    print swb("catsanddog", ["cat", "cats", "and", "sand", "dog"])
