class Solution(object):
    def minCost(self, costs):
        """
        :type costs: List[List[int]]
        :rtype: int
        """
        L = len(costs)
        if L == 0:
            return 0
        elif L == 1:
            return min(costs[0][0], costs[0][1], costs[0][2])

        DP = [[0 for _ in xrange(3)] for _ in xrange(L)]
        DP[0][0] = costs[0][0]
        DP[0][1] = costs[0][1]
        DP[0][2] = costs[0][2]
        
        for i in xrange(1, L):
            DP[i][0] = costs[i][0] + min(DP[i-1][1], DP[i-1][2])
            DP[i][1] = costs[i][1] + min(DP[i-1][0], DP[i-1][2])
            DP[i][2] = costs[i][2] + min(DP[i-1][0], DP[i-1][1])

        return min(DP[-1][0], DP[-1][1], DP[-1][2])
