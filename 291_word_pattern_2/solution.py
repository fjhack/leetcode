
class Solution(object):
    def wpm(self, pattern, string, matched, words):
        # print " "*(14-len(string)), pattern, string, len(pattern), len(string)
        # print " "*(14-len(string)), matched
        
        if len(pattern) == 0:
            if len(string) == 0:
                return True
            else:
                return False
        if pattern[0] in matched:
            # print " "*(14-len(string)), "MATCHED:", matched[pattern[0]]
            if not string.startswith(matched[pattern[0]]):
                return False
            else:
                return self.wpm(pattern[1:], string[len(matched[pattern[0]]):], matched, words)
        else:
            for i in xrange(1, len(string)+1):
                _t = string[:i]
                if _t in words:
                    continue
                else:
                    # print " "*(14-len(string)), "NEXT:", _t
                    matched[pattern[0]] = _t
                    words.add(_t)
                    result = self.wpm(pattern[1:], string[i:], matched, words)
                    if not result:
                        words.discard(_t)
                        matched.pop(pattern[0])
                    else:
                        return result
            return False

    def wordPatternMatch(self, pattern, string):
        """
        :type pattern: str
        :type str: str
        :rtype: bool
        """
        matched = dict()
        words = set()
        return self.wpm(pattern, string, matched, words)

if __name__ == "__main__":
    S = Solution()
    p, s = "abab", "redblueredblue"
    r = S.wordPatternMatch(p, s)
    print r

    p, s = "d", "e"
    r = S.wordPatternMatch(p, s)
    print r

    p, s = "aba", "aaaa"
    r = S.wordPatternMatch(p, s)
    print r
    
