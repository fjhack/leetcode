import unittest

# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

def list2LL(L):
    head = ListNode(L[0])
    ptr = head
    for i in xrange(1, len(L)):
        ptr.next = ListNode(L[i])
        ptr = ptr.next
    return head

def printLL(LL):
    head = LL
    while head is not None:
        print head.val, '->', 
        head = head.next
    print

def nodeStr(node):
    if node is None:
        return "<None>"
    else:
        return "{0} @ {1}".format(node.val, repr(node))

class Solution(object):
    def insertionSortList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        vhead = ListNode(-1)
        vhead.next = head
        prev, curr = vhead, head

        if head is None:
            return head

        p_sorted_tail = head
        sorted_tail = head.next

        counter = 0
        while sorted_tail is not None:
            sorted_prev, sorted_curr = vhead, vhead.next
            inserted = False
            while sorted_tail != sorted_curr:
                # print nodeStr(sorted_curr), nodeStr(sorted_tail)
                if sorted_curr.val > sorted_tail.val:
                    print "Need to insert [", nodeStr(sorted_tail), "]\n  between [", nodeStr(sorted_prev), "] and [", nodeStr(sorted_curr), "]"
                    print "  also relink [", nodeStr(p_sorted_tail), "] to [", nodeStr(sorted_tail.next), "]"

                    # relink node prev of sorted_tail
                    old_p_sorted_tail, old_sorted_tail_next = p_sorted_tail, sorted_tail.next
                    p_sorted_tail.next = sorted_tail.next

                    # insert sorted_tail into the desired position
                    sorted_prev.next = sorted_tail
                    sorted_tail.next = sorted_curr
                    inserted = True
                    p_sorted_tail, sorted_tail = old_p_sorted_tail, old_sorted_tail_next
                    break
                else:
                    sorted_prev, sorted_curr = sorted_curr, sorted_curr.next

            counter += 1

            # just for debugging
            # if counter > 4:
            #     break

            printLL(vhead)
            print nodeStr(sorted_curr), nodeStr(sorted_prev)
            # increment
            if not inserted:
                p_sorted_tail = p_sorted_tail.next
                sorted_tail = sorted_tail.next

            print "  Prev of sorted tail:", nodeStr(p_sorted_tail), "  sorted tail:", nodeStr(sorted_tail)
            
            # Dumb-proof method which could be pretty wasteful
            # p_sorted_tail, sorted_tail = vhead, vhead.next
            # for i in xrange(counter+1):
            #     p_sorted_tail, sorted_tail = sorted_tail, sorted_tail.next

        return vhead.next

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.SiS = self.S.insertionSortList

    def test_1(self):
        A1 = [6,5,3,4,1,8,7,4,2]
        LL1 = list2LL(A1)
        printLL(LL1)
        newLL = self.SiS(LL1)
        print "Result:", 
        printLL(newLL)

    def test_2(self):
        A1 = [3,6,7,4,8]
        LL1 = list2LL(A1)
        printLL(LL1)
        newLL = self.SiS(LL1)
        print "Result:", 
        printLL(newLL)

    def test_3(self):
        A1 = [10,8,6,4,2,1,3,5,7,9]
        LL1 = list2LL(A1)
        printLL(LL1)
        newLL = self.SiS(LL1)
        print "Result:", 
        printLL(newLL)

    def test_4(self):
        A1 = [1,3,5,7,9,2,4]
        LL1 = list2LL(A1)
        printLL(LL1)
        newLL = self.SiS(LL1)
        print "Result:", 
        printLL(newLL)

    def test_5(self):
        A1 = [1,3,2,4]
        LL1 = list2LL(A1)
        printLL(LL1)
        newLL = self.SiS(LL1)
        print "Result:", 
        printLL(newLL)

    def test_6(self):
        print
        print
        A1 = [3,1,2,4]
        LL1 = list2LL(A1)
        printLL(LL1)
        newLL = self.SiS(LL1)
        print "Result:", 
        printLL(newLL)

if __name__ == "__main__":
    unittest.main()
