class Solution(object):
    def __init__(self):
        self.keys = {'2':['a','b','c'],
                     '3':['d','e','f'],
                     '4':['g','h','i'],
                     '5':['j','k','l'],
                     '6':['m','n','o'],
                     '7':['p','q','r','s'],
                     '8':['t','u','v'],
                     '9':['w','x','y','z']
        }
        
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        def getComb(digits, trail, results):
            if len(trail) == len(digits):
                results.append(trail)
            else:
                cc = digits[len(trail)]
                for _c in self.keys[cc]:
                    getComb(digits, trail + _c, results)
        if len(digits) == 0:
            return ""
        results = []
        getComb(digits, "", results)
        return results

if __name__ == "__main__":
    S = Solution()
    SlC = S.letterCombinations
    print SlC('23')
