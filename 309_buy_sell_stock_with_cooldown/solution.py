import unittest

class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        L = len(prices)
        if L < 5:
            return 0
        buy = [0 for _ in xrange(L)]
        sell = [0 for _ in xrange(L)]
        rest = [0 for _ in xrange(L)]
        buy[0] = 0 - prices[0]
        for i in xrange(1,L):
            buy[i] = max(rest[i-1] - prices[i], buy[i-1])
            sell[i] = max(buy[i-1] + prices[i], sell[i-1])
            rest[i] = sell[i-1]
        return max(buy[L-1], sell[L-1], rest[L-1])

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [1, 2, 3, 0, 2]
        print self.S.maxProfit(A)

    def test_2(self):
        A = [2,1,2,0,1]
        print self.S.maxProfit(A)
        
if __name__ == "__main__":
    unittest.main()
