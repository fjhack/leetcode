# Definition for singly-linked list.
import unittest

class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def removeNthFromEnd0(self, head, n):
        """
        :type head: ListNode
        :type n: int
        :rtype: ListNode
        """
        f = head
        s = head

        for i in xrange(n):
            f = f.next

        if f is None:
            return s.next

        while f.next is not None:
            s, f = s.next, f.next

        s.next = s.next.next
        return head

    def removeNthFromEnd(self, head, n):
        # write your code here
        dummy = ListNode(-1)
        dummy.next = head
        ptr1, ptr2 = dummy, head
        offset = 0
        while ptr2 is not None and offset < n:
            ptr2 = ptr2.next
            offset += 1
        if ptr2 is None and offset <= n:
            return head
        
        while ptr2.next is not None:
            ptr2 = ptr2.next
            ptr1 = ptr1.next
            
        ptr1.next = ptr1.next.next
        return dummy.next
    
class TestSolution(unittest.TestCase):
    def setUp(self):
        Lnh = ListNode(1)
        Lnh.next = ListNode(2)
        Lnh.next.next = ListNode(3)
        Lnh.next.next.next = ListNode(4)
        Lnh.next.next.next.next = ListNode(5)
        self.ll1 = Lnh

    def test_0(self):
        S = Solution()
        h = self.ll1
        S.removeNthFromEnd(self.ll1, 2)
        while h is not None:
            print h.val,
            h = h.next
        print

if __name__ == "__main__":
    unittest.main()
