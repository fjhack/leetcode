
class Solution(object):
    def wordPattern(self, pattern, str):
        """
        :type pattern: str
        :type str: str
        :rtype: bool
        """
        Lp = len(pattern)
        strs = str.split()
        Ls = len(strs)
        if Lp != Ls:
            return False

        p2s = dict()
        words = set()
        for i in xrange(Lp):
            if pattern[i] not in p2s:
                if strs[i] in words:
                    return False
                else:
                    p2s[pattern[i]] = strs[i]
                    words.add(strs[i])
            else:
                if strs[i] != p2s[pattern[i]]:
                    return False
        return True
