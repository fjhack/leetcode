import unittest

class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        stk = []
        L = len(s)
        for i in xrange(L):
            if s[i] == '(' or s[i] == '[' or s[i] == '{':
                stk.append(s[i])
            elif s[i] == ')':
                if len(stk) > 0 and stk[-1] == '(':
                    stk.pop()
                else:
                    return False
            elif s[i] == ']':
                if len(stk) > 0 and stk[-1] == '[':
                    stk.pop()
                else:
                    return False
            elif s[i] == '}':
                if len(stk) > 0 and stk[-1] == '{':
                    stk.pop()
                else:
                    return False
        if len(stk) == 0:
            return True
        else:
            return False

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.isValid = self.S.isValid

    def test_1(self):
        s = "()())()"
        R = self.isValid(s)
        print R

    def test_2(self):
        s = "()(aaa())"
        R = self.isValid(s)
        print R

    def test_3(self):
        s = "(a)())()"
        R = self.isValid(s)
        print R

if __name__ == "__main__":
    unittest.main()
