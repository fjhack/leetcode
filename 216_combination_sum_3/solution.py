import unittest

class Solution(object):
    def __init__(self):
        self.results = []
        
    def combinationSum3(self, k, n):
        """
        :type k: int
        :type n: int
        :rtype: List[List[int]]
        """
        if k > 9:
            return []
        c2 = [_ for _ in xrange(1,10)]
        if n > sum(c2[-n:]):
            return []

        self.results = []
        self.csi1(c2, n, k, [])
        return self.results

    def csi1(self, candidates, target, k, rl):
        found = False
        # print rl
        for n in candidates:
            r = target - n
            nc = [c for c in candidates if (c > n and c <= r)]
            nt = r
            if (r == 0) and (len(rl) == (k-1)) :
                found = True
                self.results.append(rl+[n])
            else:
                self.csi1(nc, nt, k, rl + [n])

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        pass

    def test_a_1(self):
        print self.S.combinationSum3(3, 7)

    def test_a_2(self):
        print self.S.combinationSum3(3, 9)


if __name__ == "__main__":
    unittest.main()
