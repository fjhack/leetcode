2: 1+1(1)
3: 2+1(2)

4: 3+1 2+2(4)
5: 2+3(6) (2+1+2)
6: 3+3(9) 2+4 5+1 2+2+2

7: 3+4(12) 3+2+2(12) 
8: 4+4 3+3+2(18)
9: 4+5 3+3+3(27)

10: 5+5 4+3+3(36) 3+3+2+2
11: 5+6 4+4+3(48)
12: 6+6 4+4+4(64)

13: 4+3+3+3(108)
14:
15:

class Solution(object):
    def integerBreak(self, n):
        """
        :type n: int
        :rtype: int
        """
        rs = {2:1, 3:2, 4:4, 5:6, 6:9}
        if n < 7 and n >= 2:
            return rs[n]
        elif n<2:
            return n
        else:
            return 3*self.integerBreak(n-3)


if __name__ == "__main__":
    S = Solution()
    pass
