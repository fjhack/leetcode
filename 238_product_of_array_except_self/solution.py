import unittest

class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        forward_p = [1]
        backward_p = [1]
        L = len(nums)
        for i in xrange(1, L):
            forward_p.append(forward_p[i-1] * nums[i-1])
            backward_p.append(backward_p[i-1] * nums[L-i])
        result = []
        for i in xrange(L):
            result.append(forward_p[i] * backward_p[L-1-i])
        return result


class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [1,2,3,4]
        print self.S.productExceptSelf(A)

    def test_2(self):
        A = [1,2,3,0,4]
        print self.S.productExceptSelf(A)

    def test_3(self):
        A = [4]
        print self.S.productExceptSelf(A)


if __name__ == "__main__":
    unittest.main()
