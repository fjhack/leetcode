class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        L = len(nums)
        fwd = [1 for _ in xrange(L+2)]
        fwdp = 1
        for i in xrange(1, L+1):
            print i, fwdp
            fwdp = fwdp * nums[i-1]
            fwd[i] = fwdp

        bwdp = 1
        for i in xrange(L, 0, -1):
            print i, bwdp, fwd[i-1]
            fwd[i] = fwd[i-1] * bwdp
            bwdp = bwdp * nums[i-1]

        return fwd[1:L+1]

if __name__ == "__main__":
    S = Solution()
    
    N = [1,2,3,4]
    S.productExceptSelf(N)
    
