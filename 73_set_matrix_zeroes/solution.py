class Solution(object):
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        zrows, zcols = set(), set()
        rows = len(matrix)
        if rows == 0:
            return
        cols = len(matrix[0])
        
        for i in xrange(rows):
            for j in xrange(cols):
                if matrix[i][j] == 0:
                    zrows.add(i)
                    zcols.add(j)
        
        for zrow in zrows:
            for i in xrange(cols):
                matrix[zrow][i] = 0
        
        for zcol in zcols:
            for i in xrange(rows):
                matrix[i][zcol] = 0
        
        return
