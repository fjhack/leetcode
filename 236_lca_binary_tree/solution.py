"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of the binary search tree.
    @param A and B: two nodes in a Binary.
    @return: Return the least common ancestor(LCA) of the two nodes.
    """ 
    def lowestCommonAncestor(self, root, A, B):
        # write your code here
        if root is None:
            return None
        if root is A or root is B:
            return root
        L = self.lowestCommonAncestor(root.left, A, B)
        R = self.lowestCommonAncestor(root.right, A, B)
        
        if L is not None and R is not None:
            return root
        elif L is not None:
            return L
        elif R is not None:
            return R
        else:
            return None
