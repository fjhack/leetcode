import unittest

class NumMatrix(object):

    def __init__(self, matrix):
        """
        :type matrix: List[List[int]]
        """
        self._m = matrix
        self.rows, self.cols = len(matrix), len(matrix[0])
        self._summ = [[0 for _ in xrange(self.cols)] for _ in xrange(self.rows)]
        
        self._summ[0][0] = matrix[0][0]

        for x in xrange(1, self.cols):
            self._summ[0][x] = self._summ[0][x-1] + matrix[0][x]

        for y in xrange(1, self.rows):
            self._summ[y][0] = self._summ[y-1][0] + matrix[y][0]

        for y in xrange(1, self.rows):
            for x in xrange(1, self.cols):
                self._summ[y][x] = self._summ[y][x-1] + self._summ[y-1][x] - self._summ[y-1][x-1] + matrix[y][x]

        for r in xrange(self.rows):
            print matrix[r]
        print
        for r in xrange(self.rows):
            print self._summ[r]


    def update(self, row, col, val):
        """
        :type row: int
        :type col: int
        :type val: int
        :rtype: void
        """
        offset = val - self._m[row][col]
        
        for y in xrange(row, self.rows):
            for x in xrange(col, self.cols):
                self._summ[y][x] += offset
        self._m[row][col] = val
        

    def sumRegion(self, row1, col1, row2, col2):
        """
        :type row1: int
        :type col1: int
        :type row2: int
        :type col2: int
        :rtype: int
        """
        if row1 > 0:
            sumr1 = self._summ[row1-1][col2]
        else:
            sumr1 = 0
        if col1 > 0:
            sumr2 = self._summ[row2][col1-1]
        else:
            sumr2 = 0
        if row1 > 0 and col1 > 0:
            sumr0 = self._summ[row1-1][col1-1]
        else:
            sumr0 = 0
        return self._summ[row2][col2] - sumr1 - sumr2 + sumr0


# Your NumMatrix object will be instantiated and called as such:
# obj = NumMatrix(matrix)
# obj.update(row,col,val)
# param_2 = obj.sumRegion(row1,col1,row2,col2)

class testSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_0(self):
        M = [
            [3, 0, 1, 4, 2],
            [5, 6, 3, 2, 1],
            [1, 2, 0, 1, 5],
            [4, 1, 0, 1, 7],
            [1, 0, 3, 0, 5]
        ]
        S = NumMatrix(M)
        print "\n", S.sumRegion(2,1,4,3)
        print "\n", S.update(3,2,2)
        print "\n", S.sumRegion(2,1,4,3)

    def test_1(self):
        M = [[2,4], [-3,5]]
        S = NumMatrix(M)
        print "\n", S.update(0,1,3)
        print "\n", S.update(1,1,-3)
        print "\n", S.update(0,1,1)
        print "\n", S.sumRegion(0,0,1,1)

if __name__ == "__main__":
    unittest.main()
