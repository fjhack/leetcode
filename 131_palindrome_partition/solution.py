import unittest

class Solution(object):
    def is_palin(self, s):
        L = len(s)
        for i in xrange(L/2):
            if s[i] != s[L-i-1]:
                return False
        return True
    
    def partition(self, s):
        """
        :type s: str
        :rtype: List[List[str]]
        """
        L = len(s)
        results = []
        def match_palin(s, trail, results):
            L = len(s)
            # print " "*len(trail), s, L, trail
            if L == 0:
                results.append(trail)
                return
            for i in xrange(1, L+1):
                _prefix = s[:i]
                # print " "*len(trail), _prefix, s[i:], trail
                if self.is_palin(_prefix):
                    # print " "*len(trail), "Found palin", _prefix
                    match_palin(s[i:], trail + [_prefix], results)
        
        match_palin(s, [], results)
        return results

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_ispalin_1(self):
        s = "aabbaa"
        print self.S.is_palin(s)
        s = "a"
        print self.S.is_palin(s)
        s = "abbcbba"
        print self.S.is_palin(s)

    def test_ispalin_2(self):
        s = "aabb"
        print self.S.is_palin(s)
        s = "bba"
        print self.S.is_palin(s)

    def test_palinpart_1(self):
        print
        s = "aab"
        print self.S.partition(s)
        s = "accac"
        print self.S.partition(s)

if __name__ == "__main__":
    unittest.main()
