import unittest
def radixMSD(A):
    # default radix = 10
    K = len(str(max(A)))
    buckets = [[] for _ in xrange(10)]
    strA = [str(v) for v in A]
    for i in xrange(K):
        for aa in strA:
            if i >= len(aa):
                d = 0                
            else:
                d = int(aa[i])

            #print i, aa, d
            buckets[d].insert(0, aa)
            #print buckets
        del strA[:]
        for bucket in buckets:
            print bucket
            strA.extend(bucket)
        print "_", strA
        buckets = [[] for _ in xrange(10)]
    R = [int(v) for v in strA]
    return R

class Solution:	
    # @param num: A list of non negative integers
    # @return: A string
    def largestNumber(self, num):
        nums = sorted(num, cmp=lambda x, y: 1 if str(x) + str(y) < str(y) + str(x) else -1)
        print nums
        largest = ''.join([str(x) for x in nums])
        i, length = 0, len(largest)
        while i + 1 < length:
            if largest[i] != '0':
                break
            i += 1
        return largest[i:]
    
def ln1(num):
    def compare(x, y):
        if str(x) + str(y) < str(y) + str(x):
            return 1
        else:
            return -1
    L = len(num)
    for i in xrange(L-1):
        for j in xrange(i, L):
            if compare(num[i], num[j]) == 1:
                t = num[i]
                num[i] = num[j]
                num[j] = t
    print num
        
if __name__ == "__main__":
    SlN = Solution().largestNumber
    
    A = [3, 30, 34, 5, 9]
    # print radixMSD(A)

    print ln1(A)
