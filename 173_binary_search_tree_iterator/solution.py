import unittest

# Definition for a  binary tree node
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class BSTIterator(object):
    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self._visited = set()
        self._stk = [root]

    def hasNext(self):
        """
        :rtype: bool
        """
        while len(self._stk) > 0 and self._stk[-1] is not None and self._stk[-1].left is not None and self._stk[-1].left not in self._visited:
            self._stk.append(self._stk[-1].left)
            print [n.val for n in self._stk]
        return not(len(self._stk) == 0) and self._stk[-1] is not None
        

    def next(self):
        """
        :rtype: int
        """
        if self.hasNext():
            top = self._stk[-1]
            self._stk.pop()
            if top is not None and (top.left in self._visited or top.left is None) and top.right is not None:
                self._stk.append(top.right)
            self._visited.add(top)
            return top.val

# Your BSTIterator will be called like this:
# i, v = BSTIterator(root), []
# while i.hasNext(): v.append(i.next())

class testSolution(unittest.TestCase):
    def setUp(self):
        pass

    def test_0(self):
        root = TreeNode(5)
        root.left = TreeNode(2)
        root.left.left = TreeNode(1)
        root.left.right = TreeNode(3)
        root.left.right.right = TreeNode(4)
        root.right = TreeNode(9)
        root.right.left = TreeNode(7)
        root.right.left.left = TreeNode(6)
        root.right.left.right = TreeNode(8)
        root.right.right = TreeNode(11)
        root.right.right.left = TreeNode(10)

        biter = BSTIterator(root)
        biter.hasNext()
        # print biter._stk
        c = 0
        while biter.hasNext() and c < 12:
            print biter.next()
            c += 1

if __name__ == "__main__":
    unittest.main()
    
