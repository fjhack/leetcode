import unittest

class Solution(object):
    def simplifyPath(self, path):
        """
        :type path: str
        :rtype: str
        """
        L = len(path)
        if L == 0:
            return "/"
        elif path[0] != '/':
            return "/"
        stk = []
        i = 0
        while i < L:
            if path[i] == '/':
                while i < L and path[i] == '/':
                    i += 1
                continue
            elif path[i] == '.':
                step = 0
                while i+step < L and step < 2 and path[i+step] == '.':
                    step += 1
                if (i+step < L and path[i+step] == '/') or (i+step == L):
                    if step == 1:
                        i += 1
                    elif step == 2:
                        if len(stk) > 0:
                            poped = stk.pop()
                        i += 2
                    continue
                elif i+step < L:
                    print "\tboo"
                    while i+step < L and path[i+step] != '/':
                        step += 1
                    curr_dir = path[i:i+step]
                    # print "\t", i, path[i], i+step, path[i+step]
                    print "\t", curr_dir
                    stk.append(curr_dir)
                    i += step
            else:
                curr_dir = ""
                while i < L and path[i] != '/':
                    curr_dir = curr_dir + path[i]
                    i += 1
                stk.append(curr_dir)
        full_dir = "/".join(stk)
        return "/" + full_dir

if __name__ == "__main__":
    SsP = Solution().simplifyPath
    D = "/home/abc/"
    print SsP(D)

    D = "/home/abc/../cba/src/derp/../../pics"
    print SsP(D)

    D = "/home/abc/../../../"
    print SsP(D)

    D = "/.../"
    print SsP(D)

    D = "/.ac"
    print SsP(D)

    D = "/home/abc/.../"
    print SsP(D)

    D = "/home/abc///.../../..//abc//.../"
    print SsP(D)

    D = "/"
    print SsP(D)

    D = "/.aa/....hidden"
    print SsP(D)
