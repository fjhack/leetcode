class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """
        head, tail = 0, x
        while head <= tail:
            mid = head + (tail - head)/2
            if mid*mid == x:
                return mid
            elif mid*mid < x:
                head = mid+1
            else:
                tail = mid-1
        return tail
