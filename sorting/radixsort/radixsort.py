import unittest

def radixSort(A):
    # default radix = 10
    K = len(str(max(A)))
    buckets = [[] for _ in xrange(10)]
    aA = [str(v) for v in A]
    for i in xrange(1,K+1):
        for aa in aA:
            if i <= len(aa):
                d = int(aa[-i])
            else:
                d = 0
            # print i, aa, d
            buckets[d].append(aa)
            # print buckets
        del aA[:]
        for bucket in buckets:
            print bucket
            aA.extend(bucket)
        print "_", aA
        buckets = [[] for _ in xrange(10)]
    R = [int(v) for v in aA]
    return R

def radixMSD(A):
    # default radix = 10
    K = len(str(max(A)))
    buckets = [[] for _ in xrange(10)]
    strA = [str(v) for v in A]
    for i in xrange(K):
        for aa in strA:
            if i >= len(aa):
                d = 0                
            else:
                d = int(aa[i])

            #print i, aa, d
            buckets[d].insert(0, aa)
            #print buckets
        del strA[:]
        for bucket in buckets:
            print bucket
            strA.extend(bucket)
        print "_", strA
        buckets = [[] for _ in xrange(10)]
    R = [int(v) for v in strA]
    return R


if __name__ == "__main__":
    A = [170, 45, 75, 90, 802, 2, 24, 66]
    # print radixSort(A)

    A = [3, 30, 34, 5, 9]
    print radixMSD(A)
