import unittest

def quickSort(A):
    pass

def quickSort1(A, start, end):
    L = len(A)
    def partition(A, start, end):
        pivot = A[end]
        i = start
        for j in xrange(start, end):
            if A[j] < pivot:
                t = A[i]
                A[i] = A[j]
                A[j] = t
                i += 1
        t = A[i]
        A[i] = A[end]
        A[end] = t
        print A
        return i
    if start < end:
        pivot = partition(A, start, end)
        quickSort1(A, start, pivot-1)
        quickSort1(A, pivot+1, end)
        
if __name__ == "__main__":
    A = [8,3,2,7,11,5,12,9]
    quickSort1(A, 0, len(A)-1)
    print A
    print
    A = [8,3,2,7,11,5,3,12,1]
    quickSort1(A, 0, len(A)-1)
    print A
    print
    A = [8,3,2,7,11,5,3,12,1,9,4]
    quickSort1(A, 0, len(A)-1)
    print A


