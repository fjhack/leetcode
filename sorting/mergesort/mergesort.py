import unittest

# Top-down version

def mergeSortTD(A):
    L = len(A)
    B = [None for _ in xrange(L)]
    mergerTD(A, 0, L-1, B, 0)
    return B

def mergerTD(A, start, end, B, v):
    print "  "*v, A[start:end+1]
    if (end - start) == 0:
        return
    else:
        mid = (start + end) / 2
        mergerTD(A, start, mid, B, v+1)
        mergerTD(A, mid+1, end, B, v+1)
        i, j = start, mid+1
        k = start

        while k <= end and i <= mid and j <= end:
            if A[i] < A[j]:
                B[k] = A[i]
                i += 1
                k += 1
            else:
                B[k] = A[j]
                j += 1
                k += 1
        if i <= mid:
            while k <= end:
                B[k] = A[i]
                i += 1
                k += 1
        else:
            while k <= end:
                B[k] = A[j]
                j += 1
                k += 1
        print "  "*v, B
        for k in xrange(start, end+1):
            A[k] = B[k]

# Bottom up version
def mergeSortBU(A):
    L = len(A)
    B = [None for _ in xrange(L)]
    width = 1
    while width < L:
        start = 0
        while start < L:

            s1, e1 = start, min(start+width, L)
            s2, e2 = min(start+width, L), min(start+width*2, L)
            print " " * (L/width), s1,e1,A[s1:e1], s2,e2,A[s2:e2]
            
            # merge
            i, j = s1, s2
            k = s1

            while k < e2 and i < s2 and j < e2:
                if A[i] < A[j]:
                    B[k] = A[i]
                    i += 1
                    k += 1
                else:
                    B[k] = A[j]
                    j += 1
                    k += 1
            if i < s2:
                while k < e2:
                    B[k] = A[i]
                    i += 1
                    k += 1
            else:
                while k < e2:
                    B[k] = A[j]
                    j += 1
                    k += 1
            for k in xrange(s1, e2):
                A[k] = B[k]
            print B
            start += width*2
        width *= 2
    return B




if __name__ == "__main__":
    A = [8,3,2,7,11,5,3,12,1]
    mergeSortTD(A)

    A = [8,3,2,7,11,5,12,9]
    mergeSortBU(A)

    A = [8,3,2,7,11,5,3,12,1,9,4]
    mergeSortBU(A)
