import unittest

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    # In-order traversal, non recursive
    def kthSmallest(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: int
        """
        stk = []
        cn = root
        while cn is not None:
            stk.append(cn)
            cn = cn.left
        x = 1
        while len(stk) > 0 and x <= k:
            cn = stk.pop()
            x += 1
            rn = cn.right
            while rn is not None:
                stk.append(rn)
                rn = rn.left
        return cn.val

    # In-order traversal, recursive
    def kthSmallestR(self, root, k):
        self._k = 0
        def iot(node, tk):
            if node.left is not None:
                _r = iot(node.left, tk)
                if _r is not None:
                    return _r
            self._k += 1
            print node.val, self._k, tk, self._k == tk
            if self._k == tk:
                return node.val
            if node.right is not None:
                _r = iot(node.right, tk)
                if _r is not None:
                    return _r

        return iot(root, k)

class testSolution(unittest.TestCase):
    def setUp(self):
        self.root = TreeNode(11)
        self.root.left = TreeNode(5)
        self.root.left.left = TreeNode(3)
        self.root.left.left.left = TreeNode(1)
        self.root.left.left.left.right = TreeNode(2)
        self.root.left.left.right = TreeNode(4)
        self.root.left.right = TreeNode(8)
        self.root.left.right.left = TreeNode(6)
        self.root.left.right.right = TreeNode(10)
        self.root.right = TreeNode(18)
        self.root.right.left = TreeNode(13)
        self.root.right.left.right = TreeNode(15)
        self.root.right.right = TreeNode(20)
        self.root.right.right.left = TreeNode(19)
        self.root.right.right.right = TreeNode(21)
        self.root.right.right.right.right = TreeNode(23)

    def test_1(self):
        sts = Solution().kthSmallestR
        print sts(self.root, 6)
        print sts(self.root, 8)
        print sts(self.root, 10)

if __name__ == "__main__":
    unittest.main()
