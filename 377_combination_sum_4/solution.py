import unittest

class Solution(object):
    def combinationSum4(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        combs = [0 for _ in xrange(target+1)]
        combs[0] = 1
        for i in xrange(target+1):
            for j in nums:
                if (i+j) <= target:
                    combs[i+j] += combs[i]
        return combs[target]

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [1,2,3]
        t = 4
        '''
        (4)
        1 2 3
        x     0 1 2 3 4
        t=0   1
        t=1   0 1 
        t=2   0 1 2
        t=3   0 1 2 4
        t=4   0   1 3 7
        
        '''
        print self.S.combinationSum4(A, t)

if __name__ == "__main__":
    unittest.main()
