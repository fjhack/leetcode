# Definition for a binary tree node.

class Solution(object):
    def __init__(self):
        self.robbed = {}
        
    def rob(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        def is_leaf(node):
            # print "\t:is_leaf: ", node.val, node.left, node.right
            return ((node.left is None) and (node.right is None))
        
        def is_leaf_parent(node):
            if node.right is None:
                return is_leaf(node.left)
            elif node.left is None:
                return is_leaf(node.right)
            else:
                return (is_leaf(node.left) and is_leaf(node.right))
        
        def sum_1st_child(node):
            s = 0
            if node.left:
                s += node.left.val
            if node.right:
                s += node.right.val
            return s
        
        def sum_2nd_child(node):
            s = 0
            if node.left:
                if node.left.left:
                    s += self.rob(node.left.left)
                if node.left.right:
                    s += self.rob(node.left.right)
            if node.right:
                if node.right.left:
                    s += self.rob(node.right.left)
                if node.right.right:
                    s += self.rob(node.right.right)
            return s

        # print "Checking: ", root.val
        if id(root) in self.robbed:
            return self.robbed[id(root)]
        elif root is None:
            return 0
        elif is_leaf(root):
            # print "\t", root.val, "is leaf"
            self.robbed[id(root)] = root.val
        elif is_leaf_parent(root):
            # print "\t", root.val, "is leaf parent"
            self.robbed[id(root)] = max(root.val, sum_1st_child(root))
        else:
            # print "\t", root.val, "..descending"
            self.robbed[id(root)] = max((self.rob(root.left) + self.rob(root.right)),
                                        (root.val + sum_2nd_child(root)))
        return self.robbed[id(root)]

