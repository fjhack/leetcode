# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def robNode(self, N):
        if N is None:
            return 0, 0
        else:
            lsum_in, lsum_ex = self.robNode(N.left)
            rsum_in, rsum_ex = self.robNode(N.right)
            r_in = lsum_ex + rsum+ex + N.val
            r_ex = max(lsum_in+rsum_in, lsum_in+rsum_ex, lsum_ex+rsum_in, lsum_ex+rsum_ex)
            return r_in, r_ex
    
    def rob(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        r_in, r_ex = self.robNode(root)
        return max(r_in, r_ex)

    
