class Solution(object):
    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        start, end = 0, len(nums)-1
        if start == end:
            return start
        while start < end:
            mid = start + (end - start)/2
            if mid == 0 and nums[mid] > nums[mid+1]:
                return mid
            if mid == (len(nums)-1) and nums[mid] > nums[mid-1]:
                return mid
            if nums[mid] > nums[mid+1] and nums[mid] > nums[mid-1]:
                return mid
            elif mid > 0 and nums[mid-1] > nums[mid]:
                # In python index underflow could cause problems
                # Q: What condition will avoid by adding this check (mid > 0)?
                end = mid
            else:
                start = mid+1
        return end
