import unittest

class Solution(object):
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        L = len(s)
        stk = []
        stk.append((-1, ')'))
        maxlen = 0
        for i in xrange(L):
            if s[i] == '(':
                stk.append((i, '('))
            elif s[i] == ')':
                t = stk.pop()
                if len(stk) > 0:
                    maxlen = max(maxlen, i - stk[-1][0])
                else:
                    stk.append((i, ')'))
                    
        return maxlen

if __name__ == "__main__":
    SlvP = Solution().longestValidParentheses
    s = "(()"
    print SlvP(s), s

    s = ")((())()"
    print SlvP(s), s

    s = "(())())("
    print SlvP(s), s

    s = ")()()("
    print SlvP(s), s
    
