import unittest

class Solution(object):
    def subsetsWithDup(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        def generate(nums, curr_num, results, last_local_results):
            if curr_num == len(nums):
                return
            else:
                _local_results = []
                L = len(results)
                print " "*curr_num, results
                curr_idx = len(nums) - curr_num - 1
                print curr_idx, nums[curr_idx], '\t', curr_idx+1
                print (curr_idx == len(nums)-1) or (curr_idx < (len(nums)-1) and nums[curr_idx] != nums[curr_idx+1])
                if (curr_idx == len(nums)-1) or (curr_idx < (len(nums)-1) and nums[curr_idx] != nums[curr_idx+1]):
                    for i in xrange(L):
                        print " "*curr_num, results[i], nums[curr_idx]
                        _local_results.append([nums[curr_idx]] + results[i])
                    print " "*curr_num, results, "\t", last_local_results, _local_results
                    print
                    results += _local_results
                    generate(nums, curr_num+1, results, _local_results)
                else:
                    L = len(last_local_results)
                    for i in xrange(L):
                        print " "*curr_num, results[i], nums[curr_idx]
                        _local_results.append([nums[curr_idx]] + last_local_results[i])
                    print " "*curr_num, results
                    print
                    results += _local_results
                    generate(nums, curr_num+1, results, _local_results)
                    
        results = [[]]
        numss = sorted(nums)
        generate(numss, 0, results, [])
        return results


class SolutionPU(object):
    def permuter(self, results, nums, trail):
        if len(trail) == len(nums):
            return
        else:
            for i in xrange(len(nums)):
                if i in trail:
                    continue
                elif i > 0 and nums[i] == nums[i-1] and i-1 not in trail:
                    continue
                else:
                    # print trail
                    # print i, nums[i], i-1, nums[i-1]
                    results.append([nums[p] for p in trail])
                    self.permuter(results, nums, trail + [i])

    def permuteUnique(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        results = []
        nums2 = sorted(nums)
        self.permuter(results, nums2, [])
        return results

class Solution2:
    """
    @param S: The set of numbers.
    @return: A list of lists. See example.
    """
    def subsets(self, S):
        # write your code here
        r = [[]]
        S = sorted(S)
        L = len(S)
        def _inSet(newl, lstl, S):
            # print newl, lstl
            newlV, lstlV = [S[i] for i in newl], [[S[_i] for _i in idxlst] for idxlst in lstl]
            # print "\t", newlV, "\t", lstlV
            return not (newlV in lstlV)

        for i in xrange(L):
            r = [s + [i] for s in r if _inSet(s+[i], r, S)] + r
            # print r, [[S[_i] for _i in ss] for ss in r]
            # print
        return [[S[_i] for _i in ss] for ss in r] 
    
if __name__ == "__main__":
    S = Solution()
    Ss = S.subsetsWithDup
    Spu = SolutionPU().permuteUnique
    Ss2 = Solution2().subsets

    print
    print Ss2([1,2,2])

    print
    print Ss2([4,4,4,1,4])
