class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        ax = abs(x)
        if ax != x:
            sign = -1
        else:
            sign = 1
        s = 0
        bx = ax
        while bx > 0:
            s = s * 10 + bx % 10
            bx = bx / 10
        return s * sign

if __name__ == "__main__":
    S = Solution()
    print S.reverse(123)
    print S.reverse(-321)
