# Pure BFS solution

class Solution:
    # @param {boolean[][]} grid a boolean 2D matrix
    # @return {int} an integer
    
    def __init__(self):
        self.G = None
        self.rows = 0
        self.cols = 0
        self.island_count = 0
        
    def neighbors(self, i, j):
        def _valid(x, y):
            return (x >= 0 and x < self.rows) and (y >= 0 and y < self.cols) and (self.G[x][y] == 1)
        NV = [(-1,0), (0,1), (1,0), (0,-1)]
        
        if self.G[i][j] == 1:
            n = [(i+d[0], j+d[1]) for d in NV if _valid(i+d[0], j+d[1])]
            return n
        else:
            return []
    
    def bfs_expand(self, i, j):
        if self.G[i][j] == 1:
            q = []
            q.append((i,j))
            while len(q) > 0:
                cc = q.pop(0)
                ns = self.neighbors(cc[0], cc[1])
                q = q + ns
                self.G[cc[0]][cc[1]] = 0
            self.island_count += 1
        
    def numIslands(self, grid):
        # Write your code here
        self.G = grid
        self.rows = len(grid)
        if self.rows == 0:
            return 0
        self.cols = len(grid[0])
        self.island_count = 0        
        
        for i in xrange(self.rows):
            for j in xrange(self.cols):
                if self.G[i][j] == 1:
                    self.bfs_expand(i, j)
        
        return self.island_count
