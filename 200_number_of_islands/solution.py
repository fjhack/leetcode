import unittest

class Solution(object):
    def __init__(self):
        self.grid = []
        self.walked = []
        self.height = -1
        self.width = -1
        self.icount = -1

    def setGrid(self, grid, walked, height, width):
        self.grid = grid
        self.walked = walked
        self.height = height
        self.width = width
        self.icount = 0

    def printMatrix(self, m):
        for r in m:
            print r
        print
    
    def getNext(self, i, j):
        results = []
        if i-1 >= 0 and self.walked[i-1][j] == 0:
            results.append((i-1, j))
        if j+1 < self.width and self.walked[i][j+1] == 0:
            results.append((i, j+1))
        if i+1 < self.height and self.walked[i+1][j] == 0:
            results.append((i+1, j))
        if j-1 >= 0 and self.walked[i][j-1] == 0:
            results.append((i, j-1))
        return results

    def dfs(self, i, j):
        if i == self.height-1 and j == self.width-1:
            return
        print "Walked to:", i, j
        self.walked[i][j] = 1
        _next = self.getNext(i, j)
        print _next
        for nc in _next:
            if self.walked[nc[0]][nc[1]] == 0 and self.grid[nc[0]][nc[1]] == 1:
                self.printMatrix(self.walked)
                print "Found 1, should use BFS to fill it."
                self.bfs(nc[0], nc[1])
                self.printMatrix(self.walked)
                self.printMatrix(self.grid)
                self.icount += 1
            else:
                self.dfs(nc[0], nc[1])

    def bfs(self, i, j):
        bfsqueue = [(i, j)]
        while len(bfsqueue) > 0:
            _qhead = bfsqueue.pop(0)
            _nextc = self.getNext(_qhead[0], _qhead[1])
            for _nc in _nextc:
                if self.walked[_nc[0]][_nc[1]] == 0 and self.grid[_nc[0]][_nc[1]] == 1:
                    self.grid[_nc[0]][_nc[1]] = 0
                    bfsqueue.append((_nc[0], _nc[1]))
            
    def numIslands(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
                    
        H = len(grid)
        if H == 0:
            return 0
        W = len(grid[0])
        if W == 0:
            return 0
        walked = [[0 for _ in xrange(W)] for _ in xrange(H)]
        self.setGrid(grid, walked, H, W)
        i, j = 0, 0
        # self.dfs(i, j)
        for i in xrange(H):
            for j in xrange(W):
                if self.grid[i][j] != 1:
                    continue
                self.icount += 1
                self.bfs(i, j)
        return self.icount
        
if __name__ == "__main__":
    A = [[1,1,1,1,0],
         [1,1,0,1,0],
         [1,0,0,1,0],
         [0,0,1,0,1],
         [0,0,0,0,1]]
    NI = Solution().numIslands
    print NI(A)

    A = [[0,1,1,1,0],
         [0,0,0,1,0],
         [1,0,0,1,0],
         [1,1,0,0,1],
         [0,0,0,0,1]]
    print NI(A)

    A = [[1,1,1,1,0],
         [1,1,0,1,0],
         [1,1,0,0,0],
         [0,0,0,0,0]]
    print NI(A)
