import unittest

class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next

def list2LL(L):
    head = ListNode(L[0])
    ptr = head
    for i in xrange(1, len(L)):
        ptr.next = ListNode(L[i])
        ptr = ptr.next
    return head

def printLL(LL):
    head = LL
    while head is not None:
        print head.val, '->', 
        head = head.next
    print

def nodeStr(node):
    if node is None:
        return "<None>"
    else:
        return "{0} @ {1}".format(node.val, repr(node))

class Solution:
    """
    @param head: The first node of the linked list.
    @return: nothing
    """
    def reorderList(self, head):
        dummy = ListNode(-1)
        dummy.next = head
        p1, p2 = dummy, dummy
        S1 = []
        while p2 is not None and p2.next is not None:
            p2 = p2.next.next
            p1 = p1.next
            S1.append(p1)
            if p2 is None:
                break
        # print [n.val for n in S1]    
        # S1.append(p1)
        S2 = []
        while p1 is not None:
            p1 = p1.next
            if p1 is not None:
                S2.append(p1)
            
        L1, L2 = len(S1), len(S2)
        pre = dummy
        
        # print [n.val for n in S2]
        while len(S1) > 0 and len(S2) > 0:
            c1 = S1.pop(0)
            c2 = S2.pop()
            # print c1, c1.val, c2, c2.val
            pre.next = c1
            c1.next = c2
            pre = c2
        # print S1, S2
        if len(S1) > 0:
            c1 = S1.pop(0)
            pre.next = c1
            c1.next = None
        else:
            pre.next = None
        return dummy.next

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        
    def test_1(self):
        A1 = range(1, 6)
        LL1 = list2LL(A1)
        printLL(LL1)
        LL2 = self.S.reorderList(LL1)
        printLL(LL2)

    def test_2(self):
        A1 = range(1, 5)
        LL1 = list2LL(A1)
        printLL(LL1)
        LL2 = self.S.reorderList(LL1)
        printLL(LL2)

if __name__ == "__main__":
    unittest.main()
    
