import unittest

class Solution0(object):
    def find_turn_point(self, nums, start, end):
        L = len(nums)
        mid = start + (end - start) / 2
        print start, mid, end, nums[start:end+1]
        if start == mid and mid == end:
            return start
        elif (end - start) == 1:
            if nums[start] > nums[end]:
                return start
            else:
                return end
        if nums[start] < nums[mid] and nums[mid] < nums[end]:
            # already sorted, no rotate
            return end
        elif nums[start] > nums[mid] and nums[mid] < nums[end]:
            # turn is in first half (not including mid)
            return self.find_turn_point(nums, start, mid -1)
        elif nums[start] < nums[mid] and nums[mid] > nums[end]:
            # turn is in second half (include mid)
            return self.find_turn_point(nums, mid, end)
        
    def binary_search(self, nums, t, start, end):
        L = len(nums)
        L2 = end - start + 1
        mid = start + (end - start) / 2
        print "\t", start, mid, end, t, nums[start:end]
        if (start == end) and (t != nums[start]):
            return -1
        elif nums[mid] == t:
            return mid
        elif t < nums[mid]:
            print "\t\tfront" 
            return self.binary_search(nums, t, start, mid)
        elif t > nums[mid]:
            print "\t\tback" 
            return self.binary_search(nums, t, mid+1, end)
        else:
            return -1

    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        L = len(nums)
        tp = self.find_turn_point(nums, 0, L-1)
        print tp
        print nums[0:tp+1], nums[tp+1:], target
        if L == 1 and target != nums[0]:
            return -1
        if tp+1 >= L:
            return self.binary_search(nums, target, 0, L-1)
        elif nums[0] == target:
            return 0
        elif nums[tp] == target:
            return tp
        elif nums[L-1] == target:
            return L-1
        elif nums[0] <= target and target <= nums[tp]:
            return self.binary_search(nums, target, 0, tp)
        elif nums[tp+1] <= target and target <= nums[L-1]:
            return self.binary_search(nums, target, tp+1, L-1)

class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        return self.search2(nums, target)

    def search0(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        def srs(nums, target, head, tail):
            # print nums[head:tail+1]
            if head > tail:
                return -1
            mid = (head+tail) / 2
            if mid == head:
                if nums[head] == target:
                    return head
                elif nums[tail] == target:
                    return tail
                else:
                    return -1
            if nums[mid] == target:
                return mid
            elif nums[mid] < nums[tail] and nums[tail] < nums[head]:
                if target > nums[mid] and target <= nums[tail]:
                    return srs(nums, target, mid+1, tail)
                else:
                    return srs(nums, target, head, mid-1)
            elif nums[mid] > nums[head] and nums[head] > nums[tail]:
                if target >= nums[head] and target < nums[mid]:
                    return srs(nums, target, head, mid-1)
                else:
                    return srs(nums, target, mid+1, tail)
            else:
                if target < nums[mid]:
                    return srs(nums, target, head, mid-1)
                else:
                    return srs(nums, target, mid+1, tail)
        return srs(nums, target, 0, len(nums)-1)

    def search1(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        def srs(nums, target, head, tail):
            # print nums[head:tail+1]
            if head > tail:
                return -1
            mid = (head+tail) / 2
            if mid == head:
                if nums[head] == target:
                    return head
                elif nums[tail] == target:
                    return tail
                else:
                    return -1
            if nums[mid] == target:
                return mid
            elif nums[mid] < nums[tail]:
                if target > nums[mid] and target <= nums[tail]:
                    return srs(nums, target, mid+1, tail)
                else:
                    return srs(nums, target, head, mid-1)
            elif nums[mid] > nums[head] and nums[head] > nums[tail]:
                if target >= nums[head] and target < nums[mid]:
                    return srs(nums, target, head, mid-1)
                else:
                    return srs(nums, target, mid+1, tail)

        return srs(nums, target, 0, len(nums)-1)

    def search2(self, nums, target):
        L = len(nums)
        head, tail = 0, L-1
        while head <= tail:
            mid = head + (tail - head) / 2
            if nums[mid] == target:
                return mid

            if mid == head:
                if nums[head] == target:
                    return head
                elif nums[tail] == target:
                    return tail
                else:
                    return -1
            
            if nums[mid] < nums[tail]:
                if target > nums[mid] and target <= nums[tail]:
                    head, tail = mid+1, tail
                else:
                    head, tail = head, mid-1
            elif nums[mid] > nums[head] and nums[head] > nums[tail]:
                if target >= nums[head] and target < nums[mid]:
                    head, tail = head, mid-1
                else:
                    head, tail = mid+1, tail
        return -1

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def generate_array(self):
        import random
        L = random.randint(10, 16)
        p = random.randint(0, L)
        A = [random.randint(1, 200) for _ in xrange(L)]
        As = sorted(list(set(A)))
        Ar = As[p:] + As[:p]
        # print L, As
        # print As[:p], As[p:]
        # print Ar
        return Ar

    def _gold(self, A, t):
        L = len(A)
        for i in xrange(L):
            if A[i] == t:
                return i
        return -1


    def test_1(self):
        A = [4,5,6,7,0,1,2]
        t = 6
        print
        print self.S.search(A, t)

    def test_1_1(self):
        A = [1]
        t = 0
        print
        print self.S.search(A, t)

    def test_1_2(self):
        A = [1]
        t = 2
        print
        print self.S.search(A, t)

    def test_1_3(self):
        A = [1,3]
        t = 2
        print
        print self.S.search(A, t)

    def test_1_4(self):
        A = [3,1]
        t = 1
        print
        print self.S.search(A, t)

    def test_random(self):
        self.random_driver(1000)

    def random_driver(self, iters):
        import random
        passed_count = 0
        for i in xrange(iters):
            # print
            A = self.generate_array()
            # print A
            t = A[random.randint(0, len(A)-1)]
            rc = self.S.search(A, t)
            rg = self._gold(A, t)
            if rc != rg:
                print "Error found:"
                print "  Array:", A, "\ttarget:", t
                print "  gold result:", rg, "\tactual result:", rc
            else:
                passed_count += 1
        print "Total passed cases: {0} out of total {1} cases".format(passed_count, iters)

if __name__ == "__main__":
    unittest.main()
