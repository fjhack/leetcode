class Solution(object):
    def ssd(self, n):
        """
        return sum of square of each digits
        """
        sn, L = str(n), len(str(n))
        s = 0
        print n,
        for i in xrange(L):
            s += int(sn[i]) * int(sn[i])
        print s
        return s
        
    def isHappy(self, n):
        """
        :type n: int
        :rtype: bool
        """
        D = set()
        n1 = n
        while True:
            n1 = self.ssd(n1)
            print n1
            if n1 == 1:
                return True
            elif n1 in D:
                return False
            else:
                D.add(n1)

if __name__ == "__main__":
    SiH = Solution().isHappy
    print SiH(7)
