
import unittest

import sys

class Solution(object):
    def __init__(self):
        self._G = None
        self._visited = None
        self._rows, self._cols = 0, 0
        
    def getNext(self, i, j):
        directions = [[0,1], [1,0], [0,-1], [-1, 0]]
        return [[i+d[0], j+d[1]] for d in directions if (i+d[0] >= 0 and i+d[0] < self._rows
                                                         and j+d[1] >= 0 and j+d[1] < self._cols
                                                         and self._G[i+d[0]][j+d[1]] != 2
                                                         and not self._visited[i+d[0]][j+d[1]])]

    def bfs(self, startR, startC):
        totallen = 0
        currlen = 1
        reached_bldg = 0

        Q = [[startR, startC]]
        levelsize = 1
        print "  Starting from: ", Q[0]
        while len(Q) > 0:
            nextsize = 0
            for i in xrange(levelsize):
                head = Q.pop(0)
                print "  {0} Poped: ".format(currlen), head
                self._visited[head[0]][head[1]] = True
                nextcells = self.getNext(head[0], head[1])
                print "  {0} Next of head: ".format(currlen), nextcells
                for cell in nextcells:
                    if self._G[cell[0]][cell[1]] == 1:
                        print "  {0} Hit building at: ".format(currlen), cell
                        totallen += currlen
                        self._visited[cell[0]][cell[1]] = True
                        reached_bldg += 1
                    elif self._G[cell[0]][cell[1]] == 0:
                        nextsize += 1
                        Q.append(cell)
            levelsize = nextsize
            currlen += 1
        
        if reached_bldg == self.bcount:
            return totallen
        else:
            return -1
                
    def shortestDistance(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        self._G = grid
        self._rows = len(grid)
        self._cols = len(grid[0])
        self._visited = [[False for _ in xrange(self._cols)] for _ in xrange(self._rows)]

        self.bcount = sum(val for line in grid for val in line if val == 1)
        minlen = sys.maxint

        # l11 = self.bfs(1, 1)
        # print l11
        # self._visited = [[False for _ in xrange(self._cols)] for _ in xrange(self._rows)]
        # l12 = self.bfs(1, 2)
        # print l12

        # return 0

        minpos = None
        for i in xrange(self._rows):
            for j in xrange(self._cols):
                if self._G[i][j] != 0:
                    continue
                else:
                    self._visited = [[False for _ in xrange(self._cols)] for _ in xrange(self._rows)]
                    clen = self.bfs(i, j)
                    print "Point({0},{1}): {2}".format(i,j,clen)
                    if clen > 0:
                        if clen < minlen:
                            minlen = clen
                            minpos = [i, j]

        print minpos
        return minlen
    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_a_1(self):
        G = [[1,0,2,0,1],
             [0,0,0,0,0],
             [0,0,1,0,0]]
        R = self.S.shortestDistance(G)
        print R

    def test_b_1(self):
        G = [[2,0,0,2,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,1,2,0,2,0,1,1,0],[0,1,0,1,1,2,0,0,2,0,0,2,0,2,2,0,2,0,2,0,0,0,0,0,0,0,0,0],[1,0,0,1,2,0,0,2,0,2,0,0,0,0,0,0,0,0,0,2,0,2,0,0,0,0,0,2],[0,0,2,2,2,1,0,0,2,0,0,0,0,0,0,0,0,0,2,2,2,2,1,0,0,0,0,0],[0,2,0,2,2,2,2,1,0,0,0,0,1,0,2,0,0,0,0,2,2,0,0,0,0,2,2,1],[0,0,2,1,2,0,2,0,0,0,2,2,0,2,0,2,2,2,2,2,0,0,0,0,2,0,2,0],[0,0,0,2,1,2,0,0,2,2,2,1,0,0,0,2,0,2,0,0,0,0,2,2,0,0,1,1],[0,0,0,2,2,0,0,2,2,0,0,0,2,0,2,2,0,0,0,2,2,0,0,0,0,2,0,0],[2,0,2,0,0,0,2,0,2,2,0,2,0,0,2,0,0,2,1,0,0,0,2,2,0,0,0,0],[0,0,0,0,0,2,0,2,2,2,0,0,0,0,0,0,2,1,0,2,0,0,2,2,0,0,2,2]]
        R = self.S.shortestDistance(G)
        print R

        
if __name__ == "__main__":
    unittest.main()
