import unittest

class Solution:
    # @param {integer[]} nums
    # @return {integer}
    def maxSubArray(self, nums):
        L = len(nums)
        maxtn = [nums[i] for i in xrange(L)]
        maxsf = maxtn[0]
        # print nums, maxtn
        for i in xrange(1, L):
            maxtn[i] = max(maxtn[i], maxtn[i-1]+nums[i])
            if maxtn[i] > maxsf:
                maxsf = maxtn[i]
        # print nums
        # print maxtn
        return maxsf

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [-1, -2]
        result = self.S.maxSubArray(A)
        print result

    def test_2(self):
        A = [-2,1,-3,4,-1,2,1,-5,4]
        result = self.S.maxSubArray(A)
        print result

if __name__ == "__main__":
    unittest.main()

