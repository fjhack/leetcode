class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        L = len(nums)
        if L == 0:
            return 0
        max_sofar = nums[0]
        last_max = nums[0]
        for i in xrange(1, L):
            curr_max = max(last_max + nums[i], nums[i])
            if curr_max > max_sofar:
                max_sofar = curr_max
            last_max = curr_max
        return max_sofar

if __name__ == "__main__":
    S = Solution()

    A = [-2,1,-3,4,-1,2,1,-5,4]
    print A, S.maxSubArray(A)
    
    # expected: -1
    # this will fail if: Only compare (sum of all numbers to current) and current value, since the max sum subarray is ended in the middle (first one)
    A = [-1, -2]
    print A, S.maxSubArray(A)

    # expected: 1
    # this will fail if: Only compare (sum of everything till n-1) and (sum of everything till n), in this case the max sum is single element - second 
    A = [-2, 1]
    print A, S.maxSubArray(A)

    # expected: 1
    # this will fail if: Only track (sum of everything non-continiously), so we need to track both current max and last max.
    A = [1, -1, 1]
    print A, S.maxSubArray(A)
