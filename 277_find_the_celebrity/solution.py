# The knows API is already defined for you.
# @param a, person a
# @param b, person b
# @return a boolean, whether a knows b
# def knows(a, b):


class Solution(object):
    def findCelebrity(self, n):
        """
        :type n: int
        :rtype: int
        """
        L, R = 0, n-1
        while L < R:
            if knows(L,R):
                L += 1
            else:
                R -= 1

        candidate = L
        for i in xrange(0, n):
            if i == candidate:
                continue
            if not knows(i, candidate):
                return -1
            if knows(candidate, i):
                return -1

        return candidate
