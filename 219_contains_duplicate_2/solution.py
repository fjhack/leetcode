import unittest

class Solution(object):
    def containsNearbyDuplicate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        L = len(nums)
        D = {}
        for i in xrange(L):
            v = nums[i]
            if v not in D:
                D[v] = [i]
            else:
                D[v].append(i)
        for v, pos in D.iteritems():
            _L = len(pos)
            close_enough = False
            for i in xrange(_L-1):
                for j in xrange(i+1, _L):
                    if (pos[j] - pos[i]) <= k:
                        close_enough = True
            if close_enough:
                return True
        return False
