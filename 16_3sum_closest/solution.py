import unittest

class Solution(object):
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        import sys
        L = len(nums)
        nums2 = sorted(nums)
        print nums2
        mindiff = sys.maxint
        for i in xrange(L-2):
            l, r = i+1, L-1
            while l<r:
                print nums2[i], nums2[l], nums2[r]
                diff = (nums2[i] + nums2[l] + nums2[r]) - target
                if diff == 0:
                    return target
                elif abs(diff) < abs(mindiff):
                    print "  saved min diff:", diff
                    mindiff = diff
                elif diff < 0:
                    l += 1
                else:
                    r -= 1
        return target+mindiff

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [0, 2, 1, -3]
        t = 1
        print self.S.threeSumClosest(A, t)

if __name__ == "__main__":
    unittest.main()
