import unittest

class Solution(object):
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        max_sofar = [nums[0]]
        min_sofar = [nums[0]]
        maxp = nums[0]
        L = len(nums)
        for i in xrange(1, L):
            # print max_sofar[i-1]*nums[i], min_sofar[i-1]*nums[i], nums[i]
            max_sofar.append(max(max_sofar[i-1]*nums[i], min_sofar[i-1]*nums[i], nums[i]))
            min_sofar.append(min(max_sofar[i-1]*nums[i], min_sofar[i-1]*nums[i], nums[i]))
        # print max_sofar
        # print min_sofar
        return max(max_sofar)

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [2,3,-2,4]
        print self.S.maxProduct(A)
        
    def test_2(self):
        A = [2,3,-2,4,-1,-6]
        print self.S.maxProduct(A)

    def test_3(self):
        A = [-2,-1]
        print self.S.maxProduct(A)

if __name__ == "__main__":
    unittest.main()
