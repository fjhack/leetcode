class Solution(object):
    def trimMultiple(self, s):
        L = len(s)
        sv = []
        i = 0
        wc = 0
        while i < L:
            if s[i] != ' ':
                sv.append(s[i])
                i += 1
                wc += 1
            else:
                sv.append(s[i])
                while i < L and s[i] == ' ':
                    i += 1
        return ''.join(sv)
                
        
    def reverseWords(self, s):
        """
        :type s: str
        :rtype: str
        """
        L = len(s)
        sv = [c for c in s]
        
        for i in xrange(L/2):
            sv[i], sv[L-1-i] = sv[L-1-i], sv[i]
        
        segstart = 0
        i = 0
        for i in xrange(L):
            if sv[i] != ' ':
                continue
            else:
                seglen = i - segstart
                for j in xrange(seglen/2):
                    sv[segstart+j], sv[i-1-j] = sv[i-1-j], sv[segstart+j]
                segstart = i
                while segstart < L and sv[segstart] == ' ':
                    segstart += 1

        seglen = i - segstart
        for j in xrange(seglen/2):
            sv[segstart+j], sv[i-j] = sv[i-j], sv[segstart+j]
        
        return self.trimMultiple(''.join(sv).strip())

if __name__ == "__main__":
    S = Solution()
    s1 = "the sky is blue"
    print len(s1)
    R = S.reverseWords(s1)
    print R
    s2 = "a, yqo! qjktum ym. .fumuhau"
    R = S.reverseWords(s2)
    print R
    
