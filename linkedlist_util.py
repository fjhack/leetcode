# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

def list2LL(L):
    head = ListNode(L[0])
    ptr = head
    for i in xrange(1, len(L)):
        ptr.next = ListNode(L[i])
        ptr = ptr.next
    return head

def printLL(LL):
    head = LL
    while head is not None:
        print head.val, '->', 
        head = head.next
    print

def nodeStr(node):
    if node is None:
        return "<None>"
    else:
        return "{0} @ {1}".format(node.val, repr(node))
