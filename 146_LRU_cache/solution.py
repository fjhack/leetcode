import unittest

class DListNode(object):
    def __init__(self, val=None, key=None):
        self.key = key
        self.val = val
        self.next = None
        self.prev = None

class LRUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.capacity = capacity
        self.size = 0
        self.cache = dict()
        self.head = DListNode()
        self.tail = DListNode()
        self.head.next = self.tail
        self.tail.prev = self.head

    def inserthead(self, node):
        orig_head = self.head.next
        self.head.next = node
        node.prev = self.head
        node.next = orig_head
        orig_head.prev = node
        
        # node.next = self.head.next
        # self.head.next = node
        # node.prev = self.head
        # node.next.prev = node

    def kickhead(self, node):
        if node == self.head.next:
            return

        orig_prev = node.prev
        orig_prev.next = node.next
        node.next.prev = orig_prev
        node.next = self.head.next
        self.head.next.prev = node
        self.head.next = node
        node.prev = self.head

    def deletetail(self):
        if self.head.next == self.tail:
            return
        else:
            tail_prev = self.tail.prev.prev
            tail = self.tail.prev
            tail_prev.next = self.tail
            self.tail.prev = tail_prev
            tailkey = tail.key
            del self.cache[tailkey]
            del tail
            return

    def get(self, key):
        if key not in self.cache:
            return -1
        node = self.cache[key]
        self.kickhead(node)
        return node.val

    def put(self, key, value):
        if key in self.cache:
            node = self.cache[key]
            node.val = value
            self.kickhead(node)
        elif self.size < self.capacity:
            newnode = DListNode(value, key)
            self.cache[key] = newnode
            self.inserthead(newnode)
            self.size += 1
        else:
            newnode = DListNode(value, key)
            self.cache[key] = newnode
            self.inserthead(newnode)
            self.deletetail()

def llprint(head):
    curr = head
    while curr.next is not None:
        print "({0})@{1} -> ".format(curr.val, curr),
        curr = curr.next
    print "({0})@{1}".format(curr.val, curr)

def llprintR(tail):
    curr = tail
    while curr.prev is not None:
        print "({0})@{1} -> ".format(curr.val, curr),
        curr = curr.prev
    print "({0})@{1}".format(curr.val, curr)

import unittest

# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)

class testSolution(unittest.TestCase):
    def setUp(self):
        pass

    def LRU_ops(self, LRU, ops, params):
        for i in xrange(len(ops)):
            if ops[i] == "put":
                print "put:", params[i][0], params[i][1]
                LRU.put(params[i][0], params[i][1])
            if ops[i] == "get":
                print "get:", params[i][0],
                r = LRU.get(params[i][0])
                print 
            llprint(LRU.head)
            llprintR(LRU.tail)
            print LRU.cache
            print

    def test_e_0(self):
        LRU1 = LRUCache(2)
        ops = ["put","put","get","put","get","put","get","get","get"]
        params = [[1,1],[2,2],[1],[3,3],[2],[4,4],[1],[3],[4]]
        print " ===== Test 0 ====="
        print ops
        print params
        self.LRU_ops(LRU1, ops, params)
        print " =================="

    def test_e_1(self):
        LRU1 = LRUCache(2)
        ops = ["put","put","get","put","get","put","get","get","get"]
        params = [[1,1],[2,2],[1],[3,3],[2],[4,4],[1],[3],[4]]

        print " ===== Test 1 ====="
        print ops
        print params
        self.LRU_ops(LRU1, ops, params)
        print " =================="


if __name__ == "__main__":
    unittest.main()
