# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def getIntersectionNode(self, headA, headB):
        """
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        if headA is None or headB is None:
            return None
        
        pA, pB = headA, headB
        La, Lb = 0, 0
        while pA is not None:
            La += 1
            pA = pA.next
        
        while pB is not None:
            Lb += 1
            pB = pB.next
        
        d = abs(La - Lb)
        pA, pB = headA, headB
        offset = 0
        if La > Lb:
            while pA is not None and offset < d:
                pA = pA.next
                offset += 1
        elif Lb > La:
            while pB is not None and offset < d:
                pB = pB.next
                offset += 1
        
        while pA != pB and pA is not None and pB is not None:
            pA = pA.next
            pB = pB.next
        if pA == pB:
            return pA
        else:
            return None
