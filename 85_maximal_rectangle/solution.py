def largestRectangleArea(heights):
    """
    :type heights: List[int]
    :rtype: int
    """
    L = len(heights)
    if L == 0:
        return 0
    if L == 1:
        return heights[0]
        
    stk = [[heights[0], 0]]
    # print stk
    maxarea = stk[0][0]
    for i in xrange(1, L):
        top = stk[-1]
        if heights[i] >= top[0]:
            stk.append([heights[i], i])
        else:
            _top = None
            while len(stk) > 0 and heights[i] < stk[-1][0]:
                _top = stk.pop()
                _newlen = i - _top[1]
                # print "  poped:", _top, _newlen
                maxarea = max(maxarea, _top[0]*_newlen)

            if len(stk) > 0:
                newpos = _top[1]
            else:
                # print heights[i], stk, top
                newpos = 0

            stk.append([heights[i], newpos])
        # print stk
    for e in stk:
        # print e, L-e[1]
        maxarea = max(maxarea, e[0]*(L-e[1]))
    return maxarea


class Solution(object):
    def maximalRectangle(self, matrix):
        """
        :type matrix: List[List[str]]
        :rtype: int
        """
        # Build the histogram first
        rows = len(matrix)
        if rows == 0:
            return 0
        cols = len(matrix[0])
        if cols == 0:
            return 0

        max_area = 0

        topdown_histo = [[0 for _ in xrange(cols)] for _ in xrange(rows)]
        for c in xrange(cols):
            if matrix[0][c] == '1' or matrix[0][c] == 1:
                topdown_histo[0][c] = 1
            max_in_line = largestRectangleArea(topdown_histo[0])
            max_area = max(max_area, max_in_line)

        for r in xrange(1, rows):
            for c in xrange(cols):
                if matrix[r][c] == '1' or matrix[r][c] == 1:
                    topdown_histo[r][c] = topdown_histo[r-1][c] + 1
                else:
                    topdown_histo[r][c] = 0
            max_in_line = largestRectangleArea(topdown_histo[r])
            max_area = max(max_area, max_in_line)

        return max_area


if __name__ == "__main__":
    # Testing the largest rect in histogram function

    # print
    # H = [2,1,5,6,2,3]
    # R = largestRectangleArea(H)
    # print "Result:", R

    # print
    # H = [2,1,2]
    # R = largestRectangleArea(H)
    # print "Result:", R

    # print
    # H = [2, 4]
    # R = largestRectangleArea(H)
    # print "Result:", R

    # print
    # H = [3,6,5,7,4,8,1,0]
    # R = largestRectangleArea(H)
    # print "Result:", R

    # print
    # H = [1,0,1,1,1,0]
    # R = largestRectangleArea(H)
    # print "Result:", R

    S = Solution()
    M = [[1,0,1,0,0],
         [1,0,1,1,1],
         [1,1,1,1,1],
         [1,0,0,1,0]]
    R = S.maximalRectangle(M)
    print R
