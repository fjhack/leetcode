class MyQueue:

    def __init__(self):
        self.stack1 = [] # push stack
        self.stack2 = [] # pop stack
        
    def _topop(self):
        while len(self.stack1) > 0:
            e = self.stack1.pop()
            self.stack2.append(e)
    
    def _topush(self):
        while len(self.stack2) > 0:
            e = self.stack2.pop()
            self.stack1.append(e)
    
    def push(self, e):
        # write your code here
        if len(self.stack1) == 0 and len(self.stack2) == 0:
            self.stack1.append(e)
        elif len(self.stack1) > 0 and len(self.stack2) == 0:
            self.stack1.append(e)
        elif len(self.stack1) == 0 and len(self.stack2) > 0:
            self._topush()
            self.stack1.append(e)

    def top(self):
        # write your code here
        # return the top element
        if len(self.stack1) == 0 and len(self.stack2) == 0:
            return None
        elif len(self.stack1) == 0 and len(self.stack2) > 0:
            return self.stack2[-1]
        elif len(self.stack1) > 0 and len(self.stack2) == 0:
            self._topop()
            return self.stack2[-1]

    def pop(self):
        # write your code here
        # pop and return the top element
        if len(self.stack1) == 0 and len(self.stack2) == 0:
            return None
        elif len(self.stack1) == 0 and len(self.stack2) > 0:
            return self.stack2.pop()
        elif len(self.stack1) > 0 and len(self.stack2) == 0:
            self._topop()
            return self.stack2.pop()
