import unittest

class Solution(object):
    def searchRange(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        def findRHead(nums, target):
            L = len(nums)
            head, tail = 0, L-1
            while head <= tail:
                mid = head + (tail-head)/2
                if nums[mid] < target:
                    head = mid + 1
                elif nums[mid] > target:
                    tail = mid - 1
                else:
                    tail = mid - 1
            if head >= 0 and head < L and nums[head] == target:
                return head
            else:
                return -1
        def findRTail(nums, target):
            L = len(nums)
            head, tail = 0, L-1
            while head <= tail:
                mid = head + (tail-head)/2
                if nums[mid] < target:
                    head = mid + 1
                elif nums[mid] > target:
                    tail = mid - 1
                else:
                    head = mid + 1
            if tail >= 0 and tail < L and nums[tail] == target:
                return tail
            else:
                return -1
        return [findRHead(nums, target), findRTail(nums, target)]
    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def generate_array(self):
        import random
        L = random.randint(10, 16)
        A = [random.randint(1, 200) for _ in xrange(L)]
        As = sorted(list(set(A)))
        # print L, As
        return As

    def _gold(self, A, t):
        L = len(A)
        for i in xrange(L):
            if A[i] == t:
                return i
        return -1

    def test_1(self):
        A = [0,1,2,4,5,6,7]
        t = 6
        print
        print self.S.search(A, t)

    def test_1_1(self):
        A = [1]
        t = 0
        print
        print self.S.search(A, t)

    def test_1_2(self):
        A = [1]
        t = 2
        print
        print self.S.search(A, t)

    def test_1_3(self):
        A = [1,3]
        t = 2
        print
        print self.S.search(A, t)

    def test_random(self):
        self.random_driver(100)

    def random_driver(self, iters):
        import random
        passed_count = 0
        for i in xrange(iters):
            # print
            A = self.generate_array()
            # print A
            t = A[random.randint(0, len(A)-1)]
            rc = self.S.search(A, t)
            rg = self._gold(A, t)
            if rc != rg:
                print "Error found:"
                print "  Array:", A, "\ttarget:", t
                print "  gold result:", rg, "\tactual result:", rc
            else:
                passed_count += 1
        print "Total passed cases: {0} out of total {1} cases".format(passed_count, iters)

if __name__ == "__main__":
    unittest.main()
