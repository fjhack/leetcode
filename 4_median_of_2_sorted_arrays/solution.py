class Solution(object):
    def getKth(self, nums1, l1, nums2, l2, k):
        if l1 <= 0:
            return nums2[k-1]
        if l2 <= 0:
            return nums1[k-1]
        if k <= 1:
            return min(nums1[0], nums2[0])
            
        if nums2[l2/2] >= nums1[l1/2]:
            if (l1/2 + 1 + l2/2) >= k:
                return self.getKth(nums1, l1, nums2, l2/2, k)
            else:
                return self.getKth(nums1[l1/2+1:], l1-(l1/2+1), nums2, l2, k-(l1/2+1))
        else:
            if (l1/2 + 1 + l2/2) >= k:
                return self.getKth(nums1, l1/2, nums2, l2, k)
            else:
                return self.getKth(nums1, l1, nums2[l2/2+1:], l2-(l2/2+1), k-(l2/2+1))
                
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        L1, L2 = len(nums1), len(nums2)
        if (L1+L2)%2 == 0:
            return (self.getKth(nums1, L1, nums2, L2, (L1+L2)/2) + self.getKth(nums1, L1, nums2, L2, (L1+L2)/2+1))/2.0
        else:
            return self.getKth(nums1, L1, nums2, L2, (L1+L2)/2+1)
            
