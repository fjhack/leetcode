# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def closestValue(self, root, target):
        """
        :type root: TreeNode
        :type target: float
        :rtype: int
        """
        return self.walk(root, target, -1, -1)

    def isLeaf(self, node):
        return node.left is None and node.right is None

    def closest(self, nval, lval, target):
        if nval == -1:
            return lval
        elif lval == -1:
            return nval
        else:
            if abs(float(nval)-target) < abs(float(lval)-target):
                return nval
            else:
                return lval

    def walk(self, root, target, lastLarger, lastSmaller):
        if target == float(root.val):
            return root.val
        elif target < float(root.val):
            if self.isLeaf(root) or root.left is None:
                return self.closest(root.val, lastLarger, target)
            else:
                return self.walk(root.left, target, lastLarger, root.val)
        else:
            if self.isLeaf(root) or root.right is None:
                return self.closest(root.val, lastSmaller, target)
            else:
                return self.walk(root.right, target, root.val, lastSmaller)
