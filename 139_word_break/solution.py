class Solution:
    # @param s: A string s
    # @param dict: A dictionary of words dict
    def wordBreak(self, s, wordDict):
        # write your code here
        L = len(s)
        F = [False for _ in xrange(L+1)]
        F[0] = True
        if len(wordDict) == 0:
            return len(s) == 0
        
        maxWordLen = max([len(w) for w in wordDict])
        for i in xrange(1, L+1):
            minStart = max(0, i-maxWordLen)
            for j in xrange(minStart, i):
                s2 = s[j:i]
                if F[j] and s2 in wordDict:
                    F[i] = True
                    break
        return F[-1]

if __name__ == "__main__":
    swb = Solution().wordBreak
    print swb("leetcode", ["leet", "code"])
