class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        L = len(s)
        if L == 0:
            return False
        DP = [False for _ in xrange(L)]
        DP[0] = s[:1] in wordDict
        for i in xrange(1, L):
            if s[:i+1] in wordDict:
                DP[i] = True
                continue
            for j in xrange(i):
                if not DP[j]:
                    continue
                elif s[j+1:i+1] in wordDict:
                    DP[i] = True
                    break
        return DP[-1]

    def wordBreak1(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: int
        """
        L = len(s)
        if L == 0:
            return 0
        DP = [0 for _ in xrange(L)]
        DP[0] = s[:1] in wordDict
        for i in xrange(1, L):
            if s[:i+1] in wordDict:
                DP[i] = 1
                continue
            for j in xrange(i):
                if DP[j] == 0:
                    continue
                elif s[j+1:i+1] in wordDict:
                    DP[i] = min(DP[i], DP[j] + 1)

        return DP[-1]

if __name__ == "__main__":
    S = Solution()
    s1 = "leetcode"
    d1 = ["le", "leet", "co", "code", "leetcode"]
    R1 = S.wordBreak(s1, d1)
    R2 = S.wordBreak1(s1, d1)
    print R1, R2
