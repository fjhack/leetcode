import unittest

class Solution(object):
    def trap(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        inputsize = len(height)
        if inputsize == 0:
            return 0
        lpos, rpos = 0, inputsize - 1
        lmax, rmax = height[lpos], height[rpos]
        water = 0

        while lpos < rpos:
            if lmax < rmax:
                lpos += 1
                if height[lpos] > lmax:
                    lmax = height[lpos]
                water += max(0, min(lmax, rmax) - height[lpos])
            else:
                rpos -= 1
                if height[rpos] > rmax:
                    rmax = height[rpos]
                water += max(0, min(lmax, rmax) - height[rpos])

        return water

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_e_0(self):
        print
        heights = [0,1,0,2,1,0,1,3,2,1,2,1]
        R = self.S.trap(heights)
        print "Result: ", R

if __name__ == "__main__":
    unittest.main()
