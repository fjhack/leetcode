import unittest

class Solution(object):
    def __init__(self):
        self._tsum = 0
        self.jump = [[0 for _ in xrange(10)] for _ in xrange(10)]
        self.jump[1][3], self.jump[3][1] = 2, 2
        self.jump[1][7], self.jump[7][1] = 4, 4
        self.jump[1][9], self.jump[9][1] = 5, 5
        self.jump[2][8], self.jump[8][2] = 5, 5
        self.jump[3][7], self.jump[7][3] = 5, 5
        self.jump[3][9], self.jump[9][3] = 6, 6
        self.jump[1][3], self.jump[3][1] = 2, 2
        self.jump[4][6], self.jump[6][4] = 5, 5
        self.jump[7][9], self.jump[9][7] = 8, 8
        
    def numberOfPatterns(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        visited = [False for _ in xrange(10)]
        if m > n:
            return 0
        s = 0

        pos = [(1,4), (2,4), (5,1)]
        for p in pos:
            i, multiplier = p[0], p[1]
            self._tsum = 0
            visited[i] = True
            self.dfs(visited, i, 1, m, n)
            visited[i] = False
            s += (self._tsum * multiplier)
        return s
        
    def dfs(self, visited, curr, cpos, minlen, maxlen):
        if cpos == maxlen:
            self._tsum += 1
        else:
            if cpos >= minlen:
                self._tsum += 1
            for i in xrange(1, 10):
                if i != curr and not visited[i] and (self.jump[curr][i] == 0 or visited[self.jump[curr][i]]):
                    visited[i] = True
                    self.dfs(visited, i, cpos+1, minlen, maxlen)
                    visited[i] = False

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_0(self):
        m, n = 1, 1
        R = self.S.numberOfPatterns(m, n)
        print R

    def test_1(self):
        m, n = 3, 3
        R = self.S.numberOfPatterns(m, n)
        print R

    def test_2(self):
        m, n = 1, 2
        R = self.S.numberOfPatterns(m, n)
        print R

        
if __name__ == "__main__":
    unittest.main()

        
