# The read4 API is already defined for you.
# @param buf, a list of characters
# @return an integer
# def read4(buf):

class Solution(object):
    def read(self, buf, n):
        """
        :type buf: Destination buffer (List[str])
        :type n: Maximum number of characters to read (int)
        :rtype: The number of characters read (int)
        """
        tbuf = ["", "", "", ""]
        creadcount = read4(tbuf)
        for i in xrange(creadcount):
            buf[i] = tbuf[i]
        readcount = creadcount
        while creadcount == 4 and readcount < n:
            creadcount = read4(tbuf)
            for i in xrange(readcount, readcount+creadcount):
                buf[i] = tbuf[i-readcount]
            readcount += creadcount
        
        if readcount == n:
            return readcount
        elif readcount < n and creadcount < 4:
            return readcount
        elif readcount > n:
            more = readcount - n
            return n
