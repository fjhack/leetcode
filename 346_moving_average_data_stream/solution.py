class MovingAverage(object):

    def __init__(self, size):
        """
        Initialize your data structure here.
        :type size: int
        """
        self._size = size
        self._csum = 0
        self._clen = 0
        self._l = []

    def next(self, val):
        """
        :type val: int
        :rtype: float
        """
        if self._clen < self._size:
            self._csum += val
            self._clen += 1
            self._l.append(val)
            return float(self._csum) / float(self._clen)
        else:
            old = self._l.pop(0)
            self._csum = self._csum - old + val
            self._l.append(val)
            return float(self._csum) / float(self._size)
