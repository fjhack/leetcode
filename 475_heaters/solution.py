class Solution(object):
    def findRadius(self, houses, heaters):
        """
        :type houses: List[int]
        :type heaters: List[int]
        :rtype: int
        """
        houses = sorted(houses)
        heaters = sorted(heaters)
        hpos, epos = 0, 0
        maxdist = 0

        while hpos < len(houses):
            lookahead_dist, dist = abs(heaters[epos+1] - houses[hpos]), abs(heaters[epos] - houses[hpos])
            while epos < len(heaters) and lookahead_dist <= dist:
                epos += 1
            maxdist = max(maxdist, min(lookahead_dist, dist))
        return maxdist
