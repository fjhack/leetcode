#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
    int findRadius(vector<int>& houses, vector<int>& heaters) {
	std::sort(houses.begin(), houses.end());
	std::sort(heaters.begin(), heaters.end());
	int hpos;
	int epos;
	int maxdist;
	hpos = 0; epos = 0; maxdist = 0;

	while (hpos < houses.size()) {
	    while (epos < heaters.size()-1 && (abs(heaters[epos+1] - houses[hpos]) <= abs(heaters[epos] - houses[hpos]))) {
		epos++;
	    }
	    maxdist = std::max(maxdist, abs(heaters[epos] - houses[hpos]));
	    hpos++;
	}
	return maxdist;
    }
};
