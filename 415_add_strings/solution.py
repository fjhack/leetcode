class Solution(object):
    def addStrings(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        L1, L2 = len(num1), len(num2)
        if L1 == 0:
            return num2
        elif L2 == 0:
            return num1
        
        i1, i2 = 0, 0
        Lmin = min(L1, L2)
        result = ""
        ds, carry = 0, 0
        while i1 < Lmin and i2 < Lmin:
            d1, d2 = num1[L1-1-i1], num2[L2-1-i2]
            ds = int(d1) + int(d2) + carry
            if ds >= 10:
                carry = 1
                ds = ds - 10
            else:
                carry = 0
            result = str(ds) + result
            i1 += 1
            i2 += 1
        
        while i1 < L1:
            d1 = num1[L1-1-i1]
            ds = int(d1) + carry
            if ds >= 10:
                carry = 1
                ds = ds - 10
            else:
                carry = 0
            result = str(ds) + result
            i1 += 1
        
        while i2 < L2:
            d2 = num2[L2-1-i2]
            ds = int(d2) + carry
            if ds >= 10:
                carry = 1
                ds = ds - 10
            else:
                carry = 0
            result = str(ds) + result
            i2 += 1
            
        if carry > 0:
            result = str(carry) + result
        return result
