import sys
import unittest

class Solution(object):
    # Brute force solution, O(n^2)
    def largestRectangleAreaBF(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        L = len(heights)
        max_area = 0
        for i in xrange(L):
            for j in xrange(i, L):
                min_height = min(heights[i:j+1])
                _area = min_height * (j-i+1)
                print i, j, heights[i:j+1], _area
                if  _area > max_area:
                    max_area = _area
        return max_area

    # Double scan solution
    # http://www.programcreek.com/2014/05/leetcode-largest-rectangle-in-histogram-java/
    def largestRectangleArea(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        L = len(heights)
        stk = []
        wstk = []
        max_area = 0
        if L == 0:
            return max_area
        elif L == 1:
            return heights[0]

        i = 0
        while i < L:
            if len(stk) == 0 or heights[i] >= heights[stk[-1]]:
                stk.append(i)
                i += 1
            else:
                top = stk.pop()
                _h = heights[top]
                if len(stk) == 0:
                    w = i
                else:
                    w = i - stk[-1] - 1
                max_area = max(max_area, _h*w)

        while len(stk) > 0:
            top = stk.pop()
            _h = heights[top]
            if len(stk) == 0:
                w = i
            else:
                w = i - stk[-1] - 1
            max_area = max(max_area, _h*w)
            
        return max_area

if __name__ == "__main__":
    sBF = Solution().largestRectangleAreaBF
    s = Solution().largestRectangleArea
    A = [1]
    print sBF(A), s(A)

    A = [2,1,5,6,2,3]
    print sBF(A), s(A)
