import unittest

class Solution(object):
    def lengthLongestPath(self, input):
        """
        :type input: str
        :rtype: int
        """
        def isFile(s):
            if '.' in s:
                return True
            else:
                return False
        
        tokens = input.split('\n')
        maxlen = 0
        stk = []
        L = len(tokens)
        if L == 0:
            return 0
        if L == 1:
            if isFile(tokens[0]):
                return len(tokens[0])
            else:
                return 0
        i = 0
        print tokens
        stk.append(tokens[0])
        maxlen, maxstr = 0, ""
        for i in xrange(1,L):
            curr_level = tokens[i].count('\t')
            if curr_level > stk[-1].count('\t'):
                stk.append(tokens[i])
            else:
                _topc = stk[-1].count('\t')
                while _topc > curr_level:
                    _top = stk.pop()
                    _topc = stk[-1].count('\t')
                _top = stk.pop()
                stk.append(tokens[i])
            currlen = sum(len(s.strip('\t')) for s in stk) + len(stk) - 1
            if currlen > maxlen and isFile(stk[-1]):
                maxlen = currlen
                maxstr = '/'.join([s.strip('\t') for s in stk])
                print stk
        print maxstr
        return maxlen

if __name__ == "__main__":
    LLP = Solution().lengthLongestPath
    s1 = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"
    r = LLP(s1)
    print r

    s1 = "dir\n\tfile.txt"
    r = LLP(s1)
    print r

    s1 = "dir\n    file.txt"
    r = LLP(s1)
    print r

    s1 = "a"
    r = LLP(s1)
    print r
