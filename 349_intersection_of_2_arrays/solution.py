class Solution(object):
    def intersection(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        L1, L2 = len(nums1), len(nums2)
        if L1 < L2:
            ss, s1 = set(nums1), set(nums2)
        else:
            ss, s1 = set(nums2), set(nums1)
        r = {v for v in s1 if v in ss}
        return list(r)
