import unittest

class Solution(object):
    def mp_print(self, n, a2):
        outrows = ["    k={0}  ".format(_k) for _k in xrange(len(a2[0]))]
        for i in xrange(n):
            for j in xrange(len(a2[i])):
                outrows[j] = outrows[j] + " " + str(a2[i][j])
        for r in outrows:
            print r
            
    def maxProfit(self, k, prices):
        """
        :type k: int
        :type prices: List[int]
        :rtype: int
        """
        L = len(prices)
        if L < 2:
            return 0

        if k > L/2:
            profit = 0
            for i in xrange(1, L):
                d = prices[i] - prices[i-1]
                if d > 0:
                    profit += d
            return profit
        
        localmp = [[0 for _ in xrange(k+1)] for _ in xrange(L)]
        globalmp = [[0 for _ in xrange(k+1)] for _ in xrange(L)]
        print
        print "prices:  ",
        for p in prices:
            print str(p),
        print

        for i in xrange(1, L):
            d = prices[i] - prices[i-1]
            for j in xrange(1, k+1):
                localmp[i][j] = max(globalmp[i-1][j-1], localmp[i-1][j]+d)
                globalmp[i][j] = max(globalmp[i-1][j], localmp[i][j])
            print "i={0}, localmp:".format(i)
            self.mp_print(i, localmp)
            print "i={0}, globalmp:".format(i)
            self.mp_print(i, globalmp)

        return globalmp[L-1][k]

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [3,2,6,5,0,3]
        k = 2
        print self.S.maxProfit(k, A)

    def test_2(self):
        A = [2,1,4,5,2,9,7]
        k = 2
        print self.S.maxProfit(k, A)
        
    # def test_3(self):
    #     k = 4
    #     A = [1,2,4,2,5,7,2,4,9,0]
    #     print self.S.maxProfit(k, A)

    # def test_4(self):
    #     k = 5
    #     A = [0,3,8,6,8,6,6,8,2,0,2,7]
    #     print self.S.maxProfit(k, A)

    # def test_5(self):
    #     A = [48,12,60,93,97,42,25,64,17,56,85,93,9,48,52,42,58,85,81,84,69,36,1,54,23,15,72,15,11,94]
    #     k = 11
    #     print self.S.maxProfit(k, A)

if __name__ == "__main__":
    unittest.main()
