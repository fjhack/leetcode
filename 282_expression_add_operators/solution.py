import unittest

class Solution(object):
    def __init__(self):
        self.results = []
        
    def dfs(self, num, target, pos, exp, val, prod):
        L = len(num)
        if pos == L:
            if val == target:
                self.results.append(''.join(exp))
            return

        for i in xrange(pos, L):
            if i != pos and num[pos] == '0':
                break
            currval = num[pos:i+1]
            if pos == 0:
                self.dfs(num, target, i+1, exp + [currval], int(currval), int(currval))
            else:
                self.dfs(num, target, i+1, exp + ['+', currval], val+int(currval), int(currval))
                self.dfs(num, target, i+1, exp + ['-', currval], val-int(currval), -int(currval))
                self.dfs(num, target, i+1, exp + ['*', currval], val-prod+prod*int(currval), prod*int(currval))
        
    def addOperators(self, num, target):
        """
        :type num: str
        :type target: int
        :rtype: List[str]
        """
        self.results = []
        self.dfs(num, target, 0, [], 0, 0)
        return self.results
    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.getresult = self.S.addOperators

    # def test_t(self):
    #     N, T =  ,
    #     R = self.getresult(N, T)
    #     print R
    
    def test_1(self):
        N = "123"
        T = 6
        R = self.getresult(N, T)
        print R

    def test_2(self):
        N, T = "232", 8
        R = self.getresult(N, T)
        print R

    def test_3(self):
        N, T =  "105", 5
        R = self.getresult(N, T)
        print R

if __name__ == "__main__":
    unittest.main()
    
