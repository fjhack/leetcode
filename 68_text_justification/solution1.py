class Solution(object):
    def fullJustify(self, words, maxWidth):
        """
        :type words: List[str]
        :type maxWidth: int
        :rtype: List[str]
        """
        allresults, current = [], []
        curr_wordlen = 0

        for w in words:
            if curr_wordlen + len(w) + len(current) > maxWidth:
                spaces = maxWidth - (curr_wordlen + len(current) - 1)
                for s in xrange(spaces):
                    current[s % (len(current)-1 or 1)] += ' '
                allresults.append(' '.join(current))
                current = []
                curr_wordlen = 0
            current.append(w)
            curr_wordlen += len(w)
        allresults.append(' '.join(current))
        return allresults
    
if __name__ == "__main__":
    S = Solution()
    words = ["This", "is", "an", "example", "of", "text", "justification."]
    R = S.fullJustify(words, 16)
    print R
