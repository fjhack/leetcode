import unittest

class Solution(object):

    # Without debug
    def wordsTypingND(self, sentence, rows, cols):
        start = 0
        spaced = " ".join(sentence) + " "
        spaced_len = len(spaced)

        for i in xrange(rows):
            start += cols
            if spaced[start % spaced_len] == ' ':
                start += 1
            else:
                while start > 0 and spaced[(start-1) % spaced_len] != ' ':
                    start -= 1
        return start / spaced_len

    def wordsTyping(self, sentence, rows, cols):
        start = 0
        spaced = " ".join(sentence) + " "
        spaced_len = len(spaced)
        print "[" + spaced + "]"
        for i in xrange(rows):
            start += cols

            # Debug
            _cchar = spaced[start%spaced_len]
            print "  start={0}, start%len={1}, spaced[start%len]='{2}'".format(start, (start%spaced_len), _cchar)
            if _cchar == ' ':
                _cchar = '_'
            print "    {0}\n    {1}".format(spaced, " "*(start%spaced_len)+_cchar)
            
            if spaced[start % spaced_len] == ' ':
                start += 1
            else:
                print "  Decresing start..."
                while start > 0 and spaced[(start-1) % spaced_len] != ' ':
                    start -= 1
                    
                    # Debug
                    _cchar = spaced[start%spaced_len]
                    print "    start={0}, start%len={1}, spaced[start%len]='{2}'".format(start, (start%spaced_len), _cchar)
                    if _cchar == ' ':
                        _cchar = '_'
                    print "      {0}\n      {1}".format(spaced, " "*(start%spaced_len)+_cchar)

        return start / spaced_len

    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_a_1(self):
        print
        print "Test 1"
        rows, cols = 2, 8
        sentence = ["hello", "world"]
        R = self.S.wordsTyping(sentence, rows, cols)
        print R

    def test_a_2(self):
        print
        print "Test 2"
        rows, cols = 3, 6
        sentence = ["a", "bcd", "e"]
        R = self.S.wordsTyping(sentence, rows, cols)
        print R

    def test_a_3(self):
        print
        print "Test 3"
        rows, cols = 4, 5
        sentence = ["I", "had", "apple", "pie"]
        R = self.S.wordsTyping(sentence, rows, cols)
        print R

if __name__ == "__main__":
    unittest.main()
