#Lintcode's input is int - different with Leetcode that have string input for matrix.

class Solution:
    #param matrix: a matrix of 0 and 1
    #return: an integer
    def maxSquare0(self, matrix):
        # write your code here
        max_dim = 0
        rows, cols = len(matrix), len(matrix[0])
        A = [[0 for _ in xrange(cols)] for _ in xrange(rows)]

        for i in xrange(cols):
            A[0][i] = matrix[0][i]
            
        for i in xrange(rows):
            A[i][0] = matrix[i][0]
            
        if rows == 1:
            if 1 in A[rows-1]:
                return 1
            else:
                return 0
        if cols == 1:
            return A[0][cols-1]
            
        for i in xrange(1, rows):
            for j in xrange(1, cols):
                if matrix[i][j] == 1:
                    A[i][j] = min(A[i-1][j-1], A[i-1][j], A[i][j-1]) + 1
                    max_dim = max(max_dim, A[i][j])
                else:
                    A[i][j] = 0
        
        return max_dim * max_dim

    def maxSquare(self, matrix):
        # write your code here
        max_dim = 0
        rows, cols = len(matrix), len(matrix[0])
        A1 = [matrix[0][i] for i in xrange(cols)]
        
        for i in xrange(1, rows):
            newA1 = [0 for _ in xrange(cols)]
            newA1[0] = matrix[i][0]
            max_dim = max(max_dim, newA1[0])
            for j in xrange(1, cols):
                if matrix[i][j] == 1:
                    newA1[j] = min(A1[j-1], A1[j], newA1[j-1]) + 1
                    max_dim = max(max_dim, newA1[j])
                else:
                    newA1[j] = 0
            A1 = newA1
        
        return max_dim * max_dim
