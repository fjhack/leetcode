class Trie(object):
    def __init__(self):
        self._d = dict()
        self.terminate = False
        self._result = []

    def insert(self, word):
        if len(word) == 0:
            self.terminate = True
            return
        else:
            if word[0] not in self._d:
                self._d[word[0]] = Trie()
            self._d[word[0]].insert(word[1:])
            return

    def search(self, word):
        if len(word) == 0:
            return self.terminate
        else:
            if word[0] in self._d:
                return self._d[word[0]].search(word[1:])
            else:
                return False

    def allStartsWith(self, prefix):
        if len(prefix) == 0:
            results = set()
            Q = [(self, '')]
            while len(Q) > 0:
                curr = Q.pop(0)
                # print curr[0], curr[0].terminate, curr[1]
                if curr[0].terminate:
                    results.add(curr[1])
                if len(curr[0]._d) == 0:
                    results.add(curr[1])
                for k, v in curr[0]._d.iteritems():
                    Q.append((v, curr[1]+k))
            return results
        else:
            if prefix[0] in self._d:
                results = self._d[prefix[0]].allStartsWith(prefix[1:])
                return [prefix[0]+result for result in results]
            else:
                return []


    def startsWith(self, prefix):
        if len(prefix) == 0:
            return True
        else:
            if prefix[0] in self._d:
                return self._d[prefix[0]].startsWith(prefix[1:])
            else:
                return False

if __name__ == "__main__":
    T = Trie()
    words = ["efg","defi","gh","iuw","ww","iw","ghih","dasf","aaa"]
    for w in words:
        T.insert(w)
    print T.allStartsWith('i')
    print T.startsWith('i')

    T2 = Trie()
    words = ["a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa"]
    for w in words:
        T2.insert(w)
    print T2.allStartsWith('a')
    print T2.startsWith('a')
