class Trie(object):
    def _c2i(self, c):
        return ord(c) - 97

    def _i2c(self, i):
        return chr(97+i)

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self._chars = [None for _ in xrange(26)]


    def insert(self, word):
        """
        Inserts a word into the trie.
        :type word: str
        :rtype: void
        """
        L = len(word)
        i = 0
        cc = self._chars
        while i < L:
            if cc[self._c2i(word[i])] is None:
                if i < L-1:
                    cc[self._c2i(word[i])] = (Trie(), False)
                elif i == L-1:
                    cc[self._c2i(word[i])] = (Trie(), True)
            elif i == L-1:
                    cc[self._c2i(word[i])] = (cc[self._c2i(word[i])][0], True)
            cc = cc[self._c2i(word[i])][0]._chars
            i += 1


    def search(self, word):
        """
        Returns if the word is in the trie.
        :type word: str
        :rtype: bool
        """
        L = len(word)
        i = 0
        cc = self._chars
        while i < L-1:
            if cc[self._c2i(word[i])] is None:
                return False
            else:
                cc = cc[self._c2i(word[i])][0]._chars
                i += 1
        if cc[self._c2i(word[i])] is None:
            return False
        return cc[self._c2i(word[i])][1]

    def startsWith(self, prefix):
        """
        Returns if there is any word in the trie that starts with the given prefix.
        :type prefix: str
        :rtype: bool
        """
        L = len(prefix)
        i = 0
        cc = self._chars
        while i < L:
            if cc[self._c2i(prefix[i])] is None:
                return False
            else:
                cc = cc[self._c2i(prefix[i])][0]._chars
                i += 1
        return True


# Your Trie object will be instantiated and called as such:
# obj = Trie()
