import unittest

class Trie(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self._topd = dict()

    def insert(self, word):
        """
        Inserts a word into the trie.
        :type word: str
        :rtype: void
        """
        L = len(word)
        curr_dict = self._topd
        for i in xrange(L):
            if word[i] in curr_dict:
                curr_dict = curr_dict[word[i]]
            else:
                curr_dict[word[i]] = dict()
                curr_dict = curr_dict[word[i]]
        print "\t", curr_dict
        curr_dict["TERM"] = True
        return 

    def search(self, word):
        """
        Returns if the word is in the trie.
        :type word: str
        :rtype: bool
        """
        L = len(word)
        curr_dict = self._topd
        for i in xrange(L):
            if word[i] in curr_dict:
                curr_dict = curr_dict[word[i]]
            else:
                return False
        if "TERM" in curr_dict:
            return True
        else:
            return False

    def startsWith(self, prefix):
        """
        Returns if there is any word in the trie that starts with the given prefix.
        :type prefix: str
        :rtype: bool
        """
        L = len(prefix)
        curr_dict = self._topd
        for i in xrange(L):
            if prefix[i] in curr_dict:
                curr_dict = curr_dict[prefix[i]]
            else:
                return False
        return True
        

# Your Trie object will be instantiated and called as such:
# obj = Trie()
# obj.insert(word)
# param_2 = obj.search(word)
# param_3 = obj.startsWith(prefix)

class testSolution(unittest.TestCase):
    def setUp(self):
        self.trie = Trie()

    def test_0(self):
        self.trie.insert("foobar")
        search_r = self.trie.search("foobar")
        print search_r
        searchs_r = self.trie.startsWith("foo")
        print searchs_r
        searchs_r = self.trie.startsWith("bar")
        print searchs_r
        
        print self.trie._topd
        self.trie.insert("foo")
        print self.trie._topd
        print
        search_r = self.trie.search("foobar")
        print search_r
        searchs_r = self.trie.startsWith("foo")
        print searchs_r

if __name__ == "__main__":
    unittest.main()
