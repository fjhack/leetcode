# Definition for an interval.
class Interval(object):
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

def ll2intervals(LL):
    return [Interval(L[0], L[1]) for L in LL]

def printitvls(LL):
    for itvl in LL:
        print "["+str(itvl.start)+","+str(itvl.end)+"]",
    print
    
class Solution(object):
    def insert(self, intervals, newInterval):
        """
        :type intervals: List[Interval]
        :type newInterval: Interval
        :rtype: List[Interval]
        """
        L = len(intervals)
        if L == 0:
            return [newInterval]
        ci = None
        i = 0
        while i < len(intervals):
            ci = intervals[i]
            # print ci.start, ci.end
            if newInterval.start >= ci.start and newInterval.start <= ci.end:
                # new interval starts in middle of current interval
                # do lookahead
                j = i
                cji = intervals[j]
                # print "\t", cji.start, cji.end
                while newInterval.end >= cji.end and j < len(intervals)-1:
                    j += 1
                    # print j, len(intervals), intervals[j].start, intervals[j].end
                    cji = intervals[j]
                lastji = intervals[j-1]
                # print "\t", cji.start, cji.end, lastji.start, lastji.end
                if newInterval.end < cji.start:
                    ci.end = max(lastji.end, newInterval.end)
                    for _ in xrange(i+1, j):
                        del intervals[i+1]

                else:
                    ci.end = max(cji.end, newInterval.end)
                    for _ in xrange(i+1, j+1):
                        del intervals[i+1]
                    
                # print i, j
                i += 1
            elif newInterval.start < ci.start and i == 0:
                intervals.insert(0, newInterval)
                if newInterval.end < ci.start:
                    i += 1
                else:
                    # # do lookahead
                    # j = i
                    # cji = intervals[j]
                    # # print "\t", cji.start, cji.end
                    # while newInterval.end >= cji.end and j < len(intervals)-1:
                    #     j += 1
                    #     # print j, len(intervals), intervals[j].start, intervals[j].end
                    #     cji = intervals[j]
                    # lastji = intervals[j-1]
                    # print "\t", cji.start, cji.end,
                    # print lastji.start, lastji.end

                    continue

            else:
                i += 1
        return intervals

    # Correct solution
    def insertO(self, intervals, newInterval):
        results = []
        insertPos = 0
        for interval in intervals:
            if interval.end < newInterval.start:
                results.append(interval)
                insertPos += 1
            elif interval.start > newInterval.end:
                results.append(interval)
            else:
                newInterval.start = min(interval.start, newInterval.start)
                newInterval.end = max(interval.end, newInterval.end)
        results.insert(insertPos, newInterval)
        return results

    
if __name__ == "__main__":
    Si = Solution().insert
    SiO = Solution().insertO
    
    Is = [[1,3],[6,9]]
    nI = Interval(2,5)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[1,3],[6,9]]
    nI = Interval(2,7)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[1,2],[3,5],[6,7],[8,10],[12,16]]
    nI = Interval(4,9)
    printitvls(Si(ll2intervals(Is), nI))
    
    Is = [[1,2],[3,5],[6,7],[8,10],[12,16]]
    nI = Interval(4,11)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[1,2],[3,5],[6,7],[8,10],[12,16]]
    nI = Interval(9,13)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[1,2],[3,5],[6,7],[8,10],[12,16]]
    nI = Interval(9,18)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[3,5],[6,9]]
    nI = Interval(1,2)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[3,5],[6,9]]
    nI = Interval(1,4)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[3,5],[6,7],[8,10],[12,16]]
    nI = Interval(1,9)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[1,5]]
    nI = Interval(2,7)
    printitvls(Si(ll2intervals(Is), nI))

    Is = [[1,5]]
    nI = Interval(6,8)
    printitvls(Si(ll2intervals(Is), nI))
