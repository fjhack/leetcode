class Solution(object):
    def canCompleteCircuit(self, gas, cost):
        """
        :type gas: List[int]
        :type cost: List[int]
        :rtype: int
        """
        sg, sc = sum(gas), sum(cost)
        if sg < sc:
            return -1
        L = len(cost)
        surplus = [gas[i]-cost[i] for i in xrange(L)]
        
        def reachable2(i):
            tank, station = 0, i
            L = len(cost)
            for x in xrange(L):
                tank += surplus[(i+x)%L]
                if tank < 0:
                    return False, x
            return True, -1
        
        i = 0    
        while i < L:
            R, lookahead = reachable2(i)
            if R:
                return i
            else:
                i += lookahead+1
        return -1
