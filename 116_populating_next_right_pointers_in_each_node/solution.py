# Definition for binary tree with next pointer.
class TreeLinkNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.next = None

class Solution:
    def __init__(self):
        self.depthnext = None

    def dfs(self, node, cdepth):
        if node is None:
            return
        else:
            if cdepth not in self.depthnext:
                self.depthnext[cdepth] = node
            else:
                self.depthnext[cdepth].next = node
                self.depthnext[cdepth] = node
            self.dfs(node.left, cdepth+1)
            self.dfs(node.right, cdepth+1)
            
    # @param root, a tree link node
    # @return nothing
    def connect(self, root):
        self.depthnext = dict()
        self.dfs(root, 0)
