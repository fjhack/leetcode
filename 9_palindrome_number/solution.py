class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        if x < 0:
            return False
        elif x == 0:
            return True
        else:
            origx = x
            s = 0
            while x > 0:
                s = s*10 + x%10
                x = x/10
            if s == origx:
                return True
            else:
                return False
