import unittest

class NestedIterator(object):

    def __init__(self, nestedList):
        """
        Initialize your data structure here.
        :type nestedList: List[NestedInteger]
        """
        self.stk = [nestedList]
        self.ptrstk = [0]
        
    def next(self):
        """
        :rtype: int
        """
        # print "Before next:"
        # print " ", self.stk, len(self.stk[-1])
        # print " ", self.ptrstk


        while isinstance(self.stk[-1][self.ptrstk[-1]], list):
            self.stk.append(self.stk[-1][self.ptrstk[-1]])
            self.ptrstk.append(0)
        
        R = self.stk[-1][self.ptrstk[-1]]
        self.ptrstk[-1] += 1

        # print "After next:"
        # print " ", self.stk, len(self.stk[-1])
        # print " ", self.ptrstk

        return R

    def hasNext(self):
        """
        :rtype: bool
        """
        while len(self.ptrstk) > 0 and len(self.stk) > 0 and self.ptrstk[-1] >= len(self.stk[-1]):
            # print "Poping..."
            # print "   ", self.stk
            # print "   ", self.ptrstk
            self.stk.pop()
            self.ptrstk.pop()
            if len(self.ptrstk) == 0 and len(self.stk) == 0:
                return False
            self.ptrstk[-1] += 1

        return not( len(self.stk) == 1 and self.ptrstk[-1] == len(self.stk[-1]) and len(self.stk[-1] > 0))

class testSolution(unittest.TestCase):
    def setUp(self):
        pass

    def iterList(self, L):
        c = 0
        res = []
        NI1 = NestedIterator(L)
        while NI1.hasNext():
            res.append(NI1.next())
            c += 1
        return res

    def test_0(self):
        NL1 = [[1,1],2,[1,1]]
        R = self.iterList(NL1)
        print R

    def test_1(self):
        NL2 = [[1,1],2,[3,4,[1,5]],2,1,[6]]
        R = self.iterList(NL2)
        print R

    def test_3(self):
        NL3 = []
        R = self.iterList(NL3)
        print R
        
if __name__ == "__main__":
    unittest.main()
