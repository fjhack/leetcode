import unittest


class NestedIterator(object):
    def __init__(self, nestedList):
        """
        Initialize your data structure here.
        :type nestedList: List[NestedInteger]
        """
        self.stk = [nestedList]
        self.ptrstk = [0]
        
    def next(self):
        """
        :rtype: int
        """

        if self.hasNext():
            R = self.stk[-1][self.ptrstk[-1]]
            self.ptrstk[-1] += 1
            print R
            return R

    def hasNext(self):
        """
        :rtype: bool
        """
        
        while len(self.stk) > 0:
            lastL, lastpos = self.stk[-1], self.ptrstk[-1]
            print lastL, lastpos
            if lastpos == len(lastL):
                self.stk.pop()
                self.ptrstk.pop()
            else:
                if lastL[lastpos].isInteger():
                    return True
                else:
                    self.ptrstk[-1] += 1
                    self.stk.append(lastL[lastpos].getList())
                    self.ptrstk.append(0)
        return False
        
