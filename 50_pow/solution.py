class Solution(object):
    def myPow(self, x, n):
        """
        :type x: float
        :type n: int
        :rtype: float
        """
        def _pow(x, n):
            if n == 0:
                return 1
            r = _pow(x, n/2)
            if n % 2 == 0:
                return r * r
            else:
                return r * r * x
        
        if n < 0:
            return 1.0 / _pow(x,-n)
        else:
            return _pow(x, n)
