import sys
import unittest

class Solution:
    def __init__(self):
        self.B = dict()

    def dumpBuffer(self):
        for key in sorted(self.B):
            print key, self.B[key]

    def minDistance(self, word1, word2):
        return self.minDistanceL2(word1, word2)

    # RM: Recursive and memoization
    # @return an integer
    def minDistanceRM(self, word1, word2):
        l1, l2 = len(word1), len(word2)
        # wp = (word1, word2)
        wp = (l1, l2)

        # if wp in self.B:
        #     return self.B[wp]

        if l1 == 0 and l2 != 0:
            self.B[wp] = l2
            return l2
        elif l2 == 0 and l1 != 0:
            self.B[wp] = l1
            return l1
        elif l1 == 0 and l2 == 0:
            self.B[wp] = 0
            return 0
        elif word1 == word2:
            self.B[wp] = 0
            return 0
        
        _cost = 0
        if word1[-1] != word2[-1]:
            _cost = 1

        # insert_cost = self.minDistanceRM(word1, word2[:-1]) + 1
        # delete_cost = self.minDistanceRM(word1[:-1], word2) + 1
        # replace_cost = self.minDistanceRM(word1[:-1], word2[:-1]) + _cost
        insert_cost = (self.B[(l1, l2-1)] + 1) if (l1, l2-1) in self.B else (self.minDistanceRM(word1, word2[:-1]) + 1)
        delete_cost = (self.B[(l1-1, l2)] + 1) if (l1-1, l2) in self.B else (self.minDistanceRM(word1[:-1], word2) + 1)
        replace_cost = (self.B[(l1-1, l2-1)] + _cost) if (l1-1, l2-1) in self.B else (self.minDistanceRM(word1[:-1], word2[:-1]) + _cost)

        self.B[wp] = min(insert_cost, delete_cost, replace_cost)
        return self.B[wp]

    # Non-recursive DP, dict with tuple as key
    def minDistanceL(self, word1, word2):
        l1, l2 = len(word1), len(word2)
        B = dict()
        
        for _i in xrange(l1+1):
            B[(_i, 0)] = _i

        for _j in xrange(l2+1):
            B[(0, _j)] = _j

        for i in xrange(1, l1+1):
            for j in xrange(1, l2+1):
                cost = 0 if word1[i-1] == word2[j-1] else 1
                B[(i,j)] = min(B[(i-1, j)]+1,
                               B[(i, j-1)]+1,
                               B[(i-1, j-1)]+cost)

        return B[(l1, l2)]

    # Non-recursive DP, 2D array
    def minDistanceL2(self, word1, word2):
        l1, l2 = len(word1), len(word2)
        B = [[0 for _j in xrange(l2+1)] for _i in xrange(l1+1)]
        
        for _i in xrange(l1+1):
            B[_i][0] = _i

        for _j in xrange(l2+1):
            B[0][_j] = _j

        for i in xrange(1, l1+1):
            for j in xrange(1, l2+1):
                cost = 0 if word1[i-1] == word2[j-1] else 1
                B[i][j] = min(B[i-1][j] + 1,
                              B[i][j-1] + 1,
                              B[i-1][j-1] + cost)

        return B[l1][l2]

    
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test1(self):
        A, B = "prosperity", "properties"
        print self.S.minDistance(A, B)

    def test2(self):
        A, B = "abc", "acd"
        print self.S.minDistance(A, B)

    def test7(self):
        A, B = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdef", "bcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefg"
        print self.S.minDistance(A, B)

if __name__ == "__main__":
    sys.setrecursionlimit(2048)
    unittest.main()
