# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def hasCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        ptr1, ptr2 = head, head
        while ptr1 is not None and ptr2 is not None:
            ptr1 = ptr1.next
            if ptr2.next is not None:
                ptr2 = ptr2.next.next
            else:
                return False
            if ptr1 == ptr2:
                return True
        return False
