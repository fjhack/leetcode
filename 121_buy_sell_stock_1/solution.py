import unittest

class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        L = len(prices)
        if L == 0:
            return 0
        min_sofar = prices[0]
        profit_sofar = 0
        for i in xrange(1, L):
            if prices[i] < min_sofar:
                min_sofar = prices[i]
            else:
                if (prices[i] - min_sofar) > profit_sofar:
                    profit_sofar = prices[i] - min_sofar
        return profit_sofar

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_1(self):
        A = [7,1,5,3,6,4]
        print self.S.maxProfit(A)

    def test_2(self):
        A = [7,6,4,3,1]
        print self.S.maxProfit(A)

if __name__ == "__main__":
    unittest.main()
