# Definition for singly-linked list.

import unittest

class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def rotateRight(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        orig_head = head
        walk_ptr_1 = head
        walk_ptr_2 = head
        walk_cntr = 0
        if head is None:
            return None
        if head.next is None:
            return head
        if k == 0:
            return head
        while walk_ptr_1.next != None:
            if walk_cntr < k:
                walk_cntr += 1
                walk_ptr_1 = walk_ptr_1.next
            else:
                walk_cntr += 1
                walk_ptr_1 = walk_ptr_1.next
                walk_ptr_2 = walk_ptr_2.next
        print walk_ptr_1.val, walk_ptr_2.val, walk_cntr
        if walk_cntr > k-1:
            new_head = walk_ptr_2.next
            walk_ptr_2.next = None
            walk_ptr_1.next = orig_head
            return new_head
        elif walk_cntr == k-1:
            return head
        else:
            newk = k % (walk_cntr + 1)
            return self.rotateRight(head, newk)
            
class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def buildList(self, A):
        if len(A) == 0:
            return None
        else:
            curr = ListNode(A[0])
            curr.next = self.buildList(A[1:])
            return curr

    def printList(self, L):
        if L.next == None:
            print L.val
            return
        else:
            print L.val, '->',
            self.printList(L.next)
            return
        
    def test_1(self):
        A = [i for i in xrange(1, 6)]
        L = self.buildList(A)
        self.printList(L)
        newL = self.S.rotateRight(L, 2)
        self.printList(newL)

    def test_2(self):
        A = [i for i in xrange(1, 6)]
        L = self.buildList(A)
        self.printList(L)
        newL = self.S.rotateRight(L, 5)
        self.printList(newL)

    def test_3(self):
        A = [i for i in xrange(1, 6)]
        L = self.buildList(A)
        self.printList(L)
        newL = self.S.rotateRight(L, 6)
        self.printList(newL)

if __name__ == "__main__":
    unittest.main()
