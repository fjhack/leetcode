class Solution(object):
    def __init__(self):
        self.results = []
    
    def walk(self, m, startrow, endrow, startcol, endcol):
        if startrow == endrow:
            for j in xrange(startcol, endcol+1):
                self.results.append(m[startrow][j])
            return
        if startcol == endcol:
            for i in xrange(startrow, endrow+1):
                self.results.append(m[i][startcol])
            return
        
        for j in xrange(startcol, endcol):
            self.results.append(m[startrow][j])
            
        for i in xrange(startrow, endrow):
            self.results.append(m[i][endcol])
            
        for j in xrange(endcol, startcol, -1):
            self.results.append(m[endrow][j])
        
        for i in xrange(endrow, startrow, -1):
            self.results.append(m[i][startcol])
        
        if (endrow - startrow) == 1 or (endcol - startcol) == 1:
            return
        else:
            self.walk(m, startrow+1, endrow-1, startcol+1, endcol-1)
    
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        rows = len(matrix)
        if rows == 0:
            return self.results
            
        cols = len(matrix[0])
        self.results = []
        self.walk(matrix, 0, rows-1, 0, cols-1)
        return self.results
