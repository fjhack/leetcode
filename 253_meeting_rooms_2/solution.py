# Definition for an interval.
class Interval(object):
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

import heapq
import unittest

class Solution(object):
    def minMeetingRooms(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: int
        """
        sorted_intervals = sorted(intervals, key=lambda x: x.start)
        positions = sorted(list(set([intvl.start for intvl in intervals] + [intvl.end for intvl in intervals])))
        endheap = []
        iidx = 0
        max_rooms = 0
        #print positions
        #print sorted_intervals

        for pos in positions:
            #print pos
            while iidx < len(intervals) and sorted_intervals[iidx].start <= pos:
                heapq.heappush(endheap, (sorted_intervals[iidx].end, sorted_intervals[iidx].start))
                iidx += 1
            #print "\t", endheap
            #print endheap[0][0]
            while len(endheap) > 0 and endheap[0][0] <= pos:
                heapq.heappop(endheap)

            max_rooms = max(max_rooms, len(endheap))

        return max_rooms

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()
        self.mmr = self.S.minMeetingRooms

    def build_intervals(self, pair):
        return Interval(s=pair[0], e=pair[1])
    
    def test_1(self):
        _itvls = [[13,15],[1,13],[6,9]]
        itvls = [self.build_intervals(p) for p in _itvls]
        R = self.mmr(itvls)
        print R

    def test_2(self):
        _itvls = [[13,15],[1,13]]
        itvls = [self.build_intervals(p) for p in _itvls]
        R = self.mmr(itvls)
        print R

if __name__ == "__main__":
    unittest.main()
