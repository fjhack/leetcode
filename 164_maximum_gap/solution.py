class Solution(object):
    # For comparion - not really a solution
    def maximumGapGold(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n2 = sorted(nums)
        L = len(n2)
        gap = 0
        for i in xrange(L-1):
            if n2[i+1] - n2[i] > gap:
                gap = n2[i+1] - n2[i]
        return gap
        
