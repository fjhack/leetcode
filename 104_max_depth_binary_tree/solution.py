# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self._md = 0
    
    def maxDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self._md = 0
        def mdt(root, depth):
            if root is not None and depth > self._md:
                self._md = depth
            if root is None:
                return
            if root.left is not None:
                mdt(root.left, depth+1)
            if root.right is not None:
                mdt(root.right, depth+1)
        if root is None:
            return 0
        mdt(root, 1)
        return self._md
