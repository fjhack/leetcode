import unittest

class Solution(object):
    def lengthOfLongestSubstringKDistinct(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        L = len(s)
        if L == 0 or k == 0:
            return 0
        if L < k:
            return L
        if L == k:
            return k
        left, right = 0, 0
        maxlen = 0
        uniq = dict()
        uniq[s[left]] = 1
        
        uniqget = uniq.get
        
        while right < L-1:
            right += 1
            # if s[right] in uniq:
            #     uniq[s[right]] += 1
            # else:
            #     uniq[s[right]] = 1
            
            uniq[s[right]] = uniqget(s[right], 0) + 1
            
            print s[left:right+1], uniq

            while len(uniq) > k and left < right:
                if s[left] in uniq:
                    if uniq[s[left]] > 0:
                        uniq[s[left]] -= 1
                    if uniq[s[left]] == 0:
                        del uniq[s[left]]
                        
                left += 1
            clen = right - left + 1
            
            maxlen = max(maxlen, clen)
        return maxlen

class testSolution(unittest.TestCase):
    def setUp(self):
        self.S = Solution()

    def test_a_1(self):
        S = "abaccc"
        k = 2
        R = self.S.lengthOfLongestSubstringKDistinct(S, k)
        print R

    def test_a_2(self):
        S = "bacc"
        k = 2
        R = self.S.lengthOfLongestSubstringKDistinct(S, k)
        print R

if __name__ == "__main__":
    unittest.main()
