class Solution:
    # @param board, a 9x9 2D array
    # @return a boolean
    def isValidSudoku(self, board):
        used_row = [[False for _ in xrange(9)] for _ in xrange(9)]
        used_col = [[False for _ in xrange(9)] for _ in xrange(9)]
        used_box = [[False for _ in xrange(9)] for _ in xrange(9)]
        for r in xrange(9):
            for c in xrange(9):
                if board[r][c] != '.':
                    _val = int(board[r][c]) - 1
                    _box = r/3*3 + c/3
                    if used_row[r][_val] or used_col[c][_val] or used_box[_box][_val]:
                        return False
                    else:
                        used_row[r][_val] = True
                        used_col[c][_val] = True
                        used_box[_box][_val] = True
        return True
